## Synopsis

The Agricultural Ecosystem Services (AgES) model is a modular, Java-based spatially distributed 
environmental model which implements hydrologic/water quality simulation components under the 
Java Connection Framework (JCF) environmental modeling framework.

## Contributors

James C. Ascough II,
Timothy R. Green,
Gregory S. McMaster,
Nathan P. Lighthart,
Holm Kipka,
Debora A. Edmunds,
Robert H. Erskine,
Olaf David, and
Marialaura Bancheri
