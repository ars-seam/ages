@echo off
rem This script will run the ages model
rem Usage: ages [project_name] [simulation_file_name]
rem Both project_name and simulation_file_name are optional arguments
rem If the simulation_file_name is not given it is assumed to be project_name.sim

rem Find the project name:
rem 	1) Passed as first argument
rem		2) Prompted as input value
if "%1"=="" (
	set /p name="Enter project name: "
) else (
	set name=%1
)

rem Determine if project directory exists
set proj=projects\%name%
if not exist "%proj%" (
	echo Invalid project directory: %proj%
	exit /b 1
)

rem Determine if project directory has simulation directory
set sim_dir=%proj%\simulation
if not exist "%sim_dir%" (
	echo Missing simulation directory: %sim_dir%
	exit /b 2
)

rem Find the simulation file name:
rem 	1) Passed as second argument
rem		2) Default to project name
if "%2"=="" (
	set sim_name=%name%.sim
) else (
	set sim_name=%2
)

rem Loop until a valid simulation file is specified
:sim_loop
set sim=%sim_dir%\%sim_name%
if not exist "%sim%" (
	echo Cannot find simulation file: %sim_name%
	set /p sim_name="Enter simulation file name: "
	goto sim_loop
)

rem Setup Java environment options
rem optional java memory parameters -Xms512M -Xmx4G
set options=-Doms_prj=.

rem Setup classpath
set jar=dist/AgES.jar
set lib=dist/lib
set path_separator=;
set classpath="%jar%%path_separator%%lib%"

rem Setup oms command line arguments
set args=-l OFF -r "%sim%"

rem Run the Java command
@echo on
java %options% -cp %classpath% oms3.CLI %args%
