#!/bin/sh
# This script will run the ages model
# Usage: ages [project_name] [simulation_file_name]
# Both project_name and simulation_file_name are optional arguments
# If the simulation_file_name is not given it is assumed to be project_name.sim

# Find the project name:
#	1) Passed as first argument
#	2) Prompted as input value
name=""
if [ "$1" = "" ]; then
	read -p "Enter project name: " name
else
	name=$1
fi

# Determine if project directory exists
proj=projects/$name
if [ ! -e "$proj" ]; then
	echo "Invalid project directory: $proj"
	exit 1
fi

# Determine if project directory has simulation directory
sim_dir=$proj/simulation
if [ ! -e "$sim_dir" ]; then
	echo "Missing simulation directory: $sim_dir"
	exit 2
fi

# Find the simulation file name:
#	1) Passed as second argument
#	2) Default to project name
sim_name=""
if [ "$2" = "" ]; then
	sim_name=$name.sim
else
	sim_name=$2
fi

# Loop until a valid simulation file is specified
sim=$sim_dir/$sim_name
while [ ! -e "$sim" ]; do
	echo "Cannot find simulation file: $sim_name"
	read -p "Enter simulation file name: " sim_name
	sim=$sim_dir/$sim_name
done

# Setup Java command options
# optional java memory parameters -Xms512M -Xmx4G
options="-Doms_prj=."

# Setup classpath
jar="dist/AgES.jar"
lib="dist/lib"

# Find path separator
# unix-based machines use ":" while windows uses ";"
case "$(uname -s)" in
  CYGWIN*|MINGW*|MSYS*)
	# Windows through Cygwin, MinGW, or Msys
	path_separator=";"
	;;

  Linux)
	# Linux
	path_separator=":"
	;;

  Darwin)
	# Mac OS X
	path_separator=":"
	;;

  *)
	# other OS
	path_separator=":"
	;;
esac

# Finalize classpath
classpath="\"${jar}${path_separator}${lib}\""

# Setup oms command line arguments
args="-l OFF -r \"$sim\""

# Run the Java command
# command needs to be evaluated as there are syntatic quotes in $classpath and $args
command="java $options -cp $classpath oms3.CLI $args"
echo $command
eval $command
