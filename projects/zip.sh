#!/bin/sh
# This script will create individual zip files of all project directories specified
# This script will also create a single zip file that will contain all the project directories

# Loop through all directories (each project is a directory)
for file in *
do
	if [ -d "$file" ]; then
		# Zip the project directory
    	echo Zipping $file
		ant zip-project -S -q -f ../build.xml -Dproject.name="$file"
	fi
done

# Zip all the projects in a single zip file
echo Zipping all projects
ant zip-all-projects -S -q -f ../build.xml
