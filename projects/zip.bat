@echo off
rem This script will create individual zip files of all project directories specified
rem This script will also create a single zip file that will contain all the project directories

where /q ant
if %ERRORLEVEL% neq 0 (
	echo ant is not available in the environment path
	echo Please add ant to the environment path
	echo Example ant location for netbeans is:
    echo C:\Program Files\Netbeans 8.1\extide\ant\bin
	exit /b 1
)

rem Loop through all directories (each project is a directory)
for /d %%i in (*.*) do (
	rem Zip the project directory
	echo Zipping %%i
	call ant zip-project -S -q -f ..\build.xml -Dproject.name="%%i"
)

echo Zipping all projects
call ant zip-all-projects -S -q -f ..\build.xml
