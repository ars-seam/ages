
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class TillageOperation extends ManagementOperation {
    private final Tillage tillage;

    public TillageOperation(int managementId, Crop crop, MonthDay date, Tillage tillage) {
        super(managementId, crop, date);
        this.tillage = tillage;
    }

    public Tillage getTillage() {
        return tillage;
    }
}
