
package management;

/**
 *
 * @author Nathan Lighthart
 */
public class HRURotation {
    private final int hruId;
    private final int rotationId;
    private final double reductionFactor;

    public HRURotation(int hid, int rid, double reductionFactor) {
        hruId = hid;
        rotationId = rid;
        this.reductionFactor = reductionFactor;
    }

    public int getHRUId() {
        return hruId;
    }

    public int getRotationId() {
        return rotationId;
    }

    public double getReductionFactor() {
        return reductionFactor;
    }
}
