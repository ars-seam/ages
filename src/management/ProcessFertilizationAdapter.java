
package management;

import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessFertilizationAdapter extends AnnotatedAdapter {
    @Description("Indicates fertilization optimization with plant demand.")
    @Input public int opti;

    @Description("Current Time")
    @Input public LocalDate time;

    @Description("plant groth nitrogen stress factor")
    @Optional
    @Input public double nstrs;

    @Description("Mineral nitrogen content in the soil profile down to 60 cm depth")
    @Optional
    @Input public double nmin;

    @Description("Optimal nitrogen content in Biomass")
    @Units("kgN/ha")
    @Optional
    @Input public double BioNOpt;

    @Description("Actual nitrogen content in Biomass")
    @Units("kgN/ha")
    @Optional
    @Input public double BioNAct;

    @Description("Fraction of actual potential heat units sum")
    @Optional
    @Input public double FPHUact;

    @Description("current crop")
    @Input public Crop crop;

    @Description("Date to start reduction")
    @Input public LocalDate startReduction;

    @Description("Date to end reduction")
    @Input public LocalDate endReduction;

    @Description("Reduction Factor for Fertilisation 0 - 10")
    @Input public double reductionFactor;

    @Description("current management operation")
    @Input public ManagementOperation management;

    @Description("Number of fertilisation action in crop")
    @Optional
    @Input @Output public double nfert;

    @Description("Rest after former nfert amount of N-fertilizer")
    @Units("kg/ha*a")
    @Optional
    @Input @Output public double restfert;

    @Description("Minimum counter between 2 fertilizer actions in days (only used when opti = 2)")
    @Optional
    @Input @Output private int optimalInterval;

    @Description("Ammonium input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertNH4;

    @Description("Nitrate input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertNO3;

    @Description("Active organic N input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertorgNactive;

    @Description("Current organic fertilizer amount added to residue pool")
    @Output public double fertorgNfresh;

    @Description("Fertilisation reduction due to the plant demand routine")
    @Units("kgN/ha")
    @Output public double Nredu;

    private int oldCropId;

    @Override
    protected void run(Context context) {
        fertNO3 = 0;
        fertNH4 = 0;
        fertorgNactive = 0;
        fertorgNfresh = 0;
        Nredu = 0;

        if (crop != null) {
            if (oldCropId != crop.cid) {
                nfert = 0.0;
            }
            oldCropId = crop.cid;
        }

        if (shouldProcessOptimal()) {
            if (canProcessOptimal()) {
                processFertilizationOptimal();
                optimalInterval = 30;
            }
        } else if (management instanceof FertilizerOperation) {
            processFertilization((FertilizerOperation) management);
            restfert = crop.maxfert;
        }

        if (optimalInterval > 0) {
            --optimalInterval;
        }
    }

    private boolean shouldProcessOptimal() {
        return opti == 2 && nfert > 0 && isAnnualCrop();
    }

    private boolean isAnnualCrop() {
        switch (crop.idc) {
            case 1:
            case 2:
            case 4:
            case 5:
                return true;
            default:
                return false;
        }
    }

    private boolean canProcessOptimal() {
        return isOptimalTime() && nstrs > 0.03 && nfert < 4 && optimalInterval == 0;
    }

    private boolean isOptimalTime() {
        int dayOfYear = time.getDayOfYear();
        return dayOfYear > 90 && dayOfYear < 300;
    }

    private void processFertilization(FertilizerOperation fertOp) {
        ProcessFertilization component = new ProcessFertilization();
        Fertilizer fertilizer = fertOp.getFertilizer();

        component.time = time;
        component.nfert = nfert;
        component.nmin = nmin;
        component.endbioN = crop.endbioN;
        component.startReduction = startReduction;
        component.endReduction = endReduction;
        component.reductionFactor = reductionFactor;
        component.BioNOpt = BioNOpt;
        component.BioNAct = BioNAct;
        component.FPHUact = FPHUact;
        component.fertilizerAmount = fertOp.getFertilizerAmount();
        component.fminn = fertilizer.fminn;
        component.forgn = fertilizer.forgn;
        component.fnh4n = fertilizer.fnh4n;

        component.processFertilization();

        nfert = component.nfert;
        fertNO3 = component.fertNO3;
        fertNH4 = component.fertNH4;
        fertorgNactive = component.fertorgNactive;
        fertorgNfresh = component.fertorgNfresh;
        Nredu = component.Nredu;
    }

    private void processFertilizationOptimal() {
        ProcessFertilizationOptimal component = new ProcessFertilizationOptimal();

        component.nfert = nfert;
        component.nmin = nmin;
        component.BioNOpt = BioNOpt;
        component.BioNAct = BioNAct;
        component.FPHUact = FPHUact;
        component.bion02 = crop.bion02;
        component.bion04 = crop.bion04;
        component.bion06 = crop.bion06;
        component.bion08 = crop.bion08;
        component.endbioN = crop.endbioN;
        component.restfert = restfert;

        component.processFertilizationOptimal();

        nfert = component.nfert;
        restfert = component.restfert;
        fertNO3 = component.fertNO3;
        fertNH4 = component.fertNH4;
        fertorgNactive = component.fertorgNactive;
        fertorgNfresh = component.fertorgNfresh;
    }
}
