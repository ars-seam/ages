
package management;

import java.time.LocalDate;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessFertilization {
    public LocalDate time;
    public double endbioN;
    public LocalDate startReduction;
    public LocalDate endReduction;
    public double reductionFactor;
    public double nmin;
    public double BioNOpt;
    public double BioNAct;
    public double FPHUact;
    public double fertilizerAmount;
    public double fminn;
    public double forgn;
    public double fnh4n;

    // output
    public double nfert;
    public double fertNH4;
    public double fertNO3;
    public double fertorgNactive;
    public double fertorgNfresh;
    public double Nredu;

    public void processFertilization() {
        double run_nfert = nfert;
        double fertN_total = 0;

        double redu = reductionFactor;

        if (time.isAfter(startReduction) && time.isBefore(endReduction)) {
            redu = Math.max(0, redu);
        } else {
            redu = 1;
        }

        double famount = fertilizerAmount * redu;
        fertN_total = famount * (fminn + forgn);

        // fertilize depending on the demand and N content in the soil (only for the first nfert)
        if (run_nfert == 0.0) {
            double demand_factor = Math.min(Math.sqrt(FPHUact + 0.15), 1);
            double future_demand = (demand_factor * endbioN) - BioNOpt;
            double actual_demand = BioNOpt - BioNAct;
            double total_demand = (future_demand + actual_demand) - nmin + 30;
            redu = total_demand / fertN_total;
            if (redu < 0) {
                redu = 0;
            }
            redu = Math.min(redu, 1.0);
            Nredu = (1 - redu) * (forgn + fminn) * famount;
            famount = redu * famount;
        }
        run_nfert = run_nfert + 1;
        nfert = run_nfert;

        double fertNH4N = famount * fminn * fnh4n;
        double fertNO3N = famount * fminn * (1 - fnh4n);
        fertorgNfresh = 0.5 * forgn * famount;
        fertorgNactive = 0.5 * forgn * famount;

        fertNO3 = fertNO3N;
        fertNH4 = fertNH4N;
    }
}
