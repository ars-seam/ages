
package management;

import java.time.LocalDate;

/**
 *
 * @author Nathan Lighthart
 */
public class ActualDateIrrigation extends Irrigation {
    private final LocalDate date;

    public ActualDateIrrigation(int irrigationId, double rate, double depth, LocalDate date) {
        super(irrigationId, rate, depth);
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }
}
