
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class HarvestOperation extends ManagementOperation {
    private final int harvestType;
    private final double harvestFraction;

    public HarvestOperation(int managementId, Crop crop, MonthDay date, int harvestType, double harvestFraction) {
        super(managementId, crop, date);
        this.harvestType = harvestType;
        this.harvestFraction = harvestFraction;
    }

    public int getHarvestType() {
        return harvestType;
    }

    public double getHarvestFraction() {
        return harvestFraction;
    }
}
