
package management;

import crop.Crop;
import java.time.LocalDate;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Lighthart
 */
public class ManagementSchedule {
    /**
     * This schedules rotation id
     */
    private final int rotationId;
    /**
     * This schedules rotation. The list of management ids for the rotation
     */
    private final List<Integer> rotation;
    /**
     * This is the list of all serialized management operations for all of the
     * different managements in the rotation.
     */
    private final List<ManagementOperation> managements;
    /**
     * This is a map from management id to irrigation list (may be null)
     */
    private final Map<Integer, List<Irrigation>> irrigationsMap;

    private LocalDate time;

    private Crop currentCrop;

    private int managementPosition;
    private int currentManagementId;
    private ManagementOperation currentOperation;
    private ManagementOperation nextOperation;
    private LocalDate nextOperationTime;

    private LocalDate nextTillageDate;
    private LocalDate nextFertilizationDate;
    private LocalDate nextPlantingDate;
    private LocalDate nextHarvestingDate;
    private LocalDate nextFallowDate;
    private TillageOperation nextTillageOperation;
    private FertilizerOperation nextFertilizationOperation;
    private PlantOperation nextPlantingOperation;
    private HarvestOperation nextHarvestingOperation;
    private FallowOperation nextFallowOperation;

    private int currentIrrigationManagementId;
    private List<Irrigation> irrigations;
    private int irrigationPosition;
    private int daysSinceIrrigationStart;
    private Irrigation currentIrrigation;
    private Irrigation nextIrrigation;
    private LocalDate currentIrrigationEndTime;
    private LocalDate nextIrrigationStartTime;
    private LocalDate nextIrrigationEndTime;

    public ManagementSchedule(int rotationId, List<Integer> rotation,
            Map<Integer, List<ManagementOperation>> managementMap, LocalDate startTime) {
        this(rotationId, rotation, managementMap, null, null, startTime);
    }

    public ManagementSchedule(int rotationId, List<Integer> rotation,
            Map<Integer, List<ManagementOperation>> managementMap,
            Map<Integer, List<Integer>> managementIrrigationMap,
            Map<Integer, List<Irrigation>> irrigationMap, LocalDate startTime) {
        this.rotationId = rotationId;
        this.rotation = rotation;
        managements = new ArrayList<>();
        createManagements(managementMap);

        // set time to day before start time (so first call to moveToNextDay is the start time)
        time = startTime.minusDays(1L);

        // initialize management
        managementPosition = 0;
        currentOperation = null;
        nextOperation = managements.get(0);
        currentCrop = nextOperation.getCrop(); // do so initial crop is not null
        currentManagementId = nextOperation.getManagementId();
        updateOperationTime();
        computeNextOperationTimes();

        // initialize irrigation
        if (irrigationMap != null) {
            this.irrigationsMap = new HashMap<>();
            createIrrigations(managementIrrigationMap, irrigationMap);

            currentIrrigationManagementId = currentManagementId;
            currentIrrigation = null;
            daysSinceIrrigationStart = 0;
            irrigationPosition = 0;

            irrigations = this.irrigationsMap.get(currentIrrigationManagementId);
            nextIrrigation = irrigations.get(0);
            updateIrrigationTimes();
            currentIrrigationEndTime = nextIrrigationEndTime;
        } else {
            irrigationsMap = null;
        }
    }

    public int getRotationId() {
        return rotationId;
    }

    public Crop getCurrentCrop() {
        return currentCrop;
    }

    public ManagementOperation getCurrentOperation() {
        return currentOperation;
    }

    public Irrigation getCurrentIrrigation() {
        if (currentIrrigation instanceof FixedIntervalIrrigation) {
            // fixed interval has special temporal condition that it only occurs every n days in its interval
            FixedIntervalIrrigation irrigation = (FixedIntervalIrrigation) currentIrrigation;
            if (daysSinceIrrigationStart % irrigation.getInterval() != 0) {
                return null;
            }
        }
        return currentIrrigation;
    }

    public LocalDate getNextTillageDate() {
        return nextTillageDate;
    }

    public LocalDate getNextFertilizationDate() {
        return nextFertilizationDate;
    }

    public LocalDate getNextPlantingDate() {
        return nextPlantingDate;
    }

    public LocalDate getNextHarvestingDate() {
        return nextHarvestingDate;
    }

    public LocalDate getNextFallowDate() {
        return nextFallowDate;
    }

    public TillageOperation getNextTillageOperation() {
        return nextTillageOperation;
    }

    public FertilizerOperation getNextFertilizationOperation() {
        return nextFertilizationOperation;
    }

    public PlantOperation getNextPlantingOperation() {
        return nextPlantingOperation;
    }

    public HarvestOperation getNextHarvestingOperation() {
        return nextHarvestingOperation;
    }

    public FallowOperation getNextFallowOperation() {
        return nextFallowOperation;
    }

    public void moveToNextDay() {
        time = time.plusDays(1L);
        updateManagement();
        updateIrrigation();
    }

    private void createManagements(Map<Integer, List<ManagementOperation>> managementMap) {
        for (int managementId : rotation) {
            List<ManagementOperation> operations = managementMap.get(managementId);
            if (operations == null) {
                throw new IllegalArgumentException("Invalid MID: " + managementId);
            } else {
                managements.addAll(operations);
            }
        }
    }

    private void createIrrigations(Map<Integer, List<Integer>> managementIrrigationMap,
            Map<Integer, List<Irrigation>> irrigationMap) {
        // create irrigation map for each management id in the rotation
        for (int managementId : rotation) {
            List<Irrigation> irrigationList = new ArrayList<>();

            // find irrigation ids for the specified management id
            List<Integer> irrigationIds = managementIrrigationMap.get(managementId);
            if (irrigationIds == null) {
                throw new IllegalArgumentException("Invalid MID for irrigation: " + managementId);
            }

            // create the list of irrigations for the specified irrigations ids
            for (int irrigationId : irrigationIds) {
                List<Irrigation> irri = irrigationMap.get(irrigationId);
                if (irri == null) {
                    throw new IllegalArgumentException("Invalid IID: " + irrigationId);
                }
                irrigationList.addAll(irri);
            }

            this.irrigationsMap.put(managementId, irrigationList);
        }
    }

    private void updateManagement() {
        if (time.equals(nextOperationTime)) {
            currentOperation = nextOperation;
            currentCrop = currentOperation.getCrop();
            currentManagementId = currentOperation.getManagementId();
            managementPosition = (managementPosition + 1) % managements.size();
            nextOperation = managements.get(managementPosition);
            updateOperationTime();
            computeNextOperationTimes();
        } else {
            currentOperation = null;
        }
    }

    private void updateIrrigation() {
        if (irrigationsMap == null) {
            return;
        }
        if (currentIrrigationManagementId != currentManagementId) {
            // switch irrigations list
            currentIrrigationManagementId = currentManagementId;
            irrigations = irrigationsMap.get(currentIrrigationManagementId);
            currentIrrigation = null;
            daysSinceIrrigationStart = 0;
            irrigationPosition = 0;
            nextIrrigation = irrigations.get(0);
            computeNewIrrigationTimes();
        }
        if (time.equals(nextIrrigationStartTime)) {
            currentIrrigation = nextIrrigation;
            currentIrrigationEndTime = nextIrrigationEndTime;
            daysSinceIrrigationStart = 0;
            irrigationPosition = (irrigationPosition + 1) % irrigations.size();
            nextIrrigation = irrigations.get(irrigationPosition);
            updateIrrigationTimes();
        } else if (time.isAfter(currentIrrigationEndTime)) {
            currentIrrigation = null;
            daysSinceIrrigationStart = 0;
        } else {
            ++daysSinceIrrigationStart;
        }
    }

    private LocalDate computeNextTime(MonthDay date) {
        return computeNextTime(date, time);
    }

    private LocalDate computeNextTime(MonthDay date, LocalDate reference) {
        LocalDate nextTime = date.atYear(reference.getYear());
        if (!nextTime.isAfter(reference)) {
            nextTime = nextTime.plusYears(1L);
        }
        return nextTime;
    }

    private LocalDate computeNextTimeAllowSameDay(MonthDay date) {
        return computeNextTimeAllowSameDay(date, time);
    }

    private LocalDate computeNextTimeAllowSameDay(MonthDay date, LocalDate reference) {
        LocalDate nextTime = date.atYear(reference.getYear());
        if (nextTime.isBefore(reference)) {
            nextTime = nextTime.plusYears(1L);
        }
        return nextTime;
    }

    private void updateOperationTime() {
        nextOperationTime = computeNextTime(nextOperation.getDate());
    }

    private void computeNextOperationTimes() {
        boolean setFallow = false;
        boolean setTillage = false;
        boolean setFertilization = false;
        boolean setPlanting = false;
        boolean setHarvesting = false;
        boolean setAll;

        int startPosition = managementPosition;
        int position = startPosition;
        LocalDate currentTime = time;
        do {
            ManagementOperation operation = managements.get(position);
            currentTime = computeNextTime(operation.getDate(), currentTime);
            if (operation instanceof TillageOperation) {
                if (!setTillage) {
                    nextTillageDate = currentTime;
                    nextTillageOperation = (TillageOperation) operation;
                    setTillage = true;
                }
            } else if (operation instanceof FertilizerOperation) {
                if (!setFertilization) {
                    nextFertilizationDate = currentTime;
                    nextFertilizationOperation = (FertilizerOperation) operation;
                    setFertilization = true;
                }
            } else if (operation instanceof PlantOperation) {
                if (!setPlanting) {
                    nextPlantingDate = currentTime;
                    nextPlantingOperation = (PlantOperation) operation;
                    setPlanting = true;
                }
            } else if (operation instanceof HarvestOperation) {
                if (!setHarvesting) {
                    nextHarvestingDate = currentTime;
                    nextHarvestingOperation = (HarvestOperation) operation;
                    setHarvesting = true;
                }
            } else if (operation instanceof FallowOperation) {
                if (!setFallow) {
                    nextFallowDate = currentTime;
                    nextFallowOperation = (FallowOperation) operation;
                    setFallow = true;
                }
            }
            setAll = setTillage && setFertilization && setPlanting && setHarvesting && setFallow;
            position = (position + 1) % managements.size();
        } while (!setAll && position != startPosition);
    }

    private void updateIrrigationTimes() {
        if (nextIrrigation instanceof FixedCropDateIrrigation) {
            FixedCropDateIrrigation irrigation = (FixedCropDateIrrigation) nextIrrigation;
            nextIrrigationStartTime = computeNextTime(irrigation.getDate());
            nextIrrigationEndTime = nextIrrigationStartTime;
        } else if (nextIrrigation instanceof ActualDateIrrigation) {
            ActualDateIrrigation irrigation = (ActualDateIrrigation) nextIrrigation;
            nextIrrigationStartTime = irrigation.getDate();
            nextIrrigationEndTime = nextIrrigationStartTime;
        } else {
            MonthDay start;
            MonthDay end;
            if (nextIrrigation instanceof FixedIntervalIrrigation) {
                FixedIntervalIrrigation irrigation = (FixedIntervalIrrigation) nextIrrigation;
                start = irrigation.getStart();
                end = irrigation.getEnd();
            } else if (nextIrrigation instanceof DepletionLevelIrrigation) {
                DepletionLevelIrrigation irrigation = (DepletionLevelIrrigation) nextIrrigation;
                start = irrigation.getStart();
                end = irrigation.getEnd();
            } else {
                return;
            }
            nextIrrigationStartTime = computeNextTime(start);
            nextIrrigationEndTime = computeNextTime(end, nextIrrigationStartTime);
        }
    }

    /**
     * This function needs to allow same day for time because the day that
     * irrigation is switch may be the same day that new irrigation needs to
     * start
     */
    private void computeNewIrrigationTimes() {
        if (nextIrrigation instanceof FixedCropDateIrrigation) {
            FixedCropDateIrrigation irrigation = (FixedCropDateIrrigation) nextIrrigation;
            nextIrrigationStartTime = computeNextTimeAllowSameDay(irrigation.getDate());
            nextIrrigationEndTime = nextIrrigationStartTime;
        } else if (nextIrrigation instanceof ActualDateIrrigation) {
            ActualDateIrrigation irrigation = (ActualDateIrrigation) nextIrrigation;
            nextIrrigationStartTime = irrigation.getDate();
            nextIrrigationEndTime = nextIrrigationStartTime;
        } else {
            MonthDay start;
            MonthDay end;
            if (nextIrrigation instanceof FixedIntervalIrrigation) {
                FixedIntervalIrrigation irrigation = (FixedIntervalIrrigation) nextIrrigation;
                start = irrigation.getStart();
                end = irrigation.getEnd();
            } else if (nextIrrigation instanceof DepletionLevelIrrigation) {
                DepletionLevelIrrigation irrigation = (DepletionLevelIrrigation) nextIrrigation;
                start = irrigation.getStart();
                end = irrigation.getEnd();
            } else {
                return;
            }
            nextIrrigationStartTime = computeNextTimeAllowSameDay(start);
            nextIrrigationEndTime = computeNextTimeAllowSameDay(end, nextIrrigationStartTime);
        }
    }
}
