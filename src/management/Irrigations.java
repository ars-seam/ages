/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package management;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class Irrigations {
    private static final String NOTHING = "-";
    private static final DateTimeFormatter monthDayFormatter = DateTimeFormatter.ofPattern("MM-dd");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM-dd-yy");

    public static Irrigation create(String[] values) {
        try {
            return create0(values);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Error parsing irrigation: " + Arrays.toString(values), ex);
        }
    }

    private static Irrigation create0(String[] values) {
        int irrigationId = Integer.parseInt(values[1]);
        double depth = Double.parseDouble(values[6]);

        double rate;
        if (NOTHING.equals(values[5])) {
            rate = Double.NaN;
        } else {
            rate = Double.parseDouble(values[5]);
        }

        if (!NOTHING.equals(values[2])) {
            // fixed crop date
            int cropId = Integer.parseInt(values[2]);
            MonthDay date = MonthDay.parse(values[3], monthDayFormatter);
            return new FixedCropDateIrrigation(irrigationId, rate, depth, cropId,
                    date);
        }
        if (!NOTHING.equals(values[4])) {
            // fixed intervals
            String[] dates = values[3].split("\\s*/\\s*");
            MonthDay start = MonthDay.parse(dates[0], monthDayFormatter);
            MonthDay end = MonthDay.parse(dates[1], monthDayFormatter);
            int interval = Integer.parseInt(values[4]);
            return new FixedIntervalIrrigation(irrigationId, rate, depth, start,
                    end, interval);
        }
        if (!NOTHING.equals(values[7])) {
            // depletion level
            String[] dates = values[3].split("\\s*/\\s*");
            MonthDay start = MonthDay.parse(dates[0], monthDayFormatter);
            MonthDay end = MonthDay.parse(dates[1], monthDayFormatter);
            double depletionTriggerLevel = Double.parseDouble(values[7]);
            double depletionRefillLevel = Double.parseDouble(values[8]);
            return new DepletionLevelIrrigation(irrigationId, rate, depth, start,
                    end, depletionTriggerLevel, depletionRefillLevel);
        }
        // actual date
        LocalDate date = LocalDate.parse(values[3], dateFormatter);
        return new ActualDateIrrigation(irrigationId, rate, depth, date);
    }

    private Irrigations() {
    }
}
