
package management;

import crop.Crop;
import java.time.MonthDay;

/**
 *
 * @author Nathan Lighthart
 */
public class PlantOperation extends ManagementOperation {
    public PlantOperation(int managementId, Crop crop, MonthDay date) {
        super(managementId, crop, date);
    }
}
