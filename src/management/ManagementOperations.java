/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package management;

import crop.Crop;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;

public class ManagementOperations {
    private static final String NOTHING = "-";
    private static final DateTimeFormatter monthDayFormatter = DateTimeFormatter.ofPattern("MM-dd");

    public static ManagementOperation create(String[] values, Map<Integer, Crop> crops,
            Map<Integer, Tillage> tillages, Map<Integer, Fertilizer> fertilizers) {
        try {
            return create0(values, crops, tillages, fertilizers);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Error parsing operation: " + Arrays.toString(values), ex);
        }
    }

    private static ManagementOperation create0(String[] values, Map<Integer, Crop> crops,
            Map<Integer, Tillage> tillages, Map<Integer, Fertilizer> fertilizers) {
        int managementId = Integer.parseInt(values[1]);
        int cropId = Integer.parseInt(values[2]);
        Crop crop = crops.get(cropId);
        if (crop == null) {
            throw new IllegalArgumentException("Invalid crop id: " + cropId);
        }
        MonthDay date = MonthDay.parse(values[3], monthDayFormatter);

        if (!NOTHING.equals(values[4])) {
            int tillageId = Integer.parseInt(values[4]);
            Tillage tillage = tillages.get(tillageId);
            if (tillage == null) {
                throw new IllegalArgumentException("Invalid tillage id: " + tillageId);
            }
            return new TillageOperation(managementId, crop, date, tillage);
        }
        if (!NOTHING.equals(values[5])) {
            int fertilizerId = Integer.parseInt(values[5]);
            Fertilizer fertilizer = fertilizers.get(fertilizerId);
            if (fertilizer == null) {
                throw new IllegalArgumentException("Invalid fertilizer id: " + fertilizerId);
            }
            double fertilizerAmount = Double.parseDouble(values[6]);
            return new FertilizerOperation(managementId, crop, date, fertilizer, fertilizerAmount);
        }
        if (!NOTHING.equals(values[7])) {
            return new PlantOperation(managementId, crop, date);
        }
        if (!NOTHING.equals(values[8])) {
            int harvestType = Integer.parseInt(values[8]);
            double harvestFraction;
            if (NOTHING.equals(values[9])) {
                harvestFraction = 0.0;
            } else {
                harvestFraction = Double.parseDouble(values[9]);
            }
            return new HarvestOperation(managementId, crop, date, harvestType, harvestFraction);
        }
        return new FallowOperation(managementId, crop, date);
    }

    private ManagementOperations() {
    }
}
