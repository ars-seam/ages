/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import crop.Crop;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import management.Fertilizer;
import management.HRURotation;
import management.Irrigation;
import management.Irrigations;
import management.ManagementOperation;
import management.ManagementOperations;
import management.Tillage;

public class ManagementParameterReader {
    public static Map<Integer, Fertilizer> readFertilizer(Path fertilizerFilePath) throws IOException {
        Map<Integer, Fertilizer> map = new HashMap<>();
        CSTable table = DataIO.table(fertilizerFilePath.toFile());
        for (String[] row : table.rows()) {
            if (map.put(new Integer(row[1]), new Fertilizer(row)) != null) {
                throw new IllegalArgumentException("Duplicate Fertilizer ID: " + row[1]);
            }
        }
        return map;
    }

    public static Map<Integer, Tillage> readTillage(Path tillageFilePath) throws IOException {
        Map<Integer, Tillage> map = new HashMap<>();
        CSTable table = DataIO.table(tillageFilePath.toFile());
        for (String[] row : table.rows()) {
            if (map.put(new Integer(row[1]), new Tillage(row)) != null) {
                throw new IllegalArgumentException("Duplicate Tillage ID: " + row[1]);
            }
        }
        return map;
    }

    public static Map<Integer, List<Irrigation>> readIrrigation(Path irrigationFilePath) throws IOException {
        int oldiid = -1;
        int iid = -1;
        ArrayList<Irrigation> irrigations = null;
        Map<Integer, List<Irrigation>> map = new HashMap<>();

        CSTable table = DataIO.table(irrigationFilePath.toFile());
        for (String[] row : table.rows()) {
            iid = Integer.parseInt(row[1]);
            if (iid != oldiid) {
                if (map.put(oldiid, irrigations) != null) {
                    throw new IllegalArgumentException("Duplicate IID: " + oldiid);
                }
                irrigations = new ArrayList<>();
                oldiid = iid;
            }
            irrigations.add(Irrigations.create(row));
        }
        if (map.put(iid, irrigations) != null) {
            throw new IllegalArgumentException("Duplicate IID: " + iid);
        }

        return map;
    }

    public static Map<Integer, List<ManagementOperation>> readManagement(Path managementFilePath,
            Map<Integer, Crop> crops, Map<Integer, Tillage> tillages,
            Map<Integer, Fertilizer> fertilizers) throws IOException {
        int oldMid = -1;
        int mid = -1;
        ArrayList<ManagementOperation> managements = null;
        Map<Integer, List<ManagementOperation>> map = new HashMap<>();
        CSTable table = DataIO.table(managementFilePath.toFile());
        for (String[] row : table.rows()) {
            mid = Integer.parseInt(row[1]);
            if (mid != oldMid) {
                if (map.put(oldMid, managements) != null) {
                    throw new IllegalArgumentException("Duplicate MID: " + oldMid);
                }
                managements = new ArrayList<>();
                oldMid = mid;
            }
            managements.add(ManagementOperations.create(row, crops, tillages, fertilizers));
        }
        if (map.put(mid, managements) != null) {
            throw new IllegalArgumentException("Duplicate MID: " + mid);
        }
        return map;
    }

    public static Map<Integer, Crop> readCrops(Path cropFilePath) throws IOException {
        Map<Integer, Crop> map = new HashMap<>();
        CSTable table = DataIO.table(cropFilePath.toFile());
        for (String[] row : table.rows()) {
            Integer cid = Integer.parseInt(row[1]);
            if (map.put(cid, new Crop(row)) != null) {
                throw new IllegalArgumentException("Duplicate CID: " + cid);
            }
        }
        return map;
    }

    public static void readCropsUPGM(Path cropUPGMFilePath, Map<Integer, Crop> crops) throws IOException {
        // read the UPGM crop parameters and append
        CSTable table = DataIO.table(cropUPGMFilePath.toFile());
        for (String[] row : table.rows()) {
            Integer cid = Integer.parseInt(row[1]);
            Crop c;
            if ((c = crops.get(cid)) != null) {
                c.appendUPGM(row);
            } else {
                throw new IllegalArgumentException("No Corresponding CID: " + cid);
            }
        }
    }

    public static Map<Integer, List<Integer>> readManagementIrrigations(Path managementIrrigationFilePath) throws IOException {
        Map<Integer, List<Integer>> map = new HashMap<>();
        CSTable table = DataIO.table(managementIrrigationFilePath.toFile());
        for (String[] row : table.rows()) {
            List<Integer> irriList = new ArrayList<>();
            Integer mid = Integer.parseInt(row[1]);
            for (int col = 2; col < row.length; col++) {
                if (row[col].isEmpty()) {
                    break;
                }
                Integer iid = Integer.parseInt(row[col]);
                irriList.add(iid);
            }
            if (map.put(mid, irriList) != null) {
                throw new IllegalArgumentException("Duplicate RID: " + mid);
            }
        }
        return map;
    }

    public static Map<Integer, List<Integer>> readRotations(Path rotationFilePath) throws IOException {
        Map<Integer, List<Integer>> map = new HashMap<>();
        CSTable table = DataIO.table(rotationFilePath.toFile());
        for (String[] row : table.rows()) {
            List<Integer> manList = new ArrayList<>();
            Integer rid = Integer.parseInt(row[1]);
            for (int col = 2; col < row.length; col++) {
                if (row[col].isEmpty()) {
                    break;
                }
                Integer mid = Integer.parseInt(row[col]);
                manList.add(mid);
            }
            if (map.put(rid, manList) != null) {
                throw new IllegalArgumentException("Duplicate RID: " + rid);
            }
        }
        return map;
    }

    public static Map<Integer, HRURotation> readHRURotations(Path hruRotationFilePath) throws IOException {
        Map<Integer, HRURotation> map = new HashMap<>();
        CSTable table = DataIO.table(hruRotationFilePath.toFile());
        for (String[] row : table.rows()) {
            Integer hid = new Integer(row[1]);
            Integer rid = new Integer(row[2]);
            double redu_fac = Double.parseDouble(row[3]);
            HRURotation hruRotation = new HRURotation(hid, rid, redu_fac);
            map.put(hid, hruRotation);
        }
        return map;
    }
}
