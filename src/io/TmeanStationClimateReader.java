
package io;

import climate.ClimateType;
import gov.usda.jcf.core.Context;
import java.io.IOException;
import regionalization.Regionalizer;

/**
 *
 * @author Nathan Lighthart
 */
public class TmeanStationClimateReader implements ClimateReader {
    private final Regionalizer regionalizer;

    public TmeanStationClimateReader() {
        regionalizer = Regionalizer.of(ClimateType.MEAN_TEMPERATURE);
    }

    @Override
    public void readData() throws IOException {
        // no file to read so do nothing
    }

    @Override
    public double setValue(Context context) {
        regionalizer.regionalize(context);
        return regionalizer.getRegionalizedValue(context);
    }

    @Override
    public void reset() {
        // no file to reset so do nothing
    }

    @Override
    public void close() {
        // no file to close so do nothing
    }
}
