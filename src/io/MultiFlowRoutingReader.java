/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import routing.RoutingEdge;

public class MultiFlowRoutingReader {
    public static Map<Integer, List<RoutingEdge>> readRoutingTable(Path routingFilePath,
            boolean includeReaches) throws IOException {
        Map<Integer, List<RoutingEdge>> routingEdges = new HashMap<>();
        CSTable table = DataIO.table(routingFilePath.toFile(), "routing");

        for (String[] row : table.rows()) {
            if (row.length % 2 == 1 || row.length < 4) {
                throw new IOException("insufficient routing information");
            }

            int from = Integer.parseInt(row[1].trim());
            if (from == 0 || (!includeReaches && from < 0)) {
                continue;
            }

            List<RoutingEdge> routes = new ArrayList<>();
            for (int i = 2; i < row.length - 1; i += 2) {
                String toString = row[i].trim();
                String weightString = row[i + 1].trim();
                if (toString.isEmpty() || weightString.isEmpty()) {
                    continue;
                }

                int to = Integer.parseInt(toString);
                double weight = Double.parseDouble(weightString);

                if (to != 0 && (includeReaches || to > 0)) {
                    routes.add(new RoutingEdge(to, weight));
                }
            }

            routingEdges.put(from, routes);
        }

        return routingEdges;
    }
}
