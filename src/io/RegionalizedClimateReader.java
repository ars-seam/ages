
package io;

import ages.types.HRU;
import climate.ClimateType;
import gov.usda.jcf.core.Context;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Nathan Lighthart
 */
public class RegionalizedClimateReader implements ClimateReader {
    private final Path dataFilePath;
    private final LocalDate startTime;
    private final LocalDate endTime;
    private final String climateName;

    private Iterator<String[]> rows;
    private DateTimeFormatter dateFormatter;

    private Map<Integer, Integer> indexMap;
    String[] row;

    public RegionalizedClimateReader(Path dataFilePath, LocalDate startTime, LocalDate endTime, ClimateType climateType) {
        this.dataFilePath = dataFilePath;
        this.startTime = startTime;
        this.endTime = endTime;
        climateName = climateType.getName();
    }

    @Override
    public void readData() throws IOException {
        if (rows == null) {
            init();
        }
        row = rows.next();
    }

    @Override
    public double setValue(Context context) {
        HRU hru = context.get("hru", HRU.class);
        if (!indexMap.containsKey(hru.ID)) {
            throw new IllegalArgumentException("Invalid regionalization file: HRU " + hru.ID + " does not exist");
        }
        int index = indexMap.get(hru.ID);
        double value = Double.parseDouble(row[index]);
        context.put(climateName, value);
        return value;
    }

    @Override
    public void reset() {
        close();
    }

    @Override
    public void close() {
        if (rows != null) {
            if (rows instanceof TableIterator) {
                TableIterator<String[]> iter = (TableIterator<String[]>) rows;
                iter.close();
            }
            rows = null;
        }
    }

    private void init() throws IOException {
        CSTable table = DataIO.table(dataFilePath.toFile());
        rows = table.rows().iterator();

        String datefmt = table.getInfo().get(DataIO.DATE_FORMAT);
        if (datefmt == null) {
            throw new IOException("Missing date format in " + dataFilePath);
        }
        dateFormatter = DateTimeFormatter.ofPattern(datefmt);

        String tableStartTimeString = table.getInfo().get(DataIO.DATE_START);
        String tableEndTimeString = table.getInfo().get(DataIO.DATE_END);
        if (tableStartTimeString == null || tableEndTimeString == null) {
            throw new IOException("Missing start/end time in " + dataFilePath);
        }

        LocalDate tableStartTime = LocalDate.parse(tableStartTimeString, dateFormatter);
        LocalDate tableEndTime = LocalDate.parse(tableEndTimeString, dateFormatter);
        if (startTime.isBefore(tableStartTime) || endTime.isAfter(tableEndTime) || tableEndTime.isBefore(tableStartTime)) {
            throw new IOException("Illegal start/end in " + dataFilePath + " "
                    + dateFormatter.format(startTime) + " - " + dateFormatter.format(endTime));
        }

        long daysUntilStart = tableStartTime.until(startTime, ChronoUnit.DAYS);
        while (daysUntilStart > 0) {
            rows.next();
            --daysUntilStart;
        }

        indexMap = new HashMap<>();
        for (int i = 2; i <= table.getColumnCount(); ++i) {
            int hruId = Integer.parseInt(table.getColumnName(i));
            indexMap.put(hruId, i);
        }
    }
}
