
package io;

import climate.ClimateType;
import gov.usda.jcf.core.Context;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import lib.Regression;
import regionalization.Regionalizer;

/**
 *
 * @author Nathan Lighthart
 */
public class StationClimateReader implements ClimateReader {
    private final Path dataFilePath;
    private final LocalDate startTime;
    private final LocalDate endTime;
    private final ClimateType climateType;
    private final Regionalizer regionalizer;

    private Iterator<String[]> rows;
    private DateTimeFormatter dateFormatter;
    private double[] elevation;
    private double missingDataValue;

    private double[] dataArray;
    private double[] regCoeff;

    public StationClimateReader(Path dataFilePath, LocalDate startTime, LocalDate endTime, ClimateType climateType) {
        this.dataFilePath = dataFilePath;
        this.startTime = startTime;
        this.endTime = endTime;
        this.climateType = climateType;
        this.regionalizer = Regionalizer.of(climateType);
    }

    @Override
    public void readData() throws IOException {
        if (rows == null) {
            init();
        }
        String[] row = rows.next();
        LocalDate time = LocalDate.parse(row[1], dateFormatter);
        boolean containsData = false;
        for (int i = 0; i < dataArray.length; i++) {
            // first column is 2 (columns start with 1 + skipping date)
            dataArray[i] = Double.parseDouble(row[i + 2]);
            if (Double.compare(dataArray[i], missingDataValue) != 0) {
                containsData = true;
            }
        }
        if (!containsData) {
            throw new IOException("Error in input-file: " + dataFilePath
                    + ", only missing values for the time-step " + dateFormatter.format(time));
        }
        regCoeff = Regression.calcLinReg(elevation, dataArray);
    }

    @Override
    public double setValue(Context context) {
        String suffix = climateType.getSuffix();
        context.put("dataArray" + suffix, dataArray);
        context.put("regCoeff" + suffix, regCoeff);

        regionalizer.regionalize(context);
        return regionalizer.getRegionalizedValue(context);
    }

    @Override
    public void reset() {
        close();
    }

    @Override
    public void close() {
        if (rows != null) {
            if (rows instanceof TableIterator) {
                TableIterator<String[]> iter = (TableIterator<String[]>) rows;
                iter.close();
            }
            rows = null;
        }
    }

    private void init() throws IOException {
        CSTable table = DataIO.table(dataFilePath.toFile());
        rows = table.rows().iterator();
        dataArray = new double[table.getColumnCount() - 1];
        elevation = new double[table.getColumnCount() - 1];
        for (int i = 0; i < elevation.length; i++) {
            elevation[i] = Double.parseDouble(table.getColumnInfo(i + 2).get("elevation"));
        }
        String datefmt = table.getInfo().get(DataIO.DATE_FORMAT);
        if (datefmt == null) {
            throw new IOException("Missing date format in " + dataFilePath);
        }
        dateFormatter = DateTimeFormatter.ofPattern(datefmt);

        String missingDataValueString = table.getInfo().get(DataIO.KEY_MISSING_VAL);
        if (missingDataValueString == null) {
            missingDataValueString = table.getInfo().get("missing_val");
            if (missingDataValueString == null) {
                throw new IOException("Missing data value cannot be found");
            }
        }
        missingDataValue = Double.parseDouble(missingDataValueString);

        String tableStartTimeString = table.getInfo().get(DataIO.DATE_START);
        String tableEndTimeString = table.getInfo().get(DataIO.DATE_END);
        if (tableStartTimeString == null || tableEndTimeString == null) {
            throw new IOException("Missing start/end time in " + dataFilePath);
        }

        LocalDate tableStartTime = LocalDate.parse(tableStartTimeString, dateFormatter);
        LocalDate tableEndTime = LocalDate.parse(tableEndTimeString, dateFormatter);
        if (startTime.isBefore(tableStartTime) || endTime.isAfter(tableEndTime) || tableEndTime.isBefore(tableStartTime)) {
            throw new IOException("Illegal start/end in " + dataFilePath + " "
                    + dateFormatter.format(startTime) + " - " + dateFormatter.format(endTime));
        }

        long daysUntilStart = tableStartTime.until(startTime, ChronoUnit.DAYS);
        while (daysUntilStart > 0) {
            rows.next();
            --daysUntilStart;
        }
    }
}
