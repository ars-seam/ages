
package io;

import ages.types.HRU;
import climate.ClimateType;
import gov.usda.jcf.core.Context;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lib.FileHasher;

/**
 *
 * @author Nathan Lighthart
 */
public class RegionalizationCacher {
    private TimeInterval timeInterval;

    private ClimateRegionalizer regionalizerTmin;
    private ClimateRegionalizer regionalizerTmax;
    private ClimateRegionalizer regionalizerTmean;
    private ClimateRegionalizer regionalizerHum;
    private ClimateRegionalizer regionalizerSol;
    private ClimateRegionalizer regionalizerWind;
    private ClimateRegionalizer regionalizerPrecip;

    private List<HRU> hrus;
    private Map<Integer, Context> hruContexts;
    private Context parameterContext;
    private Context temporalContext;
    private List<Context> hruContextList;
    private List<ClimateRegionalizer> readers;
    private List<ClimateRegionalizer> writers;

    public RegionalizationCacher(Path dataFileTmin, Path dataFileTmax, Path dataFileHum, Path dataFilePrecip, Path dataFileSol, Path dataFileWind) {
        regionalizerTmin = new ClimateRegionalizer(dataFileTmin, ClimateType.MINIMUM_TEMPERATURE);
        regionalizerTmax = new ClimateRegionalizer(dataFileTmax, ClimateType.MAXIMUM_TEMPERATURE);
        regionalizerTmean = new ClimateRegionalizer(dataFileTmin, ClimateType.MEAN_TEMPERATURE);
        regionalizerHum = new ClimateRegionalizer(dataFileHum, ClimateType.HUMIDITY);
        regionalizerSol = new ClimateRegionalizer(dataFileSol, ClimateType.SOLAR);
        regionalizerWind = new ClimateRegionalizer(dataFileWind, ClimateType.WIND);
        regionalizerPrecip = new ClimateRegionalizer(dataFilePrecip, ClimateType.PRECIPITATION);
    }

    public void cache(List<HRU> hrus, Map<Integer, Context> hruContexts, Context parameterContext, Context temporalContext) throws IOException {
        this.hrus = hrus;
        this.hruContexts = hruContexts;
        this.parameterContext = parameterContext;
        this.temporalContext = temporalContext;

        initializeRunStatus();
        initializeReadersAndWriters();
        if (writers.isEmpty()) {
            System.out.println("--> Skipping climate file regionalization ...");
            return;
        } else {
            System.out.println("--> Performing climate file regionalization ...");
            long now1 = System.currentTimeMillis();
            cacheImpl();
            long now2 = System.currentTimeMillis();
            System.out.println("---> Climate file regionalization completed ... " + (now2 - now1));
        }

    }

    private void cacheImpl() throws IOException {
        initializeTime();
        initializeIO();
        for (ClimateRegionalizer regionalizer : writers) {
            regionalizer.writeHeader();
        }

        initializeContextList();

        final LocalDate endTime = timeInterval.endTime;
        for (LocalDate time = timeInterval.startTime; !time.isAfter(endTime); time = time.plusDays(1L)) {
            temporalContext.put("time", time);
            for (ClimateRegionalizer regionalizer : readers) {
                regionalizer.readLine();
            }
            for (ClimateRegionalizer regionalizer : writers) {
                regionalizer.readLine();
                regionalizer.write(time);
            }
            hruContextList.parallelStream().forEach((hruContext) -> {
                for (ClimateRegionalizer regionalizer : readers) {
                    regionalizer.read(hruContext);
                }
                for (ClimateRegionalizer regionalizer : writers) {
                    regionalizer.read(hruContext);
                }
            });
            for (Context hruContext : hruContextList) {
                for (ClimateRegionalizer regionalizer : writers) {
                    regionalizer.write(hruContext);
                }
            }
            for (ClimateRegionalizer regionalizer : writers) {
                regionalizer.finishLine();
            }
        }

        for (ClimateRegionalizer regionalizer : readers) {
            regionalizer.closeReader();
        }
        for (ClimateRegionalizer regionalizer : writers) {
            regionalizer.closeReader();
            regionalizer.closeWriter();
        }
    }

    private void initializeTime() throws IOException {
        timeInterval = null;

        ClimateRegionalizer[] regionalizers = new ClimateRegionalizer[]{
            regionalizerTmin,
            regionalizerTmax,
            regionalizerTmean,
            regionalizerHum,
            regionalizerSol,
            regionalizerWind,
            regionalizerPrecip
        };

        for (ClimateRegionalizer regionalizer : regionalizers) {
            TimeInterval interval = regionalizer.findTimeInterval();

            if (timeInterval == null) {
                timeInterval = interval;
            } else {
                timeInterval.merge(interval);
            }
        }
    }

    private void initializeRunStatus() throws IOException {
        Set<String> hruIdSet = new HashSet<>();
        for (HRU hru : hrus) {
            hruIdSet.add(Integer.toString(hru.ID));
        }

        regionalizerTmin.calculateRunStatus(hruIdSet);
        regionalizerTmax.calculateRunStatus(hruIdSet);
        regionalizerTmean.calculateRunStatus(hruIdSet);
        regionalizerHum.calculateRunStatus(hruIdSet);
        regionalizerSol.calculateRunStatus(hruIdSet);
        regionalizerWind.calculateRunStatus(hruIdSet);
        regionalizerPrecip.calculateRunStatus(hruIdSet);

        resolveDependencies();
    }

    private void resolveDependencies() {
        resolveForwardDependencies();
        resolveBackwardDependencies();
    }

    /**
     * Resolves forward dependencies. This ensures that a dependent is set to
     * write if any of its dependencies are writing.
     */
    private void resolveForwardDependencies() {
        if (regionalizerTmin.shouldWrite() || regionalizerTmax.shouldWrite()) {
            regionalizerTmean.ensureWrite();
        }
        if (regionalizerTmean.shouldWrite() || regionalizerWind.shouldWrite()) {
            regionalizerPrecip.ensureWrite();
        }
    }

    /**
     * Resolves backward dependencies. This ensures that if a dependent is
     * reading that all of its dependencies are set to read.
     */
    private void resolveBackwardDependencies() {
        if (regionalizerPrecip.shouldRead()) {
            regionalizerTmean.ensureRead();
            regionalizerWind.ensureRead();
        }
        if (regionalizerTmean.shouldRead()) {
            regionalizerTmin.ensureRead();
            regionalizerTmax.ensureRead();
        }
    }

    private void initializeIO() throws IOException {
        regionalizerTmin.initializeIO();
        regionalizerTmax.initializeIO();
        regionalizerTmean.initializeIO();
        regionalizerHum.initializeIO();
        regionalizerSol.initializeIO();
        regionalizerWind.initializeIO();
        regionalizerPrecip.initializeIO();
    }

    private void initializeContextList() {
        hruContextList = new ArrayList<>();
        for (HRU hru : hrus) {
            hruContextList.add(hruContexts.get(hru.ID));
        }
    }

    private void initializeReadersAndWriters() {
        // order by dependecies
        ClimateRegionalizer[] regionalizers = new ClimateRegionalizer[]{
            regionalizerTmin,
            regionalizerTmax,
            regionalizerHum,
            regionalizerSol,
            regionalizerWind,
            regionalizerTmean,
            regionalizerPrecip
        };

        readers = new ArrayList<>();
        writers = new ArrayList<>();
        for (ClimateRegionalizer regionalizer : regionalizers) {
            if (regionalizer.shouldWrite()) {
                writers.add(regionalizer);
            } else if (regionalizer.shouldRead()) {
                readers.add(regionalizer);
            }
        }
    }

    private static class TimeInterval {
        private LocalDate startTime;
        private LocalDate endTime;

        public TimeInterval(LocalDate start, LocalDate end) {
            startTime = start;
            endTime = end;
        }

        public void merge(TimeInterval interval) {
            if (interval.startTime.isAfter(startTime)) {
                startTime = interval.startTime;
            }
            if (interval.endTime.isBefore(endTime)) {
                endTime = interval.endTime;
            }
        }

        public boolean isInvalid() {
            return startTime.isAfter(endTime);
        }

        @Override
        public String toString() {
            return startTime + " - " + endTime;
        }
    }

    private enum RunStatus {
        DO_NOTHING(false, false),
        READ(true, false),
        WRITE(true, true);

        private final boolean shouldRead;
        private final boolean shouldWrite;

        private RunStatus(boolean shouldRead, boolean shouldWrite) {
            this.shouldRead = shouldRead;
            this.shouldWrite = shouldWrite;
        }

        public boolean shouldRead() {
            return shouldRead;
        }

        public boolean shouldWrite() {
            return shouldWrite;
        }
    }

    private class ClimateRegionalizer {
        private static final String REGIONALIZATION_PREFIX = "reg_";

        private final Path dataFile;
        private final ClimateType climateType;
        private final String climateName;
        private Path regFile;
        private String dateFormat;
        private DateTimeFormatter dateFormatter;
        private Map<String, String> parameterMap;
        private RunStatus runStatus;
        private ClimateReader reader;
        private PrintWriter writer;

        public ClimateRegionalizer(Path dataFile, ClimateType climateType) {
            this.dataFile = dataFile;
            this.climateType = climateType;
            climateName = climateType.getName();
            setRegionalizationFile();

            dateFormat = "yyyy-MM-dd";
            dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
        }

        private void setRegionalizationFile() {
            switch (climateType) {
                case MEAN_TEMPERATURE:
                    regFile = dataFile.resolveSibling(REGIONALIZATION_PREFIX + ClimateType.MEAN_TEMPERATURE.getName() + ".csv");
                    break;
                default:
                    regFile = dataFile.resolveSibling(REGIONALIZATION_PREFIX + dataFile.getFileName());
                    break;
            }
        }

        public TimeInterval findTimeInterval() throws IOException {
            CSTable table = DataIO.table(dataFile.toFile(), "climate");

            dateFormat = table.getInfo().get(DataIO.DATE_FORMAT);
            dateFormatter = DateTimeFormatter.ofPattern(dateFormat);

            String startTimeString = table.getInfo().get(DataIO.DATE_START);
            String endTimeString = table.getInfo().get(DataIO.DATE_END);

            LocalDate startTime = LocalDate.parse(startTimeString, dateFormatter);
            LocalDate endTime = LocalDate.parse(endTimeString, dateFormatter);

            return new TimeInterval(startTime, endTime);
        }

        public void calculateRunStatus(Set<String> hruIdSet) throws IOException {
            calculateParameterMap();
            boolean shouldWrite = calculateWriteFlag(hruIdSet);
            runStatus = (shouldWrite ? RunStatus.WRITE : RunStatus.DO_NOTHING);
        }

        public boolean shouldRead() {
            return runStatus.shouldRead();
        }

        public void ensureRead() {
            if (RunStatus.DO_NOTHING == runStatus) {
                runStatus = RunStatus.READ;
            }
        }

        public boolean shouldWrite() {
            return runStatus.shouldWrite();
        }

        public void ensureWrite() {
            runStatus = RunStatus.WRITE;
        }

        public void initializeIO() throws IOException {
            if (shouldRead()) {
                switch (climateType) {
                    case MEAN_TEMPERATURE:
                        reader = new TmeanStationClimateReader();
                        break;
                    default:
                        reader = new StationClimateReader(dataFile, timeInterval.startTime, timeInterval.endTime, climateType);
                        break;
                }
            }
            if (shouldWrite()) {
                writer = new PrintWriter(Files.newBufferedWriter(regFile));
            }
        }

        public void readLine() throws IOException {
            reader.readData();
        }

        public void read(Context hruContext) {
            reader.setValue(hruContext);
        }

        public void writeHeader() {
            String climate = climateType.getName();
            boolean isTmean = ClimateType.MEAN_TEMPERATURE == climateType;

            writer.println("@T,regionalization");
            writer.println("name," + regFile.getFileName());
            writer.println("desc,regionalization of " + (isTmean ? climate : dataFile.getFileName()));
            writer.println("hrus," + hrus.size());
            writer.println("climate," + climate);
            writer.println(DataIO.KEY_CREATED_AT + ",\"" + new Date() + "\"");

            writer.println(DataIO.DATE_FORMAT + "," + dateFormat);
            writer.println(DataIO.DATE_START + "," + dateFormatter.format(timeInterval.startTime));
            writer.println(DataIO.DATE_END + "," + dateFormatter.format(timeInterval.endTime));

            for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
                writer.println(entry.getKey() + "," + entry.getValue());
            }

            writer.print("@H,time");
            for (HRU hru : hrus) {
                writer.print("," + hru.ID);
            }
            writer.println();
        }

        public void finishLine() {
            writer.println();
        }

        public void write(LocalDate time) {
            writer.print("," + dateFormatter.format(time));
        }

        public void write(Context hruContext) {
            double value = hruContext.get(climateName, double.class);
            writer.print("," + value);
        }

        public void closeReader() {
            reader.reset();
        }

        public void closeWriter() {
            writer.flush();
            writer.close();
        }

        private boolean calculateWriteFlag(Set<String> hruIdSet) throws IOException {
            CSTable regTable = getRegionalizationTable();
            if (regTable == null) {
                return true;
            }

            // compare HRU IDs with regionalized IDs to confirm equality
            if (!verifyHRUs(hruIdSet, regTable)) {
                return true;
            }

            Map<String, String> regInfo = regTable.getInfo();
            if (!verifyParameters(regInfo)) {
                return true;
            }

            return false;
        }

        private CSTable getRegionalizationTable() {
            try {
                return DataIO.table(regFile.toFile());
            } catch (Exception ex) {
                return null;
            }
        }

        private boolean verifyHRUs(Set<String> hruIdSet, CSTable regTable) {
            Set<String> regIds = new HashSet<>();
            for (int i = 2; i < regTable.getColumnCount() + 1; ++i) {
                regIds.add(regTable.getColumnName(i));
            }
            return hruIdSet.equals(regIds);
        }

        public void calculateParameterMap() throws IOException {
            Set<String> parameters = getParameterSet();
            parameterMap = new LinkedHashMap<>();

            for (String parameter : parameters) {
                String value;
                if ("hash".equals(parameter)) {
                    value = computeHash();
                } else {
                    value = parameterContext.get(parameter, String.class);
                }
                parameterMap.put(parameter, value);
            }
        }

        private Set<String> getParameterSet() {
            Set<String> parameters = new LinkedHashSet<>();
            String suffix = climateType.getSuffix();

            if (ClimateType.MEAN_TEMPERATURE != climateType) {
                parameters.add("hash");
            }

            parameters.add("elevationCorrection" + suffix);
            parameters.add("rsqThreshold" + suffix);
            parameters.add("nidw" + suffix);

            if (ClimateType.PRECIPITATION == climateType) {
                parameters.add("precipCorrectMethod");
                parameters.add("pIDW");
                parameters.add("tempNIDW");
                parameters.add("windNIDW");
                parameters.add("regThres");
                parameters.add("snow_trs");
                parameters.add("snow_trans");
            }

            return parameters;
        }

        private String computeHash() throws IOException {
            return FileHasher.sha256Hash(dataFile);
        }

        private boolean verifyParameters(Map<String, String> regParameters) {
            for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
                String regValue = regParameters.get(entry.getKey());
                if (regValue == null) {
                    return false;
                }
                if (!regValue.equals(entry.getValue())) {
                    return false;
                }
            }
            return true;
        }
    }
}
