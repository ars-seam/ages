
package io;

import gov.usda.jcf.core.Context;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * @author Nathan Lighthart
 */
public class EntityIDFilter implements Predicate<Context> {
    private static final int MAX_IDS = 20;
    private Set<Integer> ids;

    public static EntityIDFilter create(String idSet) {
        Set<Integer> ids = parseIDs(idSet);
        if (ids.isEmpty() || ids.size() > MAX_IDS) {
            return null;
        }
        return new EntityIDFilter(ids);
    }

    private static Set<Integer> parseIDs(String idSet) {
        if (idSet == null) {
            return new HashSet<>();
        } else {
            idSet = idSet.trim();
            if ("".equals(idSet) || "-".equals(idSet)) {
                return new HashSet<>();
            }
            String[] split = idSet.split("\\s*;\\s*");

            Set<Integer> ids = new HashSet<>();
            for (String id : split) {
                ids.add(Integer.parseInt(id));
            }
            return ids;
        }
    }

    private EntityIDFilter(Set<Integer> ids) {
        this.ids = Collections.unmodifiableSet(ids);
    }

    @Override
    public boolean test(Context context) {
        int ID = context.get("ID", int.class);
        return ids.contains(ID);
    }

}
