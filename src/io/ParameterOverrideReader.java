/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParameterOverrideReader {
    private static final Map<String, Class<?>> parameterMap = createParameterMap();

    private static Map<String, Class<?>> createParameterMap() {
        Map<String, Class<?>> map = new HashMap<>();

        // initialization
        map.put("initLPS", double.class);
        map.put("initMPS", double.class);
        map.put("FCAdaptation", double.class);
        map.put("ACAdaptation", double.class);

        // plant interception
        map.put("a_rain", double.class);
        map.put("a_snow", double.class);

        // soil water
        map.put("soilMaxDPS", double.class);
        map.put("soilPolRed", double.class);
        map.put("soilLinRed", double.class);
        map.put("soilMaxInfSummer", double.class);
        map.put("soilMaxInfWinter", double.class);
        map.put("soilMaxInfSnow", double.class);
        map.put("soilImpGT80", double.class);
        map.put("soilImpLT80", double.class);
        map.put("soilDistMPSLPS", double.class);
        map.put("soilDiffMPSLPS", double.class);
        map.put("soilOutLPS", double.class);
        map.put("soilLatVertLPS", double.class);
        map.put("soilMaxPerc", double.class);
        map.put("geoMaxPerc", double.class);
        map.put("kdiff_layer", double.class);
        map.put("BetaW", double.class);

        // soil nitrogen
        map.put("piadin", int.class);
        map.put("denitfac", double.class);
        map.put("Beta_min", double.class);
        map.put("Beta_trans", double.class);
        map.put("Beta_Ndist", double.class);
        map.put("Beta_NO3", double.class);
        map.put("Beta_rsd", double.class);
        map.put("deposition_factor", double.class);
        map.put("infil_conc_factor", double.class);

        // groundwater
        map.put("initRG1", double.class);
        map.put("initRG2", double.class);
        map.put("gwRG1RG2dist", double.class);
        map.put("gwRG1Fact", double.class);
        map.put("gwRG2Fact", double.class);
        map.put("gwCapRise", double.class);

        // groundwater N
        map.put("N_delay_RG1", double.class);
        map.put("N_delay_RG2", double.class);
        map.put("N_concRG1", double.class);
        map.put("N_concRG2", double.class);

        // crop and temperature
        map.put("LExCoef", double.class);
        map.put("rootfactor", double.class);
        map.put("temp_lag", double.class);

        // tile drainage
        map.put("drspac", double.class);
        map.put("drrad", double.class);
        map.put("depdr", double.class);

        return Collections.unmodifiableMap(map);
    }

    private Path hruOverrideFilePath;
    private CSTable table;
    private Map<Integer, Context> hruContexts;

    public ParameterOverrideReader(Path hruOverrideFilePath, Map<Integer, Context> hruContexts) {
        this.hruOverrideFilePath = hruOverrideFilePath;
        this.hruContexts = hruContexts;
    }

    public void read() throws IOException {
        System.out.println("--> Reading HRU parameter override ...");

        // retrieve table
        table = DataIO.table(hruOverrideFilePath.toFile());

        Iterable<String[]> rows = table.rows();
        for (String[] row : rows) {
            overrideRow(row);
        }
    }

    private void overrideRow(final String[] row) {
        // parse HRU ID
        int hruID;
        try {
            hruID = Integer.parseInt(row[1]);
        } catch (NumberFormatException ex) {
            System.err.println("Invalid HRU ID in overrid file: " + row[1]);
            return;
        } catch (IndexOutOfBoundsException ex) {
            System.err.println("Invalid line in override file");
            return;
        }

        // retrieve HRU
        Context hruContext = hruContexts.get(hruID);
        if (hruContext == null) {
            System.err.println("Invalid HRU ID in override file: " + hruID);
            return;
        }

        // set HRU values
        for (int i = 2; i < row.length; ++i) {
            String parameter = table.getColumnName(i);
            if (parameterMap.containsKey(parameter)) {
                String value = row[i];
                setValue(hruContext, parameter, value);
            } else {
                System.err.println("Invalid parameter name: " + parameter);
            }
        }
    }

    private void setValue(Context hruContext, String parameter, String value) {
        Class<?> parameterType = parameterMap.get(parameter);
        Object typedValue = TypeConversions.INSTANCE.convert(value, parameterType);
        try {
            hruContext.put(parameter, typedValue, parameterType);
        } catch (IllegalArgumentException ex) {
            System.err.println("Cannot modify hru context: " + ex.getMessage());
        }
    }
}
