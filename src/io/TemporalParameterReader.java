
package io;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Map;

public class TemporalParameterReader {
    private Path temporalParameterFilePath;
    private Context temporalContext;

    private ParameterTableReader reader;

    private int rowCount;

    public TemporalParameterReader(Path temporalParameterFilePath, Context temporalContext) {
        this.temporalParameterFilePath = temporalParameterFilePath;
        this.temporalContext = temporalContext;

        reader = null;
    }

    public void read() throws IOException {
        try {
            read0();
        } catch (Exception ex) {
            throw new IOException("Error reading temporal parameter file: " + temporalParameterFilePath, ex);
        }
    }

    private void read0() throws Exception {
        if (reader == null) {
            readFirstDay();
        } else {
            readNextDay();
        }
    }

    private void readFirstDay() throws Exception {
        reader = new ParameterTableReader(temporalParameterFilePath);

        if (!"date".equals(reader.getTable().getColumnName(1))) {
            throw new RuntimeException("Invalid additional file: date needs to be the first column");
        }

        LocalDate currentModelTime = getCurrentModelTime();
        LocalDate dataFileTime = null;
        rowCount = 1;
        Map<String, Object> data;
        do {
            data = reader.readNextLine();

            // retrieve actual time form data file
            LocalDate actualTime = getActualTime(data);
            if (dataFileTime == null) {
                // no previous time so it is the first

                // verify that actual time is before or equal to current model time
                if (actualTime.isAfter(currentModelTime)) {
                    throw new RuntimeException("Start date in file is after the model start date: "
                            + actualTime + " model time: " + currentModelTime);
                }

                dataFileTime = actualTime;
            } else {
                // previous time is known so verify that day is not skipped
                LocalDate expectedTime = dataFileTime.plusDays(1L);

                if (actualTime.equals(expectedTime)) {
                    // time in the data file is the next day as expected
                    dataFileTime = expectedTime;
                } else {
                    // time is not what is expected so malformed file
                    throw new RuntimeException(getUnexpectedTimeString(expectedTime, actualTime));
                }
            }

            ++rowCount;
        } while (!currentModelTime.equals(dataFileTime));

        // current model time found
        // so add to context
        readIntoContext(data);
    }

    private void readNextDay() throws Exception {
        LocalDate currentModelTime = getCurrentModelTime();

        Map<String, Object> data = reader.readNextLine();
        LocalDate actualTime = getActualTime(data);

        if (currentModelTime.equals(actualTime)) {
            // time is as expected so add data to context
            readIntoContext(data);
        } else {
            // time is not what is expected so malformed file
            throw new RuntimeException(getUnexpectedTimeString(currentModelTime, actualTime));
        }

        ++rowCount;
    }

    private LocalDate getCurrentModelTime() {
        return temporalContext.get("time", LocalDate.class);
    }

    private LocalDate getActualTime(Map<String, Object> data) {
        Object timeObject = data.get("date");
        if (timeObject == null) {
            throw new RuntimeException("Missing time on data row " + rowCount);
        }

        return TypeConversions.INSTANCE.convert(timeObject, LocalDate.class);
    }

    private void readIntoContext(Map<String, Object> data) {
        // Add the parameter values and metadata to the context
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            String parameterName = entry.getKey();

            // ID should be skipped as it is not a parameter
            if ("date".equals(parameterName)) {
                continue;
            }

            // Add the parameter to the context
            temporalContext.put(parameterName, entry.getValue(), reader.getType(parameterName));

            // Add the units to the context if it exists
            String units = reader.getUnits(parameterName);
            if (units != null) {
                temporalContext.putProperty(parameterName, "units", units);
            }
        }
    }

    private String getUnexpectedTimeString(LocalDate expectedTime, LocalDate actualTime) {
        return "Unexpected time occured on data row "
                + rowCount + ": " + actualTime + " Expected: " + expectedTime;
    }
}
