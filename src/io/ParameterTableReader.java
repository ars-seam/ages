/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io;

import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lib.ArrayNameParser;
import lib.ArrayValueParser;
import lib.TypeFinder;

public class ParameterTableReader {
    private CSTable table;

    // important table metadata
    private String dateFormat;
    private DateFormat dateFormatter;
    private DateTimeFormatter dateTimeFormatter;

    // variable information
    private Map<String, ParameterInformation> parameters;

    // iterator
    private Iterator<String[]> rowIterator;

    public ParameterTableReader(Path parameterFilePath) throws IOException {
        table = DataIO.table(parameterFilePath.toFile());

        parseHeader();

        rowIterator = table.rows().iterator();
    }

    public CSTable getTable() {
        return table;
    }

    public String getUnits(String parameterName) {
        ParameterInformation info = parameters.get(parameterName);
        return (info == null
                ? null
                : info.getUnits());
    }

    public Class<?> getType(String parameterName) {
        ParameterInformation info = parameters.get(parameterName);
        return (info == null
                ? null
                : info.getType());
    }

    public boolean hasNextLine() {
        return rowIterator.hasNext();
    }

    public Map<String, Object> readNextLine() throws IOException {
        String[] row = rowIterator.next();

        Map<String, Object> values = new HashMap<>();
        for (Map.Entry<String, ParameterInformation> parameter : parameters.entrySet()) {
            Object value;
            try {
                value = parameter.getValue().read(row);
            } catch (RuntimeException ex) {
                throw new IOException("Error reading value for " + parameter.getKey(), ex);
            }
            values.put(parameter.getKey(), value);
        }

        return values;
    }

    private void parseHeader() throws IOException {
        Map<String, String> metadata = table.getInfo();
        dateFormat = metadata.get(DataIO.DATE_FORMAT);

        if (dateFormat == null) {
            dateFormatter = new SimpleDateFormat();
            dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
        } else {
            dateFormatter = new SimpleDateFormat(dateFormat);
            dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
        }

        parseParameterInformation();
    }

    private void parseParameterInformation() throws IOException {
        // OMS table does not include first column in the count
        final int totalColumns = table.getColumnCount() + 1;

        parameters = new HashMap<>();

        for (int i = 1; i < totalColumns; ++i) {
            String variableName = getParameterName(i);
            if (variableName == null) {
                continue;
            }

            ParameterInformation info = new ParameterInformation(
                    variableName, i, table.getColumnInfo(i));

            ParameterInformation existing = parameters.get(variableName);
            if (existing == null) {
                // no existing information so new variable
                parameters.put(variableName, info);
            } else {
                // existing variable so this represents an array
                try {
                    existing.merge(info);
                } catch (IllegalArgumentException ex) {
                    throw new IOException("Duplicate variable name does not meet the criteria for an array", ex);
                }
            }
        }
    }

    private String getParameterName(int columnIndex) {
        String variableName = table.getColumnName(columnIndex);
        if (variableName == null) {
            return null;
        }

        // remove extra whitespace
        variableName = variableName.trim();

        // determine the true name in cases where it is an array
        // I.E. orun[1] -> orun
        if (variableName.endsWith("]")) {
            // potential array need to find true name
            ArrayNameParser parser = ArrayNameParser.minimalParser();
            List<String> arrayNames = parser.parse(variableName);
            if (arrayNames != null) {
                // name is a valid array
                variableName = arrayNames.get(0);
            }
        }

        return variableName;
    }

    private class ParameterInformation {
        private final String name;
        private String units;
        // represents type field as specified by the file
        // if multiple columns this is the component type for array
        // otherwise this the actual type
        private Class<?> type;
        // represents the actual data type returned by a read
        // if multiple columns this is an array of variables of "type"
        // otherwise this is identical to "type"
        private Class<?> fullType;

        private final List<Integer> columnIndices;

        public ParameterInformation(String name, int columnIndex, Map<String, String> properties) {
            this.name = name;
            columnIndices = new ArrayList<>();
            columnIndices.add(columnIndex);

            parseUnitInformation(properties);
            parseTypeInformation(properties);
        }

        public String getName() {
            return name;
        }

        public String getUnits() {
            return units;
        }

        public Class<?> getType() {
            return fullType;
        }

        private void merge(ParameterInformation info) {
            if (!name.equals(info.name)) {
                throw new IllegalArgumentException("Cannot merge variables of different names");
            }
            if (!Objects.equals(type, info.type)) {
                throw new IllegalArgumentException("Same variable cannot have different types");
            }

            columnIndices.addAll(info.columnIndices);
        }

        public Object read(String[] row) throws IOException {
            if (type == null) {
                deduceType(row[columnIndices.get(0)]);
            }

            if (columnIndices.size() == 1) {
                fullType = type;
                return convert(row[columnIndices.get(0)]);
            } else {
                final int length = columnIndices.size();
                Object array = Array.newInstance(type, length);
                fullType = array.getClass();
                for (int i = 0; i < length; ++i) {
                    Object value = convert(row[columnIndices.get(i)]);
                    Array.set(array, i, value);
                }
                return array;
            }
        }

        private void parseUnitInformation(Map<String, String> properties) {
            units = properties.get(DataIO.KEY_UNIT);
            if (units == null) {
                units = properties.get("units");
            }

            if ("".equals(units) || "-".equals(units)) {
                units = null;
            }
        }

        private void parseTypeInformation(Map<String, String> properties) {
            String typeName = properties.get(DataIO.KEY_TYPE);
            type = TypeFinder.findType(typeName);
        }

        private void deduceType(String value) {
            // try to parse as double
            // if successful type is a double otherwise it is a string
            try {
                Double.parseDouble(value);
                type = double.class;
            } catch (NumberFormatException | NullPointerException ex) {
                type = String.class;
            }
        }

        private Object convert(String s) throws IOException {
            return convert(s, type);
        }

        private Object convert(String s, Class<?> clazz) throws IOException {
            try {
                return convert0(s, clazz);
            } catch (Exception ex) {
                throw new IOException("Error parsing value for parameter " + name, ex);
            }
        }

        private Object convert0(String s, Class<?> clazz) throws Exception {
            if (clazz.isArray()) {
                return ArrayValueParser.parseArray(s, clazz);
            } else if (Date.class == clazz) {
                return parseDate(s);
            } else if (Calendar.class == clazz) {
                return parseCalendar(s);
            } else if (Instant.class == clazz) {
                return parseInstant(s);
            } else if (ZonedDateTime.class == clazz) {
                return parseZonedDateTime(s);
            } else if (OffsetDateTime.class == clazz) {
                return parseOffsetDateTime(s);
            } else if (LocalDateTime.class == clazz) {
                return parseLocalDateTime(s);
            } else if (LocalDate.class == clazz) {
                return parseLocalDate(s);
            } else if (LocalTime.class == clazz) {
                return parseLocalTime(s);
            } else {
                return TypeConversions.INSTANCE.convert(s, clazz);
            }
        }

        private Date parseDate(String s) throws ParseException {
            return dateFormatter.parse(s);
        }

        private Calendar parseCalendar(String s) throws ParseException {
            Date d = parseDate(s);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return c;
        }

        private Instant parseInstant(String s) {
            return dateTimeFormatter.parse(s, Instant::from);
        }

        private ZonedDateTime parseZonedDateTime(String s) {
            return dateTimeFormatter.parse(s, ZonedDateTime::from);
        }

        private OffsetDateTime parseOffsetDateTime(String s) {
            return dateTimeFormatter.parse(s, OffsetDateTime::from);
        }

        private LocalDateTime parseLocalDateTime(String s) {
            return dateTimeFormatter.parse(s, LocalDateTime::from);
        }

        private LocalDate parseLocalDate(String s) {
            return dateTimeFormatter.parse(s, LocalDate::from);
        }

        private LocalTime parseLocalTime(String s) {
            return dateTimeFormatter.parse(s, LocalTime::from);
        }
    }
}
