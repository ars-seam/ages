
package io;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SpatialParameterReader {
    private Path spatialParameterFilePath;
    private Map<Integer, Context> spatialContexts;

    public SpatialParameterReader(Path spatialParamterFilePath, Map<Integer, Context> spatialContexts) {
        this.spatialParameterFilePath = spatialParamterFilePath;
        this.spatialContexts = spatialContexts;
    }

    public void read() throws IOException {
        try {
            read0();
        } catch (Exception ex) {
            throw new IOException("Error reading spatial parameter file: " + spatialParameterFilePath, ex);
        }
    }

    private void read0() throws Exception {
        ParameterTableReader reader = new ParameterTableReader(spatialParameterFilePath);

        // Check that first column is ID
        if (!"ID".equals(reader.getTable().getColumnName(1))) {
            throw new RuntimeException("Invalid additional file: ID needs to be the first column");
        }

        // Read each line tracking which ID have been seen
        Set<Integer> idSet = new HashSet<>();
        int rowCount = 1;
        while (reader.hasNextLine()) {
            Map<String, Object> data = reader.readNextLine();

            // Make sure ID object exists
            Object idObject = data.get("ID");
            if (idObject == null) {
                throw new RuntimeException("Missing ID on data row " + rowCount);
            }

            // Make sure ID is an int
            int id = TypeConversions.INSTANCE.convert(idObject, int.class);

            // Check if ID is a duplicate
            if (!idSet.add(id)) {
                throw new RuntimeException("Duplicate id: " + id);
            }

            // Retrieve the context associated with the ID
            Context spatialContext = spatialContexts.get(id);
            if (spatialContext == null) {
                throw new RuntimeException("Unknown id: " + id);
            }

            // Add the parameter values and metadata to the context
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                String parameterName = entry.getKey();

                // ID should be skipped as it is not a parameter
                if ("ID".equals(parameterName)) {
                    continue;
                }

                // Add the parameter to the context
                spatialContext.put(parameterName, entry.getValue(), reader.getType(parameterName));

                // Add the units to the context if it exists
                String units = reader.getUnits(parameterName);
                if (units != null) {
                    spatialContext.putProperty(parameterName, "units", units);
                }
            }

            ++rowCount;
        }

        // verify that id sets are equal
        // this makes sure that all entities got all parameters
        if (!idSet.equals(spatialContexts.keySet())) {
            throw new RuntimeException("Parameter file ID set does not match spatial ID set");
        }
    }
}
