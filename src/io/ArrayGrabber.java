/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import java.time.LocalDate;

public class ArrayGrabber {
    public String tempRes;
    public LocalDate time;
    public double[] extRadArray;
    public double[] LAIArray;
    public double[] effHArray;
    public double[] rsc0Array;
    public double[] slAsCfArray;
    public double extRad;
    public double actLAI;
    public double actEffH;
    public double actRsc0;
    public double actSlAsCf;

    public void execute() {
        int monthIndex = time.getMonthValue() - 1;
        int dayIndex = time.getDayOfYear() - 1;

        actRsc0 = rsc0Array[monthIndex];

        if (tempRes.equals("d")) {
            actLAI = LAIArray[dayIndex];
            actEffH = effHArray[dayIndex];
            extRad = extRadArray[dayIndex];
            actSlAsCf = slAsCfArray[dayIndex];
        } else if (tempRes.equals("h")) {
            int hour = 24 * dayIndex;
            actLAI = LAIArray[dayIndex];
            actEffH = effHArray[dayIndex];
            extRad = extRadArray[hour];
            actSlAsCf = slAsCfArray[dayIndex];
        }
    }
}
