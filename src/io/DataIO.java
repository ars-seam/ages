/*
 * $Id$
 *
 * This file is part of the Object Modeling System (OMS),
 * 2007-2012, Olaf David and others, Colorado State University.
 *
 * OMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 2.1.
 *
 * OMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with OMS.  If not, see <http://www.gnu.org/licenses/lgpl.txt>.
 */
package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Data Input/Output management.
 *
 * @author od
 */
public class DataIO {

    private static final String P = "@";
    public static final String TABLE = P + "T";
    public static final String HEADER = P + "H";
    public static final String PROPERTIES = P + "S";
    public static final String PROPERTY = P + "P";
    public static final String TABLE1 = P + "Table";
    public static final String HEADER1 = P + "Header";
    public static final String PROPERTIES1 = P + "Properties";
    public static final String PROPERTY1 = P + "Property";
    //
    //
    public static final String CSPROPERTIES_EXT = "csp";
    public static final String CSTABLE_EXT = "cst";
    //
    private static final String ROOT_ANN = "___root___";
    private static final String COMMENT = "#";
    private static final Pattern varPattern = Pattern.compile("\\$\\{([^$}]+)\\}");
    /*
     * some static helpers, might have to go somewhere else
     */
    private static final String ISO8601 = "yyyy-MM-dd'T'hh:mm:ss";
    //
    // all meta data keys
    public static final String KEY_CONVERTED_FROM = "converted_from";
    public static final String DATE_FORMAT = "date_format";
    public static final String DATE_START = "date_start";
    public static final String DATE_END = "date_end";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_MODIFIED_AT = "modifed_at";
    public static final String KEY_CREATED_BY = "created_by";
    public static final String KEY_UNIT = "unit";
    public static final String KEY_FORMAT = "format";
    public static final String KEY_TYPE = "type";
    public static final String KEY_NAME = "name";
    public static final String KEY_MISSING_VAL = "missing_value";
    public static final String KEY_FC_START = "forecast_start";
    public static final String KEY_FC_DAYS = "forecast_days";
    public static final String KEY_HIST_YEAR = "historical_year";
    public static final String KEY_HIST_YEARS = "historical_years";
    public static final String KEY_ESP_DATES = "esp_dates";
    public static final String KEY_DIGEST = "digest";
    public static final String VAL_DATE = "Date";
    //
    //TimeStep Enumerations
    public static final int DAILY = 0;
    public static final int MEAN_MONTHLY = 1;
    public static final int MONTHLY_MEAN = 2;
    public static final int ANNUAL_MEAN = 3;
    public static final int PERIOD_MEAN = 4;
    public static final int PERIOD_MEDIAN = 5;
    public static final int PERIOD_STANDARD_DEVIATION = 6;
    public static final int PERIOD_MIN = 7;
    public static final int PERIOD_MAX = 8;
    public static final int RAW = 9;
    public static final int TIME_STEP = 10;

    /**
     * Get a column as an int array.
     *
     * @param t
     * @param columnName
     * @return the column data as doubles.
     */
    public static double[] getColumnValues(CSTable t, String columnName) {
        int col = columnIndex(t, columnName);
        if (col == -1) {
            throw new IllegalArgumentException("No such column: " + columnName);
        }
        List<Double> l = new ArrayList<Double>();
        for (String[] s : t.rows()) {
            l.add(new Double(s[col]));
        }
        double[] f = new double[l.size()];
        for (int i = 0; i < f.length; i++) {
            f[i] = l.get(i);
        }
        return f;
    }

    /**
     * Parse properties from a reader
     *
     * @param r the Reader
     * @param name the name of the properties
     * @return properties from a file.
     * @throws java.io.IOException
     */
    public static CSProperties properties(Reader r, String name) throws IOException {
        return new CSVProperties(r, name);
    }

    public static CSProperties properties(File r, String name) throws IOException {
        return new CSVProperties(new FileReader(r), name);
    }

    /**
     * Create a CSProperty from an array of reader.
     *
     * @param r
     * @param name
     * @return merged properties.
     * @throws java.io.IOException
     */
    public static CSProperties properties(Reader[] r, String name) throws IOException {
        CSVProperties p = new CSVProperties(r[0], name);
        for (int i = 1; i < r.length; i++) {
            CSVParser csv = new CSVParser(r[i], CSVStrategy.DEFAULT_STRATEGY);
            locate(csv, name, PROPERTIES, PROPERTIES1);
            p.readProps(csv);
            r[i].close();
        }
        return p;
    }

    /**
     * Convert CSProperties into Properties
     *
     * @param p
     * @return the Properties.
     */
    public static Properties properties(CSProperties p) {
        Properties pr = new Properties();
        pr.putAll(p);
        return pr;
    }

    /**
     * Convert Properties to CSProperties.
     *
     * @param p the Properties
     * @return CSProperties
     */
    public static CSProperties properties(Properties p) {
        return new BasicCSProperties(p);
    }

    /**
     * Convert from a Map to properties.
     *
     * @param p the source map
     * @return CSProperties
     */
    public static CSProperties properties(Map<String, Object> p) {
        return new BasicCSProperties(p);
    }

    /**
     * Create Empty properties
     *
     * @return get some empty properties.
     */
    public static CSProperties properties() {
        return new BasicCSProperties();
    }

    /**
     * Parse the first table from a file
     *
     * @param file the file to parse
     * @return the CSTable
     * @throws IOException
     */
    public static CSTable table(File file) throws IOException {
        return table(file, null);
    }

    /**
     * Parse a table from a given File.
     *
     * @param file
     * @param name
     * @return a CSTable.
     * @throws java.io.IOException
     */
    public static CSTable table(File file, String name) throws IOException {
        return new FileTable(file, name);
    }

    /**
     * Gets a column index by name
     *
     * @param table The table to check
     * @param name the column name
     * @return the index of the column
     */
    public static int columnIndex(CSTable table, String name) {
        for (int i = 1; i <= table.getColumnCount(); i++) {
            if (table.getColumnName(i).equals(name)) {
                return i;
            }
        }
        return -1;
    }

    /////////////////////////////////////////////////////////////////////////////
    /// private
    private static String locate(CSVParser csv, String name, String... type) throws IOException {
        if (name == null) {
            // match anything
            name = ".+";
        }
        Pattern p = Pattern.compile(name);
        String[] line = null;
        while ((line = csv.getLine()) != null) {
            if (line[0].startsWith(COMMENT) || !line[0].startsWith(P)) {
//            if (line.length != 2 || line[0].startsWith(COMMENT) || !line[0].startsWith(P)) {
                continue;
            }
            for (String s : type) {
                if (line[0].equalsIgnoreCase(s) && p.matcher(line[1].trim()).matches()) {
                    return line[1];
                }
            }
        }
        throw new IllegalArgumentException("Not found : " + Arrays.toString(type) + ", " + name);
    }

//    @SuppressWarnings("serial")
    private static class BasicCSProperties extends LinkedHashMap<String, Object> implements CSProperties {

        private static final long serialVersionUID = 1L;

        String name = "";
        Map<String, Map<String, String>> info = new HashMap<String, Map<String, String>>();

        BasicCSProperties(Properties p) {
            this();
            for (Object key : p.keySet()) {
                put(key.toString(), p.getProperty(key.toString()));
            }
        }

        BasicCSProperties(Map<String, Object> p) {
            this();
            for (String key : p.keySet()) {
                put(key, p.get(key));
            }
        }

        BasicCSProperties() {
            info.put(ROOT_ANN, new LinkedHashMap<String, String>());
        }

        @Override
        public void putAll(CSProperties p) {
            for (String key : p.keySet()) {
                if (containsKey(key)) {
                    throw new IllegalArgumentException("Duplicate key in parameter sets: " + key);
                }
            }
            super.putAll(p);
            for (String s : p.keySet()) {
                Map<String, String> m = p.getInfo(s);
                setInfo(s, m);
            }
            getInfo().putAll(p.getInfo());
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public synchronized Map<String, String> getInfo(String property) {
            Map<String, String> im = info.get(property);
//            return (im == null) ? NOINFO : im;
            if (im == null) {
                im = new HashMap<String, String>();
                info.put(property, im);
            }
            return im;
        }

        @Override
        public Map<String, String> getInfo() {
            return getInfo(ROOT_ANN);
        }

        @Override
        public void setInfo(String propertyname, Map<String, String> inf) {
            info.put(propertyname, inf);
        }

//        @Override
//        public String get(Object key) {
//            Object val = super.get(key.toString());
//            return val.toString();
////            return resolve(val != null ? val.toString() : null);
//        }
//
        @Override
        public Object get(Object key) {
            Object val = super.get(key);
            if (val != null && val.getClass() == String.class) {
                return resolve((String) val);
            }
            return val;
        }

        /**
         * Resolve variable substitution.
         *
         * @P, dir, "/tmp/input"
         * @P, file, "${dir}/test.txt"
         *
         * - The referenced key has to be in the same properties set. - there
         * could be a chain of references, however, no recursion testing is
         * implemented.
         *
         * @param str
         * @return
         */
        private String resolve(String str) {
            if (str != null && str.contains("${")) {
                Matcher ma = null;
                while ((ma = varPattern.matcher(str)).find()) {
                    String key = ma.group(1);
                    String val = (String) get(key);
                    if (val == null) {
                        throw new IllegalArgumentException("value substitution failed for " + key);
                    }
                    Pattern repl = Pattern.compile("\\$\\{" + key + "\\}");
                    str = repl.matcher(str).replaceAll(val);
                }
            }
            return str;
        }
    }

    /**
     * Note: to keep the order of properties, it is sub-classed from
     * LinkedHashMap
     */
    @SuppressWarnings("serial")
    private static class CSVProperties extends BasicCSProperties implements CSProperties {

        CSVProperties(Reader reader, String name) throws IOException {
            super();
            CSVParser csv = new CSVParser(reader, CSVStrategy.DEFAULT_STRATEGY);
            this.name = locate(csv, name, PROPERTIES, PROPERTIES1);
            readProps(csv);
            reader.close();
        }

        private void readProps(CSVParser csv) throws IOException {
            Map<String, String> propInfo = null;
            String[] line = null;
            String propKey = ROOT_ANN;
            while ((line = csv.getLine()) != null
                    && !line[0].equalsIgnoreCase(PROPERTIES)
                    && !line[0].equalsIgnoreCase(PROPERTIES1)
                    && !line[0].equalsIgnoreCase(TABLE)
                    && !line[0].equalsIgnoreCase(TABLE1)) {
                if (line[0].startsWith(COMMENT) || line[0].isEmpty()) {
                    continue;
                }
                if (line[0].equalsIgnoreCase(PROPERTY) || line[0].equalsIgnoreCase(PROPERTY1)) {
                    if (line.length < 2) {
                        throw new IOException("Expected property name in line " + csv.getLineNumber());
                    }
                    propKey = line[1];
                    // maybe there is no value for the property, so we add null
                    put(propKey, (line.length > 2) ? line[2] : null);
                    propInfo = null;
                } else {
                    if (propInfo == null) {
                        info.put(propKey, propInfo = new HashMap<String, String>());
                    }
                    propInfo.put(line[0], (line.length > 1) ? line[1] : null);
                }
            }
        }
    }

    /**
     * CSVTable implementation
     */
    private static abstract class CSVTable implements CSTable {

        Map<Integer, Map<String, String>> info = new HashMap<>();
        String name;
        int colCount;
        String columnNames[];
        int firstline;
        static final CSVStrategy strategy = CSVStrategy.DEFAULT_STRATEGY;

        protected abstract Reader newReader();

        protected void init(String tableName) throws IOException {
            Reader r = newReader();
            BufferedReader br = new BufferedReader(r);
            if (r == null) {
                throw new NullPointerException("reader");
            }
            CSVParser csv = new CSVParser(br, strategy);
            name = locate(csv, tableName, TABLE, TABLE1);
            firstline = readTableHeader(csv);
            br.close();
        }

        private void skip0(BufferedReader csv, int lines) {
            try {
                while (lines-- > 0) {
                    csv.readLine();
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        private ThreadLocal<String[]> tempArray = new ThreadLocal<>();

        public String[] tokenize(String string, char delimiter) {
            String[] temp = tempArray.get();
            int tempLength = (string.length() / 2) + 2;
            if (temp == null || temp.length < tempLength) {
                temp = new String[tempLength];
                tempArray.set(temp);
            }
            int wordCount = 0;
            int i = 0;
            int j = string.indexOf(delimiter);
            while (j >= 0) {
                temp[wordCount++] = string.substring(i, j).trim();
                i = j + 1;
                j = string.indexOf(delimiter, i);
            }
            temp[wordCount++] = string.substring(i).trim();
            String[] result = new String[wordCount];
            System.arraycopy(temp, 0, result, 0, wordCount);
            return result;
        }

        private String[] readRow(BufferedReader csv) {
            try {
                String s = csv.readLine();
                if (s == null) {
                    return null;
                }
                while (s.trim().startsWith("#")) {
                    s = csv.readLine();
                    if (s == null) {
                        return null;
                    }
                }
                return tokenize(s.trim(), ',');

//                String s = csv.readLine();
//                return (s == null) ? null : tokenize(s.trim(), ',');
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        /**
         * Gets a row iterator.
         *
         * @return
         */
        @Override
        public Iterable<String[]> rows() {
            return rows(0);
        }

        /**
         * Gets a row iterator that starts at a give row.
         *
         * @param startRow the row to start parsing.
         * @return
         */
        @Override
        public Iterable<String[]> rows(final int startRow) {
            if (startRow < 0) {
                throw new IllegalArgumentException("startRow<0 :" + startRow);
            }

            return new Iterable<String[]>() {
                @Override
                public TableIterator<String[]> iterator() {
                    final Reader r = newReader();
                    if (r == null) {
                        throw new NullPointerException("reader");
                    }
                    final BufferedReader csv = new BufferedReader(r, 4096 * 4);

                    skip0(csv, firstline);
                    skip0(csv, startRow);

                    return new TableIterator<String[]>() {
                        String[] line = readRow(csv);
                        int row = startRow;

                        @Override
                        public void close() {
                            try {
                                csv.close();
                            } catch (IOException E) {
                            }
                        }

                        @Override
                        public boolean hasNext() {
                            boolean hn = (line != null && line.length > 1 && line[0].isEmpty());
                            if (!hn) {
                                close();
                            }
                            return hn;
                        }

                        @Override
                        public String[] next() {
                            String[] s = line;
                            s[0] = Integer.toString(++row);
                            line = readRow(csv);
                            return s;
                        }

                        @Override
                        public void remove() {
                            throw new UnsupportedOperationException();
                        }

                        @Override
                        public void skip(int n) {
                            if (n < 1) {
                                throw new IllegalArgumentException("n<1 : " + n);
                            }
                            skip0(csv, n - 1);
                            line = readRow(csv);
                            row += n;
                        }

                        @Override
                        protected void finalize() throws Throwable {
                            try {
                                close();
                            } finally {
                                super.finalize();
                            }
                        }
                    };
                }
            };
        }

        private int readTableHeader(CSVParser csv) throws IOException {
            Map<String, String> tableInfo = new LinkedHashMap<String, String>();
            info.put(-1, tableInfo);
            String[] line = null;
            while ((line = csv.getLine()) != null && !line[0].equalsIgnoreCase(HEADER)) {
                if (line[0].startsWith(COMMENT)) {
                    continue;
                }
                tableInfo.put(line[0], line.length > 1 ? line[1] : null);
            }
            if (line == null) {
                throw new IOException("Invalid table structure.");
            }
            colCount = line.length - 1;
            columnNames = new String[line.length];
            columnNames[0] = "ROW";
            for (int i = 1; i < line.length; i++) {
                columnNames[i] = line[i];
                info.put(i, new LinkedHashMap<String, String>());
            }
            while ((line = csv.getLine()) != null && !line[0].isEmpty()) {
                // avoid running into the next element.
                if (line[0].startsWith("@")) {
                    break;
                }
                if (line[0].startsWith(COMMENT)) {
                    continue;
                }
                for (int i = 1; i < line.length; i++) {
                    info.get(i).put(line[0], line[i]);
                }
            }
            assert (line != null && line[0].isEmpty());
            return csv.getLineNumber() - 1;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Map<String, String> getInfo() {
            return getColumnInfo(-1);
        }

        @Override
        public Map<String, String> getColumnInfo(int column) {
            return Collections.unmodifiableMap(info.get(column));
        }

        @Override
        public int getColumnCount() {
            return colCount;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }
    }

    private static class FileTable extends CSVTable {

        File file;

        FileTable(File f, String name) throws IOException {
            this.file = f;
            init(name);
        }

        @Override
        protected Reader newReader() {
            try {
                return new FileReader(file);
            } catch (FileNotFoundException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
