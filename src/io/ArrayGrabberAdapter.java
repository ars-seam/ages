/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import ages.types.Landuse;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;

@Description("Add ArrayGrabber module definition here")
@Author(name = "Olaf David, Peter Krause, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("I/O")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/io/ArrayGrabber.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/io/ArrayGrabber.xml")
public class ArrayGrabberAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Description("Current Time")
    @Input public LocalDate time;

    @Description("extraterrestric radiation of each time step of the year")
    @Units("MJ/m2 timeUnit")
    @Input public double[] extRadArray;

    @Description("Leaf Area Index Array")
    @Input public double[] LAIArray;

    @Description("Effective Height Array")
    @Input public double[] effHArray;

    @Description("landuse")
    @Input public Landuse landuse;

    @Description("Slope Ascpect Correction Factor Array")
    @Input public double[] slAsCfArray;

    @Description("daily extraterrestic radiation")
    @Units("MJ/m2/day")
    @Output public double extRad;

    @Description("LAI")
    @Output public double actLAI;

    @Description("effective height")
    @Output public double actEffH;

    @Description("state variable rsc0")
    @Output public double actRsc0;

    @Description("state var slope-aspect-correction-factor")
    @Output public double actSlAsCf;

    @Override
    protected void run(Context context) {
        ArrayGrabber component = new ArrayGrabber();

        component.tempRes = tempRes;
        component.time = time;
        component.extRadArray = extRadArray;
        component.LAIArray = LAIArray;
        component.effHArray = effHArray;
        component.rsc0Array = landuse.RSC0;
        component.slAsCfArray = slAsCfArray;

        component.execute();

        extRad = component.extRad;
        actLAI = component.actLAI;
        actEffH = component.actEffH;
        actRsc0 = component.actRsc0;
        actSlAsCf = component.actSlAsCf;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
