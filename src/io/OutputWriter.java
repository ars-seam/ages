/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import lib.ArrayNameParser;

public class OutputWriter {
    private static final DateTimeFormatter TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz yyyy");
    private final DecimalFormat numberFormat = new DecimalFormat("0.#####");
    private final DecimalFormat engNumberFormat = new DecimalFormat("0.#####E0");
    private final ArrayNameParser arrayParser;

    private Path outFilePath;
    private List<String> attributes;

    private boolean initialized;
    private boolean disabled;
    private PrintWriter primaryWriter;
    private Predicate<Context> filter;
    private String splitAttribute;
    private Map<Object, PrintWriter> splitWriters;

    private Context currentContext;

    public OutputWriter(Path outFilePath, String attributes) {
        this.outFilePath = outFilePath;
        this.attributes = parseAttributes(attributes);
        this.splitWriters = new HashMap<>();
        arrayParser = ArrayNameParser.minimalParser();

        initialized = false;
        disabled = false;
        primaryWriter = null;
        filter = null;
    }

    private static List<String> parseAttributes(String attributeSet) {
        if (attributeSet == null) {
            return new ArrayList<>();
        } else {
            attributeSet = attributeSet.trim();
            if ("".equals(attributeSet) || "-".equals(attributeSet)) {
                return new ArrayList<>();
            }
            String[] split = attributeSet.split("\\s*;\\s*");
            return new ArrayList<>(Arrays.asList(split));
        }
    }

    private static String format(Object object, DecimalFormat decimalFormat) {
        if (object == null) {
            return "null";
        } else if (object instanceof Number) {
            // format numbers
            if (object instanceof Double) {
                double d = (Double) object;
                // use toString method of Double for NaN and +/- Infinity
                if (!Double.isFinite(d)) {
                    return Double.toString(d);
                }
            } else if (object instanceof Float) {
                float f = (Float) object;
                // use toString method of Float for NaN and +/- Infinity
                if (!Float.isFinite(f)) {
                    return Float.toString(f);
                }
            }
            // use decimal format on finite numbers
            return decimalFormat.format(object);
        } else if (object.getClass().isArray()) {
            final int length = Array.getLength(object);
            if (length == 0) {
                return "[]";
            }

            // recursively call format for array elements
            StringBuilder str = new StringBuilder();
            str.append('[').append(format(Array.get(object, 0), decimalFormat));
            for (int i = 1; i < length; ++i) {
                str.append(',').append(format(Array.get(object, i), decimalFormat));
            }
            str.append(']');
            return str.toString();
        }
        return TypeConversions.INSTANCE.convert(object, String.class);
    }

    private static Class<?> getType(Object object) {
        return object == null ? Object.class : object.getClass();
    }

    private static Class<?> getBaseType(Object object) {
        if (object == null) {
            return Object.class;
        }

        Class<?> clazz = object.getClass();
        while (clazz.isArray()) {
            clazz = clazz.getComponentType();
        }
        return clazz;
    }

    public void setFilter(Predicate<Context> filter) {
        this.filter = filter;
    }

    public void setSplitAttribute(String splitAttribute) {
        this.splitAttribute = splitAttribute;
    }

    public void write(Context context) throws IOException {
        this.currentContext = context;
        if (!initialized) {
            disabled = isDisabled();
            if (!disabled) {
                initialize();
            }
            initialized = true;
        }
        if (disabled) {
            return;
        }

        if (filter == null || filter.test(context)) {
            printValues(primaryWriter);

            if (splitAttribute != null) {
                printValues(getSplitWriter());
            }
        }
    }

    public void close() {
        if (primaryWriter != null) {
            primaryWriter.flush();
            primaryWriter.close();
        }
        for (PrintWriter writer : splitWriters.values()) {
            writer.flush();
            writer.close();
        }
    }

    private boolean isDisabled() {
        return attributes.isEmpty() || "-".equals(outFilePath.getFileName().toString());
    }

    private void initialize() throws IOException {
        primaryWriter = new PrintWriter(Files.newBufferedWriter(outFilePath));

        // add implicit time to attributes
        attributes.add(0, "time");

        printHeader(primaryWriter);
    }

    private void printHeader(PrintWriter writer) {
        writer.println("@T, output");
        writer.println(DataIO.KEY_CREATED_AT + ",\"" + TIMESTAMP_FORMAT.format(ZonedDateTime.now()) + "\"");
        writer.println(DataIO.DATE_FORMAT + ",yyyy-MM-dd");
        String digest = System.getProperty("oms3.digest");
        if (digest != null) {
            writer.println("Digest," + digest);
        }

        StringBuilder header = new StringBuilder("@H");
        StringBuilder types = new StringBuilder("Type");
        StringBuilder units = new StringBuilder("Unit");

        for (String attribute : attributes) {
            Object value = getValue(attribute);
            String type = getType(value).getSimpleName();
            String unit = getUnit(attribute);

            header.append(",").append(attribute);
            types.append(",").append(type);
            units.append(",").append(unit);
        }

        writer.println(header.toString());
        writer.println(types.toString());
        writer.println(units.toString());
    }

    private void printValues(PrintWriter writer) throws IOException {
        StringBuilder values = new StringBuilder();

        for (String attribute : attributes) {
            Object value = getValue(attribute);

            String output;
            if (isEngFormat(attribute)) {
                output = engFormat(value);
            } else {
                output = format(value);
            }
            if (output.contains(",")) {
                output = "\"" + output + "\"";
            }
            values.append(",").append(output);
        }

        writer.println(values.toString());
    }

    private Object getValue(String attribute) {
        List<String> arrayElements = arrayParser.parse(attribute);
        if (arrayElements == null) {
            return currentContext.get(attribute);
        } else {
            Object value = currentContext.get(arrayElements.get(0));
            if (value == null) {
                return null;
            }
            for (int i = 1; i < arrayElements.size(); ++i) {
                int index = Integer.parseInt(arrayElements.get(i));
                Object indexValue;
                if (index < Array.getLength(value)) {
                    indexValue = Array.get(value, index);
                } else {
                    indexValue = null;
                }
                if (indexValue == null) {
                    Class<?> componentType = value.getClass().getComponentType();
                    if (componentType == double.class || componentType == Double.class) {
                        return Double.NaN;
                    } else if (componentType == float.class || componentType == Float.class) {
                        return Float.NaN;
                    } else {
                        return null;
                    }
                } else {
                    value = indexValue;
                }
            }
            return value;
        }
    }

    private String getUnit(String attribute) {
        String name;
        List<String> arrayElements = arrayParser.parse(attribute);
        if (arrayElements == null) {
            name = attribute;
        } else {
            name = arrayElements.get(0);
        }

        if (currentContext.containsProperty(name, "units")) {
            return currentContext.getProperty(name, "units");
        } else {
            return "-";
        }
    }

    private boolean isEngFormat(String attribute) {
        return "balance".equals(attribute);
    }

    private String format(Object val) {
        return format(val, numberFormat);
    }

    private String engFormat(Object val) {
        return format(val, engNumberFormat);
    }

    private PrintWriter getSplitWriter() throws IOException {
        Object value = getValue(splitAttribute);
        PrintWriter writer = splitWriters.get(value);
        if (writer == null) {
            writer = createSplitWriter(value);
            splitWriters.put(value, writer);
        }
        return writer;
    }

    private PrintWriter createSplitWriter(Object value) throws IOException {
        // get path
        Path splitPath = getSplitPath(value);

        // create writer
        PrintWriter writer = new PrintWriter(Files.newBufferedWriter(splitPath));

        // print header
        printHeader(writer);

        return writer;
    }

    private Path getSplitPath(Object value) {
        String name = outFilePath.getFileName().toString();

        // find prefix and extension
        String prefix;
        String extension;
        int lastDot = name.lastIndexOf(".");
        if (lastDot == -1) {
            prefix = name;
            extension = "";
        } else {
            prefix = name.substring(0, lastDot);
            extension = name.substring(lastDot);
        }

        String splitName = prefix + "_" + String.valueOf(value) + extension;
        return outFilePath.resolveSibling(splitName);
    }
}
