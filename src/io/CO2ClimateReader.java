/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import climate.ClimateType;
import gov.usda.jcf.core.Context;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Iterator;

public class CO2ClimateReader implements ClimateReader {
    private static final String CO2_NAME = ClimateType.CO2.getName();

    private final Path dataFilePath;
    private final double defaultCO2;
    private final LocalDate startTime;
    private final LocalDate endTime;
    private final boolean alwaysDefault;

    private Iterator<String[]> rows;
    private DateTimeFormatter dateFormatter;
    private double missingDataValue;

    private LocalDate time;
    private double co2;
    private RowData nextUpdate;

    public CO2ClimateReader(Path dataFilePath, double defaultCO2, LocalDate startTime, LocalDate endTime) {
        this.dataFilePath = dataFilePath;
        this.defaultCO2 = defaultCO2;
        this.startTime = startTime;
        this.endTime = endTime;

        alwaysDefault = (this.dataFilePath == null);
        // set starting co2, so it doesn't need to be modified if alwaysDefault
        co2 = this.defaultCO2;
    }

    @Override
    public void readData() throws IOException {
        if (alwaysDefault) {
            return;
        }

        try {
            readData0();
        } catch (Exception ex) {
            throw new IOException("Error reading co2 file: " + dataFilePath, ex);
        }
    }

    private void readData0() throws Exception {
        // Initialize if not intialized otherwise update the time to the next day
        if (rows == null) {
            init();
            time = startTime;
        } else {
            time = time.plusDays(1L);
        }

        // if nextUpdate is null then there is no more data to read
        // when there is no more data to read co2 should stay as the last known value
        if (nextUpdate == null) {
            return; // no more data to read so keep using last known value
        }

        // If nextUpdate.time >= time then update co2 and read next value
        if (!time.isBefore(nextUpdate.time)) {
            updateCO2(nextUpdate.co2);
            nextUpdate = readNextRow();
        }
    }

    @Override
    public double setValue(Context context) {
        context.put(CO2_NAME, co2);
        return co2;
    }

    @Override
    public void reset() {
        close();
    }

    @Override
    public void close() {
        if (rows != null) {
            if (rows instanceof TableIterator) {
                TableIterator<String[]> iter = (TableIterator<String[]>) rows;
                iter.close();
            }
            rows = null;
        }
    }

    private void init() throws IOException {
        CSTable table = DataIO.table(dataFilePath.toFile());
        rows = table.rows().iterator();

        String datefmt = table.getInfo().get(DataIO.DATE_FORMAT);
        if (datefmt == null) {
            throw new IOException("Missing date format in " + dataFilePath);
        }
        dateFormatter = DateTimeFormatter.ofPattern(datefmt);

        String missingDataValueString = table.getInfo().get(DataIO.KEY_MISSING_VAL);
        if (missingDataValueString == null) {
            missingDataValueString = table.getInfo().get("missing_val");
            if (missingDataValueString == null) {
                missingDataValueString = "NaN";
            }
        }
        missingDataValue = Double.parseDouble(missingDataValueString);

        String tableStartTimeString = table.getInfo().get(DataIO.DATE_START);
        String tableEndTimeString = table.getInfo().get(DataIO.DATE_END);
        if (tableStartTimeString == null || tableEndTimeString == null) {
            throw new IOException("Missing start/end time in " + dataFilePath);
        }

        // Parse table start and end time
        LocalDate tableStartTime;
        LocalDate tableEndTime;
        try {
            tableStartTime = parseDate(tableStartTimeString);
        } catch (DateTimeParseException ex) {
            throw new IOException("Error parsing table start time: " + tableStartTimeString, ex);
        }
        try {
            tableEndTime = parseDate(tableEndTimeString);
        } catch (DateTimeParseException ex) {
            throw new IOException("Error parsing table end time: " + tableEndTimeString, ex);
        }

        // validate that table end time is not before the table start time
        // It is valid for tableEndTime >= tableStartTime
        if (tableEndTime.isBefore(tableStartTime)) {
            throw new IOException("Invalid co2 table time range: "
                    + formatDate(tableStartTime) + " - " + formatDate(tableEndTime));
        }

        // validate table times are within simulation times
        if (startTime.isBefore(tableStartTime)) {
            System.out.println("WARNING: Simulation start time is before the start time of the co2 file. The default CO2 value of " + defaultCO2 + " will be used.");
        }

        // read co2 values until end of dates or until start time is found
        co2 = defaultCO2;
        while (true) {
            nextUpdate = readNextRow();
            if (nextUpdate == null) {
                break;
            } else if (nextUpdate.time.isAfter(startTime)) {
                break;
            } else {
                updateCO2(nextUpdate.co2);
            }
        }
    }

    private RowData readNextRow() throws IOException {
        if (!rows.hasNext()) {
            return null;
        }
        String[] row = rows.next();
        try {
            return readNextRow0(row);
        } catch (Exception ex) {
            throw new IOException("Malformed data line: " + Arrays.toString(row), ex);
        }
    }

    private RowData readNextRow0(String[] row) {
        LocalDate timeValue = parseDate(row[1]);
        double co2Value = Double.parseDouble(row[2]);

        return new RowData(timeValue, co2Value);
    }

    private LocalDate parseDate(String dateString) throws DateTimeParseException {
        return dateFormatter.parse(dateString, LocalDate::from);
    }

    private String formatDate(TemporalAccessor temporal) {
        return dateFormatter.format(temporal);
    }

    private void updateCO2(double co2Value) {
        // if new co2 value is missing value then leave as the last know value
        if (Double.compare(co2Value, missingDataValue) == 0) {
            return;
        }
        co2 = co2Value;
    }

    private class RowData {
        public final LocalDate time;
        public final double co2;

        public RowData(LocalDate time, double co2) {
            this.time = time;
            this.co2 = co2;
        }
    }
}
