/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package io;

import ages.types.SoilType;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SoilsParameterReader {
    public static Map<Integer, SoilType> readSoils(Path soilFilePath) throws IOException {
        List<Double> depth = new ArrayList<>();
        List<Double> aircapacity = new ArrayList<>();
        List<Double> fieldcapacity = new ArrayList<>();
        List<Double> deadcapacity = new ArrayList<>();
        List<Double> kf = new ArrayList<>();
        List<Double> bulk_density = new ArrayList<>();
        List<Double> corg = new ArrayList<>();
        List<Double> root = new ArrayList<>();
        List<Double> A_skel = new ArrayList<>();
        List<Double> kvalue = new ArrayList<>();
        List<Double> pcontent = new ArrayList<>();
        List<Double> initLPS = new ArrayList<>();
        List<Double> initMPS = new ArrayList<>();
        List<Double> initSWC = new ArrayList<>();

        Map<Integer, SoilType> stMap = new HashMap<>();

        CSTable table = DataIO.table(soilFilePath.toFile());

        // indicator that the depth is accumulated
        String acc = table.getInfo().get("depth_acc");
        boolean depth_acc = acc != null && Boolean.parseBoolean(acc);

        int initSWC_col = DataIO.columnIndex(table, "initSWC");
        int initMPS_col = DataIO.columnIndex(table, "initMPS");
        int initLPS_col = DataIO.columnIndex(table, "initLPS");

        /* Check for soil property units in soils_hor.csv input file, if no
         * unit is specified in the "fieldcapacity" column then set to "mm".
         * The other acceptable fieldcapacity unit is "vol".
         */
        int fc_index = DataIO.columnIndex(table, "fieldcapacity");
        String u = table.getColumnInfo(fc_index).get("unit");
        final String capacity_unit = (u == null) ? "mm" : u.toLowerCase();
        final boolean isMM = capacity_unit.equalsIgnoreCase("mm");
        if (!capacity_unit.contains("vol") && !isMM) {
            throw new IOException("Unknown capacity unit: " + capacity_unit);
        }

        int last = 0;
        Iterator<String[]> rows = table.rows().iterator();
        if (!rows.hasNext()) {
            return stMap;
        }
        String[] row = rows.next();
        while (row != null) {
            int id = parseSID(row[1]);

            depth.add(Double.parseDouble(row[2]));
            aircapacity.add(Double.parseDouble(row[3]));
            fieldcapacity.add(Double.parseDouble(row[4]));
            deadcapacity.add(Double.parseDouble(row[5]));
            kf.add(Double.parseDouble(row[6]));
            bulk_density.add(Double.parseDouble(row[7]));
            corg.add(Double.parseDouble(row[8]));
            root.add(Double.parseDouble(row[9]));
            kvalue.add(Double.parseDouble(row[10]));
            A_skel.add(Double.parseDouble(row[11]));
            pcontent.add(Double.parseDouble(row[12]));

            if (initMPS_col != -1) {
                initMPS.add(Double.parseDouble(row[initMPS_col]));
            }
            if (initLPS_col != -1) {
                initLPS.add(Double.parseDouble(row[initLPS_col]));
            }
            if (initSWC_col != -1) {
                initSWC.add(Double.parseDouble(row[initSWC_col]));
            }

            row = (rows.hasNext() ? rows.next() : null);
            if (row == null || id != parseSID(row[1])) {
                SoilType st = new SoilType();
                st.SID = id;
                st.horizons = depth.size();
                double[] d = DataIOUtils.toDoubleArray(depth);
                if (depth_acc) {
                    d = partitionAccDepth(d);
                }
                st.depth_h = d;
                if (isMM) {
                    st.airCapacity_h = DataIOUtils.toDoubleArray(aircapacity);
                    st.fieldCapacity_h = DataIOUtils.toDoubleArray(fieldcapacity);
                    st.deadCapacity_h = DataIOUtils.toDoubleArray(deadcapacity);
                } else {
                    st.volumeAirCapacity_h = DataIOUtils.toDoubleArray(aircapacity);
                    st.volumeFieldCapacity_h = DataIOUtils.toDoubleArray(fieldcapacity);
                    st.volumeDeadCapacity_h = DataIOUtils.toDoubleArray(deadcapacity);
                }
                st.kf_h = DataIOUtils.toDoubleArray(kf);
                st.bulkDensity_h = DataIOUtils.toDoubleArray(bulk_density);
                st.corg_h = DataIOUtils.toDoubleArray(corg);
                st.root_h = DataIOUtils.toDoubleArray(root);
                st.kFactor = kvalue.get(0);
                st.rockFragment = A_skel.get(0);
                st.capacity_unit = capacity_unit;
                st.pContent_h = DataIOUtils.toDoubleArray(pcontent);
                if (!initMPS.isEmpty()) {
                    st.initMPS_h = DataIOUtils.toDoubleArray(initMPS);
                }
                if (!initLPS.isEmpty()) {
                    st.initLPS_h = DataIOUtils.toDoubleArray(initLPS);
                }
                if (!initSWC.isEmpty()) {
                    st.initSWC_h = DataIOUtils.toDoubleArray(initSWC);
                }

                // compute totaldepth and bulkDensity_h
                if (isMM) {
                    // convert mm to vol
                    st.volumeDeadCapacity_h = new double[st.horizons];
                    st.volumeFieldCapacity_h = new double[st.horizons];
                    st.volumeAirCapacity_h = new double[st.horizons];
                    st.computedBulkDensity_h = new double[st.horizons];
                    double depthMM;
                    for (int h = 0; h < st.horizons; ++h) {
                        st.totaldepth += st.depth_h[h];
                        depthMM = st.depth_h[h] * 10;
                        st.volumeDeadCapacity_h[h] = st.deadCapacity_h[h] / depthMM;
                        st.volumeFieldCapacity_h[h] = st.fieldCapacity_h[h] / depthMM + st.volumeDeadCapacity_h[h];
                        st.volumeAirCapacity_h[h] = st.airCapacity_h[h] / depthMM + st.volumeFieldCapacity_h[h];
                        st.computedBulkDensity_h[h] = (1 - st.volumeAirCapacity_h[h]) * 2.65;
                    }
                } else {
                    // convert vol to mm
                    st.deadCapacity_h = new double[st.horizons];
                    st.fieldCapacity_h = new double[st.horizons];
                    st.airCapacity_h = new double[st.horizons];
                    st.computedBulkDensity_h = new double[st.horizons];
                    double depthMM;
                    for (int h = 0; h < st.horizons; ++h) {
                        st.totaldepth += st.depth_h[h];
                        depthMM = st.depth_h[h] * 10;
                        st.deadCapacity_h[h] = st.volumeDeadCapacity_h[h] * depthMM;
                        st.fieldCapacity_h[h] = (st.volumeFieldCapacity_h[h] - st.volumeDeadCapacity_h[h]) * depthMM;
                        st.airCapacity_h[h] = (st.volumeAirCapacity_h[h] - st.volumeFieldCapacity_h[h]) * depthMM;
                        st.computedBulkDensity_h[h] = (1 - st.volumeAirCapacity_h[h]) * 2.65;
                    }
                }

                stMap.put(st.SID, st);

                depth.clear();
                aircapacity.clear();
                fieldcapacity.clear();
                deadcapacity.clear();
                kf.clear();
                bulk_density.clear();
                corg.clear();
                root.clear();
                kvalue.clear();
                A_skel.clear();
                pcontent.clear();
                initMPS.clear();
                initLPS.clear();
                initSWC.clear();
            }
        }

        return stMap;
    }

    // re-partition soil depth from accumulated to absolute per horizon
    private static double[] partitionAccDepth(double[] depth) {
        double prev_depth = 0;
        for (int i = 0; i < depth.length; i++) {
            double this_depth = depth[i];
            if (this_depth < prev_depth || this_depth < 0) {
                throw new IllegalArgumentException("Invalid soil depth: " + this_depth);
            }
            depth[i] = this_depth - prev_depth;
            prev_depth = this_depth;
        }
        return depth;
    }

    private static int parseSID(String sid) {
        return Integer.parseInt(sid) / 100;
    }
}
