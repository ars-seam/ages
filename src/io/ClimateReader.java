
package io;

import gov.usda.jcf.core.Context;
import java.io.IOException;

/**
 *
 * @author Nathan Lighthart
 */
public interface ClimateReader {
    /**
     * Reads the next days data.
     *
     * @throws IOException If an I/O error occurs
     */
    void readData() throws IOException;

    /**
     * Sets the value for the climate variable in the specified context
     *
     * @param context the context to add the climate variable to
     * @return the value that was added to the context
     */
    double setValue(Context context);

    /**
     * Resets the climate reader to the beginning.
     */
    void reset();

    /**
     * Closes all files that are being used.
     */
    void close();
}
