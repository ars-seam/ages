/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package radiation;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add CalcExtraterrRadiation module definition here")
@Author(name = "Peter Krause, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Radiation")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/radiation/CalcExtraterrRadiation.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/radiation/CalcExtraterrRadiation.xml")
public class CalcExtraterrRadiationAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Description("east or west of Greenwhich (e|w)")
    @Units("w | e")
    @Role(PARAMETER)
    @Input public String locGrw;

    @Description("longitude of time-zone center [dec.deg]")
    @Units("deg")
    @Role(PARAMETER)
    @Input public double longTZ;

    @Description("Entity Latidute")
    @Units("deg")
    @Input public double latitude;

    @Description("entity longitude")
    @Units("deg")
    @Input public double longitude;

    @Description("extraterrestric radiation of each time step of the year")
    @Units("MJ/m2 timeUnit")
    @Output public double[] extRadArray;

    @Override
    protected void run(Context context) {
        CalcExtraterrRadiation component = new CalcExtraterrRadiation();

        component.tempRes = tempRes;
        component.locGrw = locGrw;
        component.longTZ = longTZ;
        component.latitude = latitude;
        component.longitude = longitude;

        component.execute();

        extRadArray = component.extRadArray;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
