/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package radiation;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;

@Description("Add CalcDailySolarRadiation module definition here")
@Author(name = "Peter Krause, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Radiation, J2000")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/radiation/CalcDailySolarRadiation.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/radiation/CalcDailySolarRadiation.xml")
public class CalcDailySolarRadiationAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Description("parameter a for Angstroem formula")
    @Role(PARAMETER)
    @Input public double angstrom_a;

    @Description("parameter b for Angstroem formula")
    @Role(PARAMETER)
    @Input public double angstrom_b;

    @Description("Current Time")
    @Input public LocalDate time;

    @Description("sunshine hours")
    @Units("h/d")
    @Input public double sol;

    @Description("state var slope-aspect-correction-factor")
    @Input public double actSlAsCf;

    @Description("Entity Latitude")
    @Units("deg")
    @Input public double latitude;

    @Description("daily extraterrestic radiation")
    @Units("MJ/m2/day")
    @Input public double extRad;

    @Description("Solrad / sunh")
    @Role(PARAMETER)
    @Input public String solar;

    @Description("Daily solar radiation")
    @Units("MJ/m2/day")
    @Output public double solRad;

    @Description("Maximum sunshine duration")
    @Units("h")
    @Output public double sunhmax;

    @Override
    protected void run(Context context) {
        CalcDailySolarRadiation component = new CalcDailySolarRadiation();

        component.tempRes = tempRes;
        component.angstrom_a = angstrom_a;
        component.angstrom_b = angstrom_b;
        component.time = time;
        component.sol = sol;
        component.actSlAsCf = actSlAsCf;
        component.latitude = latitude;
        component.extRad = extRad;
        component.solar = solar;

        component.execute();

        solRad = component.solRad;
        sunhmax = component.sunhmax;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
