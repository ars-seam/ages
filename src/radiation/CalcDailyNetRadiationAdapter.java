/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package radiation;

import ages.types.HRU;
import ages.types.Landuse;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add CalcDailyNetRadiation module definition here")
@Author(name = "Peter Krause, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Radiation")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/radiation/CalcDailyNetRadiation.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/radiation/CalcDailyNetRadiation.xml")
public class CalcDailyNetRadiationAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Relative Humidity")
    @Input public double rhum;

    @Description("solar radiation")
    @Input public double extRad;

    @Description("Daily solar radiation")
    @Units("MJ/m2/day")
    @Input public double solRad;

    @Description("landuse")
    @Input public Landuse landuse;

    @Description("HRU")
    @Input public HRU hru;

    @Description("Daily net radiation")
    @Units("MJ/m2")
    @Output public double netRad;

    @Override
    protected void run(Context context) {
        CalcDailyNetRadiation component = new CalcDailyNetRadiation();

        component.tempRes = tempRes;
        component.tmean = tmean;
        component.rhum = rhum;
        component.extRad = extRad;
        component.solRad = solRad;
        component.albedo = landuse.albedo;
        component.elevation = hru.elevation;

        component.execute();

        netRad = component.netRad;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
