/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package radiation;

import java.time.LocalDate;
import lib.DailySolarRad;
import lib.HourlySolarRad;
import lib.MathCalculations;
import lib.SolarRad;

public class CalcDailySolarRadiation {
    public String tempRes;
    public double angstrom_a;
    public double angstrom_b;
    public LocalDate time;
    public double sol;
    public double actSlAsCf;
    public double latitude;
    public double extRad;
    public String solar;
    public double solRad;
    public double sunhmax;

    public void execute() {
        int julDay = time.getDayOfYear();

        double declination = SolarRad.sunDeclination(julDay);
        double latRad = MathCalculations.rad(latitude);
        double sunsetHourAngle = DailySolarRad.sunsetHourAngle(latRad, declination);
        double maxSunshine = 0;

        if (tempRes.equals("h")) {
            maxSunshine = HourlySolarRad.hourlyMaxSunshine(extRad);
        } else if (tempRes.equals("d")) {
            maxSunshine = DailySolarRad.maxSunshineHours(sunsetHourAngle);
        }

        sunhmax = maxSunshine;

        if (solar.equals("sunh")) {
            double solarRadiation = SolarRad.solarRadiation(sol, sunhmax, extRad, angstrom_a, angstrom_b);
            // adjust solar radiation based on slope and aspect
            solRad = solarRadiation * actSlAsCf;
        } else {
            solRad = sol;
        }
    }
}
