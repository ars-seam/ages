/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package potet;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EvapoTrans {
    private static final Logger log
            = Logger.getLogger("oms3.model." + EvapoTrans.class.getSimpleName());

    public int horizons;
    public double LAI;
    public double[] actETP_h;
    public double actET;
    public double potET;
    public double aEvap;
    public double aTransp;
    public double pEvap;
    public double pTransp;
    public double[] aEP_h;
    public double[] aTP_h;

    public void execute() {
        if (aEP_h == null) {
            aEP_h = new double[horizons];
            aTP_h = new double[horizons];
        }

        if (LAI <= 3) {
            aTransp = (actET * LAI) / 3;
            pTransp = (potET * LAI) / 3;
        } else if (LAI > 3) {
            aTransp = actET;
            pTransp = potET;
        }

        aEvap = actET - aTransp;
        pEvap = potET - pTransp;

        for (int i = 0; i < horizons; i++) {
            double actETP = actETP_h[i];
            double actTran = 0;

            if (LAI <= 3) {
                actTran = (actETP * LAI) / 3;
            } else if (LAI > 3) {
                actTran = actETP;
            }

            aTP_h[i] = actTran;
            aEP_h[i] = actETP - actTran;
        }
        if (log.isLoggable(Level.INFO)) {
            log.info("aEvap:" + aEvap);
        }
    }
}
