/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package potet;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("PenmanMonteith PET calculation with CO2 impact through stomatal conductance ratio")
@Author(name = "Olaf David, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
// Nathan, do we need to change this?
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/potet/PenmanMonteith.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/potet/PenmanMonteith.xml")
public class PenmanMonteithCO2TransRatioAdapter extends AnnotatedAdapter {
    @Description("daily or hourly time steps [d|h]")
    @Units("d | h")
    @Role(PARAMETER)
    @Input public String tempRes;

    @Description("wind")
    @Input public double wind;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Relative Humidity")
    @Input public double rhum;

    @Description("Daily net radiation")
    @Units("MJ/m2")
    @Input public double netRad;

    @Description("state variable rsc0")
    @Input public double actRsc0;

    @Description("HRU")
    @Input public HRU hru;

    @Description("LAI")
    @Input public double LAI;

    @Description("effective height")
    @Input public double actEffH;

    @Description("crop")
    @Input public Crop crop;

    @Description("the daily atmospheric level of CO2")
    @Units("ppm")
    @Input public double co2;

    @Description("HRU potential Evapotranspiration")
    @Units("mm")
    @Output public double potET;

    @Description("HRU actual Evapotranspiration")
    @Units("mm")
    @Output public double actET;

    @Override
    protected void run(Context context) {
        PenmanMonteithCO2TransRatio component = new PenmanMonteithCO2TransRatio();

        component.tempRes = tempRes;
        component.wind = wind;
        component.tmean = tmean;
        component.rhum = rhum;
        component.netRad = netRad;
        component.actRsc0 = actRsc0;
        component.elevation = hru.elevation;
        component.area = hru.area;
        component.LAI = LAI;
        component.actEffH = actEffH;
        // required for co2 impact
        component.idc = crop.idc;
        component.co2 = co2;

        component.execute();

        potET = component.potET;
        actET = component.actET;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
