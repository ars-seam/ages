/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package potet;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add EvapoTrans module definition here")
@Author(name = "Olaf David, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Evapotranspiration")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/potet/EvapoTrans.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/potet/EvapoTrans.xml")
public class EvapoTransAdapter extends AnnotatedAdapter {
    @Description("soil")
    @Input public SoilType soil;

    @Description("Array of state variables LAI ")
    @Input public double LAI;

    @Description("HRU actual Evapotranspiration")
    @Units("mm")
    @Input public double[] actETP_h;

    @Description("HRU actual Evapotranspiration")
    @Units("mm")
    @Input public double actET;

    @Description("HRU potential Evapotranspiration")
    @Units("mm")
    @Input public double potET;

    @Description("Current hru object")
    @Input public HRU hru;

    @Description("HRU actual Evaporation")
    @Units("mm")
    @Output public double aEvap;

    @Description("HRU actual Transpiration")
    @Units("mm")
    @Output public double aTransp;

    @Description("HRU potential Evaporation")
    @Units("mm")
    @Output public double pEvap;

    @Description("HRU potential Transpiration")
    @Units("mm")
    @Output public double pTransp;

    @Description(" actual evaporation")
    @Units("mm")
    @Output public double[] aEP_h;

    @Description(" actual evaporation")
    @Units("mm")
    @Output public double[] aTP_h;

    @Override
    protected void run(Context context) {
        EvapoTrans component = new EvapoTrans();

        component.horizons = soil.horizons;
        component.LAI = LAI;
        component.actETP_h = actETP_h;
        component.actET = actET;
        component.potET = potET;

        component.execute();

        aEvap = component.aEvap;
        aTransp = component.aTransp;
        pEvap = component.pEvap;
        pTransp = component.pTransp;
        aEP_h = component.aEP_h;
        aTP_h = component.aTP_h;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
