/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

public class PlantGrowthStressSWAT {
    public double aTransp;
    public double pTransp;
    public double BioNOpt;
    public double BioNAct;
    public double actnup;
    public double BioOpt_delta;
    public double tmean;
    public double tbase;
    public double topt;
    public boolean plantExisting;
    public double BioAct;
    public double tstrs;
    public double nstrs;
    public double wstrs;
    public double deltabiomass;

    private double biomass_start;

    public void execute() {
        biomass_start = BioAct;
        // calculate water stress
        if (pTransp > 0) {
            wstrs = 1 - ((aTransp) / (pTransp));
        } else {
            wstrs = 0;
        }

        // calculate N stress
        double phi_nit = 0;

        if (((BioNAct + 0.01) / (BioNOpt + 0.01)) >= 0.319) {
            phi_nit = 200 * (((BioNAct + 0.01) / (BioNOpt + 0.01)) - 0.319);
        }
        nstrs = 1 - (phi_nit / (phi_nit + Math.exp(3.535 - (0.02597 * phi_nit))));
        nstrs = (nstrs > 1.0) ? 1.0 : nstrs;

        // calculate temperature stress
        tstrs = 0;

        if (BioNAct > 0) {   // is there N in biomass?
            if (tmean <= tbase) {   // has base temperature been met?
                tstrs = 1;
            } else if (tmean > tbase && tmean <= topt) {
                tstrs = 1 - (Math.exp(((-0.1054 * Math.pow((topt - tmean), 2)))
                        / Math.pow((tmean - tbase), 2)));
            } else if (tmean > topt && tmean <= ((2 * topt) - tbase)) {
                tstrs = 1 - (Math.exp(((-0.1054 * Math.pow((topt - tmean), 2)))
                        / Math.pow(((2 * topt) - tmean - tbase), 2)));
            } else if (tmean > ((2 * topt) - tbase)) {
                tstrs = 1;
            }
        }

        // calculate stress factor using temperature and N stress
        double stressfactor = 1 - Math.max(wstrs, (Math.max(tstrs, nstrs)));

        stressfactor = (stressfactor > 1) ? 1 : stressfactor;
        stressfactor = (stressfactor < 0) ? 0 : stressfactor;

        BioAct += (stressfactor * BioOpt_delta);
        deltabiomass = BioAct - biomass_start;
    }
}
