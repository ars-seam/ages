/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class PotentialCropGrowthSWAT {
    private static final Logger log
            = Logger.getLogger("oms3.model." + PotentialCropGrowthSWAT.class.getSimpleName());

    public double LExCoef;
    public double rootfactor;
    public boolean flagUPGM;
    public double area;
    public double tmean;
    public double solRad;
    public boolean dormancy;
    public double soil_root;
    public int harvesttype;
    public boolean plantExisting;
    public boolean doHarvest;
    public double phu;
    public double mlai1;
    public double mlai;
    public double mlai2;
    public double lai_min;
    public double frgrw1;
    public double frgrw2;
    public double rue;
    public double chtmx;
    public double rdmx;
    public double bn1;
    public double bn2;
    public double bn3;
    public int idc;
    public double hvsti;
    public double cnyld;
    public boolean plantStateReset;
    public double LAI;
    public double BioagAct;
    public double PHUact;
    public double BioNOpt;
    public double frLAImxAct;
    public double frLAImx_xi;
    public double frRootAct;
    public double BioAct;
    public double CanHeightAct;
    public double zrootd;
    public double FNPlant;
    public double BioOpt_delta;
    public double BioNAct;
    public double HarvIndex;
    public double FPHUact;
    public double nfert;
    public double Addresidue_pool;
    public double Addresidue_pooln;
    public double BioYield;
    public double NYield;
    public double NYield_ha;
    public double tbase;
    public double topt;
    public double fr3N;
    public double dlai;

    private double area_ha;
    private double sc1_LAI;
    private double sc2_LAI;
    public double fracharvest;
    public double fracharvestn;
    private double LAI_delta;

    private static final boolean DO_CO2_BIOMASS = false;
    // For co2 response.
    public double co2;

    public double co2eff;

    // Assuming hardcoded values for initial testing will be ok. Eventually this could be user input.
    // c4 crops
    private static final List<Double> CO2_X_C4CROPS = Arrays.asList(0.0, 220.0, 280.0, 330.0, 400.0, 490.0, 570.0, 750.0, 990.0, 9999.0);
    private static final List<Double> CO2_Y_C4CROPS = Arrays.asList(0.00, 0.85, 0.95, 1.00, 1.02, 1.04, 1.05, 1.06, 1.07, 1.08);
    // c5 crops
    private static final List<Double> CO2_X_C3CROPS = Arrays.asList(0.0, 220.0, 330.0, 440.0, 550.0, 660.0, 770.0, 880.0, 990.0, 9999.0);
    private static final List<Double> CO2_Y_C3CROPS = Arrays.asList(0.00, 0.71, 1.00, 1.08, 1.17, 1.25, 1.32, 1.38, 1.43, 1.50);

    public void execute() {

        BioYield = 0;
        NYield = 0;
        area_ha = area / 10000;

        if (plantExisting) {
            // check to see if this is the first step where crop is calculated
            if (BioNAct == 0) {
                BioAct = 1;   // set biomass at planting (assume 1 kg/ha)
                BioNAct = bn1 * BioAct;  // initial N content using optimal N content at emergence
            }

            calc_phu();
            calc_lai();
            if (DO_CO2_BIOMASS) {
                calc_co2_biomass();
            } else {
                co2eff = 1.0;
                calc_biomass();
            }
            CanHeightAct = calc_canopy();
            calc_root();
            calc_nuptake();
            Addresidue_pool = 0;
            Addresidue_pooln = 0;

            if (doHarvest) {
                calc_cropyield();
                calc_cropyield_ha();
                calc_residues();
                doHarvest = false;
            }
        } else if (plantStateReset) {   // called if there is no crop grown on HRU
            PHUact = 0;
            tbase = 0;
            topt = 0;
            frLAImxAct = 0;
            LAI_delta = 0;
            LAI = 0;
            CanHeightAct = 0;
            frRootAct = 0;
            zrootd = 0;
            FNPlant = 0;
            BioAct = 0;
            BioNOpt = 0;
            BioNAct = 0;
            BioOpt_delta = 0;
            HarvIndex = 0;
            BioagAct = 0;
            FPHUact = 0;
            frLAImx_xi = 0;
            Addresidue_pool = 0;
            Addresidue_pooln = 0;
            plantStateReset = false;
        }

        zrootd = Math.min(zrootd * rootfactor, soil_root);

        /*
		 * Don't reset if plant is clover (IDC = 3), hay/pasture/wetlands (IDC = 6), or orchard/forest (IDC = 7).
		 * Reset all other crops the next time step.
         */
        plantStateReset = (idc == 3 || idc == 6 || idc == 7) ? false : true;
    }

    // biomass production

    /*
	 * First the daily development of the LAI is calculated as a fraction of max LAI development, where the
	 * fraction of plants max LAI corresponding to a given fraction of PHU is calculated & 2 shape-coefficients,
	 * sc1 & sc2 are needed. Maximum LAI corresponding to the fraction of heat units is calculated, expressed
         * as a fraction of the known maximum LAI.
     */
    private boolean calc_phu() {
        if (tmean > tbase) {
            PHUact += (tmean - tbase);
            FPHUact = PHUact / phu;
        }
        return true;
    }

    private boolean calc_lai() {
        if (FPHUact <= dlai) {
            double sc1_lai1 = Math.log(frgrw1 / mlai1 - frgrw1);
            double sc2_lai2 = Math.log(frgrw2 / mlai2 - frgrw2);
            double sc_frpuh = frgrw2 - frgrw1;

            sc2_LAI = (sc1_lai1 - sc2_lai2) / sc_frpuh;
            sc1_LAI = sc1_lai1 + sc2_LAI * frgrw1;

            double sc_minus = FPHUact * sc2_LAI;

            frLAImx_xi = frLAImxAct; // save frLAImx from the day before
            frLAImxAct = FPHUact / (FPHUact + (Math.exp(sc1_LAI - sc_minus)));

            LAI_delta = (frLAImxAct - frLAImx_xi) * mlai * (1 - Math.exp(5.0 * (LAI - mlai)));
            LAI += LAI_delta;
            LAI = (LAI > mlai) ? mlai : LAI;
        } else {
            // code from SWATplus Revision 41, uses a logistic decline rate (Strauch)
            double rto = (1.0 - FPHUact) / (1.0 - dlai);
            LAI = (LAI - lai_min) / (1.0 + Math.exp((rto - 0.5) * -12)) + lai_min;
        }
        if (doHarvest && (idc == 3 || idc == 6 || idc == 7)) {
            frLAImxAct = 0;
            LAI_delta = 0;
            LAI = lai_min;
            frLAImx_xi = 0;
        }
        return true;
    }

    /*
	 * Second, the amount of daily solar radiation intercepted by leaf aream is calculated.
	 * Third, the amount of biomass (kg/ha, dry) produced per unit of intercepted solar radiation is calculated
	 * using plant-specific radiation-use efficiency in the crop growth database crop.csv.
     */
    private void calc_biomass() {
        double Hphosyn = 0.5 * solRad * (1 - Math.exp(LExCoef * LAI)); // intercepted photosynthetically active radiation (MJ/m^2)
        BioOpt_delta = rue * Hphosyn;
        BioOpt_delta = (dormancy) ? 0 : BioOpt_delta;
    }

    private void calc_co2_biomass() {
        co2eff = 1.0; // default = no impact.
        int indexLess = 0;
        int indexPlusOne = 0;
        if (idc == 4) {
            // c4 crops (?maybe?)
            // either exact position of co2 value, or pos - 1 in array.
            int indx = Collections.binarySearch(CO2_X_C4CROPS, co2);
            if (indx < 0) {
                indexLess = Math.abs(indx) - 2;
                indexPlusOne = Math.abs(indx) - 1;
            } else {
                indexLess = indx;
                indexPlusOne = Math.abs(indx) + 1;
            }

            co2eff = (co2 - CO2_X_C4CROPS.get(indexLess)) * (CO2_Y_C4CROPS.get(indexPlusOne) - CO2_Y_C4CROPS.get(indexLess))
                    / (CO2_X_C4CROPS.get(indexPlusOne) - CO2_X_C4CROPS.get(indexLess)) + CO2_Y_C4CROPS.get(indexLess);

        } else {
            // either exact position of co2 value, or pos - 1 in array.
            int indx = Collections.binarySearch(CO2_X_C3CROPS, co2);
            if (indx < 0) {
                indexLess = Math.abs(indx) - 2;
                indexPlusOne = Math.abs(indx) - 1;
            } else {
                indexLess = indx;
                indexPlusOne = Math.abs(indx) + 1;
            }
            co2eff = (co2 - CO2_X_C3CROPS.get(indexLess)) * (CO2_Y_C3CROPS.get(indexPlusOne) - CO2_Y_C3CROPS.get(indexLess))
                    / (CO2_X_C3CROPS.get(indexPlusOne) - CO2_X_C3CROPS.get(indexLess)) + CO2_Y_C3CROPS.get(indexLess);
        }
        double Hphosyn = 0.5 * solRad * (1 - Math.exp(LExCoef * LAI)) * co2eff; // intercepted photosynthetically active radiation (MJ/m^2)
        BioOpt_delta = rue * Hphosyn;
        BioOpt_delta = (dormancy) ? 0 : BioOpt_delta;
    }

    // canopy cover
    // canopy cover is expressed as a function of LAI
    private double calc_canopy() {
        double hc_delta = chtmx * Math.sqrt(frLAImxAct);
        CanHeightAct = hc_delta;
        return CanHeightAct;
    }

    // root development

    /*
	 * Amount of total biomass partioned to the root system. In general, this varies between 30-50% in seedlings,
	 * and decreases to 5-20% in mature plants.
     */
    private boolean calc_root() {
        double rootpartmodi = 0.20 * FPHUact;
        rootpartmodi = Math.min(rootpartmodi, 0.2);
        frRootAct = 0.40 - rootpartmodi;
        /*
		 * Root development (mm in the soil) for plant types on a given day. Varying linearly from 0.0 at the beginning of the
		 * growing season to the maximum rooting depth at fphu = 0.4. Perennials and trees do not have varying rooting depth.
         */
        zrootd = (idc == 3 || idc == 6 || idc == 7) ? rdmx : zrootd;
        // annuals
        zrootd = ((idc == 1 || idc == 2 || idc == 4 || idc == 5 || idc == 8) && FPHUact <= 0.40)
                ? 2.5 * FPHUact * rdmx : zrootd;
        zrootd = (FPHUact > 0.40) ? rdmx : zrootd;
        return true;
    }

    /* Calculates optimal biomass N content, not actual N uptake.
	 * Maturity is reached when fphu_act = 1, and then no further calculation is needed.
	 * Nutrients, water uptake, and transpiration should stop depending on the condition fphu = 1.
	 * Need to calculate water uptake by plants, potential water uptake, and nutrient uptake by plants.
     */
    private boolean calc_nuptake() {
        // fraction of N in plant biomass as a function of growth stage given optimal conditions
        if (bn1 > bn2 && bn2 > bn3 && bn3 > 0) {
            double s1 = Math.log((0.5 / (1 - ((bn2 - bn3) / (bn1 - bn3)))) - 0.5);
            double s2 = Math.log((1 / (1 - ((0.0001) / (bn1 - bn3)))) - 1);
            double n2 = (s1 - s2) / 0.5;
            double n1 = Math.log((0.5 / (1 - ((bn2 - bn3) / (bn1 - bn3)))) - 0.5) + (n2 * 0.5);
            FNPlant = ((bn1 - bn3) * (1 - (FPHUact / (FPHUact + Math.exp(n1 - n2 * FPHUact))))) + bn3;
            FNPlant = (harvesttype == 2) ? bn3 : FNPlant;
        } else {
            FNPlant = 0.01;
        }
        BioNOpt = FNPlant * BioAct;
        return true;
    }

    /* N fixation occurs when NO3-N levels in the root zone are insufficient to meet demand.
	 * Note: need to calculate phosphorus uptake.
     */
    private boolean calc_cropyield() {
        if (idc == 3 || idc == 6 || idc == 7 || (idc == 8)) {
            HarvIndex = hvsti * (100 * FPHUact) / (100 * FPHUact + Math.exp(11.1 - 10.0 * FPHUact));
            HarvIndex = Math.max(HarvIndex, hvsti * 0.75);
            HarvIndex = Math.min(HarvIndex, hvsti);
            // crop yield (kg/ha)is calculated as above ground biomass
            BioagAct = (1 - frRootAct) * BioAct;

            // calculate total yield biomass at harvest
            if (hvsti <= 1) {       // case 1: the total biomassis assumed to be yield
                BioYield = BioagAct * HarvIndex;
                BioYield = (BioYield > BioagAct) ? BioagAct : BioYield;
            } else if (hvsti > 1) { // case 2: a portion of the total biomass is assumed to be yield
                BioYield = BioAct * (1 - (1 / (1 + HarvIndex)));
            }

            // amount of nitrogen (kg N/ha) to be removed from the HRU
            NYield = BioNAct * (BioYield / BioAct);
            NYield = (NYield < 0) ? 0 : NYield;

            if (idc == 7) {
                FPHUact = 0;
            } else {
                FPHUact = Math.min(FPHUact, 1);
                FPHUact = (FPHUact * (1 - (BioYield / BioAct)));
            }
            PHUact = phu * FPHUact;

            BioAct -= BioYield;
            BioAct = (BioAct <= 0) ? 0 : BioAct;    // crop biomass is always present
            BioNAct -= NYield;
            BioNAct = (BioNAct < 0) ? 0 : BioNAct;  // crop N is always present

            fracharvest = 1 - (BioYield / BioAct);
            fracharvestn = 1 - (NYield / BioNAct);
        } else {
            /*
			 * For harvesting, four codes are implemented:
			 * 1 --> assumes harvesting with main crop and a cover crop; plant growth stopped
			 * 2 --> assumes harvesting with main crop; the cover crop remains on the field, plant growth stopped (former kill op)
			 * 3 --> assumes harvesting with main crop and a cover crop; plant growth continues (may not be suitable for meadows)
			 * 4 --> assumes harvesting with main crop; plant growth continues
			 *
			 * If harvest is determined by code 1, the above-ground plant biomass is removed as dry economic yield.
			 * For majority of crops, harvest index is between 0 and 1; however, for plants whose roots are harvested
			 * the harvest index may be > 1. Harvest index is calculated for each day of growing season.
             */
            HarvIndex = hvsti * (100 * FPHUact) / (100 * FPHUact + Math.exp(11.1 - 10.0 * FPHUact));
            HarvIndex = Math.max(HarvIndex, hvsti * 0.75);

            BioagAct = (1 - frRootAct) * BioAct;

            // total yield biomass at harvest
            // case 1: the total biomass is assumed to be yield
            BioYield = (hvsti <= 1) ? BioagAct * HarvIndex : BioAct * (1 - (1 / (1 + HarvIndex)));
            NYield = cnyld * BioYield;
            if (NYield > BioNAct * (NYield / (NYield + ((BioAct - BioYield) * (bn3 / 2.0))))) {
                NYield = BioNAct * (NYield / (NYield + ((BioAct - BioYield) * (bn3 / 2.0))));
            }
            fracharvest = 1 - (BioYield / BioAct);
            fracharvestn = 1 - (NYield / BioNAct);
        }
        return true;
    }

    private void calc_cropyield_ha() {
        NYield_ha = NYield * area_ha / 10000;
    }

    private boolean calc_residues() {
        if (idc == 7) {
            Addresidue_pool = BioYield;
            Addresidue_pooln = NYield;
        } else if (idc == 1 || idc == 2 || idc == 4 || idc == 5) {
            Addresidue_pool = BioAct - BioYield;
            Addresidue_pooln = BioNAct - NYield;
        } else if (idc == 6 || idc == 3) {
            Addresidue_pool = BioYield * 0.1;
            Addresidue_pooln = NYield * 0.1;
            Addresidue_pool = Math.min(Addresidue_pool, BioAct);
            Addresidue_pooln = Math.min(Addresidue_pooln, BioNAct);

            BioAct -= Addresidue_pool;
            BioNAct -= Addresidue_pooln;
        } else if (idc == 8) {
            Addresidue_pool = BioYield * 0.1;
            Addresidue_pooln = NYield * 0.1;
            Addresidue_pool = Math.min(Addresidue_pool, BioAct);
            Addresidue_pooln = Math.min(Addresidue_pooln, BioNAct);

            BioAct -= Addresidue_pool;
            BioNAct -= Addresidue_pooln;
        }
        return true;
    }
}
