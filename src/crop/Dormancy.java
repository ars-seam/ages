/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Dormancy {
    private static final Logger log
            = Logger.getLogger("oms3.model." + Dormancy.class.getSimpleName());

    public double sunhmax;
    public double latitude;
    public double tbase;
    public double tmean;
    public double FPHUact;
    public boolean dormancy;
    public double sunhmin;

    public void execute() {
        double sunhminrun = sunhmin > 0 ? sunhmin : sunhmax;
        sunhminrun = Math.min(sunhminrun, sunhmax);

        double tdorm = 0;

        if (latitude < 20) {
            tdorm = 0;
        } else if (latitude < 40) {
            tdorm = (latitude - 20) / 20;
        } else {
            tdorm = 1;
        }

        if (sunhmax < (sunhminrun + tdorm)) {
            dormancy = true;
        } else {
            dormancy = tmean < tbase ? true : false;
        }
        if (FPHUact > 1) {
            dormancy = true;
        }
        sunhmin = sunhminrun;

        if (log.isLoggable(Level.INFO)) {
            log.info("sunhmin:" + sunhmin);
        }
    }
}
