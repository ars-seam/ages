/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package crop;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add PlantGrowthStress module definition here")
@Author(name = "Olaf David, Manfred Fink, Robert Streetman, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Plant growth")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/crop/PlantGrowthStress.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/crop/PlantGrowthStress.xml")
public class PlantGrowthStressAdapter extends AnnotatedAdapter {
    @Description("UPGM flag")
    @Input public boolean flagUPGM;

    @Description("Actual HRU Transpiration")
    @Units("mm")
    @Input public double aTransp;

    @Description("Potential HRU Transpiration")
    @Units("mm")
    @Input public double pTransp;

    @Description("Optimal Biomass N Content")
    @Units("kgN/ha")
    @Input public double BioNOpt;

    @Description("Actual Biomass N Content")
    @Units("kgN/ha")
    @Input public double BioNAct;

    @Description("Actual N Uptake") //UPGM
    @Units("kgN/ha")
    @Input public double actnup;

    @Description("Daily biomass increase")
    @Units("kg/ha")
    @Input public double BioOpt_delta;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Base growth temperature")
    @Units("C")
    @Input public double tbase;

    @Description("Optimum growth temperature")
    @Units("C")
    @Input public double topt;

    @Description("Plant Existing Flag")
    @Input public boolean cropExists;

    @Description("Current HRU")
    @Input public HRU hru;

    @Description("Actual Crop Biomass")
    @Units("kg/ha")
    @Optional
    @Input @Output public double BioAct;

    @Description("Temperature Growth Stress Factor")
    @Optional
    @Input @Output public double tstrs;

    @Description("delta biomass increase per day")
    @Optional
    @Input @Output public double deltabiomass;

    @Description("N Growth Stress Factor")
    @Output public double nstrs;

    @Description("Water Growth Stress Factor")
    @Output public double wstrs;

    @Override
    protected void run(Context context) {
        if (flagUPGM) {
            executeUPGM();
        } else {
            executeSWAT();
        }
    }

    private void executeSWAT() {
        PlantGrowthStressSWAT component = new PlantGrowthStressSWAT();

        component.aTransp = aTransp;
        component.pTransp = pTransp;
        component.BioNOpt = BioNOpt;
        component.BioNAct = BioNAct;
        component.actnup = actnup;
        component.BioOpt_delta = BioOpt_delta;
        component.tmean = tmean;
        component.tbase = tbase;
        component.topt = topt;
        component.plantExisting = cropExists;
        component.BioAct = BioAct;

        component.execute();

        BioAct = component.BioAct;
        tstrs = component.tstrs;
        nstrs = component.nstrs;
        wstrs = component.wstrs;
        deltabiomass = component.deltabiomass;
    }

    private void executeUPGM() {
        PlantGrowthStressUPGM component = new PlantGrowthStressUPGM();

        component.aTransp = aTransp;
        component.pTransp = pTransp;
        component.BioNOpt = BioNOpt;
        component.BioNAct = BioNAct;

        component.execute();

        nstrs = component.nstrs;
        wstrs = component.wstrs;
    }
}
