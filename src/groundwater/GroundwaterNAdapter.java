/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package groundwater;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Description("Add GroundwaterN module definition here")
@Author(name = "Olaf David, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Groundwater")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/groundwater/GroundwaterN.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/groundwater/GroundwaterN.xml")
public class GroundwaterNAdapter extends AnnotatedAdapter {
    @Description("relative size of the groundwaterN damping tank RG1")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 10.0)
    @Input public double N_delay_RG1;

    @Description("relative size of the groundwaterN damping tank RG2")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 10.0)
    @Input public double N_delay_RG2;

    @Description("HRU Concentration for RG1")
    @Units("mgN/l")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 10.0)
    @Input public double N_concRG1;

    @Description("HRU Concentration for RG2")
    @Units("mgN/l")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 10.0)
    @Input public double N_concRG2;

    @Description("maximum RG1 storage")
    @Input public double maxRG1;

    @Description("maximum RG2 storage")
    @Input public double maxRG2;

    @Description("portion of percolation to RG1")
    @Units("l")
    @Input public double pot_RG1;

    @Description("portion of percolation to RG2")
    @Units("l")
    @Input public double pot_RG2;

    @Description("actual RG1 storage")
    @Input public double actRG1;

    @Description("actual RG2 storage")
    @Input public double actRG2;

    @Description("RG1 inflow")
    @Input public double inRG1;

    @Description("RG2 inflow")
    @Input public double inRG2;

    @Description("HRU statevar RD2 outflow")
    @Input public double outRG1;

    @Description("HRU statevar RG2 outflow")
    @Input public double outRG2;

    @Description("Current hru object")
    @Input public HRU hru;

    @Description("N Percolation out of the soil profile")
    @Units("kgN")
    @Input public double PercoNabs;

    @Description("RG1 N inflow")
    @Units("kgN")
    @Input @Output public double N_RG1_in;

    @Description("RG2 N inflow")
    @Units("kgN")
    @Input @Output public double N_RG2_in;

    @Description("RG1 N outflow")
    @Units("kgN")
    @Input @Output public double N_RG1_out;

    @Description("RG2 N outflow")
    @Units("kgN")
    @Input @Output public double N_RG2_out;

    @Description("gwExcess")
    @Input @Output public double gwExcess;

    @Description("NExcess")
    @Input @Output public double NExcess;

    @Description("actual RG1 N storage")
    @Units("kgN")
    @Optional
    @Input @Output public double NActRG1;

    @Description("Actual RG2 N storage")
    @Units("kgN")
    @Optional
    @Input @Output public double NActRG2;

    private Map<HRU, GroundwaterN> componentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        GroundwaterN component = componentMap.get(hru);
        if (component == null) {
            component = new GroundwaterN();
            componentMap.put(hru, component);
        }

        component.N_delay_RG1 = N_delay_RG1;
        component.N_delay_RG2 = N_delay_RG2;
        component.N_concRG1 = N_concRG1;
        component.N_concRG2 = N_concRG2;
        component.maxRG1 = maxRG1;
        component.maxRG2 = maxRG2;
        component.pot_RG1 = pot_RG1;
        component.pot_RG2 = pot_RG2;
        component.actRG1 = actRG1;
        component.actRG2 = actRG2;
        component.inRG1 = inRG1;
        component.inRG2 = inRG2;
        component.outRG1 = outRG1;
        component.outRG2 = outRG2;
        component.PercoNabs = PercoNabs;
        component.N_RG1_in = N_RG1_in;
        component.N_RG2_in = N_RG2_in;
        component.N_RG1_out = N_RG1_out;
        component.N_RG2_out = N_RG2_out;
        component.gwExcess = gwExcess;
        component.NExcess = NExcess;
        component.NActRG1 = NActRG1;
        component.NActRG2 = NActRG2;

        component.execute();

        N_RG1_in = component.N_RG1_in;
        N_RG2_in = component.N_RG2_in;
        N_RG1_out = component.N_RG1_out;
        N_RG2_out = component.N_RG2_out;
        gwExcess = component.gwExcess;
        NExcess = component.NExcess;
        NActRG1 = component.NActRG1;
        NActRG2 = component.NActRG2;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
