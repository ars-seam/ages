/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package groundwater;

import java.util.logging.Logger;

public class GroundwaterN {
    private static final Logger log
            = Logger.getLogger("oms3.model." + GroundwaterN.class.getSimpleName());

    public double N_delay_RG1;
    public double N_delay_RG2;
    public double N_concRG1;
    public double N_concRG2;
    public double maxRG1;
    public double maxRG2;
    public double pot_RG1;
    public double pot_RG2;
    public double actRG1;
    public double actRG2;
    public double inRG1;
    public double inRG2;
    public double outRG1;
    public double outRG2;
    public double PercoNabs;
    public double N_RG1_in;
    public double N_RG2_in;
    public double N_RG1_out;
    public double N_RG2_out;
    public double gwExcess;
    public double NExcess;
    public double NActRG1;
    public double NActRG2;

    boolean init = false;

    public void execute() {
        if (!init) {
            NActRG1 = (maxRG1 * N_concRG1 / 1000000) * N_delay_RG1;
            NActRG2 = (maxRG2 * N_concRG2 / 1000000) * N_delay_RG2;
            init = true;
        }

        double runNActRG1 = NActRG1;
        double runNActRG2 = NActRG2;

        double partN_Excess = 0;
        double partN_RG1 = 0;
        double partN_RG2 = 0;

        double percwatersum = pot_RG1 + pot_RG2 + gwExcess;
        if (percwatersum > 0) {
            partN_RG1 = (pot_RG1 / percwatersum) * PercoNabs;
            partN_RG2 = (pot_RG2 / percwatersum) * PercoNabs;
            partN_Excess = (gwExcess / percwatersum) * PercoNabs;
        }

        double watersum_RG1 = actRG1 + outRG1 + (maxRG1 * N_delay_RG1);
        double watersum_RG2 = actRG2 + outRG2 + (maxRG2 * N_delay_RG2);
        NExcess += partN_Excess;

        runNActRG1 += N_RG1_in + partN_RG1;
        runNActRG2 += N_RG2_in + partN_RG2;

        double runN_concRG1 = 0;
        if (watersum_RG1 > 0) {
            runN_concRG1 = runNActRG1 * 1000000 / watersum_RG1;
        }

        double runN_concRG2 = 0;
        if (watersum_RG2 > 0) {
            runN_concRG2 = runNActRG2 * 1000000 / watersum_RG2;
        }

        double runN_RG1_out = (outRG1 * runN_concRG1) / 1000000;
        double runN_RG2_out = (outRG2 * runN_concRG2) / 1000000;

        if (runN_RG1_out > runNActRG1) {
            runN_RG1_out = runNActRG1;
        }

        if (runN_RG2_out > runNActRG2) {
            runN_RG2_out = runNActRG2;
        }

        runNActRG1 -= runN_RG1_out;
        runNActRG2 -= runN_RG2_out;

        N_RG1_in = 0;
        N_RG2_in = 0;
        N_RG1_out = runN_RG1_out;
        N_RG2_out = runN_RG2_out;
        NActRG1 = runNActRG1;
        NActRG2 = runNActRG2;
    }
}
