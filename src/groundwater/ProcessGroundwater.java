/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package groundwater;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessGroundwater {
    private static final Logger log
            = Logger.getLogger("oms3.model." + ProcessGroundwater.class.getSimpleName());

    public double gwRG1Fact;
    public double gwRG2Fact;
    public double gwRG1RG2dist;
    public double gwCapRise;
    public double initRG1;
    public double initRG2;
    public double slope;
    public double RG1_k;
    public double RG2_k;
    public double percolation;
    public double soilMaxMPS;
    public double RG1_max;
    public double RG2_max;
    public double area;
    public double gwExcess;
    public double actRG1;
    public double actRG2;
    public double inRG1;
    public double inRG2;
    public double maxRG1;
    public double maxRG2;
    public double soilActMPS;
    public double pot_RG1;
    public double pot_RG2;
    public double outRG1;
    public double outRG2;
    public double genRG1;
    public double genRG2;

    boolean init = true;

    public void execute() {
        if (init) {
            maxRG1 = RG1_max * area;
            maxRG2 = RG2_max * area;
            actRG1 = maxRG1 * initRG1;
            actRG2 = maxRG2 * initRG2;
            init = false;
        }

        outRG1 = 0;
        outRG2 = 0;

        replenishSoilStor(soilMaxMPS);
        redistRG1_RG2_in();
        distRG1_RG2();
        calcLinGWout();

        if (log.isLoggable(Level.INFO)) {
            log.info("soilActMPS:" + soilActMPS);
        }
    }

    private void replenishSoilStor(double maxSoilStor) {
        double deltaSoilStor = maxSoilStor - soilActMPS;
        double sat_SoilStor = 0;
        double inSoilStor = 0;
        if ((soilActMPS > 0) && (maxSoilStor > 0)) {
            sat_SoilStor = soilActMPS / maxSoilStor;
        } else {
            sat_SoilStor = 0.000001;
        }
        if (actRG2 > deltaSoilStor) {
            inSoilStor = deltaSoilStor * (1. - Math.exp(-1 * gwCapRise / sat_SoilStor));
        }
        soilActMPS += inSoilStor;
        actRG2 -= inSoilStor;
    }

    private void redistRG1_RG2_in() {
        if (inRG1 > 0) {
            double deltaRG1 = maxRG1 - actRG1;
            if (inRG1 <= deltaRG1) {
                actRG1 += inRG1;
            } else {
                actRG1 = maxRG1;
                outRG1 += (inRG1 - deltaRG1);
            }
        }
        if (inRG2 > 0) {
            double deltaRG2 = maxRG2 - actRG2;
            if (inRG2 <= deltaRG2) {
                actRG2 += inRG2;
            } else {
                actRG2 = maxRG2;
                outRG2 += (inRG2 - deltaRG2);
            }
        }
    }

    private void distRG1_RG2() {
        double slope_weight = Math.tan(slope * (Math.PI / 180.));
        double gradh = ((1 - slope_weight) * gwRG1RG2dist);
        if (gradh < 0) {
            gradh = 0;
        } else if (gradh > 1) {
            gradh = 1;
        }

        pot_RG1 = ((1 - gradh) * percolation);
        pot_RG2 = (gradh * percolation);
        actRG1 += pot_RG1;
        actRG2 += pot_RG2;

        // determine if incoming fluxes can be stored in groundwater storage compartments
        double delta_RG2 = actRG2 - maxRG2;
        if (delta_RG2 > 0) {
            actRG1 += delta_RG2;
            actRG2 = maxRG2;
            pot_RG1 += delta_RG2;
            pot_RG2 -= delta_RG2;
        }
        double delta_RG1 = actRG1 - maxRG1;
        if (delta_RG1 > 0) {
            gwExcess += delta_RG1;
            actRG1 = maxRG1;
        }
    }

    private void calcLinGWout() {
        double k_rg1 = 1 / (RG1_k * gwRG1Fact);
        if (k_rg1 > 1) {
            k_rg1 = 1;
        }
        double rg1_out = k_rg1 * actRG1;
        actRG1 -= rg1_out;
        outRG1 += rg1_out;

        double k_rg2 = 1 / (RG2_k * gwRG2Fact);
        if (k_rg2 > 1) {
            k_rg2 = 1;
        }
        double rg2_out = k_rg2 * actRG2;
        actRG2 -= rg2_out;
        outRG2 += rg2_out;
        genRG1 = rg1_out;
        genRG2 = rg2_out;
    }
}
