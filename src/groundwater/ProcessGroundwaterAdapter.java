/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package groundwater;

import ages.types.HRU;
import ages.types.HydroGeology;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Description("Add ProcessGroundwater module definition here")
@Author(name = "Olaf David, Peter Krause, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Groundwater")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/groundwater/ProcessGroundwater.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/groundwater/ProcessGroundwater.xml")
public class ProcessGroundwaterAdapter extends AnnotatedAdapter {
    @Description("adaptation of RG1 outflow")
    @Role(PARAMETER)
    @Input public double gwRG1Fact;

    @Description("adaptation of RG2 outflow")
    @Role(PARAMETER)
    @Input public double gwRG2Fact;

    @Description("RG1-RG2 distribution coefficient")
    @Role(PARAMETER)
    @Input public double gwRG1RG2dist;

    @Description("capillary rise coefficient")
    @Role(PARAMETER)
    @Input public double gwCapRise;

    @Description("groundwater init")
    @Role(PARAMETER)
    @Input public double initRG1;

    @Description("groundwater init")
    @Role(PARAMETER)
    @Input public double initRG2;

    @Description("attribute slope")
    @Input public HRU hru;

    @Description("hydro geology")
    @Input public HydroGeology hydroGeology;

    @Description("HRU statevar percolation")
    @Units("l")
    @Input public double percolation;

    @Description("HRU attribute maximum MPS of soil")
    @Input public double soilMaxMPS;

    @Description("gwExcess")
    @Input @Output public double gwExcess;

    @Description("actual RG1 storage")
    @Optional
    @Input @Output public double actRG1;

    @Description("actual RG2 storage")
    @Optional
    @Input @Output public double actRG2;

    @Description("RG1 inflow")
    @Input @Output public double inRG1;

    @Description("RG2 inflow")
    @Input @Output public double inRG2;

    @Description("maximum RG1 storage")
    @Optional
    @Input @Output public double maxRG1;

    @Description("maximum RG2 storage")
    @Optional
    @Input @Output public double maxRG2;

    @Description("HRU state var actual MPS of soil")
    @Input @Output public double soilActMPS;

    @Description("portion of percolation to RG1")
    @Units("l")
    @Output public double pot_RG1;

    @Description("portion of percolation to RG2")
    @Units("l")
    @Output public double pot_RG2;

    @Description("HRU statevar RD2 outflow")
    @Output public double outRG1;

    @Description("HRU statevar RG2 outflow")
    @Output public double outRG2;

    @Description("RG1 generation")
    @Output public double genRG1;

    @Description("RG2 generation")
    @Output public double genRG2;

    private Map<HRU, ProcessGroundwater> componentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        ProcessGroundwater component = componentMap.get(hru);
        if (component == null) {
            component = new ProcessGroundwater();
            componentMap.put(hru, component);
        }

        component.gwRG1Fact = gwRG1Fact;
        component.gwRG2Fact = gwRG2Fact;
        component.gwRG1RG2dist = gwRG1RG2dist;
        component.gwCapRise = gwCapRise;
        component.initRG1 = initRG1;
        component.initRG2 = initRG2;
        component.slope = hru.slope;
        component.RG1_k = hydroGeology.RG1_k;
        component.RG2_k = hydroGeology.RG2_k;
        component.percolation = percolation;
        component.soilMaxMPS = soilMaxMPS;
        component.RG1_max = hydroGeology.RG1_max;
        component.RG2_max = hydroGeology.RG2_max;
        component.area = hru.area;
        component.gwExcess = gwExcess;
        component.actRG1 = actRG1;
        component.actRG2 = actRG2;
        component.inRG1 = inRG1;
        component.inRG2 = inRG2;
        component.maxRG1 = maxRG1;
        component.maxRG2 = maxRG2;
        component.soilActMPS = soilActMPS;

        component.execute();

        gwExcess = component.gwExcess;
        actRG1 = component.actRG1;
        actRG2 = component.actRG2;
        inRG1 = component.inRG1;
        inRG2 = component.inRG2;
        maxRG1 = component.maxRG1;
        maxRG2 = component.maxRG2;
        soilActMPS = component.soilActMPS;
        pot_RG1 = component.pot_RG1;
        pot_RG2 = component.pot_RG2;
        outRG1 = component.outRG1;
        outRG2 = component.outRG2;
        genRG1 = component.genRG1;
        genRG2 = component.genRG2;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
