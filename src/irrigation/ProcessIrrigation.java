/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package irrigation;

public class ProcessIrrigation {
    public double surfacetemp;
    public double irri_depth;
    public double area;
    public int irri_type;
    public int cropId;
    public int irrigationCropId;
    public double depletionTriggerLevel;
    public double depletionRefillLevel;
    public double zrootd;
    public double soilSat;
    public double[] depth_h;
    public double[] satMPS_h;
    public boolean doIrrigate;
    public double irrigation_amount;

    private double computeIrrigationAmount() {
        return irri_depth * area;
    }

    private boolean canIrrigate() {
        // time constraints need not be checked
        switch (irri_type) {
            case 1:
                // fixed date by crop needs to verify same crop id
                return cropId != -1 && irrigationCropId == cropId;
            case 2:
            case 3:
                // actual date and fixed interval no special conditions outside of the time
                return true;
            case 4:
                // depletion level needs to check soil water content
                double root_swc = computeRootSWC();
                if (doIrrigate) {
                    // already irrigating thus irrigation was triggered
                    // continue irrigating until refill level is met
                    return root_swc < depletionRefillLevel;
                } else {
                    // not currently irrigating
                    // start irrigating if below trigger level
                    return root_swc < depletionTriggerLevel;
                }
            default:
                // unknown types cannot irrigate
                return false;
        }
    }

    private double computeRootSWC() {
        if (zrootd > 0) {
            int hor = 0;
            double totalDepth = 0.;

            while (totalDepth < zrootd) {
                totalDepth += (depth_h[hor] / 100); // convert from cm to m
                hor++;
            }

            double sum_swc = 0;

            for (int i = 0; i < hor; i++) {
                sum_swc += satMPS_h[i];
            }
            return sum_swc / hor;
        } else {
            return soilSat;
        }
    }

    public void execute() {
        irrigation_amount = 0;
        doIrrigate = false;

        if (surfacetemp > 1) {
            doIrrigate = canIrrigate();
            if (doIrrigate) {
                irrigation_amount = computeIrrigationAmount();
            }
        }
    }
}
