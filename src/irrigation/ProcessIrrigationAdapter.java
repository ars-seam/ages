/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package irrigation;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import management.ActualDateIrrigation;
import management.DepletionLevelIrrigation;
import management.FixedCropDateIrrigation;
import management.FixedIntervalIrrigation;
import management.Irrigation;

@Description("Add ProcessIrrigation module definition here")
@Author(name = "Holm Kipka, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Irrigation, Hydrology")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/irrigation/ProcessIrrigation.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/irrigation/ProcessIrrigation.xml")
public class ProcessIrrigationAdapter extends AnnotatedAdapter {
    @Description("Output soil surface temperature")
    @Units("C")
    @Input public double surfacetemp;

    @Description("Current irrigation ")
    @Input public Irrigation irrigation;

    @Description("curent hru")
    @Input public HRU hru;

    @Description("Current crop")
    @Input public Crop crop;

    @Description("Actual depth of roots")
    @Units("m")
    @Input public double zrootd;

    @Description("soil")
    @Input public SoilType soil;

    @Description("Soil water h")
    @Input public double[] satMPS_h;

    @Description("Actual soilSat")
    @Optional
    @Input public double soilSat;

    @Optional
    @Description("flag whether irrigation was run today")
    @Input @Output public boolean doIrrigate;

    @Description("Current irrigation amount in liter for the whole hru")
    @Output public double irrigation_amount;

    @Override
    protected void run(Context context) {
        if (irrigation == null) {
            doIrrigate = false;
            irrigation_amount = 0.0;
            return;
        }
        ProcessIrrigation component = new ProcessIrrigation();

        component.surfacetemp = surfacetemp;
        component.irri_depth = irrigation.getDepth();
        component.area = hru.area;
        component.doIrrigate = doIrrigate;

        if (irrigation instanceof FixedCropDateIrrigation) {
            FixedCropDateIrrigation irri = (FixedCropDateIrrigation) irrigation;

            component.irri_type = 1;
            component.cropId = crop.cid;
            component.irrigationCropId = irri.getCropId();
        } else if (irrigation instanceof ActualDateIrrigation) {
            component.irri_type = 2;
        } else if (irrigation instanceof FixedIntervalIrrigation) {
            component.irri_type = 3;
        } else if (irrigation instanceof DepletionLevelIrrigation) {
            DepletionLevelIrrigation irri = (DepletionLevelIrrigation) irrigation;

            component.irri_type = 4;
            component.depletionTriggerLevel = irri.getDepletionTriggerLevel();
            component.depletionRefillLevel = irri.getDepletionRefillLevel();
            component.zrootd = zrootd;
            component.soilSat = soilSat;
            component.depth_h = soil.depth_h;
            component.satMPS_h = satMPS_h;
        }

        component.execute();

        doIrrigate = component.doIrrigate;
        irrigation_amount = component.irrigation_amount;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
