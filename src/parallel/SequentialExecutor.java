
package parallel;

import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class SequentialExecutor implements Runnable {
    private final Iterable<EntityTask> tasks;

    public SequentialExecutor(List<EntityTask> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void run() {
        // Sequentially execute each task
        for (EntityTask task : tasks) {
            task.run();
        }
    }
}
