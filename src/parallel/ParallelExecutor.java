
package parallel;

import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class ParallelExecutor implements Runnable {
    private final List<EntityTask> tasks;

    public ParallelExecutor(List<EntityTask> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void run() {
        // Execute every task in parallel
        tasks.parallelStream().forEach((task) -> {
            task.run();
        });
    }
}
