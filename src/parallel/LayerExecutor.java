
package parallel;

import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class LayerExecutor implements Runnable {
    private final List<List<EntityTask>> tasks;

    public LayerExecutor(List<List<EntityTask>> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void run() {
        // sequentially execute each layer of tasks
        for (List<EntityTask> layer : tasks) {
            // parallel execute each task in a layer
            layer.parallelStream().forEach((task) -> {
                task.run();
            });
        }
    }
}
