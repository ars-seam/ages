
package parallel;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.Program;
import java.util.function.Consumer;

/**
 *
 * @author Nathan Lighthart
 */
public class ContextTask implements Runnable {
    private Context context;
    private Consumer<Context> task;

    public ContextTask(Context context, Program program) {
        this(context, new ConsumableProgram(program));
    }

    public ContextTask(Context context, Consumer<Context> task) {
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        }
        if (task == null) {
            throw new IllegalArgumentException("Task cannot be null");
        }
        this.context = context;
        this.task = task;
    }

    @Override
    public void run() {
        task.accept(context);
    }

    private static class ConsumableProgram implements Consumer<Context> {
        private final Program program;

        public ConsumableProgram(Program program) {
            this.program = program;
        }

        @Override
        public void accept(Context context) {
            program.execute(context);
        }
    }
}
