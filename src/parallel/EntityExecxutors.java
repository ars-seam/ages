
package parallel;

import ages.types.HRU;
import ages.types.StreamReach;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import routing.RoutingTable;

/**
 *
 * @author Nathan Lighthart
 */
public class EntityExecxutors {
    public static Runnable sequential(List<EntityTask> tasks) {
        return new SequentialExecutor(tasks);
    }

    public static Runnable sequentialHRU(List<HRU> hrus, Map<Integer, ? extends Runnable> hruTasks) {
        List<EntityTask> programs = createTasksHRU(hrus, hruTasks);
        return sequential(programs);
    }

    public static Runnable sequentialReach(List<StreamReach> reaches, Map<Integer, ? extends Runnable> reachTasks) {
        List<EntityTask> programs = createTasksReach(reaches, reachTasks);
        return sequential(programs);
    }

    public static Runnable parallel(List<EntityTask> tasks) {
        return new ParallelExecutor(tasks);
    }

    public static Runnable parallelHRU(List<HRU> hrus, Map<Integer, ? extends Runnable> hruTasks) {
        List<EntityTask> tasks = createTasksHRU(hrus, hruTasks);
        return parallel(tasks);
    }

    public static Runnable parallelReach(List<StreamReach> reaches, Map<Integer, ? extends Runnable> reachTasks) {
        List<EntityTask> tasks = createTasksReach(reaches, reachTasks);
        return parallel(tasks);
    }

    public static Runnable layer(List<List<EntityTask>> tasksList) {
        return new LayerExecutor(tasksList);
    }

    public static Runnable layerHRU(List<HRU> hrus, RoutingTable routingTable, Map<Integer, ? extends Runnable> hruTasks) {
        List<List<HRU>> hruList = Layer.layerSortHRUs(hrus, routingTable);
        List<List<EntityTask>> tasksList = createTasksListHRU(hruList, hruTasks);
        return layer(tasksList);
    }

    public static Runnable layerReach(List<StreamReach> reaches, RoutingTable routingTable, Map<Integer, ? extends Runnable> reachTasks) {
        List<List<StreamReach>> reachList = Layer.layerSortReaches(reaches, routingTable);
        List<List<EntityTask>> tasksList = createTasksListReach(reachList, reachTasks);
        return layer(tasksList);
    }

    public static Runnable tango(List<List<EntityTask>> tasksList) {
        return new LayerExecutor(tasksList);
    }

    public static Runnable tangoHRU(List<HRU> hrus, RoutingTable routingTable, Map<Integer, ? extends Runnable> hruTasks) {
        List<List<HRU>> hruList = TangoSort.sortHRUs(hrus, routingTable);
        List<List<EntityTask>> tasksList = createTasksListHRU(hruList, hruTasks);
        return tango(tasksList);
    }

    public static Runnable tangoReach(List<StreamReach> reaches, RoutingTable routingTable, Map<Integer, ? extends Runnable> reachTasks) {
        List<List<StreamReach>> reachList = TangoSort.sortReaches(reaches, routingTable);
        List<List<EntityTask>> tasksList = createTasksListReach(reachList, reachTasks);
        return tango(tasksList);
    }

    public static Runnable forest(List<List<EntityTask>> tasksList) {
        return new ForestExecutor(tasksList);
    }

    public static Runnable forestHRU(List<HRU> hrus, RoutingTable routingTable, Map<Integer, ? extends Runnable> hruTasks) {
        List<List<HRU>> hruList = DisjointForest.getForestHRUs(hrus, routingTable);
        List<List<EntityTask>> tasksList = createTasksListHRU(hruList, hruTasks);
        return forest(tasksList);
    }

    public static Runnable forestReach(List<StreamReach> reaches, RoutingTable routingTable, Map<Integer, ? extends Runnable> reachTasks) {
        List<List<StreamReach>> reachList = DisjointForest.getForestReaches(reaches, routingTable);
        List<List<EntityTask>> tasksList = createTasksListReach(reachList, reachTasks);
        return forest(tasksList);
    }

    public static Runnable forHRU(String flagParallel, List<HRU> hrus, RoutingTable routingTable, Map<Integer, ? extends Runnable> hruTasks) {
        if (flagParallel == null) {
            return sequentialHRU(hrus, hruTasks);
        }

        flagParallel = flagParallel.toLowerCase();
        switch (flagParallel) {
            case "layer":
                return layerHRU(hrus, routingTable, hruTasks);
            case "tango":
                return tangoHRU(hrus, routingTable, hruTasks);
            case "forest":
                return forestHRU(hrus, routingTable, hruTasks);
            default:
                return sequentialHRU(hrus, hruTasks);
        }
    }

    public static Runnable forReach(String flagParallel, List<StreamReach> reaches, RoutingTable routingTable, Map<Integer, ? extends Runnable> reachTasks) {
        if (flagParallel == null) {
            return sequentialReach(reaches, reachTasks);
        }

        flagParallel = flagParallel.toLowerCase();
        switch (flagParallel) {
            case "layer":
                return layerReach(reaches, routingTable, reachTasks);
            case "tango":
                return tangoReach(reaches, routingTable, reachTasks);
            case "forest":
                return forestReach(reaches, routingTable, reachTasks);
            default:
                return sequentialReach(reaches, reachTasks);
        }
    }

    private static List<EntityTask> createTasksHRU(List<HRU> hrus, Map<Integer, ? extends Runnable> hruTasks) {
        List<EntityTask> tasks = new ArrayList<>(hrus.size());
        for (HRU hru : hrus) {
            Runnable hruTask = hruTasks.get(hru.ID);
            EntityTask task = new EntityTask(hru.ID, hruTask);
            tasks.add(task);
        }
        return tasks;
    }

    private static List<EntityTask> createTasksReach(List<StreamReach> reaches, Map<Integer, ? extends Runnable> reachTasks) {
        List<EntityTask> tasks = new ArrayList<>(reaches.size());
        for (StreamReach reach : reaches) {
            Runnable reachTask = reachTasks.get(reach.ID);
            EntityTask task = new EntityTask(reach.ID, reachTask);
            tasks.add(task);
        }
        return tasks;
    }

    private static List<List<EntityTask>> createTasksListHRU(List<List<HRU>> hruList, Map<Integer, ? extends Runnable> hruTasks) {
        List<List<EntityTask>> tasksList = new ArrayList<>(hruList.size());
        for (List<HRU> hrus : hruList) {
            List<EntityTask> tasks = new ArrayList<>(hrus.size());
            tasksList.add(tasks);
            for (HRU hru : hrus) {
                Runnable hruTask = hruTasks.get(hru.ID);
                EntityTask task = new EntityTask(hru.ID, hruTask);
                tasks.add(task);
            }
        }
        return tasksList;
    }

    private static List<List<EntityTask>> createTasksListReach(List<List<StreamReach>> reachList, Map<Integer, ? extends Runnable> reachTasks) {
        List<List<EntityTask>> tasksList = new ArrayList<>(reachList.size());
        for (List<StreamReach> reaches : reachList) {
            List<EntityTask> tasks = new ArrayList<>(reaches.size());
            tasksList.add(tasks);
            for (StreamReach reach : reaches) {
                Runnable reachTask = reachTasks.get(reach.ID);
                EntityTask task = new EntityTask(reach.ID, reachTask);
                tasks.add(task);
            }
        }
        return tasksList;
    }

    private EntityExecxutors() {
    }
}
