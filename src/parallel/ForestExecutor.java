
package parallel;

import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class ForestExecutor implements Runnable {
    private List<List<EntityTask>> tasks;

    public ForestExecutor(List<List<EntityTask>> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void run() {
        // parallel execute each disjoint forest of tasks
        tasks.parallelStream().forEach((forest) -> {
            // sequentially execute each task in a forest
            for (EntityTask task : forest) {
                task.run();
            }
        });
    }
}
