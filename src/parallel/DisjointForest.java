/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package parallel;

import annotations.VersionInfo;
import annotations.Bibliography;
import annotations.SourceInfo;
import annotations.License;
import annotations.Status;
import annotations.Author;
import annotations.Documentation;
import annotations.Keywords;
import gov.usda.jcf.annotations.Description;
import ages.types.HRU;
import ages.types.StreamReach;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import routing.RoutingEdge;
import routing.RoutingTable;

@Description("Add DisjointForest module definition here")
@Author(name = "Daniel Elliott, Nathan Lighthart", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/parallel/DisjointForest.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/parallel/DisjointForest.xml")
public class DisjointForest {
    public static List<List<HRU>> getForestHRUs(List<HRU> hrus, RoutingTable routingTable) {
        HashMap<Integer, HRU> hruMap = new HashMap<>();
        for (HRU h : hrus) {
            hruMap.put(h.ID, h);
        }
        // get hruUndir
        HashMap<Integer, List<Integer>> adjList = HRUListToUndirAdjList(hruMap, routingTable);
        // get components
        List<List<HRU>> forest = findGraphComponentsHRUs(adjList, hruMap, routingTable);

        return forest;
    }

    public static boolean verifyForestHRUs(List<List<HRU>> forest, RoutingTable routingTable) {
        boolean validSort = true; // assumed valid until proven otherwise
        boolean debug = true; // for controlling stderr

        // check to see if intersections of each subtree are empty
        Set<Integer> temp1;
        Set<Integer> temp2;
        for (int i = 0; i < forest.size(); i++) {
            temp1 = new TreeSet<>();
            for (HRU h : forest.get(i)) {
                temp1.add(h.ID);
            }
            for (int j = i + 1; j < forest.size(); j++) {
                temp2 = new TreeSet<>();
                for (HRU h : forest.get(j)) {
                    temp2.add(h.ID);
                }
                temp2.retainAll(temp1); // take the intersection
                if (!temp2.isEmpty()) {
                    validSort = false;
                    if (debug) {
                        String ids = "";
                        for (Integer id : temp2) {
                            ids += (id.toString() + " ");
                        }
                        System.err.printf("ERROR: verifyForest invalid on intersections on i=%d j=%d, overlapping elements %s\n", i, j, ids);
                    }
                }
            }
        }

        /* check to see if each subtree is in the proper topological order;
		 * leverage the fact that the HRU.depth property reflects a universal topological ordering
         */
        for (List<HRU> tree : forest) {
            int min = routingTable.getRoutingInformationHRU(tree.get(0).ID).getDepth();
            for (HRU hru : tree) {
                int depth = routingTable.getRoutingInformationHRU(hru.ID).getDepth();
                if (depth < min) { // error - topological sort is violated
                    validSort = false;
                    if (debug) {
                        System.err.printf("ERROR: verifyForest invalid on topological sort on HRU=%d\n", hru.ID);
                    }
                }
                if (depth > min) { // update the minimum
                    min = depth;
                }
            }
        }
        return validSort;
    }

    private static HashMap<Integer, List<Integer>> HRUListToUndirAdjList(HashMap<Integer, HRU> hrus, RoutingTable routingTable) {
        HashMap<Integer, List<Integer>> adjList = new HashMap<>();
        for (HRU hru : hrus.values()) {
            ArrayList<Integer> neighbors = new ArrayList<>();
            List<RoutingEdge> toHRUs = routingTable.getRoutingInformationHRU(hru.ID).getToHRUs();
            for (RoutingEdge neighbor : toHRUs) {
                neighbors.add(neighbor.getId());
            }
            adjList.put(hru.ID, neighbors);
        }
        for (HRU hru : hrus.values()) {
            List<RoutingEdge> toHRUs = routingTable.getRoutingInformationHRU(hru.ID).getToHRUs();
            for (RoutingEdge neighbor : toHRUs) {
                adjList.get(neighbor.getId()).add(hru.ID);
            }
        }

        return adjList;
    }

    private static List<List<HRU>> findGraphComponentsHRUs(HashMap<Integer, List<Integer>> adjList, HashMap<Integer, HRU> hrus, final RoutingTable routingTable) {
        List<List<HRU>> listOfComponents = new ArrayList<>();
        HashMap<Integer, Boolean> explored = new HashMap<>();

        for (Integer k : hrus.keySet()) {
            explored.put(k, Boolean.FALSE);
        }

        for (Integer k : hrus.keySet()) {
            if (explored.get(k) == false) {
                List<HRU> components = new ArrayList<>();
                dfsHRUs(k, explored, adjList, hrus, components);
                listOfComponents.add(components);
            }
        }

        // make sure that each component list has the HRUs in topological order
        for (List<HRU> comps : listOfComponents) {
            Collections.sort(comps, new Comparator<HRU>() {
                @Override
                public int compare(HRU h1, HRU h2) {
                    int depth1 = routingTable.getRoutingInformationHRU(h1.ID).getDepth();
                    int depth2 = routingTable.getRoutingInformationHRU(h2.ID).getDepth();
                    return Integer.compare(depth1, depth2);
                }
            });
        }

        return listOfComponents;
    }

    private static void dfsHRUs(Integer k, HashMap<Integer, Boolean> explored, HashMap<Integer, List<Integer>> adjList, HashMap<Integer, HRU> hrus, List<HRU> components) {
        if (explored.get(k)) {
            return;
        } else {
            explored.put(k, Boolean.TRUE);
            components.add(hrus.get(k));
            for (Integer n : adjList.get(k)) {
                dfsHRUs(n, explored, adjList, hrus, components);
            }
        }
    }

    public static List<List<StreamReach>> getForestReaches(List<StreamReach> reaches, RoutingTable routingTable) {
        HashMap<Integer, StreamReach> reachMap = new HashMap<>();
        for (StreamReach r : reaches) {
            reachMap.put(r.ID, r);
        }
        // get hruUndir
        HashMap<Integer, List<Integer>> adjList = ReachListToUndirAdjList(reachMap, routingTable);
        // get components
        List<List<StreamReach>> forest = findGraphComponentsReaches(adjList, reachMap, routingTable);

        return forest;
    }

    public static boolean verifyForestReaches(List<List<StreamReach>> forest, RoutingTable routingTable) {
        boolean validSort = true; // assumed valid until proven otherwise
        boolean debug = true; // for controlling stderr

        // check to see if intersections of each subtree are empty
        Set<Integer> temp1;
        Set<Integer> temp2;
        for (int i = 0; i < forest.size(); i++) {
            temp1 = new TreeSet<>();
            for (StreamReach r : forest.get(i)) {
                temp1.add(r.ID);
            }
            for (int j = i + 1; j < forest.size(); j++) {
                temp2 = new TreeSet<>();
                for (StreamReach r : forest.get(j)) {
                    temp2.add(r.ID);
                }
                temp2.retainAll(temp1); // use the intersection
                if (!temp2.isEmpty()) {
                    validSort = false;
                    if (debug) {
                        String ids = "";
                        for (Integer id : temp2) {
                            ids += (id.toString() + " ");
                        }
                        System.err.printf("ERROR: verifyForest invalid on intersections on i=%d j=%d, overlapping elements %s\n", i, j, ids);
                    }
                }
            }
        }

        // check to see if each subtree is in the proper topological order
        // leverage the fact that the HRU.depth property reflects a universal topological ordering
        for (List<StreamReach> tree : forest) {
            int min = routingTable.getRoutingInformationReach(tree.get(0).ID).getDepth();
            for (StreamReach reach : tree) {
                int depth = routingTable.getRoutingInformationReach(reach.ID).getDepth();
                if (depth < min) { // error - topological sort is violated
                    validSort = false;
                    if (debug) {
                        System.err.printf("ERROR: verifyForest invalid on topological sort on HRU=%d\n", reach.ID);
                    }
                }
                if (depth > min) { // update the minimum
                    min = depth;
                }
            }
        }
        return validSort;
    }

    private static HashMap<Integer, List<Integer>> ReachListToUndirAdjList(HashMap<Integer, StreamReach> reaches, RoutingTable routingTable) {
        HashMap<Integer, List<Integer>> adjList = new HashMap<>();
        for (StreamReach reach : reaches.values()) {
            List<Integer> neighbors = new ArrayList<>();
            RoutingEdge toReachEdge = routingTable.getRoutingInformationReach(reach.ID).getToReach();
            if (toReachEdge != null) {
                neighbors.add(toReachEdge.getId());
            }
            adjList.put(reach.ID, neighbors);
        }
        for (StreamReach reach : reaches.values()) {
            RoutingEdge toReachEdge = routingTable.getRoutingInformationReach(reach.ID).getToReach();
            if (toReachEdge != null) {
                adjList.get(toReachEdge.getId()).add(reach.ID);
            }
        }

        return adjList;
    }

    private static List<List<StreamReach>> findGraphComponentsReaches(HashMap<Integer, List<Integer>> adjList, HashMap<Integer, StreamReach> reaches, RoutingTable routingTable) {
        List<List<StreamReach>> listOfComponents = new ArrayList<>();
        HashMap<Integer, Boolean> explored = new HashMap<>();

        for (Integer k : reaches.keySet()) {
            explored.put(k, Boolean.FALSE);
        }

        for (Integer k : reaches.keySet()) {
            if (explored.get(k) == false) {
                List<StreamReach> components = new ArrayList<>();
                dfsReaches(k, explored, adjList, reaches, components);
                listOfComponents.add(components);
            }
        }

        // make sure each component list has the HRUs in topological order
        for (List<StreamReach> comps : listOfComponents) {
            Collections.sort(comps, new Comparator<StreamReach>() {
                @Override
                public int compare(StreamReach r1, StreamReach r2) {
                    int depth1 = routingTable.getRoutingInformationReach(r1.ID).getDepth();
                    int depth2 = routingTable.getRoutingInformationReach(r2.ID).getDepth();
                    return Integer.compare(depth1, depth2);
                }
            });
        }

        return listOfComponents;
    }

    private static void dfsReaches(Integer k, HashMap<Integer, Boolean> explored, HashMap<Integer, List<Integer>> adjList, HashMap<Integer, StreamReach> reaches, List<StreamReach> components) {
        if (explored.get(k)) {
            return;
        } else {
            explored.put(k, Boolean.TRUE);
            components.add(reaches.get(k));
            for (Integer n : adjList.get(k)) {
                dfsReaches(n, explored, adjList, reaches, components);
            }
        }
    }
}
