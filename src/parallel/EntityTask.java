
package parallel;

/**
 *
 * @author Nathan Lighthart
 */
public class EntityTask implements Runnable {
    private final Runnable task;

    // Extra debug information to determine the entity running the task
    private final int entityID;

    public EntityTask(int entityID, Runnable task) {
        this.entityID = entityID;
        this.task = task;
    }

    @Override
    public void run() {
        task.run();
    }
}
