/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tiledrain;

public class ProcessTileDrainage {
    public double Beta_NO3;
    public double soilMaxDPS;
    public double drspac;
    public double drrad;
    public double depdr;
    public double netRain;
    public double snowMelt;
    public double[] kf_h;
    public double area;
    public int horizons;
    public double[] depth_h;
    public double totaldepth;
    public double soilSatLPS;
    public double actDPS;
    public double[] outRD2_h;
    public double[] perco_hor;
    public double[] satLPS_h;
    public double[] satMPS_h;
    public double[] maxMPS_h;
    public double[] maxLPS_h;
    public double[] maxFPS_h;
    public double snowDepth;
    public double[] soil_Temp_Layer;
    public double tmax;
    public boolean gwtable;
    public boolean condition;
    public double soilSat;
    public double outRD1;
    public double[] actLPS_h;
    public double[] NO3_Pool;
    public double[] out_tile_water;
    public double[] TDInterflowN;

    private double[] horthk;
    private double[] clat;
    private double[] dfluxl;

    public void execute() {
        TDInterflowN = new double[horizons];
        out_tile_water = new double[horizons];

        if (condition) {
            dfluxl = new double[horizons];
            horthk = new double[horizons];
            clat = new double[horizons];

            double stor = (actDPS / area) / 100;
            double storro = 0;
            double depimp = totaldepth;
            double depwt = 0;
            double dflux_sum = 0;
            double sumlayer = 0;

            for (int i = 0; i < depth_h.length; i++) {
                dfluxl[i] = 0.0;
                out_tile_water[i] = 0;
                TDInterflowN[i] = 0;
                sumlayer += depth_h[i];
                horthk[i] = sumlayer;
                clat[i] = (kf_h[i] / 24) * 10;
            }

            if (gwtable) {
                depwt = totaldepth - (totaldepth * Math.round(soilSat * 100.0) / 100.0);
            }

            dflux_sum = tiledrain(horthk, depimp, depdr, drspac, drrad, depwt, clat, stor, storro, dfluxl);

            if (dflux_sum > 0) {

                for (int i = 0; i < horizons; i++) {

                    if (dfluxl[i] > 0 && actLPS_h[i] > 0) {
                        double tileflux = (((dfluxl[i] * 100) * 24) * area);

                        if (actLPS_h[i] >= tileflux) {
                            actLPS_h[i] -= tileflux;
                            out_tile_water[i] = tileflux;
                        } else {
                            out_tile_water[i] = actLPS_h[i];
                            actLPS_h[i] = 0;
                        }
                        double vv = 0;
                        double sro = 0;
                        double vno3 = 0;
                        double co = 0;
                        double ww = 0;
                        if (i == 0) {
                            sro = outRD1 / area;
                        } else {
                            sro = 0;
                        }
                        vv = (perco_hor[i] / area) + sro + (outRD2_h[i] / area) + 1.e-10;
                        vv += out_tile_water[i] / area;

                        ww = (-1 * vv) / ((1. - Beta_NO3) * (maxLPS_h[i] + maxMPS_h[i] + maxFPS_h[i] / area));
                        vno3 = NO3_Pool[i] * (1. - Math.exp(ww));
                        if (vv > 1.e-10) {
                            co = Math.max(vno3 / vv, 0);
                        }
                        TDInterflowN[i] = out_tile_water[i] * co;   //kgN / hru
                        TDInterflowN[i] = (TDInterflowN[i]) / 10000; //kg N / ha
                        double test_pool = NO3_Pool[i]; //kgN/ha
                        test_pool -= TDInterflowN[i];

                        if (test_pool < 0) {
                            TDInterflowN[i] = NO3_Pool[i];
                            NO3_Pool[i] = 0;
                        } else {
                            NO3_Pool[i] -= TDInterflowN[i];
                        }
                        TDInterflowN[i] = TDInterflowN[i] * area / 10000; // kgN/hru
                    }
                }
            }
        }
    }

    public static double tiledrain(double[] horthk, double depimp, double depdr, double drspac, double drrad, double depwt, double[] clat, double stor, double storro, double[] dfluxl) {
        double dfluxt = 0.0;
        int nhor = horthk.length;
        double[] d = new double[nhor];
        double[] twtl = new double[nhor];
        double effdep;
        int idr = -1, iwt = -1;

        // calculate effective depth of tile drain
        double dd = depimp - depdr;
        double rat = dd / drspac;
        double alpha = 3.55 - 1.6 * rat + 2.0 * rat * rat;

        if (rat < 0.3) {
            effdep = dd / (1 + rat * (8.0 / Math.PI * Math.log(dd / drrad) - alpha));
        } else {
            effdep = drspac * Math.PI / (8.0 * (Math.log(drspac / drrad) - 1.15));
        }

        effdep = Math.max(effdep, 0.0);
        double hordep = 0.0;
        double num = 0.0;
        double den = 0.0;

        // calculate effective lateral conductivity in saturated zone
        for (int i = 0; i < nhor; i++) {
            d[i] = 0.0;

            if (i != 0) {
                hordep = horthk[i] - horthk[i - 1];
            } else {
                hordep = horthk[i];
            }
            if (depwt < horthk[i]) {
                d[i] = Math.min(horthk[i] - depwt, hordep);
            }

            num += d[i] * clat[i];
            den += d[i];
            // locate soil horizons containing water table and tile drain
            if (idr == -1) {
                if (horthk[i] >= depdr) {
                    idr = i;
                }
            }

            if (iwt == -1) {
                if (horthk[i] >= depwt) {
                    iwt = i;
                }
            }
        }
        double effk = num / den;
        // calculate total drainage flux
        if ((stor > storro) && (depwt < 1.0)) {
            // use Kirkham's equation for surface ponded conditions
            double sum = 0;
            double tmp = Math.sinh(Math.PI * drrad / drspac);
            double t2 = tmp * tmp;
            tmp = Math.sinh(Math.PI * (2 * depdr - drrad) / drspac);
            double t3 = tmp * tmp;

            for (int n = 1; n <= 5; n++) {
                tmp = Math.sinh(2.0 * Math.PI * n * depimp / drspac);
                double t1 = tmp * tmp;
                num = t1 - t2;
                den = t1 - t3;
                sum = sum + (Math.pow(-1, n) * Math.log(num / den));
            }
            double f = 2.0 * Math.log(Math.sinh(Math.PI * (2.0 * depdr - drrad) / drspac)
                    / Math.sinh(Math.PI * drrad / drspac));
            double gee = f - 2.0 * sum;

            dfluxt = 4.0 * Math.PI * effk * (depimp - effdep + stor) / (gee * drspac);

            if (dfluxt < 0.0) {
                dfluxt = 0.0;
            }
        } else {
            // use Hooghoudt's equation
            double em = depimp - depwt - effdep;
            dfluxt = (8.0 * effk * effdep * em + 4. * effk * em * em) / (drspac * drspac);
        }

        if (dfluxt < 0.0) {
            dfluxt = 0.0;
        }
        // calculate drainage flux by horizon in saturated zone above drain
        double tlsat = depdr - depwt;

        // weight drainage flux based on horizon thickness
        int il = -1;
        double wden = 0.0;

        for (int i = iwt; i <= idr; i++) {
            il++;
            double upr = Math.max((i > 0) ? horthk[i - 1] : 0.0, depwt);
            twtl[il] = horthk[i] - upr;
            twtl[il] = Math.max(twtl[il], 0.0);
            wden += (il + 1) * twtl[il];
        }
        double wt1 = tlsat / wden;
        il = -1;

        for (int i = iwt; i <= idr; i++) {
            il++;
            double wt = (il + 1) * wt1;
            dfluxl[i] = dfluxt * wt * twtl[il] / tlsat;

            if (Double.isInfinite(dfluxl[i])) {
                dfluxl[i] = 0;
            }
        }
        return dfluxt;
    }
}
