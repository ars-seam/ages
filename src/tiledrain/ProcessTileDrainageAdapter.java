/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package tiledrain;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Calendar;

@Description("Add ProcessTileDrainage module definition here")
@Author(name = "James C. Ascough II, Olaf David, Holm Kipka", contact = "jim.ascough@ars.usda.gov")
@Keywords("Drainage")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilWater/ProcessTileDrainage.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilWater/ProcessTileDrainage.xml")
public class ProcessTileDrainageAdapter extends AnnotatedAdapter {
    @Description("percolation coefficient")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public double Beta_NO3;

    @Description("Flag")
    @Input public boolean flagTileDrain;

    @Description("maximum depression storage capacity [mm]")
    @Units("mm")
    @Role(PARAMETER)
    @Input public double soilMaxDPS;

    @Description("distance between tile drains [cm]")
    @Units("cm")
    @Role(PARAMETER)
    @Input public double drspac;

    @Description("depth of the tile drains [cm]")
    @Units("cm")
    @Role(PARAMETER)
    @Input public double depdr;

    @Description("radius of tile drains [cm]")
    @Units("cm")
    @Role(PARAMETER)
    @Input public double drrad;

    @Description("Current Time")
    @Input public Calendar time;

    @Description("state variable net rain")
    @Input public double netRain;

    @Description("daily snow melt")
    @Input public double snowMelt;

    @Description("Soil hydraulic conductivity")
    @Units("cm/d")
    @Optional
    @Input public double[] kf_h;

    @Description("HRU")
    @Input public HRU hru;

    @Description("soil")
    @Input public SoilType soil;

    @Description("whole soil saturation")
    @Optional
    @Input public double soilSatLPS;

    @Description("HRU state var actual depression storage")
    @Optional
    @Input public double actDPS;

    @Description("HRU statevar RD2 outflow")
    @Units("l")
    @Optional
    @Input public double[] outRD2_h;

    @Description("Percolation out ouf the single horizonts")
    @Units("l")
    @Optional
    @Input public double[] perco_hor;

    @Description("Actual LPS in portion of sto_LPS soil water content")
    @Input public double[] satLPS_h;

    @Description("Actual MPS in portion of sto_MPS soil water content")
    @Input public double[] satMPS_h;

    @Description("Maximum MPS  in l soil water content")
    @Input public double[] maxMPS_h;

    @Description("Maximum LPS  in l soil water content")
    @Input public double[] maxLPS_h;

    @Description("Maximum FPS  in l soil water content")
    @Input public double[] maxFPS_h;

    @Description("snow depth")
    @Input public double snowDepth;

    @Description("soil temperature in different layerdepths")
    @Units("C")
    @Input public double[] soil_Temp_Layer;

    @Description("maximum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmax;

    @Description("HRU state var saturation for the whole soil")
    @Units("0-1")
    @Optional
    @Input public double soilSat;

    @Description("HRU statevar RD1 outflow")
    @Units("mm")
    @Optional
    @Input public double outRD1;

    @Description("HRU state var actual LPS")
    @Input @Output public double[] actLPS_h;

    @Description("NO3-Pool")
    @Units("kgN/ha")
    @Input @Output public double[] NO3_Pool;

    @Description("HRU tile water")
    @Output public double[] out_tile_water;

    @Description("HRU tile water")
    @Output public double[] TDInterflowN;

    @Override
    protected void run(Context context) {
        if (!flagTileDrain) {
            skipOutput();
            return;
        }
        ProcessTileDrainage component = new ProcessTileDrainage();

        component.Beta_NO3 = Beta_NO3;
        component.soilMaxDPS = soilMaxDPS;
        component.drspac = drspac;
        component.drrad = drrad;
        component.depdr = depdr;
        component.netRain = netRain;
        component.snowMelt = snowMelt;
        component.kf_h = (kf_h == null ? soil.kf_h : kf_h);
        component.area = hru.area;
        component.horizons = soil.horizons;
        component.depth_h = soil.depth_h;
        component.totaldepth = soil.totaldepth;
        component.soilSatLPS = soilSatLPS;
        component.actDPS = actDPS;
        component.outRD2_h = outRD2_h;
        component.perco_hor = perco_hor;
        component.satLPS_h = satLPS_h;
        component.satMPS_h = satMPS_h;
        component.maxMPS_h = maxMPS_h;
        component.maxLPS_h = maxLPS_h;
        component.maxFPS_h = maxFPS_h;
        component.snowDepth = snowDepth;
        component.soil_Temp_Layer = soil_Temp_Layer;
        component.tmax = tmax;
        component.condition = (hru.landuseID == 7) && (hru.tiledrainage > 0);
        component.gwtable = true;
        component.soilSat = soilSat;
        component.outRD1 = outRD1;
        component.actLPS_h = actLPS_h;
        component.NO3_Pool = NO3_Pool;

        component.execute();

        actLPS_h = component.actLPS_h;
        NO3_Pool = component.NO3_Pool;
        out_tile_water = component.out_tile_water;
        TDInterflowN = component.TDInterflowN;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
