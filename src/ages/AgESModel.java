
package ages;

import ages.types.HRU;
import ages.types.HydroGeology;
import ages.types.Landuse;
import ages.types.SoilType;
import ages.types.Station;
import ages.types.StreamReach;
import climate.CalcLanduseStateVarsAdapter;
import climate.CalcRelativeHumidityAdapter;
import climate.ClimateType;
import crop.Crop;
import crop.DormancyAdapter;
import crop.PlantGrowthStressAdapter;
import crop.PotentialCropGrowthAdapter;
import erosion.MusleAdapter;
import geoprocessing.CalcLatLongAdapter;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.Program;
import gov.usda.jcf.core.ProgramLayout;
import gov.usda.jcf.core.ProgramListLayout;
import gov.usda.jcf.core.contexts.AlterableContext;
import gov.usda.jcf.core.contexts.CombinedContext;
import gov.usda.jcf.core.contexts.FieldContext;
import gov.usda.jcf.core.contexts.MapContext;
import gov.usda.jcf.core.statements.AdapterStatement;
import gov.usda.jcf.core.statements.StatementBlock;
import gov.usda.jcf.io.xml.XMLLayoutReader;
import gov.usda.jcf.util.Contexts;
import groundwater.GroundwaterNAdapter;
import groundwater.ProcessGroundwaterAdapter;
import interception.ProcessInterceptionAdapter;
import io.ArrayGrabberAdapter;
import io.CO2ClimateReader;
import io.CSProperties;
import io.ClimateReader;
import io.DataIO;
import io.EntityIDFilter;
import io.EntityReader;
import io.ManagementParameterReader;
import io.MultiFlowRoutingReader;
import io.OutputWriter;
import io.ParameterOverrideReader;
import io.RegionalizationCacher;
import io.RegionalizedClimateReader;
import io.SoilsParameterReader;
import io.SpatialParameterReader;
import io.StationClimateReader;
import io.StationReader;
import io.TemporalParameterReader;
import io.TmeanStationClimateReader;
import irrigation.ProcessIrrigationAdapter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import management.CropExistenceAdapter;
import management.Fertilizer;
import management.HRURotation;
import management.Irrigation;
import management.ManagementOperation;
import management.ManagementSchedule;
import management.ProcessFertilizationAdapter;
import management.Tillage;
import nitrogen.InitSoilWaterLayerNAdapter;
import nitrogen.SoilWaterLayerNAdapter;
import parallel.ContextTask;
import parallel.EntityExecxutors;
import potet.EvapoTransAdapter;
import potet.PenmanMonteithAdapter;
import radiation.CalcDailyNetRadiationAdapter;
import radiation.CalcDailySolarRadiationAdapter;
import radiation.CalcExtraterrRadiationAdapter;
import regionalization.IDWWeightCalculatorAdapter;
import routing.HRURoutingInformation;
import routing.MultiRoutingLayerNAdapter;
import routing.MultiRoutingMusleAdapter;
import routing.ProcessHRUReachRoutingAdapter;
import routing.ProcessHRUReachRoutingMusleAdapter;
import routing.ProcessHRUReachRoutingNAdapter;
import routing.ProcessHorizonMultiFlowRoutingAdapter;
import routing.ProcessReachRoutingAdapter;
import routing.ProcessReachRoutingInputAdapter;
import routing.ProcessReachRoutingInputMusleAdapter;
import routing.ProcessReachRoutingInputNAdapter;
import routing.ProcessReachRoutingMusleAdapter;
import routing.ProcessReachRoutingNAdapter;
import routing.ReachRoutingInformation;
import routing.RoutingContext;
import routing.RoutingEdge;
import routing.RoutingTable;
import snow.ProcessSnowAdapter;
import snow.RainSnowPartitioningAdapter;
import soil.SParamAdapter;
import soilTemp.MeanTemperatureLayerAdapter;
import soilTemp.SoilTemperatureLayerAdapter;
import soilWater.InitProcessSoilWaterLayerAdapter;
import soilWater.ProcessSoilWaterLayerAdapter;
import tiledrain.ProcessTileDrainageAdapter;
import upgm.UpgmAdapter;
import weighting.CalcAreaWeightAdapter;
import weighting.CatchmentAggregator;
import weighting.HRUAreaWeigher;
import weighting.OutletAggregator;

/**
 *
 * @author Nathan Lighthart
 */
public class AgESModel {
    private final AgESParameters parameters;

    // data objeccts
    private List<HRU> hrus;
    private List<HRU> hrusOutputOrdered;
    private List<StreamReach> reaches;
    private List<StreamReach> reachesOutputOrdered;
    private Map<Integer, Landuse> landuseMap;
    private Map<Integer, HydroGeology> hydroGeologyMap;
    private RoutingTable routingTable;
    private Map<Integer, Fertilizer> fertilizers;
    private Map<Integer, Tillage> tillages;
    private Map<Integer, Crop> crops;
    /**
     * Management operations per each management id
     */
    private Map<Integer, List<ManagementOperation>> managements;
    /**
     * List of management ids per each rotation id
     */
    private Map<Integer, List<Integer>> rotations;
    private Map<Integer, HRURotation> hruRotations;
    /**
     * Map of irrigations by irrigation id
     */
    private Map<Integer, List<Irrigation>> irrigations;
    /**
     * Map of management id to a list irrigation id
     */
    private Map<Integer, List<Integer>> managementIrrigationMap;
    private Map<Integer, ManagementSchedule> rotationSchedules;
    private Map<Integer, SoilType> soilMap;
    private Station[] stationsPrecip;
    private Station[] stationsTmin;
    private Station[] stationsTmax;
    private Station[] stationsHum;
    private Station[] stationsSol;
    private Station[] stationsWind;
    private ClimateReader readerTmin;
    private ClimateReader readerTmax;
    private ClimateReader readerTmean;
    private ClimateReader readerHum;
    private ClimateReader readerPrecip;
    private ClimateReader readerSol;
    private ClimateReader readerWind;
    private ClimateReader readerCO2;
    private OutputWriter writer_hru;
    private OutputWriter writer_hru_crop;
    private OutputWriter writer_hru_layer;
    private OutputWriter writer_hru_n_mb;
    private OutputWriter writer_hru_n_pool;
    private OutputWriter writer_hru_sediment;
    private OutputWriter writer_hru_crop_upgm;
    private OutputWriter writer_reach;
    private HRUAreaWeigher weigher_hru;
    private HRUAreaWeigher weigher_hru_crop;
    private HRUAreaWeigher weigher_hru_layer;
    private HRUAreaWeigher weigher_hru_n_mb;
    private HRUAreaWeigher weigher_hru_n_pool;
    private HRUAreaWeigher weigher_hru_sediment;
    private HRUAreaWeigher weigher_hru_crop_upgm;
    private OutputWriter writer_catch;
    private OutputWriter writer_catch_crop;
    private OutputWriter writer_catch_n_mb;
    private OutputWriter writer_catch_n_pool;
    private OutputWriter writer_catch_sediment;
    private OutputWriter writer_catch_crop_upgm;
    private OutputWriter writer_outlet;
    private CatchmentAggregator aggegator_catch;
    private CatchmentAggregator aggegator_catch_crop;
    private CatchmentAggregator aggegator_catch_n_mb;
    private CatchmentAggregator aggegator_catch_n_pool;
    private CatchmentAggregator aggegator_catch_sediment;
    private CatchmentAggregator aggegator_catch_crop_upgm;
    private OutletAggregator aggregator_outlet;

    // Contexts
    private Context agesContext;
    private Context temporalContext;
    private Context catchmentContext_catch;
    private Context catchmentContext_catch_crop;
    private Context catchmentContext_catch_n_mb;
    private Context catchmentContext_catch_n_pool;
    private Context catchmentContext_catch_sediment;
    private Context catchmentContext_catch_crop_upgm;
    private Context outletContext;
    private Context outletAggregateContext;
    private Map<Integer, Context> hruContexts;
    private Map<Integer, Context> hruTemporalContexts;
    private Map<Integer, Context> reachContexts;
    private Map<Integer, Context> reachTemporalContexts;
    private Map<Integer, HRUProgramSet> hruProgramSets;
    private Map<Integer, ReachProgramSet> reachProgramSets;

    public AgESModel(AgESParameters parameters) {
        this.parameters = parameters;
    }

    public void execute() throws Exception {
        setupParallelism();
        loadEntities();
        setupRouting();
        sortEntities();
        loadSoils();
        loadManagement();
        readStations();
        initializeContexts();
        addAdditionalParameters();
        addParameterOverrides();
        createPrograms();
        initializeClimateReaders();
        initializeManagementSchedules();
        initializeOutputWriters();
        runModel();
        shutdownPrograms();
        closeReaders();
        closeWriters();
    }

    private void setupParallelism() {
        if (parameters.parallelismFactor > 0.0) {
            int parallelism = (int) Math.round(Runtime.getRuntime().availableProcessors() * parameters.parallelismFactor);
            if (parallelism < 1) {
                parallelism = 1;
            }
            System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", Integer.toString(parallelism));
        }
    }

    private void loadEntities() throws IOException {
        System.out.print("--> Reading HRU input file ");
        hrus = EntityReader.readHRUs(parameters.hruFilePath);
        if (parameters.flagSort) {
            hrusOutputOrdered = new ArrayList<>(hrus);
            Collections.sort(hrusOutputOrdered, (HRU hru1, HRU hru2) -> {
                return Integer.compare(hru1.ID, hru2.ID);
            });
        } else {
            hrusOutputOrdered = hrus;
        }

        if (parameters.flagReachRouting) {
            System.out.print("--> Reading stream reach input file ");
            reaches = EntityReader.readReaches(parameters.reachFilePath);
        } else {
            System.out.print("--> No reach input file.");
            System.out.println("[0]");
            reaches = new ArrayList<>();
            reaches.add(new StreamReach());
        }
        if (parameters.flagSort) {
            reachesOutputOrdered = new ArrayList<>(reaches);
            Collections.sort(reachesOutputOrdered, (StreamReach reach1, StreamReach reach2) -> {
                return Integer.compare(reach1.ID, reach2.ID);
            });
        } else {
            reachesOutputOrdered = reaches;
        }

        System.out.print("--> Reading landuse input file");
        landuseMap = EntityReader.readLanduse(parameters.landuseFilePath);

        System.out.print("--> Reading hydrogeology input file");
        hydroGeologyMap = EntityReader.readGW(parameters.hydroGeologyFilePath);
    }

    private void setupRouting() throws IOException {
        if (parameters.flagHRURouting) {
            // Create routing table from file
            // Excludes reach information in file if flagReachRouting is false
            Map<Integer, List<RoutingEdge>> routingEdges
                    = MultiFlowRoutingReader.readRoutingTable(parameters.routingFilePath, parameters.flagReachRouting);

            // create empty list for each hru and reach not in edges
            for (HRU hru : hrus) {
                if (!routingEdges.containsKey(hru.ID)) {
                    routingEdges.put(hru.ID, new ArrayList<>());
                }
            }
            for (StreamReach reach : reaches) {
                if (!routingEdges.containsKey(reach.ID)) {
                    routingEdges.put(reach.ID, new ArrayList<>());
                }
            }

            routingTable = new RoutingTable(routingEdges);
        } else {
            // Create empty routing table
            routingTable = RoutingTable.createEmptyTable(hrus, reaches);
        }
    }

    private void sortEntities() {
        // topologically sort HRUs
        List<List<HRU>> hruLayers = new ArrayList<>(routingTable.getMaxDepthHRU());
        for (int i = 0; i <= routingTable.getMaxDepthHRU(); ++i) {
            hruLayers.add(new ArrayList<>());
        }
        for (HRU hru : hrus) {
            HRURoutingInformation info = routingTable.getRoutingInformationHRU(hru.ID);
            hruLayers.get(info.getDepth()).add(hru);
        }
        hrus.clear();
        for (List<HRU> layer : hruLayers) {
            hrus.addAll(layer);
        }

        // topologically sort reaches
        int minDepthReach = routingTable.getMinDepthReach();
        List<List<StreamReach>> reachLayers = new ArrayList<>(routingTable.getMaxDepthReach() - minDepthReach);
        for (int i = minDepthReach; i <= routingTable.getMaxDepthReach(); ++i) {
            reachLayers.add(new ArrayList<>());
        }
        for (StreamReach reach : reaches) {
            ReachRoutingInformation info = routingTable.getRoutingInformationReach(reach.ID);
            reachLayers.get(info.getDepth() - minDepthReach).add(reach);
        }
        reaches.clear();
        for (List<StreamReach> layer : reachLayers) {
            reaches.addAll(layer);
        }

        // sort routing table as well
        routingTable.sortEdgesByIndices(hrus, reaches);
    }

    private void loadManagement() throws IOException {
        System.out.println("--> Reading fertilizer parameter database ...");
        fertilizers = ManagementParameterReader.readFertilizer(parameters.fertilizerFilePath);

        System.out.println("--> Reading tillage parameter database ...");
        tillages = ManagementParameterReader.readTillage(parameters.tillageFilePath);

        if (parameters.flagIrrigation) {
            System.out.println("--> Reading irrigation input file ...");
            irrigations = ManagementParameterReader.readIrrigation(parameters.irrigationFilePath);
        } else {
            irrigations = new HashMap<>();
        }

        System.out.println("--> Reading crops parameter database ...");
        crops = ManagementParameterReader.readCrops(parameters.cropFilePath);
        if (parameters.flagUPGM) {
            ManagementParameterReader.readCropsUPGM(parameters.cropUPGMFilePath, crops);
        }

        if (parameters.flagIrrigation) {
            System.out.println("--> Reading irrigation scheduling list ...");
            managementIrrigationMap = ManagementParameterReader.readManagementIrrigations(parameters.managementIrrigationFilePath);
        }

        System.out.println("--> Reading management input file ...");
        managements = ManagementParameterReader.readManagement(parameters.managementFilePath, crops, tillages, fertilizers);

        System.out.println("--> Reading crop rotation input file ...");
        rotations = ManagementParameterReader.readRotations(parameters.rotationFilePath);

        if (parameters.flagIrrigation) {
            System.out.println("--> Linking crop rotation, irrigation, and management information ...");
        } else {
            System.out.println("--> Linking crop rotation and management information ...");
        }

        hruRotations = ManagementParameterReader.readHRURotations(parameters.hruRotationFilePath);
    }

    private void loadSoils() throws IOException {
        System.out.println("--> Reading soil input file ...");
        soilMap = SoilsParameterReader.readSoils(parameters.soilFilePath);
    }

    private void readStations() throws IOException {
        StationReader precipReader = new StationReader(parameters.dataFilePrecip);
        stationsPrecip = precipReader.readStations();
        StationReader tminReader = new StationReader(parameters.dataFileTmin);
        stationsTmin = tminReader.readStations();
        StationReader tmaxReader = new StationReader(parameters.dataFileTmax);
        stationsTmax = tmaxReader.readStations();
        StationReader humReader = new StationReader(parameters.dataFileHum);
        stationsHum = humReader.readStations();
        StationReader solReader = new StationReader(parameters.dataFileSol);
        stationsSol = solReader.readStations();
        StationReader windReader = new StationReader(parameters.dataFileWind);
        stationsWind = windReader.readStations();
    }

    private void initializeContexts() {
        System.out.println("--> Initializing contexts ...");
        initializeAgesContext();
        initializeTemporalContext();
        initializeSpatialContexts();
        initializeCatchmentContexts();
    }

    private void initializeAgesContext() {
        agesContext = new AlterableContext(new FieldContext(parameters, true));

        parameters.addAdditionalParameters(agesContext);

        double basinArea = 0.0;
        for (HRU hru : hrus) {
            basinArea += hru.area;
        }
        agesContext.put("basinArea", basinArea);

        addClimateStations(agesContext);
        addRegionalizationParameters();
    }

    private void addClimateStations(Context context) {
        addClimateStations(context, stationsPrecip, "Precip");
        addClimateStations(context, stationsTmin, "Tmin");
        addClimateStations(context, stationsTmax, "Tmax");
        addClimateStations(context, stationsHum, "Hum");
        addClimateStations(context, stationsSol, "Sol");
        addClimateStations(context, stationsWind, "Wind");
        if (Arrays.equals(stationsTmin, stationsTmax)) {
            addClimateStations(context, stationsTmin, "Tmean");
        } else {
            throw new IllegalArgumentException("Cannot compute station data for Tmean: Tmin stations do not match Tmax stations");
        }
    }

    private void addClimateStations(Context context, Station[] stations, String type) {
        double[] xCoord = new double[stations.length];
        double[] yCoord = new double[stations.length];
        double[] elevation = new double[stations.length];

        for (int i = 0; i < stations.length; ++i) {
            Station s = stations[i];
            xCoord[i] = s.getX();
            yCoord[i] = s.getY();
            elevation[i] = s.getElevation();
        }

        context.put("xCoord" + type, xCoord);
        context.put("yCoord" + type, yCoord);
        context.put("elevation" + type, elevation);
    }

    private void addRegionalizationParameters() {
        if (parameters.dataFileReg == null) {
            return;
        }

        CSProperties regionalizationProperties;
        try {
            regionalizationProperties = DataIO.properties(
                    parameters.dataFileReg.toFile(), null);
        } catch (IOException ex) {
            throw new RuntimeException("Error reading regionalization parameter file: "
                    + parameters.dataFileReg, ex);
        }

        // Grab main parameters
        addRegionalizationParameter(regionalizationProperties, "projection");
        addRegionalizationParameter(regionalizationProperties, "regThres");
        addRegionalizationParameter(regionalizationProperties, "equalWeights");

        // Grab climate specific parameters
        String[] climateTypes = new String[]{"Tmin", "Tmax", "Tmean", "Hum", "Precip", "Wind", "Sol"};
        for (String climateType : climateTypes) {
            addRegionalizationParameter(regionalizationProperties, "nidw" + climateType);
            addRegionalizationParameter(regionalizationProperties, "pidw" + climateType);
            addRegionalizationParameter(regionalizationProperties, "rsqThreshold" + climateType);
            addRegionalizationParameter(regionalizationProperties, "elevationCorrection" + climateType);
        }

        // Grab rainfall correction parameters
        addRegionalizationParameter(regionalizationProperties, "windNIDW");
        addRegionalizationParameter(regionalizationProperties, "precipCorrectMethod");
        addRegionalizationParameter(regionalizationProperties, "tempNIDW");
        addRegionalizationParameter(regionalizationProperties, "pIDW");
    }

    private void addRegionalizationParameter(Map<String, Object> dataMap, String name) {
        if (dataMap.containsKey(name)) {
            Object value = dataMap.get(name);
            agesContext.put(name, value);
        } else {
            throw new RuntimeException("Missing regionalization parameter: " + name);
        }
    }

    private void initializeTemporalContext() {
        temporalContext = new MapContext();

        temporalContext.put("co2", parameters.defaultCO2);
    }

    private void initializeSpatialContexts() {
        initializeHRUContexts();
        initializeReachContexts();
        initializeRoutingInformation();
    }

    private void initializeHRUContexts() {
        hruContexts = new LinkedHashMap<>();
        hruTemporalContexts = new LinkedHashMap<>();
        for (HRU hru : hrus) {
            Context hruContext = createHRUContext(hru);
            hruContexts.put(hru.ID, hruContext);

            Context hruTemporalContext = new CombinedContext(new MapContext(),
                    hruContext, temporalContext, agesContext);
            hruTemporalContexts.put(hru.ID, hruTemporalContext);
        }
    }

    private Context createHRUContext(HRU hru) {
        Context context = new MapContext();

        // Initialize basic hru properties
        context.put("hru", hru);
        context.put("ID", hru.ID);
        context.put("landuse", landuseMap.get(hru.landuseID));
        context.put("hydroGeology", hydroGeologyMap.get(hru.hgeoID));
        context.put("soil", soilMap.get(hru.soilID));

        // Initialize HRU management
        HRURotation hruRotation = hruRotations.get(hru.ID);

        context.put("reductionFactor", hruRotation.getReductionFactor());

        return context;
    }

    private void initializeReachContexts() {
        reachContexts = new LinkedHashMap<>();
        reachTemporalContexts = new LinkedHashMap<>();
        for (StreamReach reach : reaches) {
            Context reachContext = createReachContext(reach);
            reachContexts.put(reach.ID, reachContext);

            Context reachTemporalContext = new CombinedContext(new MapContext(),
                    reachContext, temporalContext, agesContext);
            reachTemporalContexts.put(reach.ID, reachTemporalContext);
        }
    }

    private Context createReachContext(StreamReach reach) {
        Context context = new MapContext();

        context.put("reach", reach);
        context.put("ID", reach.ID);

        return context;
    }

    private void initializeRoutingInformation() {
        initializeHRURoutingInformation();
        initializeReachRoutingInformation();
    }

    private void initializeHRURoutingInformation() {
        // Add routing information to contexts
        for (HRU hru : hrus) {
            Context hruContext = hruTemporalContexts.get(hru.ID);

            HRURoutingInformation routingInformation = routingTable.getRoutingInformationHRU(hru.ID);

            // Generate from hru routing contexts
            List<RoutingEdge> fromHRUs = routingInformation.getFromHRUs();
            RoutingContext[] routingContexts = createRoutingContext(fromHRUs, hruTemporalContexts);
            hruContext.put("routingContexts", routingContexts);

            // Generate to HRU routing contexts
            List<RoutingEdge> toHRUs = routingInformation.getToHRUs();
            RoutingContext[] hruRoutingContexts = createRoutingContext(toHRUs, hruTemporalContexts);
            hruContext.put("hruRoutingContexts", hruRoutingContexts);

            // Generate to Reach routing contexts
            List<RoutingEdge> toReaches = routingInformation.getToReaches();
            RoutingContext[] reachRoutingContexts = createRoutingContext(toReaches, reachTemporalContexts);
            hruContext.put("reachRoutingContexts", reachRoutingContexts);
        }
    }

    private void initializeReachRoutingInformation() {
        // Add routing information to contexts
        for (StreamReach reach : reaches) {
            Context reachContext = reachTemporalContexts.get(reach.ID);

            ReachRoutingInformation routingInformation = routingTable.getRoutingInformationReach(reach.ID);

            // Generate from hru routing contexts
            List<RoutingEdge> fromHRUs = routingInformation.getFromHRUs();
            RoutingContext[] hruRoutingContexts = createRoutingContext(fromHRUs, hruTemporalContexts);
            reachContext.put("hruRoutingContexts", hruRoutingContexts);

            // Generate from reach routing contexts
            List<RoutingEdge> fromReaches = routingInformation.getFromReaches();
            RoutingContext[] reachRoutingContexts = createRoutingContext(fromReaches, reachTemporalContexts);
            reachContext.put("reachRoutingContexts", reachRoutingContexts);

            // Generate toReach routing context
            RoutingEdge toReach = routingInformation.getToReach();
            RoutingContext toReachRoutingContext;
            if (toReach == null) {
                toReachRoutingContext = null;
            } else {
                toReachRoutingContext = createRoutingContext(toReach, reachTemporalContexts);
            }
            reachContext.put("toReachRoutingContext", toReachRoutingContext);
        }
    }

    private RoutingContext[] createRoutingContext(List<RoutingEdge> edges, Map<Integer, Context> contexts) {
        RoutingContext[] routingContexts = new RoutingContext[edges.size()];
        int i = 0;
        for (RoutingEdge edge : edges) {
            RoutingContext routingContext = createRoutingContext(edge, contexts);
            routingContexts[i++] = routingContext;
        }
        return routingContexts;
    }

    private RoutingContext createRoutingContext(RoutingEdge edge, Map<Integer, Context> contexts) {
        Context context = contexts.get(edge.getId());
        return new RoutingContext(context, edge.getWeight());
    }

    private void initializeCatchmentContexts() {
        catchmentContext_catch = new CombinedContext(new MapContext(), temporalContext, agesContext);
        catchmentContext_catch_crop = new CombinedContext(new MapContext(), temporalContext, agesContext);
        catchmentContext_catch_n_mb = new CombinedContext(new MapContext(), temporalContext, agesContext);
        catchmentContext_catch_n_pool = new CombinedContext(new MapContext(), temporalContext, agesContext);
        catchmentContext_catch_sediment = new CombinedContext(new MapContext(), temporalContext, agesContext);

        if (parameters.flagUPGM) {
            catchmentContext_catch_crop_upgm = new CombinedContext(new MapContext(), temporalContext, agesContext);
        }

        System.out.print("--> Finding watershed outlet: ");
        int outletId = routingTable.getOutletId();
        System.out.println(outletId);
        outletContext = reachTemporalContexts.get(outletId);
        outletAggregateContext = new CombinedContext(new MapContext(), temporalContext, agesContext);
    }

    private void addAdditionalParameters() throws IOException {
        addAdditionalHRUParameters();
        addAdditionalReachParameters();
    }

    private void addAdditionalHRUParameters() throws IOException {
        if (parameters.hruAdditionalFilePath != null) {
            SpatialParameterReader reader = new SpatialParameterReader(parameters.hruAdditionalFilePath, hruContexts);
            reader.read();
        }
    }

    private void addAdditionalReachParameters() throws IOException {
        // need to check flag reach routing as reaches may not exist
        if (parameters.reachAdditionalFilePath != null && parameters.flagReachRouting) {
            SpatialParameterReader reader = new SpatialParameterReader(parameters.reachAdditionalFilePath, reachContexts);
            reader.read();
        }
    }

    private void addParameterOverrides() throws IOException {
        if (parameters.hruOverrideFilePath == null) {
            return;
        }

        ParameterOverrideReader reader = new ParameterOverrideReader(parameters.hruOverrideFilePath, hruTemporalContexts);
        reader.read();
    }

    private void createPrograms() throws IOException {
        System.out.println("--> Creating programs ...");
        Program initProgram = createInitProgram();

        // read programs from specific files
        Program surfaceProgram = createSurfaceProgram();
        Program subSurfaceProgram = createSubSurfaceProgram();

        // read all programs if some programs were not found in specific files
        ProgramListLayout allProgramsList = null;
        if (parameters.programsFilePath != null) {
            if (surfaceProgram == null || subSurfaceProgram == null) {
                XMLLayoutReader reader = new XMLLayoutReader(parameters.programsFilePath);
                allProgramsList = (ProgramListLayout) reader.read(XMLLayoutReader.LayoutType.PROGRAM_LIST);

                if (surfaceProgram == null) {
                    surfaceProgram = allProgramsList.createProgram("surface");
                }
                if (subSurfaceProgram == null) {
                    subSurfaceProgram = allProgramsList.createProgram("subsurface");
                }
            }
        }

        // if programs are still not found use default
        if (surfaceProgram == null) {
            surfaceProgram = createDefaultSurfaceProgram();
        }
        if (subSurfaceProgram == null) {
            subSurfaceProgram = createDefaultSubSurfaceProgram();
        }

        emulateHRUPrograms(initProgram, surfaceProgram, subSurfaceProgram);

        hruProgramSets = new LinkedHashMap<>();
        for (HRU hru : hrus) {
            HRUProgramSet hruProgramSet = new HRUProgramSet();
            hruProgramSet.initProgram = initProgram.copy();
            hruProgramSet.surfaceProgram = surfaceProgram.copy();
            hruProgramSet.subSurfaceProgram = subSurfaceProgram.copy();
            hruProgramSets.put(hru.ID, hruProgramSet);
        }

        if (parameters.flagReachRouting) {
            Program reachRoutingProgram = createReachRoutingProgram();

            if (parameters.programsFilePath != null) {
                if (reachRoutingProgram == null) {
                    if (allProgramsList == null) {
                        XMLLayoutReader reader = new XMLLayoutReader(parameters.programsFilePath);
                        allProgramsList = (ProgramListLayout) reader.read(XMLLayoutReader.LayoutType.PROGRAM_LIST);
                    }

                    reachRoutingProgram = allProgramsList.createProgram("reach-routing");
                }
            }

            if (reachRoutingProgram == null) {
                reachRoutingProgram = createDefaultReachRoutingProgram();
            }

            emulateReachPrograms(reachRoutingProgram);

            reachProgramSets = new LinkedHashMap<>();
            for (StreamReach reach : reaches) {
                ReachProgramSet reachProgramSet = new ReachProgramSet();
                reachProgramSet.routeProgram = reachRoutingProgram.copy();
                reachProgramSets.put(reach.ID, reachProgramSet);
            }
        }
    }

    private Program createInitProgram() {
        AdapterStatement areaWeight = new AdapterStatement(new CalcAreaWeightAdapter());
        AdapterStatement calcLatLong = new AdapterStatement(new CalcLatLongAdapter());
        AdapterStatement luStateVars = new AdapterStatement(new CalcLanduseStateVarsAdapter());
        AdapterStatement rad = new AdapterStatement(new CalcExtraterrRadiationAdapter());
        AdapterStatement soilWater = new AdapterStatement(new InitProcessSoilWaterLayerAdapter());
        AdapterStatement soil = new AdapterStatement(new InitSoilWaterLayerNAdapter());
        AdapterStatement idwTmean = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("tmean"));
        AdapterStatement idwTmin = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("tmin"));
        AdapterStatement idwTmax = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("tmax"));
        AdapterStatement idwHum = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("hum"));
        AdapterStatement idwPrecip = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("precip"));
        AdapterStatement idwSol = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("sol"));
        AdapterStatement idwWind = new AdapterStatement(IDWWeightCalculatorAdapter.class, createTypeMap("wind"));

        StatementBlock radiationBlock = new StatementBlock(calcLatLong, rad);

        StatementBlock root = new StatementBlock(true, areaWeight, radiationBlock,
                luStateVars, soilWater, soil, idwTmean, idwTmin, idwTmax, idwHum,
                idwPrecip, idwSol, idwWind);
        return new Program(root);
    }

    private Map<String, String> createTypeMap(String type) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);
        return map;
    }

    private Program createSurfaceProgram() throws IOException {
        if (parameters.surfaceProgramFilePath == null) {
            return null;
        }
        return createProgram(parameters.surfaceProgramFilePath);
    }

    private Program createDefaultSurfaceProgram() {
        AdapterStatement ag = new AdapterStatement(new ArrayGrabberAdapter());
        AdapterStatement relHum = new AdapterStatement(new CalcRelativeHumidityAdapter());
        AdapterStatement solrad = new AdapterStatement(new CalcDailySolarRadiationAdapter());
        AdapterStatement netrad = new AdapterStatement(new CalcDailyNetRadiationAdapter());
        AdapterStatement et = new AdapterStatement(new PenmanMonteithAdapter());
        AdapterStatement pri = new AdapterStatement(new ProcessIrrigationAdapter());
        AdapterStatement rsParts = new AdapterStatement(new RainSnowPartitioningAdapter());
        AdapterStatement intc = new AdapterStatement(new ProcessInterceptionAdapter());
        AdapterStatement snow = new AdapterStatement(new ProcessSnowAdapter());

        StatementBlock solradBlock = new StatementBlock(ag, solrad);
        StatementBlock preNetradBlock = new StatementBlock(true, solradBlock, relHum);
        StatementBlock etBlock = new StatementBlock(preNetradBlock, netrad, et);

        StatementBlock preIntcBlock = new StatementBlock(true, etBlock, rsParts, pri);

        StatementBlock root = new StatementBlock(preIntcBlock, intc, snow);
        return new Program(root);
    }

    private Program createSubSurfaceProgram() throws IOException {
        if (parameters.subSurfaceProgramFilePath == null) {
            return null;
        }
        return createProgram(parameters.subSurfaceProgramFilePath);
    }

    private Program createDefaultSubSurfaceProgram() {
        AdapterStatement soilWater = new AdapterStatement(new ProcessSoilWaterLayerAdapter());
        AdapterStatement dorm = new AdapterStatement(new DormancyAdapter());
        AdapterStatement et = new AdapterStatement(new EvapoTransAdapter());
        AdapterStatement cropExist = new AdapterStatement(new CropExistenceAdapter());
        AdapterStatement fertilization = new AdapterStatement(new ProcessFertilizationAdapter());
        AdapterStatement soilParams = new AdapterStatement(new SParamAdapter());
        AdapterStatement pcg = new AdapterStatement(new PotentialCropGrowthAdapter());
        AdapterStatement soilTemp = new AdapterStatement(new SoilTemperatureLayerAdapter());
        AdapterStatement soiln = new AdapterStatement(new SoilWaterLayerNAdapter());
        AdapterStatement gw = new AdapterStatement(new ProcessGroundwaterAdapter());
        AdapterStatement musle = new AdapterStatement(new MusleAdapter());
        AdapterStatement drain = new AdapterStatement(new ProcessTileDrainageAdapter());
        AdapterStatement gwn = new AdapterStatement(new GroundwaterNAdapter());
        AdapterStatement pgs = new AdapterStatement(new PlantGrowthStressAdapter());
        AdapterStatement upgm = new AdapterStatement(new UpgmAdapter());
        AdapterStatement routing = new AdapterStatement(new ProcessHorizonMultiFlowRoutingAdapter());
        AdapterStatement sedRouting = new AdapterStatement(new MultiRoutingMusleAdapter());
        AdapterStatement routingN = new AdapterStatement(new MultiRoutingLayerNAdapter());

        StatementBlock routingBlock = new StatementBlock(true, routing, sedRouting, routingN);

        StatementBlock root = new StatementBlock(routingBlock, soilParams,
                soilWater, drain, dorm, et, cropExist, fertilization, upgm, pcg,
                soilTemp, soiln, gw, musle, gwn, pgs);
        return new Program(root);
    }

    private Program createReachRoutingProgram() throws IOException {
        if (parameters.reachRoutingProgramFilePath == null) {
            return null;
        }
        return createProgram(parameters.reachRoutingProgramFilePath);
    }

    private Program createDefaultReachRoutingProgram() {
        AdapterStatement musleHRUReach = new AdapterStatement(new ProcessHRUReachRoutingMusleAdapter());
        AdapterStatement musleReachInput = new AdapterStatement(new ProcessReachRoutingInputMusleAdapter());
        AdapterStatement musle = new AdapterStatement(new ProcessReachRoutingMusleAdapter());

        AdapterStatement reachRoutingHRUReach = new AdapterStatement(new ProcessHRUReachRoutingAdapter());
        AdapterStatement reachRoutingInput = new AdapterStatement(new ProcessReachRoutingInputAdapter());
        AdapterStatement reachRouting = new AdapterStatement(new ProcessReachRoutingAdapter());
        AdapterStatement reachRoutingHRUReachN = new AdapterStatement(new ProcessHRUReachRoutingNAdapter());
        AdapterStatement reachRoutingInputN = new AdapterStatement(new ProcessReachRoutingInputNAdapter());
        AdapterStatement reachRoutingN = new AdapterStatement(new ProcessReachRoutingNAdapter());

        StatementBlock musleInputBlock = new StatementBlock(true, musleHRUReach, musleReachInput);
        StatementBlock musleRoutingBlock = new StatementBlock(musleInputBlock, musle);

        StatementBlock reachRoutingInputBlock = new StatementBlock(true, reachRoutingHRUReach, reachRoutingInput);
        StatementBlock reachRoutingBlock = new StatementBlock(reachRoutingInputBlock, reachRouting);
        StatementBlock reachRoutingInputNBlock = new StatementBlock(true, reachRoutingBlock, reachRoutingHRUReachN, reachRoutingInputN);
        StatementBlock reachRoutingNBlock = new StatementBlock(reachRoutingInputNBlock, reachRoutingN);

        StatementBlock routingBlock = new StatementBlock(true, musleRoutingBlock, reachRoutingNBlock);
        StatementBlock root = new StatementBlock(routingBlock);
        return new Program(root);
    }

    private Program createProgram(Path programFilePath) throws IOException {
        XMLLayoutReader reader = new XMLLayoutReader(programFilePath);
        ProgramLayout pl;
        try {
            pl = (ProgramLayout) reader.read(XMLLayoutReader.LayoutType.PROGRAM);
        } catch (IOException ex) {
            throw new IOException("Failed to read program from file: " + programFilePath, ex);
        }
        return pl.createProgram();
    }

    private void runModel() throws IOException {
        LocalDate time = parameters.startTime;
        temporalContext.put("time", time);

        runInitProgram();

        if (parameters.flagRegionalization) {
            RegionalizationCacher cacher = new RegionalizationCacher(parameters.dataFileTmin, parameters.dataFileTmax,
                    parameters.dataFileHum, parameters.dataFilePrecip, parameters.dataFileSol, parameters.dataFileWind);
            cacher.cache(hrus, hruTemporalContexts, agesContext, temporalContext);
        }

        System.out.println("--> Preprocessing start ...");
        long last = System.currentTimeMillis();

        // compute mean temperature layer
        for (time = parameters.startTime; !time.isAfter(parameters.endTime); time = time.plusDays(1L)) {
            if (!parameters.flagRegionalization) {
                readerTmin.readData();
                readerTmax.readData();
            }
            readerTmean.readData();

            hruTemporalContexts.values().parallelStream().forEach((hruContext) -> {
                // tmean depends on tmin and tmax if not cached in regionalized file
                if (!parameters.flagRegionalization) {
                    readerTmin.setValue(hruContext);
                    readerTmax.setValue(hruContext);
                }
                readerTmean.setValue(hruContext);

                MeanTemperatureLayerAdapter meanTemperatureLayer = new MeanTemperatureLayerAdapter();
                meanTemperatureLayer.execute(hruContext);
            });
        }

        if (!parameters.flagRegionalization) {
            readerTmin.reset();
            readerTmax.reset();
        }
        readerTmean.reset();

        List<Runnable> outputExecutors = new ArrayList<>();

        Map<Integer, ContextTask> surfaceTask = new HashMap<>();
        Map<Integer, ContextTask> subSurfaceTask = new HashMap<>();
        Map<Integer, Runnable> hruOutputTask = new HashMap<>();
        for (HRU hru : hrus) {
            Context hruContext = hruTemporalContexts.get(hru.ID);
            HRUProgramSet hruProgramSet = hruProgramSets.get(hru.ID);

            surfaceTask.put(hru.ID, new ContextTask(hruContext, (context) -> {
                updateClimates(context);
                updateSchedule(context);
                hruProgramSet.surfaceProgram.execute(context);
            }));
            subSurfaceTask.put(hru.ID, new ContextTask(hruContext, hruProgramSet.subSurfaceProgram));
            hruOutputTask.put(hru.ID, createOutputExecutorHRU(hruContext));
        }
        Runnable surfaceExecutor = EntityExecxutors.parallelHRU(hrus, surfaceTask);
        Runnable subSurfaceExecutor = EntityExecxutors.forHRU(parameters.flagParallel, hrus, routingTable, subSurfaceTask);
        Runnable hruOutputExecutor = EntityExecxutors.sequentialHRU(hrusOutputOrdered, hruOutputTask);
        outputExecutors.add(hruOutputExecutor);

        Runnable catchmentExecutor = createCatchmentExecutor();
        outputExecutors.add(catchmentExecutor);

        Runnable reachRoutingExecutor = null;
        if (parameters.flagReachRouting) {
            Map<Integer, ContextTask> reachRoutingTask = new HashMap<>();
            Map<Integer, Runnable> reachOutputTask = new HashMap<>();

            for (StreamReach reach : reaches) {
                Context reachContext = reachTemporalContexts.get(reach.ID);
                ReachProgramSet reachProgramSet = reachProgramSets.get(reach.ID);

                reachRoutingTask.put(reach.ID, new ContextTask(reachContext, reachProgramSet.routeProgram));
                reachOutputTask.put(reach.ID, createOutputRunnableReach(reachContext));
            }

            reachRoutingExecutor = EntityExecxutors.forReach(parameters.flagParallel, reaches, routingTable, reachRoutingTask);
            Runnable reachOutputExecutor = EntityExecxutors.sequentialReach(reachesOutputOrdered, reachOutputTask);
            outputExecutors.add(reachOutputExecutor);
        }

        TemporalParameterReader temporalReader = null;
        if (parameters.temporalAdditionalFilePath != null) {
            temporalReader = new TemporalParameterReader(parameters.temporalAdditionalFilePath, temporalContext);
        }

        for (time = parameters.startTime; !time.isAfter(parameters.endTime); time = time.plusDays(1L)) {
            if (time.getDayOfMonth() == 1) {
                long now = System.currentTimeMillis();
                System.out.println(time + " [" + (now - last) + " ms]");
                last = now;
            }
            temporalContext.put("time", time);
            readerCO2.readData();
            readerCO2.setValue(temporalContext);

            if (temporalReader != null) {
                temporalReader.read();
            }

            updateClimateReaders();
            updateManagementSchedules();

            surfaceExecutor.run();
            subSurfaceExecutor.run();

            if (reachRoutingExecutor != null) {
                reachRoutingExecutor.run();
            }

            outputExecutors.parallelStream().forEach((executor) -> {
                executor.run();
            });
        }
    }

    private void emulateHRUPrograms(Program initProgram, Program surfaceProgram, Program subSurfaceProgram) {
        HRU firstHRU = hrus.get(0);
        Context hruContext = hruTemporalContexts.get(firstHRU.ID);
        Context dummyContext = Contexts.createIdentityContext(hruContext);
        initProgram.emulate(dummyContext);

        // add temporal data
        dummyContext.put("time", "time");

        // add climate data
        dummyContext.put("tmin", "tmin");
        dummyContext.put("tmax", "tmax");
        dummyContext.put("tmean", "tmean");
        dummyContext.put("hum", "hum");
        dummyContext.put("precip", "precip");
        dummyContext.put("sol", "sol");
        dummyContext.put("wind", "wind");

        // add management schedule data
        dummyContext.put("crop", "crop");
        dummyContext.put("management", "management");
        dummyContext.put("irrigation", "irrigation");
        dummyContext.put("nextTillageDate", "nextTillageDate");
        dummyContext.put("nextFertilizationDate", "nextFertilizationDate");
        dummyContext.put("nextPlantingDate", "nextPlantingDate");
        dummyContext.put("nextHarvestingDate", "nextHarvestingDate");
        dummyContext.put("nextFallowDate", "nextFallowDate");
        dummyContext.put("nextTillageOperation", "nextTillageOperation");
        dummyContext.put("nextFertilizationOperation", "nextFertilizationOperation");
        dummyContext.put("nextPlantingOperation", "nextPlantingOperation");
        dummyContext.put("nextHarvestingOperation", "nextHarvestingOperation");
        dummyContext.put("nextFallowOperation", "nextFallowOperation");

        // add mean temperature layer variables
        dummyContext.put("tmeanavg", "tmeanavg");
        dummyContext.put("tmeansum", "tmeansum");
        dummyContext.put("surfacetemp", "surfacetemp");
        dummyContext.put("soil_Temp_Layer", "soil_Temp_Layer");

        surfaceProgram.emulate(dummyContext);

        subSurfaceProgram.emulate(dummyContext);
    }

    private void emulateReachPrograms(Program routeProgram) {
        StreamReach firstReach = reaches.get(0);
        Context reachContext = reachTemporalContexts.get(firstReach.ID);
        Context dummyContext = Contexts.createIdentityContext(reachContext);
        routeProgram.emulate(dummyContext);
    }

    private void runInitProgram() {
        long now1 = System.currentTimeMillis();
        hrus.parallelStream().forEach((hru) -> {
            Context hruContext = hruTemporalContexts.get(hru.ID);
            HRUProgramSet hruProgramSet = hruProgramSets.get(hru.ID);
            hruProgramSet.initProgram.execute(hruContext);
        });
        long now2 = System.currentTimeMillis();
        System.out.println("--> Process initialization completed ... " + (now2 - now1));
    }

    private void initializeClimateReaders() {
        readerCO2 = new CO2ClimateReader(parameters.dataFileCO2, parameters.defaultCO2, parameters.startTime, parameters.endTime);

        if (parameters.flagRegionalization) {
            Path regFilePath;
            regFilePath = getRegionalizedFilePath(parameters.dataFileTmin);
            readerTmin = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.MINIMUM_TEMPERATURE);
            regFilePath = getRegionalizedFilePath(parameters.dataFileTmax);
            readerTmax = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.MAXIMUM_TEMPERATURE);
            regFilePath = regFilePath.resolveSibling("reg_tmean.csv");
            readerTmean = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.MEAN_TEMPERATURE);
            regFilePath = getRegionalizedFilePath(parameters.dataFileHum);
            readerHum = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.HUMIDITY);
            regFilePath = getRegionalizedFilePath(parameters.dataFilePrecip);
            readerPrecip = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.PRECIPITATION);
            regFilePath = getRegionalizedFilePath(parameters.dataFileSol);
            readerSol = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.SOLAR);
            regFilePath = getRegionalizedFilePath(parameters.dataFileWind);
            readerWind = new RegionalizedClimateReader(regFilePath, parameters.startTime, parameters.endTime, ClimateType.WIND);
        } else {
            readerTmin = new StationClimateReader(parameters.dataFileTmin, parameters.startTime, parameters.endTime, ClimateType.MINIMUM_TEMPERATURE);
            readerTmax = new StationClimateReader(parameters.dataFileTmax, parameters.startTime, parameters.endTime, ClimateType.MAXIMUM_TEMPERATURE);
            readerTmean = new TmeanStationClimateReader();
            readerHum = new StationClimateReader(parameters.dataFileHum, parameters.startTime, parameters.endTime, ClimateType.HUMIDITY);
            readerPrecip = new StationClimateReader(parameters.dataFilePrecip, parameters.startTime, parameters.endTime, ClimateType.PRECIPITATION);
            readerSol = new StationClimateReader(parameters.dataFileSol, parameters.startTime, parameters.endTime, ClimateType.SOLAR);
            readerWind = new StationClimateReader(parameters.dataFileWind, parameters.startTime, parameters.endTime, ClimateType.WIND);
        }
    }

    private Path getRegionalizedFilePath(Path dataFilePath) {
        return dataFilePath.resolveSibling("reg_" + dataFilePath.getFileName());
    }

    private void updateClimateReaders() throws IOException {
        readerTmin.readData();
        readerTmax.readData();
        readerTmean.readData();
        readerHum.readData();
        readerPrecip.readData();
        readerSol.readData();
        readerWind.readData();
    }

    private void updateClimates(Context hruContext) {
        readerTmin.setValue(hruContext);
        readerTmax.setValue(hruContext);
        readerHum.setValue(hruContext);
        readerSol.setValue(hruContext);
        readerWind.setValue(hruContext);

        // Order matters here for station reading
        // depends on tmin and tmax
        readerTmean.setValue(hruContext);
        // depends on wind and tmean
        readerPrecip.setValue(hruContext);
    }

    private void initializeManagementSchedules() {
        rotationSchedules = new HashMap<>();
        if (parameters.flagIrrigation) {
            for (Map.Entry<Integer, List<Integer>> rotationEntry : rotations.entrySet()) {
                ManagementSchedule schedule = new ManagementSchedule(rotationEntry.getKey(),
                        rotationEntry.getValue(), managements, managementIrrigationMap,
                        irrigations, parameters.startTime);
                rotationSchedules.put(rotationEntry.getKey(), schedule);
            }
        } else {
            for (Map.Entry<Integer, List<Integer>> rotationEntry : rotations.entrySet()) {
                ManagementSchedule schedule = new ManagementSchedule(rotationEntry.getKey(),
                        rotationEntry.getValue(), managements, parameters.startTime);
                rotationSchedules.put(rotationEntry.getKey(), schedule);
            }
        }
    }

    private void updateManagementSchedules() {
        for (ManagementSchedule schedule : rotationSchedules.values()) {
            schedule.moveToNextDay();
        }
    }

    private void updateSchedule(Context hruContext) {
        HRU hru = hruContext.get("hru", HRU.class);
        HRURotation rotation = hruRotations.get(hru.ID);
        ManagementSchedule schedule = rotationSchedules.get(rotation.getRotationId());

        Crop crop = schedule.getCurrentCrop();
        hruContext.put("crop", crop);
        if (crop == null) {
            hruContext.put("cropid", -1);
        } else {
            hruContext.put("cropid", crop.cid);
        }

        ManagementOperation currentOperation = schedule.getCurrentOperation();
        hruContext.put("management", currentOperation, ManagementOperation.class);
        hruContext.put("irrigation", schedule.getCurrentIrrigation(), Irrigation.class);
        hruContext.put("nextTillageDate", schedule.getNextTillageDate());
        hruContext.put("nextFertilizationDate", schedule.getNextFertilizationDate());
        hruContext.put("nextPlantingDate", schedule.getNextPlantingDate());
        hruContext.put("nextHarvestingDate", schedule.getNextHarvestingDate());
        hruContext.put("nextFallowDate", schedule.getNextFallowDate());
        hruContext.put("nextTillageOperation", schedule.getNextTillageOperation());
        hruContext.put("nextFertilizationOperation", schedule.getNextFertilizationOperation());
        hruContext.put("nextPlantingOperation", schedule.getNextPlantingOperation());
        hruContext.put("nextHarvestingOperation", schedule.getNextHarvestingOperation());
        hruContext.put("nextFallowOperation", schedule.getNextFallowOperation());
    }

    private void initializeOutputWriters() {
        initializeOutputWritersHRU();
        initializeOutputWritersReach();
    }

    private void initializeOutputWritersHRU() {
        EntityIDFilter filter = EntityIDFilter.create(parameters.idSet_hru);

        String splitAttribute = null;
        // splitting only takes place if flagSpit is true and there is a filter
        if (parameters.flagSplit && filter != null) {
            splitAttribute = "ID";
        }

        writer_hru = new OutputWriter(parameters.outFile_hru, parameters.attrSet_hru);
        writer_hru.setFilter(filter);
        writer_hru.setSplitAttribute(splitAttribute);
        weigher_hru = new HRUAreaWeigher(parameters.attrSet_hru_w);

        writer_catch = new OutputWriter(parameters.outFile_catch, parameters.attrSet_catch);
        aggegator_catch = new CatchmentAggregator(parameters.attrSet_catch, parameters.attrSet_catch_w);

        writer_hru_crop = new OutputWriter(parameters.outFile_hru_crop, parameters.attrSet_hru_crop);
        writer_hru_crop.setFilter(filter);
        writer_hru_crop.setSplitAttribute(splitAttribute);
        weigher_hru_crop = new HRUAreaWeigher(parameters.attrSet_hru_crop_w);

        writer_catch_crop = new OutputWriter(parameters.outFile_catch_crop, parameters.attrSet_catch_crop);
        aggegator_catch_crop = new CatchmentAggregator(parameters.attrSet_catch_crop, parameters.attrSet_catch_crop_w);

        writer_hru_layer = new OutputWriter(parameters.outFile_hru_layer, parameters.attrSet_hru_layer);
        writer_hru_layer.setFilter(filter);
        writer_hru_layer.setSplitAttribute(splitAttribute);
        weigher_hru_layer = new HRUAreaWeigher(parameters.attrSet_hru_layer_w);

        writer_hru_n_mb = new OutputWriter(parameters.outFile_hru_n_mb, parameters.attrSet_hru_n_mb);
        writer_hru_n_mb.setFilter(filter);
        writer_hru_n_mb.setSplitAttribute(splitAttribute);
        weigher_hru_n_mb = new HRUAreaWeigher(parameters.attrSet_hru_n_mb_w);

        writer_catch_n_mb = new OutputWriter(parameters.outFile_catch_n_mb, parameters.attrSet_catch_n_mb);
        aggegator_catch_n_mb = new CatchmentAggregator(parameters.attrSet_catch_n_mb, parameters.attrSet_catch_n_mb_w);

        writer_hru_n_pool = new OutputWriter(parameters.outFile_hru_n_pool, parameters.attrSet_hru_n_pool);
        writer_hru_n_pool.setFilter(filter);
        writer_hru_n_pool.setSplitAttribute(splitAttribute);
        weigher_hru_n_pool = new HRUAreaWeigher(parameters.attrSet_hru_n_pool_w);

        writer_catch_n_pool = new OutputWriter(parameters.outFile_catch_n_pool, parameters.attrSet_catch_n_pool);
        aggegator_catch_n_pool = new CatchmentAggregator(parameters.attrSet_catch_n_pool, parameters.attrSet_catch_n_pool_w);

        writer_hru_sediment = new OutputWriter(parameters.outFile_hru_sediment, parameters.attrSet_hru_sediment);
        writer_hru_sediment.setFilter(filter);
        writer_hru_sediment.setSplitAttribute(splitAttribute);
        weigher_hru_sediment = new HRUAreaWeigher(parameters.attrSet_hru_sediment_w);

        writer_catch_sediment = new OutputWriter(parameters.outFile_catch_sediment, parameters.attrSet_catch_sediment);
        aggegator_catch_sediment = new CatchmentAggregator(parameters.attrSet_catch_sediment, parameters.attrSet_catch_sediment_w);

        if (parameters.flagUPGM) {
            writer_hru_crop_upgm = new OutputWriter(parameters.outFile_hru_crop_upgm, parameters.attrSet_hru_crop_upgm);
            writer_hru_crop_upgm.setFilter(filter);
            writer_hru_crop_upgm.setSplitAttribute(splitAttribute);
            weigher_hru_crop_upgm = new HRUAreaWeigher(parameters.attrSet_hru_crop_upgm_w);

            writer_catch_crop_upgm = new OutputWriter(parameters.outFile_catch_crop_upgm, parameters.attrSet_catch_crop_upgm);
            aggegator_catch_crop_upgm = new CatchmentAggregator(parameters.attrSet_catch_crop_upgm, parameters.attrSet_catch_crop_upgm_w);
        }
    }

    private void initializeOutputWritersReach() {
        if (!parameters.flagReachRouting) {
            return;
        }

        EntityIDFilter filter = EntityIDFilter.create(parameters.idSet_reach);

        String splitAttribute = null;
        // splitting only takes place if flagSpit is true and there is a filter
        if (parameters.flagSplit && filter != null) {
            splitAttribute = "ID";
        }

        writer_reach = new OutputWriter(parameters.outFile_reach, parameters.attrSet_reach);
        writer_reach.setFilter(filter);
        writer_reach.setSplitAttribute(splitAttribute);

        writer_outlet = new OutputWriter(parameters.outFile_outlet, parameters.attrSet_outlet);
        aggregator_outlet = new OutletAggregator();
    }

    private Runnable createOutputExecutorHRU(Context hruContext) {
        List<Runnable> tasks = new ArrayList<>();

        tasks.add(createOutputRunnableHRU(writer_hru, weigher_hru, hruContext));
        tasks.add(createOutputRunnableHRU(writer_hru_crop, weigher_hru_crop, hruContext));
        tasks.add(createOutputRunnableHRU(writer_hru_layer, weigher_hru_layer, hruContext));
        tasks.add(createOutputRunnableHRU(writer_hru_n_mb, weigher_hru_n_mb, hruContext));
        tasks.add(createOutputRunnableHRU(writer_hru_n_pool, weigher_hru_n_pool, hruContext));
        tasks.add(createOutputRunnableHRU(writer_hru_sediment, weigher_hru_sediment, hruContext));

        if (parameters.flagUPGM) {
            tasks.add(createOutputRunnableHRU(writer_hru_crop_upgm, weigher_hru_crop_upgm, hruContext));
        }

        return () -> {
            tasks.parallelStream().forEach((task) -> {
                task.run();
            });
        };
    }

    private Runnable createOutputRunnableHRU(OutputWriter writer, HRUAreaWeigher weigher, Context hruContext) {
        return () -> {
            try {
                writer.write(weigher.weigh(hruContext));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    private Runnable createOutputRunnableReach(Context reachContext) {
        return () -> {
            try {
                writer_reach.write(reachContext);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    private Runnable createCatchmentExecutor() {
        List<Runnable> tasks = new ArrayList<>();

        tasks.add(createCatchmentRunnableHRU(writer_catch, aggegator_catch, catchmentContext_catch));
        tasks.add(createCatchmentRunnableHRU(writer_catch_crop, aggegator_catch_crop, catchmentContext_catch_crop));
        tasks.add(createCatchmentRunnableHRU(writer_catch_n_mb, aggegator_catch_n_mb, catchmentContext_catch_n_mb));
        tasks.add(createCatchmentRunnableHRU(writer_catch_n_pool, aggegator_catch_n_pool, catchmentContext_catch_n_pool));
        tasks.add(createCatchmentRunnableHRU(writer_catch_sediment, aggegator_catch_sediment, catchmentContext_catch_sediment));

        if (parameters.flagUPGM) {
            tasks.add(createCatchmentRunnableHRU(writer_catch_crop_upgm, aggegator_catch_crop_upgm, catchmentContext_catch_crop_upgm));
        }

        if (parameters.flagReachRouting) {
            tasks.add(() -> {
                aggregator_outlet.aggregate(reachTemporalContexts.values(), outletContext, outletAggregateContext);
                try {
                    writer_outlet.write(outletAggregateContext);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            });
        }

        return () -> {
            tasks.parallelStream().forEach((task) -> {
                task.run();
            });
        };
    }

    private Runnable createCatchmentRunnableHRU(OutputWriter writer, CatchmentAggregator aggregator, Context aggregateContext) {
        return () -> {
            aggregator.aggregate(hruTemporalContexts.values(), aggregateContext);
            try {
                writer.write(aggregateContext);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    private void closeReaders() {
        readerTmin.close();
        readerTmax.close();
        readerTmean.close();
        readerHum.close();
        readerPrecip.close();
        readerSol.close();
        readerWind.close();
        readerCO2.close();
    }

    private void closeWriters() {
        writer_hru.close();
        writer_hru_crop.close();
        writer_hru_layer.close();
        writer_hru_n_mb.close();
        writer_hru_n_pool.close();
        writer_hru_sediment.close();

        writer_catch.close();
        writer_catch_crop.close();
        writer_catch_n_mb.close();
        writer_catch_n_pool.close();
        writer_catch_sediment.close();

        if (parameters.flagUPGM) {
            writer_hru_crop_upgm.close();
            writer_catch_crop_upgm.close();
        }

        if (parameters.flagReachRouting) {
            writer_reach.close();
            writer_outlet.close();
        }
    }

    private void shutdownPrograms() {
        for (HRUProgramSet hruProgramSet : hruProgramSets.values()) {
            hruProgramSet.initProgram.shutdown();
            hruProgramSet.surfaceProgram.shutdown();
            hruProgramSet.subSurfaceProgram.shutdown();
        }
        if (reachProgramSets != null) {
            for (ReachProgramSet reachProgramSet : reachProgramSets.values()) {
                reachProgramSet.routeProgram.shutdown();
            }
        }
    }

    private static class HRUProgramSet {
        public Program initProgram;
        public Program surfaceProgram;
        public Program subSurfaceProgram;
    }

    private static class ReachProgramSet {
        public Program routeProgram;
    }
}
