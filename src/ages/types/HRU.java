
package ages.types;

/**
 *
 * @author Nathan Lighthart
 */
public class HRU {
    public int ID;
    public double x;
    public double y;
    public double elevation;
    public double area;
    public int type;
    public double slope;
    public double aspect;
    public double flowlength;
    public int soilID;
    public int landuseID;
    public int hgeoID;
    public double slopelength;
    public int tiledrainage;
}
