
package ages;

import annotations.Range;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Nathan Lighthart
 */
public class AgESParameters {
    // parameters
    @Description("hru file path")
    public final Path hruFilePath;

    @Description("reach file path")
    public final Path reachFilePath;

    @Description("landuse file path")
    public final Path landuseFilePath;

    @Description("hydro-geology file path")
    public final Path hydroGeologyFilePath;

    @Description("routing parameter file path")
    public final Path routingFilePath;

    public final Path fertilizerFilePath;
    public final Path tillageFilePath;
    public final Path irrigationFilePath;
    public final Path managementFilePath;
    public final Path cropFilePath;
    public final Path cropUPGMFilePath;

    @Description("Irrigation management mapping file path")
    public final Path managementIrrigationFilePath;

    @Description("Rotation parameter file path")
    public final Path rotationFilePath;

    @Description("HRU rotation mapping file path")
    public final Path hruRotationFilePath;
    public final Path soilFilePath;

    @Description("Precipitation climate file path")
    public final Path dataFilePrecip;

    @Description("Minimum temperature file path")
    public final Path dataFileTmin;

    @Description("Maximum temperature file path")
    public final Path dataFileTmax;

    @Description("Humidity climate file path")
    public final Path dataFileHum;

    @Description("Solar climate file path")
    public final Path dataFileSol;

    @Description("Wind climate file path")
    public final Path dataFileWind;

    @Description("CO2 climate file path")
    public final Path dataFileCO2;

    @Description("HRU parameter override file path")
    public final Path hruOverrideFilePath;

    @Description("HRU parameter override file path")
    public final Path dataFileReg;

    @Description("Surface program file path")
    public final Path surfaceProgramFilePath;

    @Description("Sub-Surface program file path")
    public final Path subSurfaceProgramFilePath;

    @Description("Reach-Routing program file path")
    public final Path reachRoutingProgramFilePath;

    @Description("All programs file path")
    public final Path programsFilePath;

    @Description("HRU additional parameters file path")
    public final Path hruAdditionalFilePath;

    @Description("Reach additional parameters file path")
    public final Path reachAdditionalFilePath;

    @Description("Temporal additional parameters file path")
    public final Path temporalAdditionalFilePath;

    @Description("ID set for HRU")
    public final String idSet_hru;

    @Description("ID set for Reach")
    public final String idSet_reach;

    @Description("Output file for HRU Mass Balance")
    public final Path outFile_hru;

    @Description("Attribute set of HRU Mass Balance")
    public final String attrSet_hru;

    @Description("Weighted attribute set of HRU Mass Balance")
    public final String attrSet_hru_w;

    @Description("Output file for HRU Crop Growth")
    public final Path outFile_hru_crop;

    @Description("Attribute set of HRU Crop Growth")
    public final String attrSet_hru_crop;

    @Description("Weighted attribute set of HRU Crop Growth")
    public final String attrSet_hru_crop_w;

    @Description("Output file for HRU Layer")
    public final Path outFile_hru_layer;

    @Description("Attribute set of HRU Layer")
    public final String attrSet_hru_layer;

    @Description("Weighted attribute set of HRU Layer")
    public final String attrSet_hru_layer_w;

    @Description("Output file for HRU Nitrogen Mass Balance")
    public final Path outFile_hru_n_mb;

    @Description("Attribute set of HRU Nitrogen Mass Balance")
    public final String attrSet_hru_n_mb;

    @Description("Weighted attribute set of HRU Nitrogen Mass Balance")
    public final String attrSet_hru_n_mb_w;

    @Description("Output file for HRU Nitrogen Pool")
    public final Path outFile_hru_n_pool;

    @Description("Attribute set of HRU Nitrogen Pool")
    public final String attrSet_hru_n_pool;

    @Description("Weighted attribute set of HRU Nitrogen Pool")
    public final String attrSet_hru_n_pool_w;

    @Description("Output file for HRU Sediment")
    public final Path outFile_hru_sediment;

    @Description("Attribute set of HRU Sediment")
    public final String attrSet_hru_sediment;

    @Description("Weighted attribute set of HRU Sediment")
    public final String attrSet_hru_sediment_w;

    @Description("Output file for HRU Crop UPGM")
    public final Path outFile_hru_crop_upgm;

    @Description("Attribute set of HRU Crop UPGM")
    public final String attrSet_hru_crop_upgm;

    @Description("Weighted attribute set of HRU Crop UPGM")
    public final String attrSet_hru_crop_upgm_w;

    @Description("Output file for Reach")
    public final Path outFile_reach;

    @Description("Attribute set of Reach")
    public final String attrSet_reach;

    @Description("Output file for Catchment Mass Balance")
    public final Path outFile_catch;

    @Description("Attribute set of Catchment Mass Balance")
    public final String attrSet_catch;

    @Description("Weighted attribute set of Catchment Mass Balance")
    public final String attrSet_catch_w;

    @Description("Output file for Catchment Crop Growth")
    public final Path outFile_catch_crop;

    @Description("Attribute set of Catchment Crop Growth")
    public final String attrSet_catch_crop;

    @Description("Weighted attribute set of Catchment Crop Growth")
    public final String attrSet_catch_crop_w;

    @Description("Output file for Catchment Nitrogen Mass Balance")
    public final Path outFile_catch_n_mb;

    @Description("Attribute set of Catchment Nitrogen Mass Balance")
    public final String attrSet_catch_n_mb;

    @Description("Weighted attribute set of Catchment Nitrogen Mass Balance")
    public final String attrSet_catch_n_mb_w;

    @Description("Output file for Catchment Nitrogen Pool")
    public final Path outFile_catch_n_pool;

    @Description("Attribute set of Catchment Nitrogen Pool")
    public final String attrSet_catch_n_pool;

    @Description("Weighted attribute set of Catchment Nitrogen Pool")
    public final String attrSet_catch_n_pool_w;

    @Description("Output file for Catchment Sediment")
    public final Path outFile_catch_sediment;

    @Description("Attribute set of Catchment Sediment")
    public final String attrSet_catch_sediment;

    @Description("Weighted attribute set of Catchment Sediment")
    public final String attrSet_catch_sediment_w;

    @Description("Output file for Catchment UPGM")
    public final Path outFile_catch_crop_upgm;

    @Description("Attribute set of Catchment UPGM")
    public final String attrSet_catch_crop_upgm;

    @Description("Weighted attribute set of Catchment UPGM")
    public final String attrSet_catch_crop_upgm_w;

    @Description("Output file for Outlet")
    public final Path outFile_outlet;

    @Description("Attribute set of Outlet")
    public final String attrSet_outlet;

    @Description("Start of simulation")
    public final LocalDate startTime;

    @Description("End of simulation")
    public final LocalDate endTime;

    // miscellaneous parameters
    @Description("Factor that controls the amount of parallel threads")
    public final double parallelismFactor;

    // InitProcesses Parameters
    @Description("Projection [GK, UTM, LatLong]")
    public final String projection;

    @Description("daily or hourly time steps [d|h]")
    public final String tempRes;

    @Description("east or west of Greenwhich (e|w)")
    public final String locGrw;

    @Description("longitude of time-zone center")
    @Units("deg")
    public final double longTZ;

    @Description("multiplier for field capacity")
    public final double FCAdaptation;

    @Description("multiplier for air capacity")
    public final double ACAdaptation;

    @Description("Initial LPS, fraction of maxiumum LPS, 0.0 .. 1.0")
    public final double initLPS;

    @Description("Initial MPS, fraction of maxiumum MPS, 0.0 ... 1.0")
    public final double initMPS;

    @Description("DB function")
    public final boolean equalWeights;

    @Description("power of IDW function for mean temperature regionalization")
    public final double pidwTmean;

    @Description("power of IDW function for minimum temperature regionalization")
    public final double pidwTmin;

    @Description("power of IDW function for maximum temperature regionalization")
    public final double pidwTmax;

    @Description("power of IDW function for humidity regionalization")
    public final double pidwHum;

    @Description("power of IDW function for precipitation regionalization")
    public final double pidwPrecip;

    @Description("power of IDW function for solar regionalization")
    public final double pidwSol;

    @Description("power of IDW function for wind regionalization")
    public final double pidwWind;

    // Regionalization Parameters
    public final double nidwTmean;
    public final double nidwTmin;
    public final double nidwTmax;
    public final double nidwHum;
    public final double nidwPrecip;
    public final double nidwSol;
    public final double nidwWind;

    @Description("elevation correction mean temperature (1=yes|0=no)")
    public final int elevationCorrectionTmean;

    @Description("elevation correction minimum temperature (1=yes|0=no)")
    public final int elevationCorrectionTmin;

    @Description("elevation correction maximum temperature (1=yes|0=no)")
    public final int elevationCorrectionTmax;

    @Description("elevation correction humidity (1=yes|0=no)")
    public final int elevationCorrectionHum;

    @Description("elevation correction precipitation (1=yes|0=no)")
    public final int elevationCorrectionPrecip;

    @Description("elevation correction solar (1=yes|0=no)")
    public final int elevationCorrectionSol;

    @Description("elevation correction wind (1=yes|0=no)")
    public final int elevationCorrectionWind;

    @Description("r-square threshold for mean temperature elevation correction")
    public final double rsqThresholdTmean;

    @Description("r-square threshold for minimum temperature elevation correction")
    public final double rsqThresholdTmin;

    @Description("r-square threshold for maximum temperature elevation correction")
    public final double rsqThresholdTmax;

    @Description("r-square threshold for humidity elevation correction")
    public final double rsqThresholdHum;

    @Description("r-square threshold for precipitation elevation correction")
    public final double rsqThresholdPrecip;

    @Description("r-square threshold for solar elevation correction")
    public final double rsqThresholdSol;

    @Description("r-square threshold for wind elevation correction")
    public final double rsqThresholdWind;

    @Description("number of closest temperature stations for precipitation correction")
    public final int tempNIDW;

    @Description("power of IDW function for precipitation correction")
    public final double pIDW;

    @Description("number of closest wind stations for precipitation correction")
    public final int windNIDW;

    @Description("precipitation correction methods: 0 OFF; 1 Richter; 2 Sevruk; 3 Baisheng")
    public final int precipCorrectMethod;

    @Description("regression threshold")
    public final double regThres;

    public final double snow_trs;
    public final double snow_trans;

    // surface parameters
    @Description("Humidity input type")
    public final String humidity;

    @Description("Solar input type")
    public final String solar;

    @Description("parameter a for Angstroem formula")
    public final double angstrom_a;

    @Description("parameter b for Angstroem formula")
    public final double angstrom_b;

    @Description("maximum storage capacity per LAI for rain")
    @Units("mm")
    public final double a_rain;

    @Description("maximum storage capacity per LAI for snow")
    @Units("mm")
    public final double a_snow;

    @Description("melting temperature")
    @Units("degC")
    public final double baseTemp;

    @Description("temperature factor for snow melt calculation")
    public final double t_factor;

    @Description("rain factor for snow melt calculation")
    public final double r_factor;

    @Description("soil heat factor for snow melt calculation")
    public final double g_factor;

    @Description("snowpack density beyond free water is released")
    @Units("dec%")
    public final double snowCritDens;

    @Description("cold content factor")
    public final double ccf_factor;

    @Description("snow_factor1")
    public final double snowFactorA;

    @Description("snow_factor2")
    public final double snowFactorB;

    @Description("snow_summand")
    public final double snowFactorC;

    @Description("snow_dens_st_minus15")
    public final double snowDensConst;

    // sub surface parameters
    @Description("percolation coefficient")
    @Range(min = 0.0, max = 1.0)
    public final double Beta_NO3;

    @Description("distance between tile drains [cm]")
    @Units("cm")
    @Range(min = 0.0, max = 10000.0)
    public final double drspac;

    @Description("radius of tile drains [cm]")
    @Units("cm")
    @Range(min = 0.0, max = 100.0)
    public final double drrad;

    @Description("depth of tile drains [cm]")
    @Units("cm")
    @Range(min = 0.0, max = 500.0)
    public final double depdr;

    @Description("maximum depression storage capacity")
    @Units("mm")
    public final double soilMaxDPS;

    @Description("potential reduction coefficient for AET computation")
    public final double soilPolRed;

    @Description("linear reduction coefficient for AET computation")
    public final double soilLinRed;

    @Description("maximum infiltration in summer")
    @Units("mm/d")
    public final double soilMaxInfSummer;

    @Description("maximum infiltration in winter")
    @Units("mm/d")
    public final double soilMaxInfWinter;

    @Description("maximum infiltration for snow covered areas")
    @Units("mm/d")
    public final double soilMaxInfSnow;

    @Description("relative infiltration for impervious areas greater than 80% sealing")
    public final double soilImpGT80;

    @Description("relative infiltration for impervious areas less than 80% sealing")
    public final double soilImpLT80;

    @Description("MPS/LPS distribution coefficient")
    public final double soilDistMPSLPS;

    @Description("MPS/LPS diffusion coefficient")
    public final double soilDiffMPSLPS;

    @Description("outflow coefficient for LPS")
    public final double soilOutLPS;

    @Description("lateral-vertical distribution coefficient")
    public final double soilLatVertLPS;

    @Description("maximum percolation rate")
    @Units("mm/d")
    public final double soilMaxPerc;

    @Description("maximum percolation rate out of soil")
    @Units("mm/d")
    public final double geoMaxPerc;

    @Description("water-use distribution parameter for transpiration")
    public final double BetaW;

    @Description("Layer MPS diffusion factor > 0 [-] resistance default = 10")
    public final double kdiff_layer;

    @Description("parameter for frozen soil adjustment on infiltration/runoff")
    public final double cn_froz;

    @Description("SCS runoff curve number for moisture condition II")
    public final double cnn;

    @Description("CN method flag (0,1,2)")
    public final int icn;

    @Description("curve number surface runoff adjustment factor for flat slopes")
    @Range(min = 0.5, max = 3.0)
    public final double r2adj;

    @Description("fraction of HRU area that is classified as directly connected impervious")
    @Range(min = 0.0, max = 1.0)
    public final double fcimp;

    @Description("Date to start reduction")
    public final LocalDate startReduction;

    @Description("Date to end reduction")
    public final LocalDate endReduction;

    @Description("Indicates fertilization optimization with plant demand.")
    public final double opti;

    @Description("light extinction coefficient")
    @Range(min = -1.0, max = 0.0)
    public final double LExCoef;

    @Description("factor of root depth")
    @Range(min = 0, max = 10)
    public final double rootfactor;

    @Description("temperature lag factor for soil")
    @Range(min = 0.0, max = 1.0)
    public final double temp_lag;

    @Description("switch for mulch drilling scenario")
    public final double sceno;

    @Description("Piadin (nitrification blocker) application")
    @Range(min = 0.0, max = 1.0)
    public final int piadin;

    @Description("rate constant between N_activ_pool and N_stable_pool")
    @Range(min = 1.0E-6, max = 1.0E-4)
    public final double Beta_trans;

    @Description("rate factor between N_activ_pool and NO3_Pool")
    @Range(min = 0.001, max = 0.003)
    public final double Beta_min;

    @Description("rate factor between Residue_pool and NO3_Pool")
    @Range(min = 0.02, max = 0.10)
    public final double Beta_rsd;

    @Description("nitrogen uptake distribution parameter")
    @Range(min = 1, max = 15)
    public final double Beta_Ndist;

    @Description("infiltration bypass parameter")
    @Range(min = 0.0, max = 1.0)
    public final double infil_conc_factor;

    @Description("denitrification saturation factor")
    @Range(min = 0.0, max = 1.0)
    public final double denitfac;

    @Description("concentration of Nitrate in rain")
    @Units("kgN/(mm * ha)")
    @Range(min = 0.0, max = 0.05)
    public final double deposition_factor;

    @Description("fraction of porosity from which anions are excluded")
    @Range(min = 0.01, max = 1)
    public final double theta_nit;

    @Description("adaptation of RG1 outflow")
    public final double gwRG1Fact;

    @Description("adaptation of RG2 outflow")
    public final double gwRG2Fact;

    @Description("RG1-RG2 distribution coefficient")
    public final double gwRG1RG2dist;

    @Description("capillary rise coefficient")
    public final double gwCapRise;

    @Description("groundwater init")
    public final double initRG1;

    @Description("groundwater init")
    public final double initRG2;

    @Range(min = 0.0, max = 1.0)
    public final double musi_co1;

    @Range(min = 0.0, max = 1.0)
    public final double musi_co2;

    @Range(min = 0.0, max = 1.0)
    public final double musi_co3;

    @Range(min = 0.0, max = 1.0)
    public final double musi_co4;

    @Description("relative size of the groundwaterN damping tank RG1")
    @Range(min = 0.0, max = 10.0)
    public final double N_delay_RG1;

    @Description("relative size of the groundwaterN damping tank RG2")
    @Range(min = 0.0, max = 10.0)
    public final double N_delay_RG2;

    @Description("N concentration for RG1")
    @Units("mgN/l")
    @Range(min = 0.0, max = 10.0)
    public final double N_concRG1;

    @Description("N concentration for RG2")
    @Units("mgN/l")
    @Range(min = 0.0, max = 10.0)
    public final double N_concRG2;

    @Description("Default CO2")
    @Units("ppm")
    public final double defaultCO2;

    // reach routing parameters
    @Description("flow routing coefficient TA")
    public final double flowRouteTA;

    @Description("K-Value for the streambed")
    @Units("cm/d")
    public final double Ksink;

    // flags
    @Description("HRU routing flag")
    public final boolean flagHRURouting;

    @Description("reach routing flag")
    public final boolean flagReachRouting;

    @Description("UPGM crop growth model flag")
    public final boolean flagUPGM;

    @Description("Irrigation flag")
    public final boolean flagIrrigation;

    @Description("Regionalization flag")
    public final boolean flagRegionalization;

    @Description("tile drainage flag")
    public final boolean flagTileDrain;

    @Description("tillage management flag")
    public final boolean flagTillage;

    @Description("flag for infiltration method (eagleson, bucket, curvenumber)")
    public final String flagInfiltration;

    @Description("sequentially sort output flag")
    public final boolean flagSort;

    @Description("split output flag")
    public final boolean flagSplit;

    @Description("parallel execution flag")
    public final String flagParallel;

    private Map<String, Parameter> additionalParameterMap;

    public void addAdditionalParameters(Context context) {
        for (Parameter p : additionalParameterMap.values()) {
            p.addToContext(context);
        }
    }

    private AgESParameters(Builder builder) {
        // parameters
        hruFilePath = builder.hruFilePath;
        reachFilePath = builder.reachFilePath;
        landuseFilePath = builder.landuseFilePath;
        hydroGeologyFilePath = builder.hydroGeologyFilePath;
        routingFilePath = builder.routingFilePath;
        fertilizerFilePath = builder.fertilizerFilePath;
        tillageFilePath = builder.tillageFilePath;
        irrigationFilePath = builder.irrigationFilePath;
        managementFilePath = builder.managementFilePath;
        cropFilePath = builder.cropFilePath;
        cropUPGMFilePath = builder.cropUPGMFilePath;
        managementIrrigationFilePath = builder.managementIrrigationFilePath;
        rotationFilePath = builder.rotationFilePath;
        hruRotationFilePath = builder.hruRotationFilePath;
        soilFilePath = builder.soilFilePath;
        dataFilePrecip = builder.dataFilePrecip;
        dataFileTmin = builder.dataFileTmin;
        dataFileTmax = builder.dataFileTmax;
        dataFileHum = builder.dataFileHum;
        dataFileSol = builder.dataFileSol;
        dataFileWind = builder.dataFileWind;
        dataFileCO2 = builder.dataFileCO2;
        hruOverrideFilePath = builder.hruOverrideFilePath;
        dataFileReg = builder.dataFileReg;

        surfaceProgramFilePath = builder.surfaceProgramFilePath;
        subSurfaceProgramFilePath = builder.subSurfaceProgramFilePath;
        reachRoutingProgramFilePath = builder.reachRoutingProgramFilePath;
        programsFilePath = builder.programsFilePath;

        hruAdditionalFilePath = builder.hruAdditionalFilePath;
        reachAdditionalFilePath = builder.reachAdditionalFilePath;
        temporalAdditionalFilePath = builder.temporalAdditionalFilePath;

        idSet_hru = builder.idSet_hru;
        idSet_reach = builder.idSet_reach;
        outFile_hru = builder.outFile_hru;
        attrSet_hru = builder.attrSet_hru;
        attrSet_hru_w = builder.attrSet_hru_w;
        outFile_hru_crop = builder.outFile_hru_crop;
        attrSet_hru_crop = builder.attrSet_hru_crop;
        attrSet_hru_crop_w = builder.attrSet_hru_crop_w;
        outFile_hru_layer = builder.outFile_hru_layer;
        attrSet_hru_layer = builder.attrSet_hru_layer;
        attrSet_hru_layer_w = builder.attrSet_hru_layer_w;
        outFile_hru_n_mb = builder.outFile_hru_n_mb;
        attrSet_hru_n_mb = builder.attrSet_hru_n_mb;
        attrSet_hru_n_mb_w = builder.attrSet_hru_n_mb_w;
        outFile_hru_n_pool = builder.outFile_hru_n_pool;
        attrSet_hru_n_pool = builder.attrSet_hru_n_pool;
        attrSet_hru_n_pool_w = builder.attrSet_hru_n_pool_w;
        outFile_hru_sediment = builder.outFile_hru_sediment;
        attrSet_hru_sediment = builder.attrSet_hru_sediment;
        attrSet_hru_sediment_w = builder.attrSet_hru_sediment_w;
        outFile_hru_crop_upgm = builder.outFile_hru_crop_upgm;
        attrSet_hru_crop_upgm = builder.attrSet_hru_crop_upgm;
        attrSet_hru_crop_upgm_w = builder.attrSet_hru_crop_upgm_w;
        outFile_reach = builder.outFile_reach;
        attrSet_reach = builder.attrSet_reach;
        outFile_catch = builder.outFile_catch;
        attrSet_catch = builder.attrSet_catch;
        attrSet_catch_w = builder.attrSet_catch_w;
        outFile_catch_crop = builder.outFile_catch_crop;
        attrSet_catch_crop = builder.attrSet_catch_crop;
        attrSet_catch_crop_w = builder.attrSet_catch_crop_w;
        outFile_catch_n_mb = builder.outFile_catch_n_mb;
        attrSet_catch_n_mb = builder.attrSet_catch_n_mb;
        attrSet_catch_n_mb_w = builder.attrSet_catch_n_mb_w;
        outFile_catch_n_pool = builder.outFile_catch_n_pool;
        attrSet_catch_n_pool = builder.attrSet_catch_n_pool;
        attrSet_catch_n_pool_w = builder.attrSet_catch_n_pool_w;
        outFile_catch_sediment = builder.outFile_catch_sediment;
        attrSet_catch_sediment = builder.attrSet_catch_sediment;
        attrSet_catch_sediment_w = builder.attrSet_catch_sediment_w;
        outFile_catch_crop_upgm = builder.outFile_catch_crop_upgm;
        attrSet_catch_crop_upgm = builder.attrSet_catch_crop_upgm;
        attrSet_catch_crop_upgm_w = builder.attrSet_catch_crop_upgm_w;
        outFile_outlet = builder.outFile_outlet;
        attrSet_outlet = builder.attrSet_outlet;

        startTime = builder.startTime;
        endTime = builder.endTime;

        // miscellaneous parameters
        parallelismFactor = builder.parallelismFactor;

        // InitProcesses Parameters
        projection = builder.projection;
        tempRes = builder.tempRes;
        locGrw = builder.locGrw;
        longTZ = builder.longTZ;
        FCAdaptation = builder.FCAdaptation;
        ACAdaptation = builder.ACAdaptation;
        initLPS = builder.initLPS;
        initMPS = builder.initMPS;
        equalWeights = builder.equalWeights;
        pidwTmean = builder.pidwTmean;
        pidwTmin = builder.pidwTmin;
        pidwTmax = builder.pidwTmax;
        pidwHum = builder.pidwHum;
        pidwPrecip = builder.pidwPrecip;
        pidwSol = builder.pidwSol;
        pidwWind = builder.pidwWind;

        // Regionalization Parameters
        nidwTmean = builder.nidwTmean;
        nidwTmin = builder.nidwTmin;
        nidwTmax = builder.nidwTmax;
        nidwHum = builder.nidwHum;
        nidwPrecip = builder.nidwPrecip;
        nidwSol = builder.nidwSol;
        nidwWind = builder.nidwWind;
        elevationCorrectionTmean = builder.elevationCorrectionTmean;
        elevationCorrectionTmin = builder.elevationCorrectionTmin;
        elevationCorrectionTmax = builder.elevationCorrectionTmax;
        elevationCorrectionHum = builder.elevationCorrectionHum;
        elevationCorrectionPrecip = builder.elevationCorrectionPrecip;
        elevationCorrectionSol = builder.elevationCorrectionSol;
        elevationCorrectionWind = builder.elevationCorrectionWind;
        rsqThresholdTmean = builder.rsqThresholdTmean;
        rsqThresholdTmin = builder.rsqThresholdTmin;
        rsqThresholdTmax = builder.rsqThresholdTmax;
        rsqThresholdHum = builder.rsqThresholdHum;
        rsqThresholdPrecip = builder.rsqThresholdPrecip;
        rsqThresholdSol = builder.rsqThresholdSol;
        rsqThresholdWind = builder.rsqThresholdWind;
        tempNIDW = builder.tempNIDW;
        pIDW = builder.pIDW;
        windNIDW = builder.windNIDW;
        precipCorrectMethod = builder.precipCorrectMethod;
        regThres = builder.regThres;
        snow_trs = builder.snow_trs;
        snow_trans = builder.snow_trans;

        // surface parameters
        humidity = builder.humidity;
        solar = builder.solar;
        angstrom_a = builder.angstrom_a;
        angstrom_b = builder.angstrom_b;
        a_rain = builder.a_rain;
        a_snow = builder.a_snow;
        baseTemp = builder.baseTemp;
        t_factor = builder.t_factor;
        r_factor = builder.r_factor;
        g_factor = builder.g_factor;
        snowCritDens = builder.snowCritDens;
        ccf_factor = builder.ccf_factor;
        snowFactorA = builder.snowFactorA;
        snowFactorB = builder.snowFactorB;
        snowFactorC = builder.snowFactorC;
        snowDensConst = builder.snowDensConst;

        // sub surface parameters
        Beta_NO3 = builder.Beta_NO3;
        drspac = builder.drspac;
        drrad = builder.drrad;
        depdr = builder.depdr;
        soilMaxDPS = builder.soilMaxDPS;
        soilPolRed = builder.soilPolRed;
        soilLinRed = builder.soilLinRed;
        soilMaxInfSummer = builder.soilMaxInfSummer;
        soilMaxInfWinter = builder.soilMaxInfWinter;
        soilMaxInfSnow = builder.soilMaxInfSnow;
        soilImpGT80 = builder.soilImpGT80;
        soilImpLT80 = builder.soilImpLT80;
        soilDistMPSLPS = builder.soilDistMPSLPS;
        soilDiffMPSLPS = builder.soilDiffMPSLPS;
        soilOutLPS = builder.soilOutLPS;
        soilLatVertLPS = builder.soilLatVertLPS;
        soilMaxPerc = builder.soilMaxPerc;
        geoMaxPerc = builder.geoMaxPerc;
        BetaW = builder.BetaW;
        kdiff_layer = builder.kdiff_layer;
        cn_froz = builder.cn_froz;
        cnn = builder.cnn;
        icn = builder.icn;
        r2adj = builder.r2adj;
        fcimp = builder.fcimp;
        startReduction = builder.startReduction;
        endReduction = builder.endReduction;
        opti = builder.opti;
        LExCoef = builder.LExCoef;
        rootfactor = builder.rootfactor;
        temp_lag = builder.temp_lag;
        sceno = builder.sceno;
        piadin = builder.piadin;
        Beta_trans = builder.Beta_trans;
        Beta_min = builder.Beta_min;
        Beta_rsd = builder.Beta_rsd;
        Beta_Ndist = builder.Beta_Ndist;
        infil_conc_factor = builder.infil_conc_factor;
        denitfac = builder.denitfac;
        deposition_factor = builder.deposition_factor;
        theta_nit = builder.theta_nit;
        gwRG1Fact = builder.gwRG1Fact;
        gwRG2Fact = builder.gwRG2Fact;
        gwRG1RG2dist = builder.gwRG1RG2dist;
        gwCapRise = builder.gwCapRise;
        initRG1 = builder.initRG1;
        initRG2 = builder.initRG2;
        musi_co1 = builder.musi_co1;
        musi_co2 = builder.musi_co2;
        musi_co3 = builder.musi_co3;
        musi_co4 = builder.musi_co4;
        N_delay_RG1 = builder.N_delay_RG1;
        N_delay_RG2 = builder.N_delay_RG2;
        N_concRG1 = builder.N_concRG1;
        N_concRG2 = builder.N_concRG2;
        defaultCO2 = builder.defaultCO2;

        // reach routing parameters
        flowRouteTA = builder.flowRouteTA;
        Ksink = builder.Ksink;

        // flags
        flagHRURouting = builder.flagHRURouting;
        flagReachRouting = builder.flagReachRouting;
        flagUPGM = builder.flagUPGM;
        flagIrrigation = builder.flagIrrigation;
        flagRegionalization = builder.flagRegionalization;
        flagTileDrain = builder.flagTileDrain;
        flagTillage = builder.flagTillage;
        flagInfiltration = (builder.flagInfiltration == null ? null : builder.flagInfiltration.toLowerCase());
        flagSort = builder.flagSort;
        flagSplit = builder.flagSplit;
        flagParallel = builder.flagParallel;

        additionalParameterMap = builder.additionalParameterMap;
    }

    public static class Builder {
        // parameters
        private Path hruFilePath;
        private Path reachFilePath;
        private Path landuseFilePath;
        private Path hydroGeologyFilePath;
        private Path routingFilePath;
        private Path fertilizerFilePath;
        private Path tillageFilePath;
        private Path irrigationFilePath;
        private Path managementFilePath;
        private Path cropFilePath;
        private Path cropUPGMFilePath;
        private Path managementIrrigationFilePath;
        private Path rotationFilePath;
        private Path hruRotationFilePath;
        private Path soilFilePath;
        private Path dataFilePrecip;
        private Path dataFileTmin;
        private Path dataFileTmax;
        private Path dataFileHum;
        private Path dataFileSol;
        private Path dataFileWind;
        private Path dataFileCO2;
        private Path hruOverrideFilePath;
        private Path dataFileReg;

        private Path surfaceProgramFilePath;
        private Path subSurfaceProgramFilePath;
        private Path reachRoutingProgramFilePath;
        private Path programsFilePath;

        private Path hruAdditionalFilePath;
        private Path reachAdditionalFilePath;
        private Path temporalAdditionalFilePath;

        private String idSet_hru;
        private String idSet_reach;
        private Path outFile_hru;
        private String attrSet_hru;
        private String attrSet_hru_w;
        private Path outFile_hru_crop;
        private String attrSet_hru_crop;
        private String attrSet_hru_crop_w;
        private Path outFile_hru_layer;
        private String attrSet_hru_layer;
        private String attrSet_hru_layer_w;
        private Path outFile_hru_n_mb;
        private String attrSet_hru_n_mb;
        private String attrSet_hru_n_mb_w;
        private Path outFile_hru_n_pool;
        private String attrSet_hru_n_pool;
        private String attrSet_hru_n_pool_w;
        private Path outFile_hru_sediment;
        private String attrSet_hru_sediment;
        private String attrSet_hru_sediment_w;
        private Path outFile_hru_crop_upgm;
        private String attrSet_hru_crop_upgm;
        private String attrSet_hru_crop_upgm_w;
        private Path outFile_reach;
        private String attrSet_reach;
        private Path outFile_catch;
        private String attrSet_catch;
        private String attrSet_catch_w;
        private Path outFile_catch_crop;
        private String attrSet_catch_crop;
        private String attrSet_catch_crop_w;
        private Path outFile_catch_n_mb;
        private String attrSet_catch_n_mb;
        private String attrSet_catch_n_mb_w;
        private Path outFile_catch_n_pool;
        private String attrSet_catch_n_pool;
        private String attrSet_catch_n_pool_w;
        private Path outFile_catch_sediment;
        private String attrSet_catch_sediment;
        private String attrSet_catch_sediment_w;
        private Path outFile_catch_crop_upgm;
        private String attrSet_catch_crop_upgm;
        private String attrSet_catch_crop_upgm_w;
        private Path outFile_outlet;
        private String attrSet_outlet;

        private LocalDate startTime;
        private LocalDate endTime;

        // miscellaneous parameters
        private double parallelismFactor = 4.0;

        // InitProcesses Parameters
        private String projection;
        private String tempRes; // also used in surface
        private String locGrw;
        private double longTZ;
        private double FCAdaptation;
        private double ACAdaptation;
        private double initLPS;
        private double initMPS;
        private boolean equalWeights;
        private double pidwTmean;
        private double pidwTmin;
        private double pidwTmax;
        private double pidwHum;
        private double pidwPrecip;
        private double pidwSol;
        private double pidwWind;

        // Regionalization Parameters
        private double nidwTmean;
        private double nidwTmin;
        private double nidwTmax;
        private double nidwHum;
        private double nidwPrecip;
        private double nidwSol;
        private double nidwWind;
        private int elevationCorrectionTmean;
        private int elevationCorrectionTmin;
        private int elevationCorrectionTmax;
        private int elevationCorrectionHum;
        private int elevationCorrectionPrecip;
        private int elevationCorrectionSol;
        private int elevationCorrectionWind;
        private double rsqThresholdTmean;
        private double rsqThresholdTmin;
        private double rsqThresholdTmax;
        private double rsqThresholdHum;
        private double rsqThresholdPrecip;
        private double rsqThresholdSol;
        private double rsqThresholdWind;
        private int tempNIDW;
        private double pIDW;
        private int windNIDW;
        private int precipCorrectMethod;
        private double regThres;
        private double snow_trs;
        private double snow_trans;

        // surface parameters
        private String humidity;
        private String solar;
        private double angstrom_a;
        private double angstrom_b;
        private double a_rain;
        private double a_snow;
        private double baseTemp;
        private double t_factor;
        private double r_factor;
        private double g_factor;
        private double snowCritDens;
        private double ccf_factor;
        private double snowFactorA;
        private double snowFactorB;
        private double snowFactorC;
        private double snowDensConst;

        // sub surface parameters
        private double Beta_NO3;
        private double drspac;
        private double drrad;
        private double depdr;
        private double soilMaxDPS;
        private double soilPolRed;
        private double soilLinRed;
        private double soilMaxInfSummer;
        private double soilMaxInfWinter;
        private double soilMaxInfSnow;
        private double soilImpGT80;
        private double soilImpLT80;
        private double soilDistMPSLPS;
        private double soilDiffMPSLPS;
        private double soilOutLPS;
        private double soilLatVertLPS;
        private double soilMaxPerc;
        private double geoMaxPerc;
        private double BetaW;
        private double kdiff_layer;
        private double cn_froz;
        private double cnn;
        private int icn;
        private double r2adj;
        private double fcimp;
        private LocalDate startReduction;
        private LocalDate endReduction;
        private double opti;
        private double LExCoef;
        private double rootfactor;
        private double temp_lag;
        private double sceno;
        private int piadin;
        private double Beta_trans;
        private double Beta_min;
        private double Beta_rsd;
        private double Beta_Ndist;
        private double infil_conc_factor;
        private double denitfac;
        private double deposition_factor;
        private double theta_nit;
        private double gwRG1Fact;
        private double gwRG2Fact;
        private double gwRG1RG2dist;
        private double gwCapRise;
        private double initRG1;
        private double initRG2;
        private double musi_co1;
        private double musi_co2;
        private double musi_co3;
        private double musi_co4;
        private double N_delay_RG1;
        private double N_delay_RG2;
        private double N_concRG1;
        private double N_concRG2;
        private double defaultCO2;

        // reach routing parameters
        private double flowRouteTA;
        private double Ksink;

        // flags
        private boolean flagHRURouting = true;
        private boolean flagReachRouting = true;
        private boolean flagUPGM;
        private boolean flagIrrigation;
        private boolean flagRegionalization = true;
        private boolean flagTileDrain;
        private boolean flagTillage;
        private String flagInfiltration;
        private boolean flagSort;
        private boolean flagSplit;
        private String flagParallel;

        private Map<String, Parameter> additionalParameterMap = new HashMap<>();

        public AgESParameters build() {
            flagIrrigation = (irrigationFilePath != null && managementIrrigationFilePath != null);
            return new AgESParameters(this);
        }

        public Builder parameters(Path parameterFilePath) {
            Map<String, Object> parameterMap = new HashMap<>();
            try (BufferedReader reader = Files.newBufferedReader(parameterFilePath)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] split = line.split(",");
                    if (split.length != 3 || !"@P".equals(split[0])) {
                        continue;
                    }
                    parameterMap.put(split[1], split[2]);
                }
            } catch (IOException ex) {
                throw new RuntimeException("Error reading parameter file: " + parameterFilePath, ex);
            }

            return parameters(parameterMap);
        }

        public Builder parameters(Map<String, Object> parameterMap) {
            Field[] fields = Builder.class.getDeclaredFields();
            for (Field field : fields) {
                if (parameterMap.containsKey(field.getName())) {
                    Object rawValue = parameterMap.get(field.getName());
                    try {
                        setParameter(field, rawValue);
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        throw new RuntimeException("Error setting parameter: " + field.getName(), ex);
                    }
                }
            }

            return this;
        }

        public Builder parameters(Map<String, Object> parameterMap, Map<String, Class<?>> typeMap) {
            // handle field values
            parameters(parameterMap);

            // handle additional parameters
            // find additional parameter names
            Set<String> additionalParameters = new HashSet<>(parameterMap.keySet());
            Field[] fields = Builder.class.getDeclaredFields();
            for (Field field : fields) {
                additionalParameters.remove(field.getName());
            }

            // Create Additional parameter objects
            for (String parameterName : additionalParameters) {
                Parameter p = new Parameter(parameterName,
                        parameterMap.get(parameterName),
                        typeMap.get(parameterName));
                additionalParameterMap.put(parameterName, p);
            }

            return this;
        }

        private void setParameter(Field field, Object rawValue) throws IllegalArgumentException, IllegalAccessException {
            Object value = TypeConversions.INSTANCE.convert(rawValue, field.getType());
            field.setAccessible(true);
            field.set(this, value);
        }

        public Builder hruFilePath(Path hruFilePath) {
            this.hruFilePath = hruFilePath;
            return this;
        }

        public Builder reachFilePath(Path reachFilePath) {
            this.reachFilePath = reachFilePath;
            return this;
        }

        public Builder landuseFilePath(Path landuseFilePath) {
            this.landuseFilePath = landuseFilePath;
            return this;
        }

        public Builder hydroGeologyFilePath(Path hydroGeologyFilePath) {
            this.hydroGeologyFilePath = hydroGeologyFilePath;
            return this;
        }

        public Builder routingFilePath(Path routingFilePath) {
            this.routingFilePath = routingFilePath;
            return this;
        }

        public Builder fertilizerFilePath(Path fertilizerFilePath) {
            this.fertilizerFilePath = fertilizerFilePath;
            return this;
        }

        public Builder tillageFilePath(Path tillageFilePath) {
            this.tillageFilePath = tillageFilePath;
            return this;
        }

        public Builder irrigationFilePath(Path irrigationFilePath) {
            this.irrigationFilePath = irrigationFilePath;
            return this;
        }

        public Builder managementFilePath(Path managementFilePath) {
            this.managementFilePath = managementFilePath;
            return this;
        }

        public Builder cropFilePath(Path cropFilePath) {
            this.cropFilePath = cropFilePath;
            return this;
        }

        public Builder cropUPGMFilePath(Path cropUPGMFilePath) {
            this.cropUPGMFilePath = cropUPGMFilePath;
            return this;
        }

        public Builder managmentIrrigationFilePath(Path managmentIrrigationFilePath) {
            this.managementIrrigationFilePath = managmentIrrigationFilePath;
            return this;
        }

        public Builder rotationFilePath(Path rotationFilePath) {
            this.rotationFilePath = rotationFilePath;
            return this;
        }

        public Builder hruRotationFilePath(Path hruRotationFilePath) {
            this.hruRotationFilePath = hruRotationFilePath;
            return this;
        }

        public Builder soilFilePath(Path soilFilePath) {
            this.soilFilePath = soilFilePath;
            return this;
        }

        public Builder dataFilePrecip(Path dataFilePrecip) {
            this.dataFilePrecip = dataFilePrecip;
            return this;
        }

        public Builder dataFileTmin(Path dataFileTmin) {
            this.dataFileTmin = dataFileTmin;
            return this;
        }

        public Builder dataFileTmax(Path dataFileTmax) {
            this.dataFileTmax = dataFileTmax;
            return this;
        }

        public Builder dataFileHum(Path dataFileHum) {
            this.dataFileHum = dataFileHum;
            return this;
        }

        public Builder dataFileSol(Path dataFileSol) {
            this.dataFileSol = dataFileSol;
            return this;
        }

        public Builder dataFileWind(Path dataFileWind) {
            this.dataFileWind = dataFileWind;
            return this;
        }

        public Builder dataFileCO2(Path dataFileCO2) {
            this.dataFileCO2 = dataFileCO2;
            return this;
        }

        public Builder hruOverrideFilePath(Path hruOverrideFilePath) {
            this.hruOverrideFilePath = hruOverrideFilePath;
            return this;
        }

        public Builder dataFileReg(Path regionalizationFilePath) {
            this.dataFileReg = regionalizationFilePath;
            return this;
        }

        public Builder surfaceProgramFilePath(Path surfaceProgramFilePath) {
            this.surfaceProgramFilePath = surfaceProgramFilePath;
            return this;
        }

        public Builder subSurfaceProgramFilePath(Path subSurfaceProgramFilePath) {
            this.subSurfaceProgramFilePath = subSurfaceProgramFilePath;
            return this;
        }

        public Builder reachRoutingProgramFilePath(Path reachRoutingProgramFilePath) {
            this.reachRoutingProgramFilePath = reachRoutingProgramFilePath;
            return this;
        }

        public Builder programsFilePath(Path programsFilePath) {
            this.programsFilePath = programsFilePath;
            return this;
        }

        public Builder hruAdditionalFilePath(Path hruAdditionalFilePath) {
            this.hruAdditionalFilePath = hruAdditionalFilePath;
            return this;
        }

        public Builder reachAdditionalFilePath(Path reachAdditionalFilePath) {
            this.reachAdditionalFilePath = reachAdditionalFilePath;
            return this;
        }

        public Builder temporalAdditionalFilePath(Path temporalAdditionalFilePath) {
            this.temporalAdditionalFilePath = temporalAdditionalFilePath;
            return this;
        }

        public Builder idSet_hru(String idSet_hru) {
            this.idSet_hru = idSet_hru;
            return this;
        }

        public Builder idSet_reach(String idSet_reach) {
            this.idSet_reach = idSet_reach;
            return this;
        }

        public Builder outFile_hru(Path outFile_hru) {
            this.outFile_hru = outFile_hru;
            return this;
        }

        public Builder attrSet_hru(String attrSet_hru) {
            this.attrSet_hru = attrSet_hru;
            return this;
        }

        public Builder attrSet_hru_w(String attrSet_hru_w) {
            this.attrSet_hru_w = attrSet_hru_w;
            return this;
        }

        public Builder outFile_hru_crop(Path outFile_hru_crop) {
            this.outFile_hru_crop = outFile_hru_crop;
            return this;
        }

        public Builder attrSet_hru_crop(String attrSet_hru_crop) {
            this.attrSet_hru_crop = attrSet_hru_crop;
            return this;
        }

        public Builder attrSet_hru_crop_w(String attrSet_hru_crop_w) {
            this.attrSet_hru_crop_w = attrSet_hru_crop_w;
            return this;
        }

        public Builder outFile_hru_layer(Path outFile_hru_layer) {
            this.outFile_hru_layer = outFile_hru_layer;
            return this;
        }

        public Builder attrSet_hru_layer(String attrSet_hru_layer) {
            this.attrSet_hru_layer = attrSet_hru_layer;
            return this;
        }

        public Builder attrSet_hru_layer_w(String attrSet_hru_layer_w) {
            this.attrSet_hru_layer_w = attrSet_hru_layer_w;
            return this;
        }

        public Builder outFile_hru_n_mb(Path outFile_hru_n_mb) {
            this.outFile_hru_n_mb = outFile_hru_n_mb;
            return this;
        }

        public Builder attrSet_hru_n_mb(String attrSet_hru_n_mb) {
            this.attrSet_hru_n_mb = attrSet_hru_n_mb;
            return this;
        }

        public Builder attrSet_hru_n_mb_w(String attrSet_hru_n_mb_w) {
            this.attrSet_hru_n_mb_w = attrSet_hru_n_mb_w;
            return this;
        }

        public Builder outFile_hru_n_pool(Path outFile_hru_n_pool) {
            this.outFile_hru_n_pool = outFile_hru_n_pool;
            return this;
        }

        public Builder attrSet_hru_n_pool(String attrSet_hru_n_pool) {
            this.attrSet_hru_n_pool = attrSet_hru_n_pool;
            return this;
        }

        public Builder attrSet_hru_n_pool_w(String attrSet_hru_n_pool_w) {
            this.attrSet_hru_n_pool_w = attrSet_hru_n_pool_w;
            return this;
        }

        public Builder outFile_hru_sediment(Path outFile_hru_sediment) {
            this.outFile_hru_sediment = outFile_hru_sediment;
            return this;
        }

        public Builder attrSet_hru_sediment(String attrSet_hru_sediment) {
            this.attrSet_hru_sediment = attrSet_hru_sediment;
            return this;
        }

        public Builder attrSet_hru_sediment_w(String attrSet_hru_sediment_w) {
            this.attrSet_hru_sediment_w = attrSet_hru_sediment_w;
            return this;
        }

        public Builder outFile_hru_crop_upgm(Path outFile_hru_crop_upgm) {
            this.outFile_hru_crop_upgm = outFile_hru_crop_upgm;
            return this;
        }

        public Builder attrSet_hru_crop_upgm(String attrSet_hru_crop_upgm) {
            this.attrSet_hru_crop_upgm = attrSet_hru_crop_upgm;
            return this;
        }

        public Builder attrSet_hru_crop_upgm_w(String attrSet_hru_crop_upgm_w) {
            this.attrSet_hru_crop_upgm_w = attrSet_hru_crop_upgm_w;
            return this;
        }

        public Builder outFile_reach(Path outFile_reach) {
            this.outFile_reach = outFile_reach;
            return this;
        }

        public Builder attrSet_reach(String attrSet_reach) {
            this.attrSet_reach = attrSet_reach;
            return this;
        }

        public Builder outFile_catch(Path outFile_catch) {
            this.outFile_catch = outFile_catch;
            return this;
        }

        public Builder attrSet_catch(String attrSet_catch) {
            this.attrSet_catch = attrSet_catch;
            return this;
        }

        public Builder attrSet_catch_w(String attrSet_catch_w) {
            this.attrSet_catch_w = attrSet_catch_w;
            return this;
        }

        public Builder outFile_catch_crop(Path outFile_catch_crop) {
            this.outFile_catch_crop = outFile_catch_crop;
            return this;
        }

        public Builder attrSet_catch_crop(String attrSet_catch_crop) {
            this.attrSet_catch_crop = attrSet_catch_crop;
            return this;
        }

        public Builder attrSet_catch_crop_w(String attrSet_catch_crop_w) {
            this.attrSet_catch_crop_w = attrSet_catch_crop_w;
            return this;
        }

        public Builder outFile_catch_n_mb(Path outFile_catch_n_mb) {
            this.outFile_catch_n_mb = outFile_catch_n_mb;
            return this;
        }

        public Builder attrSet_catch_n_mb(String attrSet_catch_n_mb) {
            this.attrSet_catch_n_mb = attrSet_catch_n_mb;
            return this;
        }

        public Builder attrSet_catch_n_mb_w(String attrSet_catch_n_mb_w) {
            this.attrSet_catch_n_mb_w = attrSet_catch_n_mb_w;
            return this;
        }

        public Builder outFile_catch_n_pool(Path outFile_catch_n_pool) {
            this.outFile_catch_n_pool = outFile_catch_n_pool;
            return this;
        }

        public Builder attrSet_catch_n_pool(String attrSet_catch_n_pool) {
            this.attrSet_catch_n_pool = attrSet_catch_n_pool;
            return this;
        }

        public Builder attrSet_catch_n_pool_w(String attrSet_catch_n_pool_w) {
            this.attrSet_catch_n_pool_w = attrSet_catch_n_pool_w;
            return this;
        }

        public Builder outFile_catch_sediment(Path outFile_catch_sediment) {
            this.outFile_catch_sediment = outFile_catch_sediment;
            return this;
        }

        public Builder attrSet_catch_sediment(String attrSet_catch_sediment) {
            this.attrSet_catch_sediment = attrSet_catch_sediment;
            return this;
        }

        public Builder attrSet_catch_sediment_w(String attrSet_catch_sediment_w) {
            this.attrSet_catch_sediment_w = attrSet_catch_sediment_w;
            return this;
        }

        public Builder outFile_catch_crop_upgm(Path outFile_catch_crop_upgm) {
            this.outFile_catch_crop_upgm = outFile_catch_crop_upgm;
            return this;
        }

        public Builder attrSet_catch_crop_upgm(String attrSet_catch_crop_upgm) {
            this.attrSet_catch_crop_upgm = attrSet_catch_crop_upgm;
            return this;
        }

        public Builder attrSet_catch_crop_upgm_w(String attrSet_catch_crop_upgm_w) {
            this.attrSet_catch_crop_upgm_w = attrSet_catch_crop_upgm_w;
            return this;
        }

        public Builder outFile_outlet(Path outFile_outlet) {
            this.outFile_outlet = outFile_outlet;
            return this;
        }

        public Builder attrSet_outlet(String attrSet_outlet) {
            this.attrSet_outlet = attrSet_outlet;
            return this;
        }

        public Builder startTime(LocalDate startTime) {
            this.startTime = startTime;
            return this;
        }

        public Builder endTime(LocalDate endTime) {
            this.endTime = endTime;
            return this;
        }

        public Builder parallelismFactor(double parallelismFactor) {
            this.parallelismFactor = parallelismFactor;
            return this;
        }

        public Builder projection(String projection) {
            this.projection = projection;
            return this;
        }

        public Builder tempRes(String tempRes) {
            this.tempRes = tempRes;
            return this;
        }

        public Builder locGrw(String locGrw) {
            this.locGrw = locGrw;
            return this;
        }

        public Builder longTZ(double longTZ) {
            this.longTZ = longTZ;
            return this;
        }

        public Builder FCAdaptation(double FCAdaptation) {
            this.FCAdaptation = FCAdaptation;
            return this;
        }

        public Builder ACAdaptation(double ACAdaptation) {
            this.ACAdaptation = ACAdaptation;
            return this;
        }

        public Builder initLPS(double initLPS) {
            this.initLPS = initLPS;
            return this;
        }

        public Builder initMPS(double initMPS) {
            this.initMPS = initMPS;
            return this;
        }

        public Builder equalWeights(boolean equalWeights) {
            this.equalWeights = equalWeights;
            return this;
        }

        public Builder pidwTmean(double pidwTmean) {
            this.pidwTmean = pidwTmean;
            return this;
        }

        public Builder pidwTmin(double pidwTmin) {
            this.pidwTmin = pidwTmin;
            return this;
        }

        public Builder pidwTmax(double pidwTmax) {
            this.pidwTmax = pidwTmax;
            return this;
        }

        public Builder pidwHum(double pidwHum) {
            this.pidwHum = pidwHum;
            return this;
        }

        public Builder pidwPrecip(double pidwPrecip) {
            this.pidwPrecip = pidwPrecip;
            return this;
        }

        public Builder pidwSol(double pidwSol) {
            this.pidwSol = pidwSol;
            return this;
        }

        public Builder pidwWind(double pidwWind) {
            this.pidwWind = pidwWind;
            return this;
        }

        public Builder nidwTmean(double nidwTmean) {
            this.nidwTmean = nidwTmean;
            return this;
        }

        public Builder nidwTmin(double nidwTmin) {
            this.nidwTmin = nidwTmin;
            return this;
        }

        public Builder nidwTmax(double nidwTmax) {
            this.nidwTmax = nidwTmax;
            return this;
        }

        public Builder nidwHum(double nidwHum) {
            this.nidwHum = nidwHum;
            return this;
        }

        public Builder nidwPrecip(double nidwPrecip) {
            this.nidwPrecip = nidwPrecip;
            return this;
        }

        public Builder nidwSol(double nidwSol) {
            this.nidwSol = nidwSol;
            return this;
        }

        public Builder nidwWind(double nidwWind) {
            this.nidwWind = nidwWind;
            return this;
        }

        public Builder elevationCorrectionTmean(int elevationCorrectionTmean) {
            this.elevationCorrectionTmean = elevationCorrectionTmean;
            return this;
        }

        public Builder elevationCorrectionTmin(int elevationCorrectionTmin) {
            this.elevationCorrectionTmin = elevationCorrectionTmin;
            return this;
        }

        public Builder elevationCorrectionTmax(int elevationCorrectionTmax) {
            this.elevationCorrectionTmax = elevationCorrectionTmax;
            return this;
        }

        public Builder elevationCorrectionHum(int elevationCorrectionHum) {
            this.elevationCorrectionHum = elevationCorrectionHum;
            return this;
        }

        public Builder elevationCorrectionPrecip(int elevationCorrectionPrecip) {
            this.elevationCorrectionPrecip = elevationCorrectionPrecip;
            return this;
        }

        public Builder elevationCorrectionSol(int elevationCorrectionSol) {
            this.elevationCorrectionSol = elevationCorrectionSol;
            return this;
        }

        public Builder elevationCorrectionWind(int elevationCorrectionWind) {
            this.elevationCorrectionWind = elevationCorrectionWind;
            return this;
        }

        public Builder rsqThresholdTmean(double rsqThresholdTmean) {
            this.rsqThresholdTmean = rsqThresholdTmean;
            return this;
        }

        public Builder rsqThresholdTmin(double rsqThresholdTmin) {
            this.rsqThresholdTmin = rsqThresholdTmin;
            return this;
        }

        public Builder rsqThresholdTmax(double rsqThresholdTmax) {
            this.rsqThresholdTmax = rsqThresholdTmax;
            return this;
        }

        public Builder rsqThresholdHum(double rsqThresholdHum) {
            this.rsqThresholdHum = rsqThresholdHum;
            return this;
        }

        public Builder rsqThresholdPrecip(double rsqThresholdPrecip) {
            this.rsqThresholdPrecip = rsqThresholdPrecip;
            return this;
        }

        public Builder rsqThresholdSol(double rsqThresholdSol) {
            this.rsqThresholdSol = rsqThresholdSol;
            return this;
        }

        public Builder rsqThresholdWind(double rsqThresholdWind) {
            this.rsqThresholdWind = rsqThresholdWind;
            return this;
        }

        public Builder tempNIDW(int tempNIDW) {
            this.tempNIDW = tempNIDW;
            return this;
        }

        public Builder pIDW(double pIDW) {
            this.pIDW = pIDW;
            return this;
        }

        public Builder windNIDW(int windNIDW) {
            this.windNIDW = windNIDW;
            return this;
        }

        public Builder precipCorrectMethod(int precipCorrectMethod) {
            this.precipCorrectMethod = precipCorrectMethod;
            return this;
        }

        public Builder regThres(double regThres) {
            this.regThres = regThres;
            return this;
        }

        public Builder snow_trs(double snow_trs) {
            this.snow_trs = snow_trs;
            return this;
        }

        public Builder snow_trans(double snow_trans) {
            this.snow_trans = snow_trans;
            return this;
        }

        public Builder humidity(String humidity) {
            this.humidity = humidity;
            return this;
        }

        public Builder solar(String solar) {
            this.solar = solar;
            return this;
        }

        public Builder angstrom_a(double angstrom_a) {
            this.angstrom_a = angstrom_a;
            return this;
        }

        public Builder angstrom_b(double angstrom_b) {
            this.angstrom_b = angstrom_b;
            return this;
        }

        public Builder a_rain(double a_rain) {
            this.a_rain = a_rain;
            return this;
        }

        public Builder a_snow(double a_snow) {
            this.a_snow = a_snow;
            return this;
        }

        public Builder baseTemp(double baseTemp) {
            this.baseTemp = baseTemp;
            return this;
        }

        public Builder t_factor(double t_factor) {
            this.t_factor = t_factor;
            return this;
        }

        public Builder r_factor(double r_factor) {
            this.r_factor = r_factor;
            return this;
        }

        public Builder g_factor(double g_factor) {
            this.g_factor = g_factor;
            return this;
        }

        public Builder snowCritDens(double snowCritDens) {
            this.snowCritDens = snowCritDens;
            return this;
        }

        public Builder ccf_factor(double ccf_factor) {
            this.ccf_factor = ccf_factor;
            return this;
        }

        public Builder snowFactorA(double snowFactorA) {
            this.snowFactorA = snowFactorA;
            return this;
        }

        public Builder snowFactorB(double snowFactorB) {
            this.snowFactorB = snowFactorB;
            return this;
        }

        public Builder snowFactorC(double snowFactorC) {
            this.snowFactorC = snowFactorC;
            return this;
        }

        public Builder snowDensConst(double snowDensConst) {
            this.snowDensConst = snowDensConst;
            return this;
        }

        public Builder Beta_NO3(double Beta_NO3) {
            this.Beta_NO3 = Beta_NO3;
            return this;
        }

        public Builder drspac(double drspac) {
            this.drspac = drspac;
            return this;
        }

        public Builder drrad(double drrad) {
            this.drrad = drrad;
            return this;
        }

        public Builder depdr(double depdr) {
            this.depdr = depdr;
            return this;
        }

        public Builder soilMaxDPS(double soilMaxDPS) {
            this.soilMaxDPS = soilMaxDPS;
            return this;
        }

        public Builder soilPolRed(double soilPolRed) {
            this.soilPolRed = soilPolRed;
            return this;
        }

        public Builder soilLinRed(double soilLinRed) {
            this.soilLinRed = soilLinRed;
            return this;
        }

        public Builder soilMaxInfSummer(double soilMaxInfSummer) {
            this.soilMaxInfSummer = soilMaxInfSummer;
            return this;
        }

        public Builder soilMaxInfWinter(double soilMaxInfWinter) {
            this.soilMaxInfWinter = soilMaxInfWinter;
            return this;
        }

        public Builder soilMaxInfSnow(double soilMaxInfSnow) {
            this.soilMaxInfSnow = soilMaxInfSnow;
            return this;
        }

        public Builder soilImpGT80(double soilImpGT80) {
            this.soilImpGT80 = soilImpGT80;
            return this;
        }

        public Builder soilImpLT80(double soilImpLT80) {
            this.soilImpLT80 = soilImpLT80;
            return this;
        }

        public Builder soilDistMPSLPS(double soilDistMPSLPS) {
            this.soilDistMPSLPS = soilDistMPSLPS;
            return this;
        }

        public Builder soilDiffMPSLPS(double soilDiffMPSLPS) {
            this.soilDiffMPSLPS = soilDiffMPSLPS;
            return this;
        }

        public Builder soilOutLPS(double soilOutLPS) {
            this.soilOutLPS = soilOutLPS;
            return this;
        }

        public Builder soilLatVertLPS(double soilLatVertLPS) {
            this.soilLatVertLPS = soilLatVertLPS;
            return this;
        }

        public Builder soilMaxPerc(double soilMaxPerc) {
            this.soilMaxPerc = soilMaxPerc;
            return this;
        }

        public Builder geoMaxPerc(double geoMaxPerc) {
            this.geoMaxPerc = geoMaxPerc;
            return this;
        }

        public Builder BetaW(double BetaW) {
            this.BetaW = BetaW;
            return this;
        }

        public Builder kdiff_layer(double kdiff_layer) {
            this.kdiff_layer = kdiff_layer;
            return this;
        }

        public Builder cn_froz(double cn_froz) {
            this.cn_froz = cn_froz;
            return this;
        }

        public Builder cnn(double cnn) {
            this.cnn = cnn;
            return this;
        }

        public Builder icn(int icn) {
            this.icn = icn;
            return this;
        }

        public Builder r2adj(double r2adj) {
            this.r2adj = r2adj;
            return this;
        }

        public Builder fcimp(double fcimp) {
            this.fcimp = fcimp;
            return this;
        }

        public Builder startReduction(LocalDate startReduction) {
            this.startReduction = startReduction;
            return this;
        }

        public Builder endReduction(LocalDate endReduction) {
            this.endReduction = endReduction;
            return this;
        }

        public Builder opti(double opti) {
            this.opti = opti;
            return this;
        }

        public Builder LExCoef(double LExCoef) {
            this.LExCoef = LExCoef;
            return this;
        }

        public Builder rootfactor(double rootfactor) {
            this.rootfactor = rootfactor;
            return this;
        }

        public Builder temp_lag(double temp_lag) {
            this.temp_lag = temp_lag;
            return this;
        }

        public Builder sceno(double sceno) {
            this.sceno = sceno;
            return this;
        }

        public Builder piadin(int piadin) {
            this.piadin = piadin;
            return this;
        }

        public Builder Beta_trans(double Beta_trans) {
            this.Beta_trans = Beta_trans;
            return this;
        }

        public Builder Beta_min(double Beta_min) {
            this.Beta_min = Beta_min;
            return this;
        }

        public Builder Beta_rsd(double Beta_rsd) {
            this.Beta_rsd = Beta_rsd;
            return this;
        }

        public Builder Beta_Ndist(double Beta_Ndist) {
            this.Beta_Ndist = Beta_Ndist;
            return this;
        }

        public Builder infil_conc_factor(double infil_conc_factor) {
            this.infil_conc_factor = infil_conc_factor;
            return this;
        }

        public Builder denitfac(double denitfac) {
            this.denitfac = denitfac;
            return this;
        }

        public Builder deposition_factor(double deposition_factor) {
            this.deposition_factor = deposition_factor;
            return this;
        }

        public Builder theta_nit(double theta_nit) {
            this.theta_nit = theta_nit;
            return this;
        }

        public Builder gwRG1Fact(double gwRG1Fact) {
            this.gwRG1Fact = gwRG1Fact;
            return this;
        }

        public Builder gwRG2Fact(double gwRG2Fact) {
            this.gwRG2Fact = gwRG2Fact;
            return this;
        }

        public Builder gwRG1RG2dist(double gwRG1RG2dist) {
            this.gwRG1RG2dist = gwRG1RG2dist;
            return this;
        }

        public Builder gwCapRise(double gwCapRise) {
            this.gwCapRise = gwCapRise;
            return this;
        }

        public Builder initRG1(double initRG1) {
            this.initRG1 = initRG1;
            return this;
        }

        public Builder initRG2(double initRG2) {
            this.initRG2 = initRG2;
            return this;
        }

        public Builder musi_co1(double musi_co1) {
            this.musi_co1 = musi_co1;
            return this;
        }

        public Builder musi_co2(double musi_co2) {
            this.musi_co2 = musi_co2;
            return this;
        }

        public Builder musi_co3(double musi_co3) {
            this.musi_co3 = musi_co3;
            return this;
        }

        public Builder musi_co4(double musi_co4) {
            this.musi_co4 = musi_co4;
            return this;
        }

        public Builder N_delay_RG1(double N_delay_RG1) {
            this.N_delay_RG1 = N_delay_RG1;
            return this;
        }

        public Builder N_delay_RG2(double N_delay_RG2) {
            this.N_delay_RG2 = N_delay_RG2;
            return this;
        }

        public Builder N_concRG1(double N_concRG1) {
            this.N_concRG1 = N_concRG1;
            return this;
        }

        public Builder N_concRG2(double N_concRG2) {
            this.N_concRG2 = N_concRG2;
            return this;
        }

        public Builder defaultCO2(double defaultCO2) {
            this.defaultCO2 = defaultCO2;
            return this;
        }

        public Builder flowRouteTA(double flowRouteTA) {
            this.flowRouteTA = flowRouteTA;
            return this;
        }

        public Builder Ksink(double Ksink) {
            this.Ksink = Ksink;
            return this;
        }

        public Builder flagHRURouting(boolean flagHRURouting) {
            this.flagHRURouting = flagHRURouting;
            return this;
        }

        public Builder flagReachRouting(boolean flagReachRouting) {
            this.flagReachRouting = flagReachRouting;
            return this;
        }

        public Builder flagUPGM(boolean flagUPGM) {
            this.flagUPGM = flagUPGM;
            return this;
        }

        public Builder flagRegionalization(boolean flagRegionalization) {
            this.flagRegionalization = flagRegionalization;
            return this;
        }

        public Builder flagTileDrain(boolean flagTileDrain) {
            this.flagTileDrain = flagTileDrain;
            return this;
        }

        public Builder flagTillage(boolean flagTillage) {
            this.flagTillage = flagTillage;
            return this;
        }

        public Builder flagInfiltration(String flagInfiltration) {
            this.flagInfiltration = flagInfiltration;
            return this;
        }

        public Builder flagSort(boolean flagSort) {
            this.flagSort = flagSort;
            return this;
        }

        public Builder flagSplit(boolean flagSplit) {
            this.flagSplit = flagSplit;
            return this;
        }

        public Builder flagParallel(String flagParallel) {
            this.flagParallel = flagParallel;
            return this;
        }
    }

    private static class Parameter {
        private String name;
        private Object rawValue;
        private Class<?> type;

        public Parameter(String name, Object rawValue, Class<?> type) {
            this.name = name;
            this.rawValue = rawValue;
            this.type = type;
        }

        public void addToContext(Context context) {
            if (type == null) {
                context.put(name, rawValue);
            } else {
                Object value = TypeConversions.INSTANCE.convert(rawValue, type);
                context.put(name, value, type);
            }
        }
    }
}
