/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilWater;

import ages.types.HRU;
import ages.types.HydroGeology;
import ages.types.Landuse;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Description("Add ProcessLayeredSoilWater module definition here")
@Author(name = "Olaf David, Peter Krause, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("Soilwater")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilWater/ProcessLayeredSoilWater.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilWater/ProcessLayeredSoilWater.xml")
public class ProcessSoilWaterLayerAdapter extends AnnotatedAdapter {
    @Description("flag for infiltration method (eagleson, bucket, curvenumber)")
    @Input public String flagInfiltration;

    @Description("maximum depression storage capacity")
    @Units("mm")
    @Role(PARAMETER)
    @Input public double soilMaxDPS;

    @Description("potential reduction coeffiecient for AET computation")
    @Role(PARAMETER)
    @Input public double soilPolRed;

    @Description("linear reduction coeffiecient for AET computation")
    @Role(PARAMETER)
    @Input public double soilLinRed;

    @Description("maximum infiltration in summer")
    @Units("mm/d")
    @Role(PARAMETER)
    @Input public double soilMaxInfSummer;

    @Description("maximum infiltration in winter")
    @Units("mm/d")
    @Role(PARAMETER)
    @Input public double soilMaxInfWinter;

    @Description("maximum infiltration for snow covered areas")
    @Units("mm")
    @Role(PARAMETER)
    @Input public double soilMaxInfSnow;

    @Description("relative infiltration for impervious areas greater than 80% sealing")
    @Role(PARAMETER)
    @Input public double soilImpGT80;

    @Description("relative infiltration for impervious areas less than 80% sealing")
    @Role(PARAMETER)
    @Input public double soilImpLT80;

    @Description("MPS/LPS distribution coefficient")
    @Role(PARAMETER)
    @Input public double soilDistMPSLPS;

    @Description("MPS/LPS diffusion coefficient")
    @Role(PARAMETER)
    @Input public double soilDiffMPSLPS;

    @Description("outflow coefficient for LPS")
    @Role(PARAMETER)
    @Input public double soilOutLPS;

    @Description("lateral-vertical distribution coefficient")
    @Role(PARAMETER)
    @Input public double soilLatVertLPS;

    @Description("maximum percolation rate")
    @Units("mm/d")
    @Role(PARAMETER)
    @Input public double soilMaxPerc;

    @Description("maximum percolation rate out of soil")
    @Units("mm/d")
    @Role(PARAMETER)
    @Input public double geoMaxPerc;

    @Description("water use distribution parameter for transpiration")
    @Role(PARAMETER)
    @Input public double BetaW;

    @Description("layer MPS diffusion factor > 0 [-]  resistance default = 10")
    @Role(PARAMETER)
    @Input public double kdiff_layer;

    @Description("current time")
    @Input public LocalDate time;

    @Description("HRU")
    @Input public HRU hru;

    @Description("landuse")
    @Input public Landuse landuse;

    @Description("state variable net rain")
    @Input public double netRain;

    @Description("state variable net snow")
    @Input public double netSnow;

    @Description("HRU potential evapotranspiration")
    @Units("mm")
    @Input public double potET;

    @Description("snow depth")
    @Input public double snowDepth;

    @Description("daily snow melt")
    @Input public double snowMelt;

    @Description("number of layers in soil profile")
    @Input public SoilType soil;

    @Description("actual depth of roots")
    @Units("m")
    @Input public double zrootd;

    @Description("maximum MPS in soil water content")
    @Units("l")
    @Input public double[] maxMPS_h;

    @Description("maximum LPS in soil water content")
    @Units("l")
    @Input public double[] maxLPS_h;

    @Description("array of state variables LAI ")
    @Input public double LAI;

    @Description("hydro geology")
    @Input public HydroGeology hydroGeology;

    @Description("soil hydraulic conductivity")
    @Units("cm/d")
    @Optional
    @Input public double[] kf_h;

    @Input public double[] swc_h;

    @Optional
    @Units("vol.%")
    @Input public double[] volumeAirCapacity_h;

    @Input public double[] soil_Temp_Layer;

    @Description("crop")
    @Input public Crop crop;

    // curve number method parameters
    @Description("parameter for frozen soil adjustment on infiltration/runoff")
    @Input public double cn_froz;

    @Description("SCS runoff curve number for moisture condition II")
    @Input public double cnn;

    @Description("CN method flag (0,1,2)")
    @Input public int icn;

    @Description("CN retention parameter adjustment factor for flat slopes")
    @Range(min = 0.5, max = 3.0)
    @Input public double r2adj;

    @Description("fraction of HRU area that is classified as directly connected impervious")
    @Range(min = 0.0, max = 1.0)
    @Input public double fcimp;

    @Description("HRU statevar RD1 inflow")
    @Input public double inRD1;

    //internal state variables
    @Description("HRU actual evapotranspiration")
    @Units("mm")
    @Input @Output public double actET;

    @Description("RD2 inflow")
    @Input @Output public double[] inRD2_h;

    @Description("HRU state var actual MPS")
    @Input @Output public double[] actMPS_h;

    @Description("HRU state var actual LPS")
    @Input @Output public double[] actLPS_h;

    @Description("HRU state var actual depression storage")
    @Optional
    @Input @Output public double actDPS;

    @Description("Actual MPS in portion of sto_MPS soil water content")
    @Input @Output public double[] satMPS_h;

    @Description("actual LPS in portion of sto_LPS soil water content")
    @Input @Output public double[] satLPS_h;

    @Description("HRU attribute maximum MPS of soil")
    @Output public double soilMaxMPS;

    @Description("HRU attribute maximum LPS of soil")
    @Output public double soilMaxLPS;

    @Description("HRU state var actual MPS of soil")
    @Output public double soilActMPS;

    @Description("HRU state var actual LPS of soil")
    @Output public double soilActLPS;

    @Description("HRU state var saturation of MPS of soil")
    @Output public double soilSatMPS;

    @Description("HRU state var saturation of LPS of soil")
    @Output public double soilSatLPS;

    @Description("HRU state var saturation of whole soil")
    @Output public double soilSat;

    @Description("HRU statevar infiltration")
    @Output public double infiltration;

    @Description("HRU statevar interflow")
    @Output public double interflow;

    @Description("HRU statevar percolation")
    @Units("l")
    @Output public double percolation;

    @Description("HRU statevar RD1 outflow")
    @Units("l")
    @Output public double outRD1;

    @Description("HRU statevar RD2 outflow")
    @Units("l")
    @Output public double outRD2;

    @Description("HRU statevar RD1 generation")
    @Output public double genRD1;

    @Description("water balance for every HRU")
    @Output public double balance;

    @Description("water balance in for every HRU")
    @Output public double balanceIn;

    @Description("water balance in for every HRU")
    @Output public double balanceMPSstart;

    @Description("water balance in for every HRU")
    @Output public double balanceMPSend;

    @Description("water balance in for every HRU")
    @Output public double balanceLPSstart;

    @Description("water balance in for every HRU")
    @Output public double balanceLPSend;

    @Description("water balance in for every HRU")
    @Output public double balanceDPSstart;

    @Description("water balance in for every HRU")
    @Output public double balanceDPSend;

    @Description("water balance in for every HRU")
    @Output public double balanceOut;

    @Description("HRU statevar RD2 outflow")
    @Units("l")
    @Output public double inRD2;

    @Description("HRU statevar RD2 outflow")
    @Units("l")
    @Output public double[] outRD2_h;

    @Description("HRU statevar RD2 generation")
    @Output public double[] genRD2_h;

    @Description("infiltration for the soil layers")
    @Units("l")
    @Output public double[] infiltration_hor;

    @Description("percolation out of soil layers")
    @Units("l")
    @Output public double[] perco_hor;

    @Description("percolation out of soil layers")
    @Units("l")
    @Output public double[] actETP_h;

    @Description("MPS diffusion between soil layers")
    @Output public double[] w_layer_diff;

    @Description("maximum root depth in soil")
    @Units("m")
    @Output public double soil_root;

    private Map<HRU, ProcessSoilWaterLayer> componentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        ProcessSoilWaterLayer component = componentMap.get(hru);
        if (component == null) {
            component = new ProcessSoilWaterLayer();
            componentMap.put(hru, component);
        }

        component.flagInfiltration = flagInfiltration;
        component.soilMaxDPS = soilMaxDPS;
        component.soilPolRed = soilPolRed;
        component.soilLinRed = soilLinRed;
        component.soilMaxInfSummer = soilMaxInfSummer;
        component.soilMaxInfWinter = soilMaxInfWinter;
        component.soilMaxInfSnow = soilMaxInfSnow;
        component.soilImpGT80 = soilImpGT80;
        component.soilImpLT80 = soilImpLT80;
        component.soilDistMPSLPS = soilDistMPSLPS;
        component.soilDiffMPSLPS = soilDiffMPSLPS;
        component.soilOutLPS = soilOutLPS;
        component.soilLatVertLPS = soilLatVertLPS;
        component.soilMaxPerc = soilMaxPerc;
        component.geoMaxPerc = geoMaxPerc;
        component.BetaW = BetaW;
        component.kdiff_layer = kdiff_layer;
        component.time = time;
        component.area = hru.area;
        component.slope = hru.slope;
        component.sealedGrade = landuse.sealedGrade;
        component.netRain = netRain;
        component.netSnow = netSnow;
        component.potET = potET;
        component.snowDepth = snowDepth;
        component.snowMelt = snowMelt;
        component.horizons = soil.horizons;
        component.depth_h = soil.depth_h;
        component.zrootd = zrootd;
        component.maxMPS_h = maxMPS_h;
        component.maxLPS_h = maxLPS_h;
        component.LAI = LAI;
        component.Kf_geo = hydroGeology.Kf_geo;
        component.kf_h = (kf_h == null ? soil.kf_h : kf_h);
        component.swc_h = swc_h;
        component.volumeAirCapacity_h = (volumeAirCapacity_h == null ? soil.volumeAirCapacity_h : volumeAirCapacity_h);
        component.volumeDeadCapacity_h = soil.volumeDeadCapacity_h;
        component.volumeFieldCapacity_h = soil.volumeFieldCapacity_h;
        component.soil_Temp_Layer = soil_Temp_Layer;
        component.cropId = (crop == null ? -1 : crop.cid);
        component.root_h = soil.root_h;
        component.cn_froz = cn_froz;
        component.cnn = cnn;
        component.icn = icn;
        component.r2adj = r2adj;
        component.fcimp = fcimp;
        component.inRD1 = inRD1;
        component.actET = actET;
        component.inRD2_h = inRD2_h;
        component.actMPS_h = actMPS_h;
        component.actLPS_h = actLPS_h;
        component.actDPS = actDPS;
        component.satMPS_h = satMPS_h;
        component.satLPS_h = satLPS_h;

        component.execute();

        actET = component.actET;
        inRD2_h = component.inRD2_h;
        actMPS_h = component.actMPS_h;
        actLPS_h = component.actLPS_h;
        actDPS = component.actDPS;
        satMPS_h = component.satMPS_h;
        satLPS_h = component.satLPS_h;
        soilMaxMPS = component.soilMaxMPS;
        soilMaxLPS = component.soilMaxLPS;
        soilActMPS = component.soilActMPS;
        soilActLPS = component.soilActLPS;
        soilSatMPS = component.soilSatMPS;
        soilSatLPS = component.soilSatLPS;
        soilSat = component.soilSat;
        infiltration = component.infiltration;
        interflow = component.interflow;
        percolation = component.percolation;
        outRD1 = component.outRD1;
        outRD2 = component.outRD2;
        genRD1 = component.genRD1;
        balance = component.balance;
        balanceIn = component.balanceIn;
        balanceMPSstart = component.balanceMPSstart;
        balanceMPSend = component.balanceMPSend;
        balanceLPSstart = component.balanceLPSstart;
        balanceLPSend = component.balanceLPSend;
        balanceDPSstart = component.balanceDPSstart;
        balanceDPSend = component.balanceDPSend;
        balanceOut = component.balanceOut;
        inRD2 = component.inRD2;
        outRD2_h = component.outRD2_h;
        genRD2_h = component.genRD2_h;
        infiltration_hor = component.infiltration_hor;
        perco_hor = component.perco_hor;
        actETP_h = component.actETP_h;
        w_layer_diff = component.w_layer_diff;
        soil_root = component.soil_root;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
