/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilWater;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add InitProcessLayeredSoilWaterN module definition here")
@Author(name = "Olaf David, Holm Kipka, Peter Krause", contact = "jim.ascough@ars.usda.gov")
@Keywords("Soilwater")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilWater/InitProcessLayeredSoilWaterN.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilWater/InitProcessLayeredSoilWaterN.xml")
public class InitProcessSoilWaterLayerAdapter extends AnnotatedAdapter {
    @Description("multiplier for field capacity")
    @Role(PARAMETER)
    @Input public double FCAdaptation;

    @Description("multiplier for air capacity")
    @Role(PARAMETER)
    @Input public double ACAdaptation;

    @Description("Initial LPS, fraction of maxiumum LPS, 0.0 .. 1.0")
    @Role(PARAMETER)
    @Input public double initLPS;

    @Description("Initial MPS, fraction of maxiumum MPS, 0.0 ... 1.0")
    @Role(PARAMETER)
    @Input public double initMPS;

    @Description("Current hru object")
    @Input public HRU hru;

    @Description("Number of layers in soil profile")
    @Input public SoilType soil;

    @Description("Maximum MPS  in l soil water content")
    @Output public double[] maxMPS_h;

    @Description("Maximum LPS  in l soil water content")
    @Output public double[] maxLPS_h;

    @Description("Maximum FPS  in l soil water content")
    @Output public double[] maxFPS_h;

    @Description("HRU state var actual MPS")
    @Output public double[] actMPS_h;

    @Description("HRU state var actual LPS")
    @Output public double[] actLPS_h;

    @Description("Soil water content dimensionless by soil layer h")
    @Units("vol.%")
    @Output public double[] swc_h;

    @Description("Actual MPS in portion of sto_MPS soil water content")
    @Output public double[] satMPS_h;

    @Description("Actual LPS in portion of sto_LPS soil water content")
    @Output public double[] satLPS_h;

    @Description("RD2 inflow")
    @Output public double[] inRD2_h;

    @Override
    protected void run(Context context) {
        InitProcessSoilWaterLayer component = new InitProcessSoilWaterLayer();

        component.FCAdaptation = FCAdaptation;
        component.ACAdaptation = ACAdaptation;
        component.initLPS = initLPS;
        component.initMPS = initMPS;
        component.area = hru.area;
        component.horizons = soil.horizons;
        component.capacity_unit = soil.capacity_unit;
        component.fieldCapacity_h = soil.fieldCapacity_h;
        component.deadCapacity_h = soil.deadCapacity_h;
        component.airCapacity_h = soil.airCapacity_h;
        component.volumeFieldCapacity_h = soil.volumeFieldCapacity_h;
        component.volumeDeadCapacity_h = soil.volumeDeadCapacity_h;
        component.volumeAirCapacity_h = soil.volumeAirCapacity_h;
        component.depth_h = soil.depth_h;
        component.initLPS_h = soil.initLPS_h;
        component.initMPS_h = soil.initMPS_h;
        component.initSWC_h = soil.initSWC_h;

        component.execute();

        maxMPS_h = component.maxMPS_h;
        maxLPS_h = component.maxLPS_h;
        maxFPS_h = component.maxFPS_h;
        actMPS_h = component.actMPS_h;
        actLPS_h = component.actLPS_h;
        swc_h = component.swc_h;
        satMPS_h = component.satMPS_h;
        satLPS_h = component.satLPS_h;
        inRD2_h = component.inRD2_h;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
