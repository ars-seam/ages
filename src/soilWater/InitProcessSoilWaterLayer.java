/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilWater;

public class InitProcessSoilWaterLayer {
    public double FCAdaptation;
    public double ACAdaptation;
    public double initLPS = 0.0;
    public double initMPS = 0.0;
    public double area;
    public int horizons;
    public String capacity_unit;
    public double[] fieldCapacity_h;
    public double[] deadCapacity_h;
    public double[] airCapacity_h;
    public double[] volumeFieldCapacity_h;
    public double[] volumeDeadCapacity_h;
    public double[] volumeAirCapacity_h;
    public double[] depth_h;
    public double[] initLPS_h;
    public double[] initMPS_h;
    public double[] initSWC_h;

    public double[] maxMPS_h;
    public double[] maxLPS_h;
    public double[] maxFPS_h;
    public double[] actMPS_h;
    public double[] actLPS_h;
    public double[] swc_h;
    public double[] satMPS_h;
    public double[] satLPS_h;
    public double[] inRD2_h;

    public void execute() {
        if (initMPS < 0.0 || initMPS > 1.0) {
            throw new IllegalArgumentException("initMPS range");
        }
        if (initLPS < 0.0 || initLPS > 1.0) {
            throw new IllegalArgumentException("initLPS range");
        }

        maxMPS_h = new double[horizons];
        maxLPS_h = new double[horizons];
        maxFPS_h = new double[horizons];
        actMPS_h = new double[horizons];
        actLPS_h = new double[horizons];
        swc_h = new double[horizons];
        satMPS_h = new double[horizons];
        satLPS_h = new double[horizons];
        inRD2_h = new double[horizons];

        for (int h = 0; h < horizons; h++) {
            double _initLPS = initLPS;
            double _initMPS = initMPS;

            if (initLPS_h != null && initMPS_h != null) {
                _initLPS = initLPS_h[h];
                _initMPS = initMPS_h[h];

            }
            if ((capacity_unit).contains("vol")) {
                maxMPS_h[h] = ((volumeFieldCapacity_h[h] - volumeDeadCapacity_h[h]) * (depth_h[h] * 10) * area) * FCAdaptation;
                maxFPS_h[h] = volumeDeadCapacity_h[h] * (depth_h[h] * 10) * area;
                maxLPS_h[h] = ((volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]) * (depth_h[h] * 10) * area) * ACAdaptation;
                if (initSWC_h != null) {
                    // convert deadcapacity, fieldcapacity, and aircapacity to equivalent depths in mm per horizon (where horizon depth is in cm)
                    _initMPS = (initSWC_h[h] - volumeDeadCapacity_h[h]) / (volumeFieldCapacity_h[h] - volumeDeadCapacity_h[h]);
                    _initMPS = Math.min(_initMPS, 1.0);
                    if (_initMPS == 1.0) {
                        _initLPS = (initSWC_h[h] - volumeFieldCapacity_h[h]) / (volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]);
                    }
                }
                actMPS_h[h] = maxMPS_h[h] * _initMPS;
                actLPS_h[h] = maxLPS_h[h] * _initLPS;
                satMPS_h[h] = actMPS_h[h];
                satLPS_h[h] = actLPS_h[h];

                swc_h[h] = volumeDeadCapacity_h[h] + ((actMPS_h[h] / maxMPS_h[h]) * (volumeFieldCapacity_h[h] - volumeDeadCapacity_h[h])) + ((actLPS_h[h] / maxLPS_h[h]) * (volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]));
            } else {
                maxMPS_h[h] = fieldCapacity_h[h] * area * FCAdaptation;
                maxFPS_h[h] = deadCapacity_h[h] * area;
                maxLPS_h[h] = airCapacity_h[h] * area * ACAdaptation;

                actMPS_h[h] = maxMPS_h[h] * _initMPS;
                actLPS_h[h] = maxLPS_h[h] * _initLPS;
                satMPS_h[h] = actMPS_h[h];
                satLPS_h[h] = actLPS_h[h];

                swc_h[h] = (actMPS_h[h] + actLPS_h[h] + maxFPS_h[h]) / ((depth_h[h] * 10) * area);
            }
        }
    }
}
