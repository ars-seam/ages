/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilWater;

import infiltration.CurveNumber;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessSoilWaterLayer {
    private static final Logger log
            = Logger.getLogger("oms3.model." + ProcessSoilWaterLayer.class.getSimpleName());
    private static final String EAGLESON = "eagleson";
    private static final String BUCKET = "bucket";
    private static final String CURVE_NUMBER = "curvenumber";

    public String flagInfiltration;
    public double soilMaxDPS;
    public double soilPolRed;
    public double soilLinRed;
    public double soilMaxInfSummer;
    public double soilMaxInfWinter;
    public double soilMaxInfSnow;
    public double soilImpGT80;
    public double soilImpLT80;
    public double soilDistMPSLPS;
    public double soilDiffMPSLPS;
    public double soilOutLPS;
    public double soilLatVertLPS;
    public double soilMaxPerc;
    public double geoMaxPerc;
    public double BetaW;
    public double kdiff_layer;
    public LocalDate time;
    public double area;
    public double slope;
    public double sealedGrade;
    public double netRain;
    public double netSnow;
    public double potET;
    public double snowDepth;
    public double snowMelt;
    public int horizons;
    public double[] depth_h;
    public double zrootd;
    public double[] maxMPS_h;
    public double[] maxLPS_h;
    public double LAI;
    public double Kf_geo;
    public double[] kf_h;
    public double[] root_h;
    public double[] swc_h;
    public double[] volumeDeadCapacity_h;
    public double[] volumeFieldCapacity_h;
    public double[] volumeAirCapacity_h;
    public double[] soil_Temp_Layer;
    public int cropId;

    // curve number method parameters
    public double cn_froz;
    public double cnn;
    public int icn;
    public double r2adj;
    public double fcimp;

    public double inRD1;

    //internal state variables
    public double actET;
    public double[] inRD2_h;
    public double[] actMPS_h;
    public double[] actLPS_h;
    public double actDPS;
    public double[] satMPS_h;
    public double[] satLPS_h;

    public double soilMaxMPS;
    public double soilMaxLPS;
    public double soilActMPS;
    public double soilActLPS;
    public double soilSatMPS;
    public double soilSatLPS;
    public double soilSat;
    public double infiltration;
    public double interflow;
    public double percolation;
    public double outRD1;
    public double outRD2;
    public double genRD1;
    public double balance;
    public double balanceIn;
    public double balanceMPSstart;
    public double balanceMPSend;
    public double balanceLPSstart;
    public double balanceLPSend;
    public double balanceDPSstart;
    public double balanceDPSend;
    public double balanceOut;
    public double inRD2;
    public double[] outRD2_h;
    public double[] genRD2_h;
    public double[] infiltration_hor;
    public double[] perco_hor;
    public double[] actETP_h;
    public double[] w_layer_diff;
    public double soil_root;

    private double run_actDPS;
    private double run_overlandflow;
    private double top_satsoil;
    private double[] run_satHor;
    private double[] runlayerdepth;
    private double[] horETP;

    private boolean debug;

    public void execute() {
        if (actETP_h == null) {
            infiltration_hor = new double[horizons];
            perco_hor = new double[horizons];
            actETP_h = new double[horizons];
        }

        double balMPSstart = 0;
        double balMPSend = 0;
        double balLPSstart = 0;
        double balLPSend = 0;
        double balDPSstart = 0;
        double balDPSend = 0;
        double balIn = 0;
        double balOut = 0;
        double balET = 0;
        double sumactETP = 0;

        debug = false;

        if (horizons == 0) {
            horizons = 1;
        }

        w_layer_diff = new double[horizons - 1];
        run_satHor = new double[horizons];

        run_actDPS = actDPS;

        balIn += inRD1;

        runlayerdepth = new double[horizons];
        genRD2_h = new double[horizons];
        outRD2_h = new double[horizons];

        interflow = 0;
        percolation = 0;
        genRD1 = 0;
        outRD1 = 0;
        top_satsoil = 0;

        balET = actET;
        balDPSstart = run_actDPS;
        for (int h = 0; h < horizons; h++) {
            // determine influx of infiltration to MPS
            balIn += inRD2_h[h];
            inRD2 += inRD2_h[h];
            balMPSstart += actMPS_h[h];
            balLPSstart += actLPS_h[h];
            genRD2_h[h] = 0;
            outRD2_h[h] = 0;
        }

        checkActLPS_maxLPS();

        // calculate soil saturations
        calcSoilSaturations();

        layer_diffusion();

        calcSoilSaturations();

        // redistribute RD1 and RD2 inflow of water
        redistRD1_RD2_in();

        // calculate ETP from depression storage and open water bodies
        calcPreInfEvaporation();
        double preinfep = actET;

        // determine available water for infiltration
        infiltration = netRain + netSnow + snowMelt + run_actDPS;

        // calculate overland flow and adjust infiltration if CN method is used
        boolean CN = CURVE_NUMBER.equals(flagInfiltration);
        double CN_overlandflow = 0;
        if (CN) {
            CN_overlandflow += calcCNrunoff();
            if (infiltration - CN_overlandflow >= 0) {
                infiltration -= CN_overlandflow;
            }
        }

        if (infiltration < 0) {
            System.out.println("negative infiltration!");
            System.out.println("inRain: " + netRain);
            System.out.println("inSnow: " + netSnow);
            System.out.println("inSnowMelt: " + snowMelt);
            System.out.println("inDPS: " + run_actDPS);
        }

        // water balance equation input
        balIn += netRain;
        balIn += netSnow;
        balIn += snowMelt;

        run_actDPS = 0;

        /*
         * infiltration on impervious areas and water bodies is directly routed
         * as direct runoff to the next polygon (need to consider routing to the
         * the next river reach)
         */
        calcInfImperv(sealedGrade);
        calcSoilSaturations();

        if (!CN) {
            // determine maximum infiltration rate
            double maxInf = calcMaxInfiltration(time.getMonthValue());
            if (maxInf < infiltration) {
                double deltaInf = infiltration - maxInf;
                run_actDPS += deltaInf;
                infiltration = maxInf;
            }
        }

        horETP = calcMPSEvapotranslayer(true, horizons);

        for (int h = 0; h < horizons; h++) {
            // determine influx of infiltration to MPS and LPS
            double bak_infiltration = infiltration;
            infiltration = calcMPSInflow(infiltration, h);
            infiltration = calcLPSInflow(infiltration, h);
            infiltration_hor[h] = (bak_infiltration - infiltration);
        }

        if (infiltration > 0) {
            run_actDPS += infiltration;
            infiltration = 0;
        }

        for (int h = 0; h < horizons; h++) {
            // determine influx of percolation to MPS
            percolation = calcMPSInflow(percolation, h);

            // determine transpiration from MPS
            calcMPSTranspiration(h);
            actETP_h[h] = actET;

            // determine influx of percolation to LPS
            percolation = calcLPSInflow(percolation, h);

            if (percolation > 0) {
                percolation = calcMPSInflow(percolation, h - 1);
                percolation = calcLPSInflow(percolation, h - 1);
            }

            // update soil saturations
            calcSoilSaturations();

            // determine outflow of water from LPS
            double MobileWater = 0;

            if (actLPS_h[h] > 0) {
                MobileWater = calcLPSoutflow(h);
            } else {
                MobileWater = 0;
            }
            // distribute mobile water to the lateral (interflow) and vertical (percolation) flow paths
            calcIntfPercRates(MobileWater, h);
            perco_hor[h] = percolation;

            // determine internal area routing
            calcRD2_out(h);

            // determine diffusion from LPS to MPS
            calcDiffusion(h);

            // update soil saturations
            calcSoilSaturations();
        }

        if (run_overlandflow < 0) {
            System.out.println("overlandflow is negative! --> " + run_overlandflow);
        }

        if (CN) {
            run_overlandflow += CN_overlandflow;
        } else {
            // determine direct runoff from depression storage
            run_overlandflow += calcDirectRunoff();
        }

        if (CN) {
            calcRD1_outCN();
        } else {
            calcRD1_out();
        }

        for (int h = 0; h < horizons; h++) {
            balMPSend += actMPS_h[h];
            balLPSend += actLPS_h[h];
            balOut += outRD2_h[h];
            sumactETP += actETP_h[h];
        }

        outRD2 = balOut;
        balDPSend = run_actDPS;
        balET = sumactETP + preinfep - balET;
        balOut += balET;
        balOut += outRD1;
        balOut += percolation;
        balET = sumactETP + preinfep;

        // calculate water balance
        balance = (balIn + (balMPSstart - balMPSend) + (balLPSstart - balLPSend) + (balDPSstart - balDPSend) - balOut);

        balanceIn = balIn;
        balanceMPSstart = balMPSstart;
        balanceMPSend = balMPSend;
        balanceLPSstart = balLPSstart;
        balanceLPSend = balLPSend;
        balanceDPSstart = balDPSstart;
        balanceDPSend = balDPSend;
        balanceOut = balOut;

        actDPS = run_actDPS;
        actET = balET;

        if (log.isLoggable(Level.INFO)) {
            log.info("RD2_out: " + outRD2_h[0] + "\t" + outRD2_h[1] + "\t" + outRD2_h[2]);
        }
    }

    private boolean checkActLPS_maxLPS() {
        for (int h = 0; h < horizons; h++) {
            if ((actLPS_h[h] > maxMPS_h[h])) {
                double recons_water = actLPS_h[h] - maxMPS_h[h];
                actLPS_h[h] -= recons_water;
                outRD2_h[h] += recons_water;
            }
        }

        return true;
    }

    private boolean calcSoilSaturations() {

        soilMaxMPS = 0;
        soilActMPS = 0;
        soilMaxLPS = 0;
        soilActLPS = 0;
        soilSatMPS = 0;
        soilSatLPS = 0;

        double upperMaxMps = 0;
        double upperActMps = 0;
        double upperMaxLps = 0;
        double upperActLps = 0;
        double[] infil_depth = new double[horizons];
        double partdepth = 0;
        double soilinfil = 50;

        for (int h = 0; h < horizons; h++) {
            if ((actLPS_h[h] > 0) && (maxLPS_h[h] > 0)) {
                satLPS_h[h] = actLPS_h[h] / maxLPS_h[h];
            } else {
                satLPS_h[h] = 0;
            }

            if ((actMPS_h[h] > 0) && (maxMPS_h[h] > 0)) {
                satMPS_h[h] = actMPS_h[h] / maxMPS_h[h];
            } else {
                satMPS_h[h] = 0;
            }

            if (((maxLPS_h[h] > 0) | (maxMPS_h[h] > 0)) & ((actLPS_h[h] > 0) | (actMPS_h[h] > 0))) {
                run_satHor[h] = ((actLPS_h[h] + actMPS_h[h]) / (maxLPS_h[h] + maxMPS_h[h]));
            } else {
                soilSat = 0;
            }

            infil_depth[h] += depth_h[h];

            if (infil_depth[h] <= soilinfil || h == 0) {
                upperMaxMps += maxMPS_h[h] * depth_h[h];
                upperActMps += actMPS_h[h] * depth_h[h];
                upperMaxLps += maxLPS_h[h] * depth_h[h];
                upperActLps += actLPS_h[h] * depth_h[h];
                partdepth += depth_h[h];

            } else if (infil_depth[h - 1] <= soilinfil) {
                double lowpart = soilinfil - partdepth;
                upperMaxMps += maxMPS_h[h] * lowpart;
                upperActMps += actMPS_h[h] * lowpart;
                upperMaxLps += maxLPS_h[h] * lowpart;
                upperActMps += actLPS_h[h] * lowpart;

            }
            soilMaxMPS += maxMPS_h[h];
            soilActMPS += actMPS_h[h];
            soilMaxLPS += maxLPS_h[h];
            soilActLPS += actLPS_h[h];

            // compute soil water content (dimensionless) for each soil horizon
            swc_h[h] = volumeDeadCapacity_h[h] + ((actMPS_h[h] / maxMPS_h[h])
                    * (volumeFieldCapacity_h[h] - volumeDeadCapacity_h[h])) + ((actLPS_h[h] / maxLPS_h[h])
                    * (volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]));
        }

        if (((soilMaxLPS > 0) | (soilMaxMPS > 0)) & ((soilActLPS > 0) | (soilActMPS > 0))) {
            soilSat = ((soilActLPS + soilActMPS) / (soilMaxLPS + soilMaxMPS));
            top_satsoil = ((upperActLps + upperActMps) / (upperMaxLps + upperMaxMps));
            soilSatMPS = (soilActMPS / soilMaxMPS);
            soilSatLPS = (soilActLPS / soilMaxLPS);
        } else {
            soilSat = 0;
        }
        return true;
    }

    private boolean redistRD1_RD2_in() {
        // inRD1 is first allocated to depression storage (DPS)
        if (inRD1 > 0) {
            run_actDPS += inRD1;
        }

        for (int h = 0; h < horizons; h++) {
            if (inRD2_h[h] > 0) {
                inRD2_h[h] = calcMPSInflow(inRD2_h[h], h);
                inRD2_h[h] = calcLPSInflow(inRD2_h[h], h);
                if (inRD2_h[h] > 0) {
                    outRD2_h[h] += inRD2_h[h];
                    inRD2_h[h] = 0;
                }
            }
        }
        return true;
    }

    private boolean layer_diffusion() {
        for (int h = 0; h < horizons - 1; h++) {
            // calculate diffusion factor (horizontal diffusion only occurs when gravitative flux is not dominating)
            if ((satLPS_h[h] < 0.05) && (satMPS_h[h] < 0.8 || satMPS_h[h + 1] < 0.8) && (satMPS_h[h] > 0 || satMPS_h[h + 1] > 0)) {

                // calculate flow gradient
                double gradient_h_h1 = (Math.log10(2 - satMPS_h[h]) - Math.log10(2 - satMPS_h[h + 1]));

                // calculate resistance
                double satbalance = Math.pow((Math.pow(satMPS_h[h], 2) + (Math.pow(satMPS_h[h + 1], 2))) / 2.0, 0.5);
                double resistance_h_h1 = Math.log10(satbalance) * -kdiff_layer;

                // calculate amount of water to equalize saturations in layers
                double avg_sat = ((maxMPS_h[h] * satMPS_h[h]) + (maxMPS_h[h + 1] * satMPS_h[h + 1])) / (maxMPS_h[h] + maxMPS_h[h + 1]);
                double pot_flux = Math.abs((avg_sat - satMPS_h[h]) * maxMPS_h[h]);

                // calculate water fluxes
                double flux = (pot_flux * gradient_h_h1 / resistance_h_h1);

                if (flux >= 0) {
                    w_layer_diff[h] = Math.min(flux, pot_flux);
                } else {
                    w_layer_diff[h] = Math.max(flux, -pot_flux);
                }
            } else {
                w_layer_diff[h] = 0;
            }
            actMPS_h[h] += w_layer_diff[h];
            actMPS_h[h + 1] -= w_layer_diff[h];
        }
        return true;
    }

    private boolean calcPreInfEvaporation() {
        double deltaETP = potET - actET;
        if (run_actDPS > 0) {
            if (run_actDPS >= deltaETP) {
                run_actDPS -= deltaETP;
                deltaETP = 0;
                actET = potET;
            } else {
                deltaETP -= run_actDPS;
                run_actDPS = 0;
                actET = potET - deltaETP;
            }
        }
        return true;
    }

    private boolean calcInfImperv(double sealedGrade) {
        if (sealedGrade > 0.8) {
            run_overlandflow += (1 - soilImpGT80) * infiltration;
            infiltration *= soilImpGT80;
        } else if (sealedGrade > 0 && sealedGrade <= 0.8) {
            run_overlandflow += (1 - soilImpLT80) * infiltration;
            infiltration *= soilImpLT80;
        }
        if (run_overlandflow < 0) {
            System.out.println("overlandflow negative because of sealing! " + soilImpGT80 + ", " + soilImpLT80 + ", " + infiltration);
        }
        return true;
    }

    private double calcMaxInfiltration(int nowmonth) {
        if (flagInfiltration == null) {
            return calcMaxInfiltrationEagleson(nowmonth);
        }
        // curve number is not accounted for as this method should not be called during curve number
        switch (flagInfiltration) {
            case BUCKET:
                return calcMaxInfiltrationBucket(nowmonth);
            case EAGLESON:
            default:
                return calcMaxInfiltrationEagleson(nowmonth);
        }
    }

    private double calcMaxInfiltrationEagleson(int nowmonth) {
        // infiltration function per Entekhabi and Eagleson (1989)
        double maxInf = 0;
        calcSoilSaturations();
        double soilsat_h = ((actLPS_h[0] + actMPS_h[0]) / (maxLPS_h[0] + maxMPS_h[0]));
        if (snowDepth > 0) {
            maxInf = soilMaxInfSnow + 10 * kf_h[0];
        } else if ((nowmonth >= 5) & (nowmonth <= 10)) {
            maxInf = (1 - soilsat_h) * soilMaxInfSummer + 10 * kf_h[0];
        } else {
            maxInf = (1 - soilsat_h) * soilMaxInfWinter + 10 * kf_h[0];
        }
        return maxInf * area;
    }

    private double calcMaxInfiltrationBucket(int nowmonth) {

        double maxInf = 0;
        double calib_kf = 1;  // potential calibration factor

        calcSoilSaturations();

        double left_in_amount = (maxLPS_h[0] + maxMPS_h[0]) - (actLPS_h[0] + actMPS_h[0]);
        double daily_pot_in = (10 * (kf_h[0] * calib_kf)) * area;

        if (snowDepth > 0) {
            if (left_in_amount >= daily_pot_in) {
                maxInf = daily_pot_in * soilMaxInfSnow;
            } else {
                maxInf = (daily_pot_in - left_in_amount) * soilMaxInfSnow;
            }
        } else if ((nowmonth >= 5) & (nowmonth <= 10)) {
            if (left_in_amount >= daily_pot_in) {
                maxInf = daily_pot_in * soilMaxInfSummer;
            } else {
                maxInf = (daily_pot_in - left_in_amount) * soilMaxInfSummer;
            }
        } else if (left_in_amount >= daily_pot_in) {
            maxInf = daily_pot_in * soilMaxInfWinter;
        } else {
            maxInf = (daily_pot_in - left_in_amount) * soilMaxInfWinter;
        }
        return maxInf;
    }

    private double[] calcMPSEvapotranslayer(boolean debug, int horizons) {
        double[] hETP = new double[horizons];
        double sumlayer = 0;
        int i = 0;
        double runrootdepth = (zrootd * 1000) + 10;
        double[] partroot = new double[horizons];
        double pTransp = 0;
        double pEvap = 0;
        double[] transp_hord = new double[horizons];
        double[] evapo_hord = new double[horizons];
        double[] transp_hor = new double[horizons];
        double[] evapo_hor = new double[horizons];
        double horbal = 0;
        double test = 0;

        // differentiate between evaporation and transpiration
        double deltaETP = potET - actET;

        if (LAI <= 3) {
            pTransp = (deltaETP * LAI) / 3;
        } else if (LAI > 3) {
            pTransp = deltaETP;
        }
        pEvap = deltaETP - pTransp;

        double soilroot = 0;
        // evapotranspiration loop 1: calculate soil root depth partitioning across horizons
        while (i < horizons) {
            sumlayer = sumlayer + depth_h[i] * 10;
            if (root_h[i] == 1.0) {
                soilroot = soilroot + depth_h[i] * 10;
            }
            runlayerdepth[i] = sumlayer;

            if (runrootdepth > runlayerdepth[0]) {
                if (runrootdepth > runlayerdepth[i] && root_h[i] == 1.0) {
                    partroot[i] = 1;
                } else if (runrootdepth > runlayerdepth[i - 1] && root_h[i] == 1.0) {
                    partroot[i] = (runrootdepth - runlayerdepth[i - 1]) / (runlayerdepth[i] - runlayerdepth[i - 1]);
                } else {
                    partroot[i] = 0;
                }
            } else if (i == 0) {
                partroot[i] = runrootdepth / runlayerdepth[0];
            }
            i++;
        }

        if (runrootdepth >= sumlayer) {
            runrootdepth = sumlayer;
        }

        i = 0;
        while (i < horizons) {
            runrootdepth = Math.min(runrootdepth, soilroot);
            // transpiration loop 2: calculate transpiration distribution function using soil horizon and rooting depth
            transp_hord[i] = (pTransp * (1 - Math.exp(-BetaW * (runlayerdepth[i] / runrootdepth)))) / (1 - Math.exp(-BetaW));
            if (transp_hord[i] > pTransp) {
                transp_hord[i] = pTransp;
            }
            // evaporation loop 2: calculate evaporation distribution function using soil horizon depth
            evapo_hord[i] = pEvap * (runlayerdepth[i] / (runlayerdepth[i] + (Math.exp(2.374 - (0.00713 * runlayerdepth[i])))));
            if (evapo_hord[i] > pEvap) {
                evapo_hord[i] = pEvap;
            }
            // allocate the remaining evaporation and transpiration to the lowest soil horizon
            if (i == horizons - 1) {
                evapo_hord[i] = pEvap;
                transp_hord[i] = pTransp;
            }
            if (i == 0) {
                transp_hor[i] = transp_hord[i];
                evapo_hor[i] = evapo_hord[i];
            } else {
                transp_hor[i] = transp_hord[i] - transp_hord[i - 1];
                evapo_hor[i] = evapo_hord[i] - evapo_hord[i - 1];
            }
            hETP[i] = transp_hor[i] + evapo_hor[i];

            if (debug) {
                horbal += hETP[i];
                test = deltaETP - horbal;
            }
            i++;
        }
        if ((test > 0.0000001 || test < -0.0000001) && debug) {
            System.out.println("evaporation balance error = " + test);
        }
        soil_root = soilroot / 1000;
        return hETP;
    }

    private boolean calcMPSTranspiration(int hor) {
        double maxTrans = 0;

        // calculate soil saturations
        calcSoilSaturations();

        double deltaETP = horETP[hor];

        // linear reduction after Menzel (1997)
        if (soilLinRed > 0) {  // reduction if actual saturation is smaller than the linear factor
            if (satMPS_h[hor] < soilLinRed) {
                double reductionFactor = satMPS_h[hor] / soilLinRed;
                maxTrans = deltaETP * reductionFactor;
            } else {
                maxTrans = deltaETP;
            }
        } else if (soilPolRed > 0) {  // polynomial reduction after Krause (2001)
            double sat_factor = -10. * Math.pow((1 - satMPS_h[hor]), soilPolRed);
            double reductionFactor = Math.pow(10, sat_factor);
            maxTrans = deltaETP * reductionFactor;

            if (maxTrans > deltaETP) {
                maxTrans = deltaETP;
            }
        }

        if (maxTrans > actMPS_h[hor]) {
            maxTrans = actMPS_h[hor];
        }

        actMPS_h[hor] -= maxTrans;

        // recalculate actual ETP and soil saturations
        actET = maxTrans;
        calcSoilSaturations();

        // ETP from water bodies should be implemented here
        return true;
    }

    private double calcMPSInflow(double infiltration, int hor) {
        double inflow = infiltration;

        // update soil saturations
        calcSoilSaturations();

        // determine if MPS can store all the water
        if (inflow < (maxMPS_h[hor] - actMPS_h[hor])) {
            // if MPS is empty it receives all the water

            if (actMPS_h[hor] == 0) {
                actMPS_h[hor] += inflow;
                inflow = 0;
            } else { // MPS is partly filled and therefore receives part of the water
                double alpha = soilDistMPSLPS;
                // sat_MPS is set to to 0.0000001 to avoid divide by zero error
                if (satMPS_h[hor] == 0) {
                    satMPS_h[hor] = 0.0000001;
                }
                double inMPS = (inflow) * (1. - Math.exp(-1 * alpha / satMPS_h[hor]));
                actMPS_h[hor] += inMPS;
                inflow -= inMPS;
            }
        } else { // infiltration exceeds MPS storage capacity
            double deltaMPS = maxMPS_h[hor] - actMPS_h[hor];
            actMPS_h[hor] = maxMPS_h[hor];
            inflow -= deltaMPS;
        }
        // update soil saturations
        calcSoilSaturations();
        return inflow;
    }

    private double calcLPSInflow(double infiltration, int hor) {
        // update soil saturations
        calcSoilSaturations();

        actLPS_h[hor] += infiltration;
        infiltration = 0;

        // if LPS is saturated then depression storage occurs
        if (actLPS_h[hor] > maxLPS_h[hor]) {
            infiltration = (actLPS_h[hor] - maxLPS_h[hor]);
            actLPS_h[hor] = maxLPS_h[hor];
        }
        // update soil saturations
        calcSoilSaturations();

        return infiltration;
    }

    private double calcLPSoutflow(int hor) {
        double alpha = soilOutLPS;

        // if satLPS_h is 1, reset value to 0.999999 to avoid error in mathematical function
        if (satLPS_h[hor] == 1.0) {
            satLPS_h[hor] = 0.999999;
        }

        double potLPSoutflow = Math.pow(run_satHor[hor], alpha) * actLPS_h[hor];

        if (potLPSoutflow > actLPS_h[hor]) {
            potLPSoutflow = actLPS_h[hor];
        }

        double LPSoutflow = potLPSoutflow;

        if (LPSoutflow > actLPS_h[hor]) {
            LPSoutflow = actLPS_h[hor];
        }

        if (LPSoutflow < 0) {
            LPSoutflow = 0;
        }

        actLPS_h[hor] -= LPSoutflow;
        return LPSoutflow;
    }

    private boolean calcIntfPercRates(double MobileWater, int hor) {
        if (MobileWater > 0) {
            double slope_weight = (Math.tan(slope * (Math.PI / 180.))) * soilLatVertLPS;

            // potential part of percolation
            double part_perc = (1 - slope_weight);

            if (part_perc > 1) {
                part_perc = 1;
            } else if (part_perc < 0) {
                part_perc = 0;
            }

            // potential part of interflow
            double part_intf = (1 - part_perc);
            interflow += MobileWater * part_intf;
            percolation += MobileWater * part_perc;

            double maxPerc = 0;

            // check if percolation rate is limited by maxPerc
            if (hor == horizons - 1) {
                maxPerc = geoMaxPerc * area * Kf_geo / 86.4;

                if (percolation > maxPerc) {
                    double rest = percolation - maxPerc;
                    percolation = maxPerc;
                    interflow += rest;
                }
            } else {
                try {
                    maxPerc = soilMaxPerc * area * kf_h[hor + 1] / 86.4;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (percolation > maxPerc) {
                    double rest = percolation - maxPerc;
                    percolation = maxPerc;
                    interflow += rest;
                }
            }
        } else { // no mobile water is available
            interflow = 0;
            percolation = 0;
        }
        return true;
    }

    private double calcDirectRunoff() {
        double directRunoff = 0;
        if (run_actDPS > 0) {
            double maxDep = 0;

            // depression storage on steep slopes is reduced by 0.5
            if (slope > 5.0) {
                maxDep = (soilMaxDPS * area) / 2;
            } else {
                maxDep = soilMaxDPS * area;
            }

            if (run_actDPS > maxDep) {
                directRunoff = run_actDPS - maxDep;
                run_actDPS = maxDep;
            }
        }
        if (directRunoff < 0) {
            System.out.println("directRunoff is negative! --> " + directRunoff);
        }
        return directRunoff;
    }

    private boolean calcRD1_out() {
        // set RD1_output to run_overlandflow
        double RD1_output = run_overlandflow;

        // remaining water is added to depression storage
        run_actDPS += (run_overlandflow - RD1_output);
        outRD1 += RD1_output;
        genRD1 = outRD1;

        run_overlandflow = 0;

        return true;
    }

    private boolean calcRD2_out(int h) {
        // set RD2_output to interflow (lateral flow)
        double RD2_output = interflow;

        // remaining water is added to LPS Storage
        actLPS_h[h] += (interflow - RD2_output);
        outRD2_h[h] += RD2_output;
        genRD2_h[h] = outRD2_h[h];

        if (genRD2_h[h] < 0) {
            genRD2_h[h] = 0;
        }

        interflow = 0;
        return true;
    }

    private boolean calcRD1_outCN() {
        outRD1 += run_overlandflow;
        genRD1 = outRD1;
        run_overlandflow = 0;
        return true;
    }

    private void calcDiffusion(int h) {
        double diffusion = 0.0;

        // update soil saturations
        calcSoilSaturations();

        double deltaMPS = maxMPS_h[h] - actMPS_h[h];

        // if sat_MPS_h is 0.0, set diffusion to 0 to avoid divide by zero error
        if (satMPS_h[h] == 0.0) {
            diffusion = 0.0;
        } else {
            double diff = soilDiffMPSLPS;
            diffusion = actLPS_h[h] * (1. - Math.exp((-1. * diff) / satMPS_h[h]));
        }
        if (diffusion > actLPS_h[h]) {
            diffusion = actLPS_h[h];
        }

        // MPS can receive all the water from diffusion
        if (diffusion < deltaMPS) {
            actMPS_h[h] += diffusion;
            actLPS_h[h] -= diffusion;
        } else { // MPS receives only part of the water from diffusion
            double rest = maxMPS_h[h] - actMPS_h[h];
            actMPS_h[h] = maxMPS_h[h];
            actLPS_h[h] -= rest;
        }
    }

    private double calcCNrunoff() {
        double directRunoff = 0;
        CurveNumber cn = new CurveNumber();
        cn.cn_froz = cn_froz;
        cn.cnn = cnn;
        cn.fcimp = fcimp;
        cn.icn = icn;

        if (cropId != 98) {
            cn.iurban = 0;
        } else {
            cn.iurban = 1;
        }

        cn.precipday = (netRain + snowMelt + run_actDPS) / area;
        cn.r2adj = r2adj;

        double sumfc = 0;
        double sumac = 0;
        double maxfc = 0;
        double maxac = 0;

        for (int h = 0; h < horizons; h++) {
            sumfc += actMPS_h[h] / area;
            sumac += actLPS_h[h] / area;
            maxfc += maxMPS_h[h] / area;
            maxac += maxLPS_h[h] / area;
        }

        cn.sol_sumfc = sumfc;
        cn.sol_sumul = maxfc + maxac;
        cn.sol_sw = sumfc + sumac;

        if (soil_Temp_Layer.length > 1) {
            cn.sol_tmp = soil_Temp_Layer[1];
        } else {
            cn.sol_tmp = soil_Temp_Layer[0];
        }

        if (snowDepth == 0) {
            cn.run();
            directRunoff = cn.surfq * area;
        }
        return directRunoff;
    }
}
