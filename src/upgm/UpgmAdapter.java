/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package upgm;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import crop.Crop;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Description("Add Upgm module definition here")
@Author(name = "Nathan Lighthart, Robert Streetman, Olaf David", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/upgm/Upgm.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/upgm/Upgm.xml")
public class UpgmAdapter extends AnnotatedAdapter {
    @Description("UPGM Flag")
    @Input public boolean flagUPGM;

    @Description("Current Date")
    @Input public LocalDate time;

    @Description("harvest date")
    @Input public LocalDate nextHarvestingDate;

    @Input public Path outFile_hru_crop_upgm;

    @Input public double co2;

    @Description("Current HRU")
    @Input public HRU hru;

    @Description("Current Crop")
    @Input public Crop crop;

    @Description("Current Soil")
    @Input public SoilType soil;

    @Description("flag for crop existence")
    @Input public boolean cropExists;

    @Description("minimum temperature")
    @Input public double tmin;

    @Description("maximum temperature")
    @Input public double tmax;

    @Description("solar radiation")
    @Input public double solRad;

    @Description("precipitation")
    @Input public double precip;

    @Description("water stress")
    @Optional
    @Input public double wstrs;

    @Optional
    @Input @Output public double grainf;

    @Optional
    @Input @Output public double root;

    @Optional
    @Input @Output public double zrootd;

    @Optional
    @Input @Output public double LAI;

    @Optional
    @Input @Output public double PHUact;

    @Optional
    @Input @Output public double FPHUact;

    @Optional
    @Input @Output public double CanHeightAct;

    @Optional
    @Input @Output public double tstrs;

    @Optional
    @Input @Output public double BioOpt_delta;

    @Optional
    @Input @Output public double deltabiomass;

    @Optional
    @Input @Output public double BioAct;

    @Optional
    @Input @Output public double BioagAct;

    @Optional
    @Input @Output public double BioYield;

    @Optional
    @Input @Output public double Addresidue_pool;

    @Optional
    @Input @Output public double HarvIndex;

    @Output public double standleaf;
    @Output public double standstem;
    @Output public double standstore;
    @Output public double flatleaf;
    @Output public double flatstem;
    @Output public double flatstore;

    private Map<HRU, Upgm> componentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        if (!flagUPGM) {
            skipOutput();
            return;
        }

        Upgm component = componentMap.get(hru);
        if (component == null) {
            component = new Upgm();
            componentMap.put(hru, component);
        }

        component.time = time;
        component.harvestDate = nextHarvestingDate;
        component.outFile_hru_crop_upgm = outFile_hru_crop_upgm;
        component.co2 = co2;
        component.hruID = hru.ID;
        component.crop = crop;
        component.soil = soil;
        component.plantExisting = cropExists;
        component.doHarvest = doHarvest();
        component.tmin = tmin;
        component.tmax = tmax;
        component.solRad = solRad;
        component.precip = precip;
        component.wstrs = wstrs;
        component.grainf = grainf;
        component.root = root;
        component.zrootd = zrootd;
        component.LAI = LAI;
        component.PHUact = PHUact;
        component.FPHUact = FPHUact;
        component.CanHeightAct = CanHeightAct;
        component.tstrs = tstrs;
        component.BioOpt_delta = BioOpt_delta;
        component.deltabiomass = deltabiomass;
        component.BioAct = BioAct;
        component.BioagAct = BioagAct;
        component.BioYield = BioYield;
        component.Addresidue_pool = Addresidue_pool;
        component.HarvIndex = HarvIndex;

        component.execute();

        grainf = component.grainf;
        root = component.root;
        zrootd = component.zrootd;
        LAI = component.LAI;
        PHUact = component.PHUact;
        FPHUact = component.FPHUact;
        CanHeightAct = component.CanHeightAct;
        tstrs = component.tstrs;
        BioOpt_delta = component.BioOpt_delta;
        deltabiomass = component.deltabiomass;
        BioAct = component.BioAct;
        BioagAct = component.BioagAct;
        BioYield = component.BioYield;
        Addresidue_pool = component.Addresidue_pool;
        HarvIndex = component.HarvIndex;
        standleaf = component.standleaf;
        standstem = component.standstem;
        standstore = component.standstore;
        flatleaf = component.flatleaf;
        flatstem = component.flatstem;
        flatstore = component.flatstore;
    }

    private boolean doHarvest() {
        if (nextHarvestingDate == null) {
            return false;
        }
        return time.plusDays(1L).equals(nextHarvestingDate);
    }

    @Override
    public void shutdown() {
        for (Upgm component : componentMap.values()) {
            component.finish();
        }
        componentMap.clear();
    }
}
