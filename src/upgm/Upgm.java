/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package upgm;

import ages.types.SoilType;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import crop.Crop;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Upgm {
    private static double acycon = 10000.0;
    private static final Path tempLibDir;

    static {
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("agesw_");
            tempDir.toFile().deleteOnExit(); // delete this directory on exit
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, "Failed to create temporary library directory", ex);
            tempDir = Paths.get("");
        }
        tempLibDir = tempDir.toAbsolutePath();
    }

    public LocalDate time;
    public LocalDate harvestDate;
    public Path outFile_hru_crop_upgm;
    public double co2;
    public int hruID;
    public Crop crop;
    public SoilType soil;
    public boolean plantExisting;
    public boolean doHarvest;
    public double tmin;
    public double tmax;
    public double solRad;
    public double precip;
    public double wstrs;

    public double grainf;
    public double root;
    public double zrootd;
    public double LAI;
    public double PHUact;
    public double FPHUact;
    public double CanHeightAct;
    public double tstrs;
    public double BioOpt_delta;
    public double deltabiomass;
    public double BioAct;
    public double BioagAct;
    public double BioYield;
    public double Addresidue_pool;
    public double HarvIndex;

    public double standleaf;
    public double standstem;
    public double standstore;
    public double flatleaf;
    public double flatstem;
    public double flatstore;

    private boolean init = false;
    private JupgmGen Jupgm = new JupgmGen();
    private JupgmInitGen Jupgminit = new JupgmInitGen();

    private float prevtmax = -9999.0f;
    private int counter = 1;
    private boolean reset;
    private nap.Libupgm lib;
    private int yearOfPlanting;

    public void execute() {
        if (lib == null) {
            try {
                createDLL();
                loadDLL();
            } catch (IOException ex) {
                Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }
        if (plantExisting) {
            if (!init) {
                init();
                init = true;
            }
            run();

            if (doHarvest) {
                harvest();
                reset = true;
            }
        } else if (reset) {
            reset();

            prevtmax = -9999.0f;
            ++counter;

            unloadDLL();
            loadDLL();

            init = false;
            reset = false;
        }
    }

    public void finish() {
        if (lib != null) {
            unloadDLL();
        }
    }

    private void init() {
        yearOfPlanting = time.getYear();

        Path outParent = outFile_hru_crop_upgm.resolveSibling("upgm");
        outParent = outParent.resolve("hru" + hruID);
        try {
            Files.createDirectories(outParent);
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        String idString = Integer.toString(counter);

        Path cropXML = outParent.resolve("Cropxml" + idString + ".dat");
        writeCropXML(cropXML);

        Path upgmCrop = outParent.resolve("Upgm_crop" + idString + ".dat");
        writeUpgmCrop(upgmCrop);

        Path upgmMgmt = outParent.resolve("Upgm_mgmt" + idString + ".dat");
        writeUpgmManagement(upgmMgmt);

        Path upgmSoil = outParent.resolve("upgm_soil_profile" + idString + ".dat");
        writeUpgmSoil(upgmSoil);

        Jupgminit.cropxmlfile = cropXML.toString();
        Jupgminit.upgmcropfile = upgmCrop.toString();
        Jupgminit.upgmmgmtfile = upgmMgmt.toString();
        Jupgminit.upgmsoilfile = upgmSoil.toString();

        Jupgminit.canopyhtoutfile = outParent.resolve("canopyht" + idString + ".out").toString();
        Jupgminit.cdbugoutfile = outParent.resolve("cdbug" + idString + ".out").toString();
        Jupgminit.cropoutfile = outParent.resolve("crop" + idString + ".out").toString();
        Jupgminit.emergeoutfile = outParent.resolve("emerge" + idString + ".out").toString();
        Jupgminit.inptoutfile = outParent.resolve("inpt" + idString + ".out").toString();
        Jupgminit.phenoloutfile = outParent.resolve("phenol" + idString + ".out").toString();
        Jupgminit.seasonoutfile = outParent.resolve("season" + idString + ".out").toString();
        Jupgminit.shootoutfile = outParent.resolve("shoot" + idString + ".out").toString();
        Jupgminit.hruid = hruID;

        try {
            Jupgminit.exec();
        } catch (Exception ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void run() {
        // set up input
        Jupgm.id = time.getDayOfMonth();
        Jupgm.im = time.getMonthValue();
        Jupgm.iy = time.getYear() - yearOfPlanting + 1;

        Jupgm.nexttmin = (float) tmin;

        Jupgm.tmin = (float) tmin;
        Jupgm.tmax = (float) tmax;
        Jupgm.rad = (float) solRad;
        Jupgm.precip = (float) precip;

        if (prevtmax == -9999.0f) {
            prevtmax = (float) tmax;
            Jupgm.prevtmax = prevtmax;
        } else {
            Jupgm.prevtmax = prevtmax;
            prevtmax = (float) tmax;
        }

        Jupgm.wstrs = (float) (1 - wstrs);

        Jupgm.co2 = (float) co2;

        // execute module
        try {
            Jupgm.exec();
        } catch (Exception ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }

        // read output
        standleaf = Jupgm.standleaf * acycon;
        standstem = Jupgm.standstem * acycon;
        standstore = Jupgm.standstore * acycon;
        flatleaf = Jupgm.flatleaf * acycon;
        flatstem = Jupgm.flatstem * acycon;
        flatstore = Jupgm.flatstore * acycon;
        grainf = Jupgm.grainf;
        root = Jupgm.root * acycon;
        zrootd = Jupgm.zrootd;
        LAI = Jupgm.lai;
        PHUact = Jupgm.phuact;
        FPHUact = Jupgm.fphuact;
        CanHeightAct = Jupgm.canheightact;
        tstrs = (1.0 - Jupgm.tstrs);
        BioOpt_delta = Jupgm.biooptdelta * acycon;
        deltabiomass = Jupgm.deltabiomass * acycon;

        BioAct = standstem + standstore + standleaf + flatstem
                + flatstore + flatleaf + root;
    }

    private void harvest() {
        BioagAct = standstem + standstore + standleaf + flatstem
                + flatstore + flatleaf;
        BioYield = (standstore + flatstore) * grainf;
        Addresidue_pool = standleaf + flatleaf + standstem + flatstem
                + (standstore + flatstore) * (1 - grainf);
        HarvIndex = BioYield / BioAct;
    }

    private void createDLL() throws IOException {
        // find upgm path .dll/so file
        NativeLibrary upgm = NativeLibrary.getInstance("upgm");
        Path upgmPath = upgm.getFile().toPath().toAbsolutePath();

        // parse extension
        String extension = upgmPath.getFileName().toString();
        extension = extension.substring(extension.lastIndexOf("."));

        // create copy
        String libName = "upgm" + hruID;
        Path copyPath = tempLibDir.resolve("lib" + libName + extension);
        copyPath.toFile().deleteOnExit(); // delete temporary library on exit
        Files.copy(upgmPath, copyPath, StandardCopyOption.REPLACE_EXISTING);
    }

    private void loadDLL() {
        String libName = "upgm" + hruID;
        NativeLibrary.addSearchPath(libName, tempLibDir.toString());
        lib = (nap.Libupgm) Native.loadLibrary(libName, nap.Libupgm.class);

        Jupgm.lib = lib;
        Jupgminit.lib = lib;
    }

    private void unloadDLL() {
        String libName = "upgm" + hruID;
        NativeLibrary library = NativeLibrary.getInstance(libName);
        library.dispose();
        lib = null;
    }

    private void writeCropXML(Path path) {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path, Charset.defaultCharset()))) {
            writer.println(crop.ac0nam);
            writer.println(crop.acdpop + " " + crop.acdmaxshoot + " " + crop.acbaflg
                    + " " + crop.acytgt + " " + crop.acbaf + " " + crop.acyraf
                    + " " + crop.achyfg + " " + crop.acynmu);
            writer.println(crop.acywct + " " + crop.acycon + " " + crop.ac0idc
                    + " " + crop.acgrf + " " + crop.ac0ck + " " + crop.acehu0
                    + " " + crop.aczmxc + " " + crop.ac0growdepth);
            writer.println(crop.aczmrt + " " + crop.actmin + " " + crop.actopt
                    + " " + crop.acthudf + " " + crop.actdtm + " " + crop.acthum
                    + " " + crop.ac0fd1[0] + " " + crop.ac0fd1[1]);
            writer.println(crop.ac0fd2[0] + " " + crop.ac0fd2[1] + " " + crop.actverndel
                    + " " + crop.ac0bceff + " " + crop.ac0alf + " " + crop.ac0blf
                    + " " + crop.ac0clf + " " + crop.ac0dlf);
            writer.println(crop.ac0arp + " " + crop.ac0brp + " " + crop.ac0crp
                    + " " + crop.ac0drp + " " + crop.ac0aht + " " + crop.ac0bht
                    + " " + crop.ac0ssa + " " + crop.ac0ssb);
            writer.println(crop.ac0sla + " " + crop.ac0hue + " " + crop.ac0transf
                    + " " + crop.ac0diammax + " " + crop.ac0storeinit + " " + crop.ac0shoot
                    + " " + crop.acfleafstem + " " + crop.acfshoot);
            writer.println(crop.acfleaf2stor + " " + crop.acfstem2stor + " " + crop.acfstor2stor
                    + " " + crop.acrbc + " " + crop.acdkrate[0] + " " + crop.acdkrate[1]
                    + " " + crop.acdkrate[2] + " " + crop.acdkrate[3]);
            writer.println(crop.acdkrate[4] + " " + crop.acxstm + " " + crop.acddsthrsh
                    + " " + crop.accovfact + " " + crop.acresevapa + " " + crop.acresevapb
                    + " " + crop.acyld_coef + " " + crop.acresid_int);
            writer.println("0.0 0.0 0.0 0.0 0.0");
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeUpgmCrop(Path path) {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path, Charset.defaultCharset()))) {
            writer.println(crop.canopyflg);
            writer.println(crop.emrgflg);
            writer.println(crop.phenolflg);
            writer.println(crop.seedbed);
            for (int i = 0; i < 4; ++i) {
                writer.println(crop.soilwat[i]);
                writer.println(crop.wfpslo[i]);
                writer.println(crop.wfpsup[i]);
                writer.println(crop.germgdd[i]);
                writer.println(crop.ergdd[i]);
            }
            writer.println(crop.pchron);
            writer.println(crop.tbase);
            writer.println(crop.toptlo);
            writer.println(crop.toptup);
            writer.println(crop.tupper);
            writer.println(crop.gmethod);
            writer.println(crop.maxht);
            writer.println(crop.ecanht);
            writer.println(crop.growth_stress);
            for (int i = 0; i < 30; ++i) {
                writer.println(crop.dummy1[i] + " " + crop.dummy2[i]);
            }
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeUpgmManagement(Path path) {
        // find planting and harvest dates
        int plantYear = 1;
        int plantMonth = time.getMonthValue();
        int plantDay = time.getDayOfMonth();
        LocalDate harvDate = harvestDate.minusDays(1L); // subtract day from harvest date to let UPGM known when it will be killed
        int harvestYear = harvDate.getYear() - yearOfPlanting + 1;
        int harvestMonth = harvDate.getMonthValue();
        int harvestDay = harvDate.getDayOfMonth();

        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path, Charset.defaultCharset()))) {
            writer.println(plantDay + " " + plantMonth + " " + plantYear + " "
                    + harvestDay + " " + harvestMonth + " " + harvestYear);
            writer.println(plantDay + " " + plantMonth + " " + plantYear + " "
                    + harvestDay + " " + harvestMonth + " " + harvestYear);
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeUpgmSoil(Path path) {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(path, Charset.defaultCharset()))) {
            for (double d : soil.depth_h) {
                writer.println(d * 10); // convert cm to mm
            }
        } catch (IOException ex) {
            Logger.getLogger(Upgm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reset() {
        BioAct = 0;
        BioagAct = 0;
        BioYield = 0;
        Addresidue_pool = 0;
        HarvIndex = 0;

        standleaf = 0;
        standstem = 0;
        standstore = 0;
        flatleaf = 0;
        flatstem = 0;
        flatstore = 0;
        grainf = 0;
        root = 0;
        zrootd = 0;
        LAI = 0;
        PHUact = 0;
        FPHUact = 0;
        CanHeightAct = 0;
        tstrs = 0;
        BioOpt_delta = 0;
        deltabiomass = 0;
    }
}
