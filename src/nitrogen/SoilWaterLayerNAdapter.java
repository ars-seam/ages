/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package nitrogen;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add SoilLayerN module definition here")
@Author(name = "Olaf David, Holm Kipka, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("Utilities")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilWater/SoilLayerN.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilWater/SoilLayerN.xml")
public class SoilWaterLayerNAdapter extends AnnotatedAdapter {
    @Description("Piadin (nitrification blocker) application (calibrate 0.0 to 1.0)")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public int piadin;

    @Description("Indicates fertilization optimization with plant demand.")
    @Role(PARAMETER)
    @Input public double opti;

    @Description("rate constant between N_activ_pool and N_stable_pool")
    @Role(PARAMETER)
    @Range(min = 1.0E-6, max = 1.0E-4)
    @Input public double Beta_trans;

    @Description("rate factor between N_activ_pool and NO3_Pool")
    @Role(PARAMETER)
    @Range(min = 0.001, max = 0.003)
    @Input public double Beta_min;

    @Description("rate factor between Residue_pool and NO3_Pool")
    @Role(PARAMETER)
    @Range(min = 0.02, max = 0.10)
    @Input public double Beta_rsd;

    @Description("percolation coefficient")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public double Beta_NO3;

    @Description("nitrogen uptake distribution parameter")
    @Role(PARAMETER)
    @Range(min = 1, max = 15)
    @Input public double Beta_Ndist;

    @Description("infiltration bypass parameter")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public double infil_conc_factor;

    @Description("denitrification saturation factor")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public double denitfac;

    @Description("concentration of Nitrate in rain")
    @Units("kgN/(mm * ha)")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 0.05)
    @Input public double deposition_factor;

    @Description("fraction of porosity from which anions are excluded")
    @Role(PARAMETER)
    @Range(min = 0.01, max = 1)
    @Input public double theta_nit;

    @Description("HRU")
    @Input public HRU hru;

    @Description("soil")
    @Input public SoilType soil;

    @Description("Actual depth of roots")
    @Units("m")
    @Input public double zrootd;

    @Description("Actual LPS in portion of sto_LPS soil water content")
    @Input public double[] satLPS_h;

    @Description("Actual MPS in portion of sto_MPS soil water content")
    @Input public double[] satMPS_h;

    @Description("Maximum MPS  in l soil water content")
    @Input public double[] maxMPS_h;

    @Description("Maximum LPS  in l soil water content")
    @Input public double[] maxLPS_h;

    @Description("Maximum FPS  in l soil water content")
    @Input public double[] maxFPS_h;

    @Description("Soil temperature in horizons depth")
    @Units("C")
    @Input public double[] soil_Temp_Layer;

    @Description(" actual evaporation")
    @Units("mm")
    @Input public double[] aEP_h;

    @Description("mps diffusion between layers value")
    @Input public double[] w_layer_diff;

    @Description("HRU statevar RD1 outflow")
    @Units("l")
    @Input public double outRD1;

    @Description("HRU statevar RD2 outflow")
    @Units("l")
    @Input public double[] outRD2_h;

    @Description("HRU statevar percolation")
    @Units("l")
    @Input public double percolation;

    @Description("Current organic fertilizer amount added to residue pool")
    @Input public double fertorgNfresh;

    @Description("Biomass added residue pool after harvesting")
    @Units("kg/ha")
    @Input public double Addresidue_pool;

    @Description("Nitrogen added residue pool after harvesting")
    @Units("kgN/ha")
    @Input public double Addresidue_pooln;

    @Description("Precipitation")
    @Units("mm")
    @Input public double precip;

    @Description("indicates dormancy of plants")
    @Input public boolean dormancy;

    @Description("intfiltration poritions for the single horizonts")
    @Units("l")
    @Input public double[] infiltration_hor;

    @Description("Percolation out ouf the single horizonts")
    @Units("l")
    @Input public double[] perco_hor;

    @Description("Percolation out ouf the single horizonts")
    @Units("l")
    @Input public double[] actETP_h;

    @Description("Ammonium input due to Fertilisation")
    @Units("kgN/ha")
    @Input public double fertNH4;

    @Description("Stable organic N input due to Fertilisation")
    @Units("kgN/ha")
    @Input public double fertstableorg;

    @Description("Active organic N input due to Fertilisation")
    @Units("kgN/ha")
    @Input public double fertorgNactive;

    @Description("Optimal nitrogen content in Biomass")
    @Units("kgN/ha")
    @Input public double BioNOpt;

    @Description("Nitrate input due to Fertilisation")
    @Units("kgN/ha")
    @Input public double fertNO3;

    @Description("N at fr3")
    @Input public double fr3N;

    @Description("delta biomass increase per day")
    @Optional
    @Input public double deltabiomass;

    @Description("Nitrate in surface runoff added to HRU horizons")
    @Units("kgN/ha")
    @Input public double SurfaceN_in;

    @Description("Nitrate in interflow in added to HRU horizons")
    @Units("kgN")
    @Input public double[] InterflowN_in;

    @Description("Residue in Layer")
    @Units("kgN/ha")
    @Input @Output public double[] residue_pool;

    @Description("N-Organic fresh Pool from Residue")
    @Units("kgN/ha")
    @Input @Output public double[] N_residue_pool_fresh;

    @Description("Actual nitrogen content in Biomass")
    @Units("kgN/ha")
    @Input @Output public double BioNAct;

    @Description("Time in days since the last PIADIN application")
    @Optional
    @Input @Output public int App_time;

    @Description("NO3-Pool")
    @Units("kgN/ha")
    @Input @Output public double[] NO3_Pool;

    @Description("NH4-Pool")
    @Units("kgN/ha")
    @Input @Output public double[] NH4_Pool;

    @Description("N-Organic Pool with reactive organic matter")
    @Units("kgN/ha")
    @Input @Output public double[] N_activ_pool;

    @Description("N-Organic Pool with stable organic matter")
    @Units("kgN/ha")
    @Input @Output public double[] N_stable_pool;

    @Description("Sum NO3 of Pool start")
    @Units("kgN/ha")
    @Output public double sNO3_Pool_start;

    @Description("Sum NH4 Pool start")
    @Units("kgN/ha")
    @Output public double sNH4_Pool_start;

    @Description("Sum of N-Organic Pool with reactive organic matter")
    @Units("kgN/ha")
    @Output public double sN_activ_Pool_start;

    @Description("Sum of N-Organic Pool with stable organic matter")
    @Units("kgN/ha")
    @Output public double sN_stable_Pool_start;

    @Description("Sum of NResiduePool")
    @Units("kgN/ha")
    @Output public double sNResiduePool_start;

    @Description("Sum of N-Organic Pool with reactive organic matter")
    @Units("kgN/ha")
    @Output public double sN_activ_pool;

    @Description("Sum of N-Organic Pool with stable organic matter")
    @Units("kgN/ha")
    @Output public double sN_stable_pool;

    @Description("Sum of NO3-Pool")
    @Units("kgN/ha")
    @Output public double sNO3_Pool;

    @Description("Sum of NH4-Pool")
    @Units("kgN/ha")
    @Output public double sNH4_Pool;

    @Description("Sum of NResiduePool")
    @Units("kgN/ha")
    @Output public double sNResiduePool;

    @Description("Sum of interflowNabs")
    @Units("kgN")
    @Output public double sinterflowNabs;

    @Description("Sum of interflowN")
    @Units("kgN/ha")
    @Output public double sinterflowN;

    @Description("Voltalisation rate from NH4_Pool")
    @Units("kgN/ha")
    @Output public double Volati_trans;

    @Description("Nitrification rate from  NO3_Pool")
    @Units("kgN/ha")
    @Output public double Nitri_rate;

    @Description("Denitrification rate from  NO3_Pool")
    @Units("kgN/ha")
    @Output public double Denit_trans;

    @Description("Nitrate in surface runoff")
    @Units("kgN/ha")
    @Output public double SurfaceN;

    @Description("Nitrate in interflow")
    @Units("kgN/ha")
    @Output public double[] InterflowN;

    @Description("Nitrate in percolation")
    @Units("kgN/ha")
    @Output public double PercoN;

    @Description("Nitrate in surface runoff")
    @Units("kgN")
    @Output public double SurfaceNabs;

    @Description("Nitrate in interflow")
    @Units("kgN")
    @Output public double[] InterflowNabs;

    @Description("Nitrate-N concentration per Layer")
    @Units("g/l")
    @Output public double[] NO3_N_h;

    @Description("N Percolation out of the soil profile")
    @Units("kgN")
    @Output public double PercoNabs;

    @Description("actual nitrate uptake of plants")
    @Units("kgN/ha")
    @Output public double actnup;

    @Description("Sum of N input due fertilisation and deposition")
    @Units("kgN/ha")
    @Output public double sum_Ninput;

    @Description("Mineral nitrogen content in the soil profile down to 60 cm depth")
    @Output public double nmin;

    @Description("Nitrate in interflow in added to HRU horizons")
    @Units("kgN")
    @Output public double sinterflowN_in;

    @Description("NitrateBalance")
    @Units("kgN")
    @Output public double NitrateBalance;

    @Description("Nitrate-NO3")
    @Units("mg/L")
    @Output public double[] NO3_N;

    @Override
    protected void run(Context context) {
        SoilWaterLayerN component = new SoilWaterLayerN();

        component.piadin = piadin;
        component.opti = opti;
        component.Beta_trans = Beta_trans;
        component.Beta_min = Beta_min;
        component.Beta_rsd = Beta_rsd;
        component.Beta_NO3 = Beta_NO3;
        component.Beta_Ndist = Beta_Ndist;
        component.infil_conc_factor = infil_conc_factor;
        component.denitfac = denitfac;
        component.deposition_factor = deposition_factor;
        component.theta_nit = theta_nit;
        component.area = hru.area;
        component.horizons = soil.horizons;
        component.depth_h = soil.depth_h;
        component.totaldepth = soil.totaldepth;
        component.zrootd = zrootd;
        component.satLPS_h = satLPS_h;
        component.satMPS_h = satMPS_h;
        component.maxMPS_h = maxMPS_h;
        component.maxLPS_h = maxLPS_h;
        component.maxFPS_h = maxFPS_h;
        component.soil_Temp_Layer = soil_Temp_Layer;
        component.corg_h = soil.corg_h;
        component.aEP_h = aEP_h;
        component.w_layer_diff = w_layer_diff;
        component.outRD1 = outRD1;
        component.outRD2_h = outRD2_h;
        component.percolation = percolation;
        component.fertorgNfresh = fertorgNfresh;
        component.Addresidue_pool = Addresidue_pool;
        component.Addresidue_pooln = Addresidue_pooln;
        component.precip = precip;
        component.dormancy = dormancy;
        component.infiltration_hor = infiltration_hor;
        component.perco_hor = perco_hor;
        component.actETP_h = actETP_h;
        component.fertNH4 = fertNH4;
        component.fertstableorg = fertstableorg;
        component.fertorgNactive = fertorgNactive;
        component.BioNOpt = BioNOpt;
        component.fertNO3 = fertNO3;
        component.fr3N = fr3N;
        component.deltabiomass = deltabiomass;
        component.SurfaceN_in = SurfaceN_in;
        component.InterflowN_in = InterflowN_in;
        component.residue_pool = residue_pool;
        component.N_residue_pool_fresh = N_residue_pool_fresh;
        component.BioNAct = BioNAct;
        component.App_time = App_time;
        component.NO3_Pool = NO3_Pool;
        component.NH4_Pool = NH4_Pool;
        component.N_activ_pool = N_activ_pool;
        component.N_stable_pool = N_stable_pool;

        component.execute();

        residue_pool = component.residue_pool;
        N_residue_pool_fresh = component.N_residue_pool_fresh;
        BioNAct = component.BioNAct;
        App_time = component.App_time;
        NO3_Pool = component.NO3_Pool;
        NH4_Pool = component.NH4_Pool;
        N_activ_pool = component.N_activ_pool;
        N_stable_pool = component.N_stable_pool;
        sNO3_Pool_start = component.sNO3_Pool_start;
        sNH4_Pool_start = component.sNH4_Pool_start;
        sN_activ_Pool_start = component.sN_activ_Pool_start;
        sN_stable_Pool_start = component.sN_stable_Pool_start;
        sNResiduePool_start = component.sNResiduePool_start;
        sN_activ_pool = component.sN_activ_pool;
        sN_stable_pool = component.sN_stable_pool;
        sNO3_Pool = component.sNO3_Pool;
        sNH4_Pool = component.sNH4_Pool;
        sNResiduePool = component.sNResiduePool;
        sinterflowNabs = component.sinterflowNabs;
        sinterflowN = component.sinterflowN;
        Volati_trans = component.Volati_trans;
        Nitri_rate = component.Nitri_rate;
        Denit_trans = component.Denit_trans;
        SurfaceN = component.SurfaceN;
        InterflowN = component.InterflowN;
        PercoN = component.PercoN;
        SurfaceNabs = component.SurfaceNabs;
        InterflowNabs = component.InterflowNabs;
        NO3_N_h = component.NO3_N_h;
        PercoNabs = component.PercoNabs;
        actnup = component.actnup;
        sum_Ninput = component.sum_Ninput;
        nmin = component.nmin;
        sinterflowN_in = component.sinterflowN_in;
        NitrateBalance = component.NitrateBalance;
        NO3_N = component.NO3_N;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
