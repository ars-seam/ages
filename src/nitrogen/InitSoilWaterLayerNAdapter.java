/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package nitrogen;

import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add InitSoilLayer module definition here")
@Author(name = "Olaf David, Holm Kipka, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("Utilities")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilWater/InitSoilLayer.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilWater/InitSoilLayer.xml")
public class InitSoilWaterLayerNAdapter extends AnnotatedAdapter {
    @Description("Soil type")
    @Input public SoilType soil;

    @Description("NO3-Pool")
    @Units("kgN/ha")
    @Output public double[] NO3_Pool;

    @Description("NH4-Pool")
    @Units("kgN/ha")
    @Output public double[] NH4_Pool;

    @Description("N-Organic Pool with reactive organic matter")
    @Units("kgN/ha")
    @Output public double[] N_activ_pool;

    @Description("N-Organic Pool with stable organic matter")
    @Units("kgN/ha")
    @Output public double[] N_stable_pool;

    @Description("Residue in Layer")
    @Units("kgN/ha")
    @Output public double[] residue_pool;

    @Description("N-Organic fresh Pool from Residue")
    @Units("kgN/ha")
    @Output public double[] N_residue_pool_fresh;

    @Description("Nitrate in interflow in added to HRU horizons")
    @Units("kgN")
    @Output public double[] InterflowN_in;

    @Description("Nitrate input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertNO3;

    @Description("Ammonium input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertNH4;

    @Description("Stable organic N input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertstableorg;

    @Description("Activ organic N input due to Fertilisation")
    @Units("kgN/ha")
    @Output public double fertactivorg;

    @Description("Actual depth of roots")
    @Units("m")
    @Output public double zrootd;

    @Description("Array of state variables LAI ")
    @Output public double LAI;

    @Override
    protected void run(Context context) {
        InitSoilWaterLayerN component = new InitSoilWaterLayerN();

        component.horizons = soil.horizons;
        component.depth_h = soil.depth_h;
        component.bulkDensity_h = soil.bulkDensity_h;
        component.corg_h = soil.corg_h;

        component.execute();

        NO3_Pool = component.NO3_Pool;
        NH4_Pool = component.NH4_Pool;
        N_activ_pool = component.N_activ_pool;
        N_stable_pool = component.N_stable_pool;
        residue_pool = component.residue_pool;
        N_residue_pool_fresh = component.N_residue_pool_fresh;
        InterflowN_in = component.InterflowN_in;
        fertNO3 = component.fertNO3;
        fertNH4 = component.fertNH4;
        fertstableorg = component.fertstableorg;
        fertactivorg = component.fertactivorg;
        zrootd = component.zrootd;
        LAI = component.LAI;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
