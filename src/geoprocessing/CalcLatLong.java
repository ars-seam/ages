/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package geoprocessing;

import lib.Geoprocessing;
import lib.UTMConversion;

public class CalcLatLong {
    public String projection;
    public double x;
    public double y;
    public double slope;
    public double aspect;
    public double latitude;
    public double longitude;
    public double[] slAsCfArray;

    public void execute() {
        String proj = projection;
        double[] latlong;

        if (proj.equals("GK")) {
            latlong = Geoprocessing.GK2LatLon(x, y);
            latitude = latlong[0];
            longitude = latlong[1];
        } else if (proj.substring(0, 3).equals("UTM")) {
            int len = proj.length();
            String zoneStr = proj.substring(3, len);
            latlong = UTMConversion.utm2LatLong(x, y, zoneStr);
            latitude = latlong[0];
            longitude = latlong[1];
        } else if (proj.equals("LatLong")) {
            latitude = y;
            longitude = x;
        } else {
            throw new IllegalArgumentException(proj);
        }

        slAsCfArray = new double[366];

        for (int i = 0; i < 366; i++) {
            slAsCfArray[i] = Geoprocessing.slopeAspectCorrFactor(i + 1, latitude, slope, aspect);
        }
    }
}
