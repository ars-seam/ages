/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package geoprocessing;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add CalcLatLong module definition here")
@Author(name = "Olaf David, Peter Krause", contact = "jim.ascough@ars.usda.gov")
@Keywords("Utilities")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/geoprocessing/CalcLatLong.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/geoprocessing/CalcLatLong.xml")
public class CalcLatLongAdapter extends AnnotatedAdapter {
    @Description("Projection [GK, UTMZZL, LatLong]")
    @Role(PARAMETER)
    @Input public String projection;

    @Description("HRU")
    @Input public HRU hru;

    @Description("Entity Latitude")
    @Units("deg")
    @Output public double latitude;

    @Description("Entity longitude")
    @Units("deg")
    @Output public double longitude;

    @Description("Slope Aspect Correction Factor Array")
    @Output public double[] slAsCfArray;

    @Override
    protected void run(Context context) {
        CalcLatLong component = new CalcLatLong();

        component.projection = projection;
        component.x = hru.x;
        component.y = hru.y;
        component.slope = hru.slope;
        component.aspect = hru.aspect;

        component.execute();

        latitude = component.latitude;
        longitude = component.longitude;
        slAsCfArray = component.slAsCfArray;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
