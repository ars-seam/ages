
package lib;

import java.io.File;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Static utility methods that help finding the type based on the type name.
 *
 * @author Nathan.Lighthart
 */
public class TypeFinder {
    /**
     * Unmodifiable mapping of common simple type names.
     */
    private static final Map<String, Class<?>> SIMPLE_TYPES = createSimpleTypes();

    /**
     * Creates map of common simple type names.
     *
     * @return an unmodifiable map
     */
    private static Map<String, Class<?>> createSimpleTypes() {
        Map<String, Class<?>> simpleTypes = new HashMap<>();

        // primitive data types
        simpleTypes.put("byte", Byte.TYPE);
        simpleTypes.put("short", Short.TYPE);
        simpleTypes.put("int", Integer.TYPE);
        simpleTypes.put("long", Long.TYPE);
        simpleTypes.put("float", Float.TYPE);
        simpleTypes.put("double", Double.TYPE);
        simpleTypes.put("boolean", Boolean.TYPE);
        simpleTypes.put("char", Character.TYPE);

        // common alternative names for primitive data types
        simpleTypes.put("integer", Integer.TYPE);
        simpleTypes.put("real", Double.TYPE);
        simpleTypes.put("bool", Boolean.TYPE);
        simpleTypes.put("character", Character.TYPE);

        // wrapper types
        simpleTypes.put("Byte", Byte.TYPE);
        simpleTypes.put("Short", Short.TYPE);
        simpleTypes.put("Integer", Integer.TYPE);
        simpleTypes.put("Long", Long.TYPE);
        simpleTypes.put("Float", Float.TYPE);
        simpleTypes.put("Double", Double.TYPE);
        simpleTypes.put("Boolean", Boolean.TYPE);
        simpleTypes.put("Character", Character.TYPE);

        // common object types
        simpleTypes.put("String", String.class);
        simpleTypes.put("BigInteger", BigInteger.class);
        simpleTypes.put("BigDecimal", BigDecimal.class);
        simpleTypes.put("Date", Date.class);
        simpleTypes.put("Calendar", Calendar.class);
        simpleTypes.put("File", File.class);
        simpleTypes.put("Path", Path.class);
        simpleTypes.put("Instant", Instant.class);
        simpleTypes.put("ZonedDateTime", ZonedDateTime.class);
        simpleTypes.put("OffsetDateTime", OffsetDateTime.class);
        simpleTypes.put("LocalDateTime", LocalDateTime.class);
        simpleTypes.put("LocalDate", LocalDate.class);
        simpleTypes.put("LocalTime", LocalTime.class);

        // common alternatives for object types
        simpleTypes.put("string", String.class);

        return Collections.unmodifiableMap(simpleTypes);
    }

    /**
     * Returns the type for the specified type name. This method is able to
     * determine types of primitive values. This method aware of a few common
     * simple class names and fully qualified class names. This method is also
     * aware of array types. This method is not aware of generic types.
     *
     * @param typeName the type name to lookup
     * @return the type represented by this name or {@code null} if type was not
     * found
     */
    public static Class<?> findType(String typeName) {
        if (typeName == null || "".equals(typeName) || "-".equals(typeName)) {
            return null;
        } else {
            return findType0(typeName);
        }
    }

    /**
     * Returns the type for the specified type name. This method is able to
     * determine types of primitive values. This method aware of a few common
     * simple class names and fully qualified class names. This method is also
     * aware of array types. This method is not aware of generic types.
     *
     * @param typeName the type name to lookup (cannot be null)
     * @return the type represented by this name or {@code null} if type was not
     * found
     */
    private static Class<?> findType0(String typeName) {
        // Count the number of dimensions that the array contains
        int count = 0;
        for (int i = typeName.length() - 1; i > 0; i -= 2) {
            if (typeName.charAt(i - 1) == '[' && typeName.charAt(i) == ']') {
                ++count;
            } else {
                break;
            }
        }

        // type is not an array
        if (count == 0) {
            return findBasicType(typeName);
        }

        // type is array so find the component type
        String componentTypeName = typeName.substring(0, typeName.length() - (2 * count));
        Class<?> componentType = findBasicType(componentTypeName);
        if (componentType == null) {
            // component type could not be found so array type cannot be found
            return null;
        }

        // convert component type to array type with the same number of dimensions
        Class<?> arrayType = Array.newInstance(componentType, new int[count]).getClass();
        return arrayType;
    }

    /**
     * Returns the type for the specified type name. This method is able to
     * determine types of primitive values. This method aware of a few common
     * simple class names and fully qualified class names. This method is not
     * aware of array types. This method is not aware of generic types.
     *
     * @param typeName the type name to lookup (cannot be null)
     * @return the type represented by this name or {@code null} if type was not
     * found
     */
    private static Class<?> findBasicType(String typeName) {
        Class<?> type = findSimpleType(typeName);
        if (type == null) {
            type = lookupClass(typeName);
        }

        return type;
    }

    /**
     * Returns the type for the specified type name. This method is able to
     * determine types of primitive values. This method aware of a few common
     * simple class names. This method is not aware of fully qualified class
     * names. This method is not aware of array types. This method is not aware
     * of generic types.
     *
     * @param typeName the type name to lookup
     * @return the type represented by this name or {@code null} if type was not
     * found
     */
    private static Class<?> findSimpleType(String typeName) {
        return SIMPLE_TYPES.get(typeName);
    }

    /**
     * Returns the type for the specified type name. This method is not able to
     * determine types of primitive values. This method is not aware of simple
     * class names. This method is aware of fully qualified class names. This
     * method is not aware of array types. This method is not aware of generic
     * types.
     *
     * @param typeName the type name to lookup (cannot be null)
     * @return the type represented by this name or {@code null} if type was not
     * found
     */
    private static Class<?> lookupClass(String typeName) {
        try {
            return Class.forName(typeName);
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }
}
