/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import gov.usda.jcf.util.conversion.TypeConversions;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nathan.Lighthart
 */
public class ArrayValueParser {
    public static <T> T parseArray(String s, Class<T> clazz) {
        List<Object> list = parseArrayList(s);
        T array = convertToArray(list, clazz);
        return array;
    }

    private static List<Object> parseArrayList(String s) {
        if (s == null || "".equals(s)) {
            return new ArrayList<>();
        }

        final char[] chars = s.toCharArray();
        if (chars[0] != '[') {
            throw new IllegalArgumentException("Invalid array syntax: missing starting '['");
        }

        List<Object> list = new ArrayList<>();
        parseList(chars, 1, list);

        return list;
    }

    private static <T> T convertToArray(List<?> list, Class<T> clazz) {
        Class<?> componentType = clazz.getComponentType();
        Object array = Array.newInstance(componentType, list.size());
        for (int i = 0; i < list.size(); ++i) {
            Array.set(array, i, convertValue(list.get(i), componentType));
        }
        return clazz.cast(array);
    }

    private static <T> T convertValue(Object value, Class<T> clazz) {
        if (clazz.isArray()) {
            return convertToArray((List<?>) value, clazz);
        } else {
            return TypeConversions.INSTANCE.convert(value, clazz);
        }
    }

    private static int parseList(final char[] chars, int index, List<Object> list) {
        // empty list
        if (chars[index] == ']') {
            return index + 1;
        } else {
            CharLoop:
            while (index < chars.length) {
                index = parseValue(chars, index, list);
                switch (chars[index]) {
                    case ',':
                        ++index;
                        while (Character.isWhitespace(chars[index])) {
                            ++index;
                        }
                        break;
                    case ']':
                        break CharLoop;
                    default:
                        throw new IllegalArgumentException("Error parsing at: " + index);
                }
            }
        }

        return index;
    }

    private static int parseValue(final char[] chars, int index, List<Object> list) {
        if (chars[index] == '[') {
            List<Object> innerList = new ArrayList<>();
            list.add(innerList);
            index = parseList(chars, index + 1, innerList);
            ++index;
        } else {
            int startIndex = index;
            while (index < chars.length
                    && chars[index] != ','
                    && chars[index] != ']') {
                ++index;
            }

            if (index == chars.length) {
                throw new IllegalArgumentException("Invalid array syntax: missing: missing end ]");
            }

            String value = new String(chars, startIndex, index - startIndex);
            list.add(value);
        }

        return index;
    }
}
