package lib;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Array parser is a class that parses an array name and its indices from a
 * given string.
 *
 * @author Nathan Lighthart
 */
public class ArrayNameParser {
	private static final String ARRAY_NAME_MINIMAL_PATTERN_STRING = "([^\\[]+)";
	private static final String ARRAY_NAME_JAVA_PATTERN_STRING = "([a-zA-Z_$][a-zA-Z0-9_$]*)";
	private static final String ARRAY_INDEX_PATTERN_STRING = "\\[([0-9]+)\\]";
	private static final String ARRAY_INDEX_LIST_PATTERN_STRING = "((?:\\[[0-9]+\\])+)";
	/**
	 * Main array pattern has two capturing groups. The first is the array name.
	 * The second is a list of the indices of the array. The array name can
	 * contain any character except for a "[".
	 *
	 * <p>
	 * Example: "test[1][2]" -> ["test", "[1][2]"]
	 * </p>
	 */
	private static final Pattern MINIMAL_ARRAY_PATTERN = Pattern.compile("^" + ARRAY_NAME_MINIMAL_PATTERN_STRING + ARRAY_INDEX_LIST_PATTERN_STRING + "$");
	/**
	 * Main array pattern has two capturing groups. The first is the array name.
	 * The second is a list of the indices of the array. The array name must be
	 * a valid Java identifier.
	 *
	 * <p>
	 * Example: "test[1][2]" -> ["test", "[1][2]"]
	 * </p>
	 */
	private static final Pattern JAVA_ARRAY_PATTERN = Pattern.compile("^" + ARRAY_NAME_JAVA_PATTERN_STRING + ARRAY_INDEX_LIST_PATTERN_STRING + "$");
	/**
	 * Pattern for array index values. This pattern has one capturing group.
	 *
	 * <p>
	 * Example: "[1]" -> "1"
	 * </p>
	 */
	private static final Pattern ARRAY_INDEX_PATTERN = Pattern.compile(ARRAY_INDEX_PATTERN_STRING);

	private final Pattern m_arrayPattern;

	/**
	 * Creates a minimal array parser. The minimal array parser has relaxed
	 * conditions for determining the array name.
	 *
	 * @return the array parser
	 */
	public static ArrayNameParser minimalParser() {
		return new ArrayNameParser(MINIMAL_ARRAY_PATTERN);
	}

	/**
	 * Creates a java array parser. The Java array parser requires the array
	 * name to be a valid Java identifier.
	 *
	 * @return the array parser
	 */
	public static ArrayNameParser javaParser() {
		return new ArrayNameParser(JAVA_ARRAY_PATTERN);
	}

	/**
	 * Creates the array parser with the specified pattern.
	 *
	 * @param arrayPattern the main array pattern
	 */
	private ArrayNameParser(Pattern arrayPattern) {
		m_arrayPattern = arrayPattern;
	}

	/**
	 * Returns the array name and the indices of the array.
	 *
	 * @param src the source string
	 * @return null if the string is not valid or a list with at least two
	 * elements the first is the array name and every value after is an index to
	 * the array
	 */
	public List<String> parse(String src) {
		if (src == null) {
			// cannot parse null string
			return null;
		}

		return parseElements(src);
	}

	/**
	 * Parses the elements from the source string and returns the list of array
	 * elements.
	 *
	 * @param src the source string
	 * @return the array elements list or {@code null}
	 */
	private List<String> parseElements(String src) {
		if (!src.endsWith("]")) {
			return null;
		}
		List<String> elements = new ArrayList<>();

		// find array name and the index list
		String indexList = parseNameAndIndices(src, elements);
		if (indexList == null) {
			return null; // not valid format for array
		}

		// add indiviual indices from the index list
		addIndices(indexList, elements);

		// verify that elements has at least two elements
		if (elements.size() > 1) {
			return elements;
		} else {
			return null;
		}
	}

	/**
	 * Adds the array name to elements and returns the index list.
	 *
	 * @param src the source string to parse
	 * @param elements the list of array elements to add name to
	 * @return the index list
	 */
	private String parseNameAndIndices(String src, List<String> elements) {
		Matcher matcher = m_arrayPattern.matcher(src.trim());
		if (matcher.find()) {
			elements.add(matcher.group(1));
			return matcher.group(2); // return index list
		} else {
			return null;
		}
	}

	/**
	 * Adds the indices in the index list to the elements list
	 *
	 * @param indexList the index list
	 * @param elements the list of array elements to add the indices to
	 */
	private void addIndices(String indexList, List<String> elements) {
		Matcher matcher = ARRAY_INDEX_PATTERN.matcher(indexList);
		while (matcher.find()) {
			elements.add(matcher.group(1));
		}
	}
}
