
package weighting;

import ages.types.HRU;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.contexts.CombinedContext;
import gov.usda.jcf.core.contexts.MapContext;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lib.ArrayNameParser;

/**
 *
 * @author Nathan Lighthart
 */
public class HRUAreaWeigher {
    private final Set<String> weightedAttributes;
    private final ArrayNameParser arrayParser;

    public HRUAreaWeigher(String weightedAttributes) {
        arrayParser = ArrayNameParser.minimalParser();
        this.weightedAttributes = parseAttributes(weightedAttributes);
    }

    private Set<String> parseAttributes(String attributeSet) {
        if (attributeSet == null) {
            return new HashSet<>();
        } else {
            attributeSet = attributeSet.trim();
            if ("".equals(attributeSet) || "-".equals(attributeSet)) {
                return new HashSet<>();
            }
            String[] split = attributeSet.split("\\s*;\\s*");
            Set<String> attributes = new HashSet<>();

            // replace array attributes with array name since the whole array needs to be weighted
            for (String attribute : split) {
                List<String> arrayElements = arrayParser.parse(attribute);
                if (arrayElements == null) {
                    attributes.add(attribute);
                } else {
                    attributes.add(arrayElements.get(0));
                }
            }

            return attributes;
        }
    }

    private static Class<?> getType(Object object) {
        return object == null ? Object.class : object.getClass();
    }

    public Context weigh(Context hruContext) {
        final double hruArea = getHRUArea(hruContext);
        if (hruArea <= 0.0) {
            return hruContext;
        }

        // shadow variables that need to be weighted (this will not modify the underlying hruContext)
        Context weighted = new CombinedContext(new MapContext(), hruContext);
        for (String attribute : weightedAttributes) {
            Object value = hruContext.get(attribute);
            value = getWeightedValue(value, hruArea);
            weighted.put(attribute, value);
        }
        return weighted;
    }

    private double getHRUArea(Context hruContext) {
        if (hruContext.contains("hru")) {
            HRU hru = hruContext.get("hru", HRU.class);
            return hru.area;
        } else {
            return 0.0;
        }
    }

    private Object getWeightedValue(Object value, double area) {
        if (value == null) {
            return null;
        }

        Class<?> type = getType(value);
        if (type.isArray()) {
            int length = Array.getLength(value);
            Object array = Array.newInstance(type.getComponentType(), length);
            for (int i = 0; i < length; ++i) {
                Object val = Array.get(value, i);
                // recursive definition will handle n-dimensional arrays
                Object weighted = getWeightedValue(val, area);
                Array.set(array, i, weighted);
            }
            return array;
        } else {
            double d = TypeConversions.INSTANCE.convert(value, double.class);
            return d / area;
        }
    }
}
