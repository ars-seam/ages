
package weighting;

import gov.usda.jcf.core.Context;

/**
 *
 * @author Nathan Lighthart
 */
public class OutletAggregator {
    private Iterable<Context> reachContexts;
    private Context outletContext;
    private Context outletAggregateContext;

    public void aggregate(Iterable<Context> reachContexts, Context outletContext, Context outletAggregateContext) {
        this.reachContexts = reachContexts;
        this.outletContext = outletContext;
        this.outletAggregateContext = outletAggregateContext;

        aggregateImpl();
    }

    private void aggregateImpl() {
        // aggregate channel storage
        double channelStorage = 0.0;
        double deepsinkN = 0.0;
        double deepsinkW = 0.0;

        for (Context reachContext : reachContexts) {
            channelStorage += reachContext.getDouble("channelStorage");
            deepsinkN += reachContext.getDouble("deepsinkN");
            deepsinkW += reachContext.getDouble("deepsinkW");
        }

        outletAggregateContext.put("channelStorage", channelStorage);
        setUnit("channelStorage", "mm");
        outletAggregateContext.put("deepsinkN", deepsinkN);
        setUnit("deepsinkN", "kg");
        outletAggregateContext.put("deepsinkW", deepsinkW);
        setUnit("deepsinkW", "L");

        double catchmentRD1 = outletContext.getDouble("outRD1");
        double catchmentRD2 = outletContext.getDouble("outRD2");
        double catchmentRG1 = outletContext.getDouble("outRG1");
        double catchmentRG2 = outletContext.getDouble("outRG2");
        double catchmentNRD1 = outletContext.getDouble("outRD1_N");
        double catchmentNRD2 = outletContext.getDouble("outRD2_N");
        double catchmentNRG1 = outletContext.getDouble("outRG1_N");
        double catchmentNRG2 = outletContext.getDouble("outRG2_N");

        outletAggregateContext.put("catchmentRD1", catchmentRD1);
        setUnit("catchmentRD1", "mm");
        outletAggregateContext.put("catchmentRD2", catchmentRD2);
        setUnit("catchmentRD2", "mm");
        outletAggregateContext.put("catchmentRG1", catchmentRG1);
        setUnit("catchmentRG1", "mm");
        outletAggregateContext.put("catchmentRG2", catchmentRG2);
        setUnit("catchmentRG2", "mm");
        outletAggregateContext.put("catchmentNRD1", catchmentNRD1);
        setUnit("catchmentNRD1", "kg");
        outletAggregateContext.put("catchmentNRD2", catchmentNRD2);
        setUnit("catchmentNRD2", "kg");
        outletAggregateContext.put("catchmentNRG1", catchmentNRG1);
        setUnit("catchmentNRG1", "kg");
        outletAggregateContext.put("catchmentNRG2", catchmentNRG2);
        setUnit("catchmentNRG2", "kg");

        double basinArea = outletContext.getDouble("basinArea");

        double catchmentNRD1_w = catchmentNRD1 / basinArea;
        double catchmentNRD2_w = catchmentNRD2 / basinArea;
        double catchmentNRG1_w = catchmentNRG1 / basinArea;
        double catchmentNRG2_w = catchmentNRG2 / basinArea;
        double catchmentRD1_w = catchmentRD1 / basinArea;
        double catchmentRD2_w = catchmentRD2 / basinArea;
        double catchmentRG1_w = catchmentRG1 / basinArea;
        double catchmentRG2_w = catchmentRG2 / basinArea;
        double channelStorage_w = channelStorage / basinArea;

        outletAggregateContext.put("catchmentRD1_w", catchmentRD1_w);
        setUnit("catchmentRD1_w", "mm/m2");
        outletAggregateContext.put("catchmentRD2_w", catchmentRD2_w);
        setUnit("catchmentRD2_w", "mm/m2");
        outletAggregateContext.put("catchmentRG1_w", catchmentRG1_w);
        setUnit("catchmentRG1_w", "mm/m2");
        outletAggregateContext.put("catchmentRG2_w", catchmentRG2_w);
        setUnit("catchmentRG2_w", "mm/m2");
        outletAggregateContext.put("catchmentNRD1_w", catchmentNRD1_w);
        setUnit("catchmentNRD1_w", "kg/m2");
        outletAggregateContext.put("catchmentNRD2_w", catchmentNRD2_w);
        setUnit("catchmentNRD2_w", "kg/m2");
        outletAggregateContext.put("catchmentNRG1_w", catchmentNRG1_w);
        setUnit("catchmentNRG1_w", "kg/m2");
        outletAggregateContext.put("catchmentNRG2_w", catchmentNRG2_w);
        setUnit("catchmentNRG2_w", "kg/m2");
        outletAggregateContext.put("channelStorage_w", channelStorage_w);
        setUnit("channelStorage_w", "mm/m2");

        // calculate catchment runoff, NO3/NO3-N, and sediment output variables
        double simRunoff = outletContext.getDouble("simRunoff");
        double catchmentSimRunoff = simRunoff / (double) (1000 * 86400);
        double simRunoff_N = outletContext.getDouble("simRunoff_N");
        double catchmentSimRunoffN = simRunoff_N;

        double catchmentSimRunoff_NO3 = ((simRunoff_N * 1000000) / simRunoff);
        double catchmentSimRunoff_NO3_N = ((simRunoff_N * 1000000) / simRunoff) * 0.2259;

        double outsed = outletContext.getDouble("outsed");
        double catchmentSed = outsed;
        double catchmentSed_mg_l = (catchmentSed * 1000 * 1000) / simRunoff;
        double catchmentSed_Load_kg_ha = catchmentSed / (basinArea / 10000);

        double catchmentNO3_N_Load_kg_ha = ((simRunoff_N) / (basinArea / 10000)) * 0.2259;

        outletAggregateContext.put("catchmentSimRunoff", catchmentSimRunoff);
        setUnit("catchmentSimRunoff", "m3/s");
        outletAggregateContext.put("catchmentSimRunoffN", catchmentSimRunoffN);
        setUnit("catchmentSimRunoffN", "kg");
        outletAggregateContext.put("catchmentSimRunoff_NO3", catchmentSimRunoff_NO3);
        setUnit("catchmentSimRunoff_NO3", "mg/L");
        outletAggregateContext.put("catchmentSimRunoff_NO3_N", catchmentSimRunoff_NO3_N);
        setUnit("catchmentSimRunoff_NO3_N", "mg/L");
        outletAggregateContext.put("catchmentSed", catchmentSed);
        setUnit("catchmentSed", "kg");
        outletAggregateContext.put("catchmentSed_mg_l", catchmentSed_mg_l);
        setUnit("catchmentSed_mg_l", "mg/L");
        outletAggregateContext.put("catchmentSed_Load_kg_ha", catchmentSed_Load_kg_ha);
        setUnit("catchmentSed_Load_kg_ha", "kg/ha");
        outletAggregateContext.put("catchmentNO3_N_Load_kg_ha", catchmentNO3_N_Load_kg_ha);
        setUnit("catchmentNO3_N_Load_kg_ha", "kg/ha");
    }

    private void setUnit(String aggrProp, String units) {
        outletAggregateContext.putProperty(aggrProp, "units", units);
    }
}
