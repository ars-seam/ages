
package weighting;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.util.conversion.TypeConversionException;
import gov.usda.jcf.util.conversion.TypeConversions;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import lib.ArrayNameParser;

/**
 *
 * @author Nathan Lighthart
 */
public class CatchmentAggregator {
    private final Set<String> attributes;
    private final Set<String> weightedAttributes;
    private final ArrayNameParser arrayParser;

    public CatchmentAggregator(String attributes, String weightedAttributes) {
        arrayParser = ArrayNameParser.minimalParser();
        this.attributes = parseAttributes(attributes);
        this.weightedAttributes = new HashSet<>(parseAttributes(weightedAttributes));

        removeNonAggegateAttributes();
    }

    private Set<String> parseAttributes(String attributeSet) {
        if (attributeSet == null) {
            return new HashSet<>();
        } else {
            attributeSet = attributeSet.trim();
            if ("".equals(attributeSet) || "-".equals(attributeSet)) {
                return new HashSet<>();
            }
            String[] split = attributeSet.split("\\s*;\\s*");
            Set<String> attributes = new HashSet<>();

            // replace array attributes with array name since the whole array needs to be weighted
            for (String attribute : split) {
                List<String> arrayElements = arrayParser.parse(attribute);
                if (arrayElements == null) {
                    attributes.add(attribute);
                } else {
                    attributes.add(arrayElements.get(0));
                }
            }

            return attributes;
        }
    }

    private static Class<?> getType(Object object) {
        return object == null ? Object.class : object.getClass();
    }

    public void aggregate(Iterable<Context> contexts, Context aggregateContext) {
        attributes.parallelStream().forEach((attribute) -> {
            Object value = getAverages(attribute, contexts);
            aggregateContext.put(attribute, value);
            addProperties(attribute, contexts, aggregateContext);
        });
    }

    private void removeNonAggegateAttributes() {
        attributes.remove("time");
        weightedAttributes.remove("time");
    }

    private Object getAverages(String attribute, Iterable<Context> contexts) {
        return getAverages(computeAverages(attribute, contexts));
    }

    private List<AverageAggregator> computeAverages(String attribute, Iterable<Context> contexts) {
        final boolean weighted = weightedAttributes.contains(attribute);
        List<AverageAggregator> averages = new ArrayList<>();
        averages.add(createAggregator(weighted));

        if (weighted) {
            for (Context context : contexts) {
                Object value = context.get(attribute);

                double weight = getWeight(context);
                value = getWeightedValue(value, weight);

                AddToAverages(averages, value, weighted);
            }
        } else {
            for (Context context : contexts) {
                Object value = context.get(attribute);
                AddToAverages(averages, value, weighted);
            }
        }

        return averages;
    }

    private Object getAverages(List<AverageAggregator> averages) {
        if (averages.size() == 1) {
            return averages.get(0).getAverage();
        } else {
            double[] average = new double[averages.size()];
            int index = 0;
            for (AverageAggregator runningAverage : averages) {
                average[index++] = runningAverage.getAverage();
            }
            return average;
        }
    }

    private AverageAggregator createAggregator(boolean weighted) {
        if (weighted) {
            return new WeightedAverage();
        } else {
            return new RunningAverage();
        }
    }

    private double getWeight(Context context) {
        if (context.contains("hru")) {
            return getWeightHRU(context);
        }
        return 1.0; // make weight not change the value
    }

    private double getWeightHRU(Context hruContext) {
        if (hruContext.contains("areaWeight")) {
            return hruContext.get("areaWeight", double.class);
        } else {
            CalcAreaWeightAdapter calcAreaWeight = new CalcAreaWeightAdapter();
            calcAreaWeight.execute(hruContext);
            return hruContext.get("areaWeight", double.class);
        }
    }

    private Object getWeightedValue(Object value, double weight) {
        if (value == null) {
            return null;
        }

        Class<?> type = getType(value);
        if (type.isArray()) {
            int length = Array.getLength(value);
            Object array = Array.newInstance(type.getComponentType(), length);
            for (int i = 0; i < length; ++i) {
                Object val = Array.get(value, i);
                // recursive definition will handle n-dimensional arrays
                Object weighted = getWeightedValue(val, weight);
                Array.set(array, i, weighted);
            }
            return array;
        } else {
            double d = TypeConversions.INSTANCE.convert(value, double.class);
            return d * weight;
        }
    }

    private boolean AddToAverages(List<AverageAggregator> averages, Object value, boolean weighted) {
        if (value == null) {
            return false;
        }
        if (value.getClass().isArray()) {
            List<Object> flattened = flatten(value);
            if (flattened.isEmpty()) {
                return false;
            }

            int diff = flattened.size() - averages.size();
            if (weighted) {

            } else {
                for (int i = 0; i < diff; ++i) {
                    averages.add(createAggregator(weighted));
                }
            }
            int index = 0;
            for (Object obj : flattened) {
                AddToAverages(averages, obj, index++);
            }
        } else {
            AddToAverages(averages, value, 0);
        }
        return true;
    }

    private void AddToAverages(List<AverageAggregator> averages, Object value, int index) {
        double d;
        try {
            d = TypeConversions.INSTANCE.convert(value, double.class);
        } catch (TypeConversionException ex) {
            return;
        }
        averages.get(index).add(d);
    }

    private List<Object> flatten(Object object) {
        List<Object> flattened = new ArrayList<>();
        Queue<Object> queue = new LinkedList<>();
        queue.add(object);
        while (!queue.isEmpty()) {
            Object value = queue.poll();
            if (value == null) {
                continue;
            } else if (value.getClass().isArray()) {
                int length = Array.getLength(value);
                for (int i = 0; i < length; ++i) {
                    queue.add(Array.get(value, i));
                }
            } else {
                flattened.add(value);
            }
        }
        return flattened;
    }

    private void addProperties(String attribute, Iterable<Context> contexts, Context aggregateContext) {
        Iterator<Context> iter = contexts.iterator();
        if (iter.hasNext()) {
            Context firstContext = iter.next();
            Map<String, String> properties = firstContext.getProperties(attribute);
            try {
                aggregateContext.putProperties(attribute, properties);
            } catch (Exception ex) {
            }
        }
    }

    private interface AverageAggregator {
        void add(double d);

        double getAverage();
    }

    private static class RunningAverage implements AverageAggregator {
        private double sum;
        private int count;

        public RunningAverage() {
            sum = 0.0;
            count = 0;
        }

        @Override
        public void add(double d) {
            sum += d;
            ++count;
        }

        @Override
        public double getAverage() {
            return (count < 1) ? Double.NaN : sum / count;
        }
    }

    private static class WeightedAverage implements AverageAggregator {
        private double weightedAverage;

        public WeightedAverage() {
            weightedAverage = 0.0;
        }

        @Override
        public void add(double d) {
            weightedAverage += d;
        }

        @Override
        public double getAverage() {
            return weightedAverage;
        }
    }
}
