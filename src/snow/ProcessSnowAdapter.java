/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package snow;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add ProcessSnow module definition here")
@Author(name = "Olaf David, Peter Krause", contact = "jim.ascough@ars.usda.gov")
@Keywords("Snow")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/snow/ProcessSnow.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/snow/ProcessSnow.xml")
public class ProcessSnowAdapter extends AnnotatedAdapter {
    @Description("Current hru object")
    @Input public HRU hru;

    @Description("state var slope-aspect-correction-factor")
    @Input public double actSlAsCf;

    @Description("Minimum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmin;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("maximum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmax;

    @Description("melting temperature")
    @Role(PARAMETER)
    @Input public double baseTemp;

    @Description("temperature factor for snow melt calculation")
    @Role(PARAMETER)
    @Input public double t_factor;

    @Description("rain factor for snow melt calculation")
    @Role(PARAMETER)
    @Input public double r_factor;

    @Description("soil heat factor for snow melt calculation")
    @Role(PARAMETER)
    @Input public double g_factor;

    @Description("snowpack density beyond free water is released [dec%]")
    @Units("dec%")
    @Role(PARAMETER)
    @Input public double snowCritDens;

    @Description("cold content factor")
    @Role(PARAMETER)
    @Input public double ccf_factor;

    @Description("new snow density equation")
    @Role(PARAMETER)
    @Input public double snowFactorA;

    @Description("new snow density equation")
    @Role(PARAMETER)
    @Input public double snowFactorB;

    @Description("new snow density equation")
    @Role(PARAMETER)
    @Input public double snowFactorC;

    @Description("new snow density equation")
    @Role(PARAMETER)
    @Input public double snowDensConst;

    @Description("state variable net rain")
    @Input @Output public double netRain;

    @Description("state variable net snow")
    @Input @Output public double netSnow;

    @Description("total snow water equivalent")
    @Units("mm")
    @Optional
    @Input @Output public double snowTotSWE;

    @Description("dry snow water equivalent")
    @Optional
    @Input @Output public double drySWE;

    @Description("total snow density")
    @Optional
    @Input @Output public double totDens;

    @Description("dry snow density")
    @Optional
    @Input @Output public double dryDens;

    @Description("snow depth")
    @Optional
    @Input @Output public double snowDepth;

    @Description("snow age")
    @Optional
    @Input @Output public double snowAge;

    @Description("snow cold content")
    @Optional
    @Input @Output public double snowColdContent;

    @Description("daily snow melt")
    @Output public double snowMelt;

    @Description("daily netSnow")
    @Output public double netSnowOut;

    @Description("daily netRain")
    @Output public double netRainOut;

    @Override
    protected void run(Context context) {
        ProcessSnow component = new ProcessSnow();

        component.area = hru.area;
        component.actSlAsCf = actSlAsCf;
        component.tmin = tmin;
        component.tmean = tmean;
        component.tmax = tmax;
        component.baseTemp = baseTemp;
        component.t_factor = t_factor;
        component.r_factor = r_factor;
        component.g_factor = g_factor;
        component.snowCritDens = snowCritDens;
        component.ccf_factor = ccf_factor;
        component.snowFactorA = snowFactorA;
        component.snowFactorB = snowFactorB;
        component.snowFactorC = snowFactorC;
        component.snowDensConst = snowDensConst;
        component.netRain = netRain;
        component.netSnow = netSnow;
        component.snowTotSWE = snowTotSWE;
        component.drySWE = drySWE;
        component.totDens = totDens;
        component.dryDens = dryDens;
        component.snowDepth = snowDepth;
        component.snowAge = snowAge;
        component.snowColdContent = snowColdContent;

        component.execute();

        netRain = component.netRain;
        netSnow = component.netSnow;
        snowTotSWE = component.snowTotSWE;
        drySWE = component.drySWE;
        totDens = component.totDens;
        dryDens = component.dryDens;
        snowDepth = component.snowDepth;
        snowAge = component.snowAge;
        snowColdContent = component.snowColdContent;
        snowMelt = component.snowMelt;
        netSnowOut = component.netSnowOut;
        netRainOut = component.netRainOut;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
