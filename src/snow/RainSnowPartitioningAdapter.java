/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package snow;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add RainSnowPartitioning module definition here")
@Author(name = "Olaf David, Peter Krause", contact = "jim.ascough@ars.usda.gov")
@Keywords("Snow")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/snow/RainSnowPartitioning.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/snow/RainSnowPartitioning.xml")
public class RainSnowPartitioningAdapter extends AnnotatedAdapter {
    @Description("HRU")
    @Input public HRU hru;

    @Description("snow_trs")
    @Role(PARAMETER)
    @Input public double snow_trs;

    @Description("snow_trans")
    @Role(PARAMETER)
    @Input public double snow_trans;

    @Description("Minimum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmin;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("Precipitation")
    @Units("mm")
    @Input public double precip;

    @Description("State variable rain")
    @Output public double rain;

    @Description("state variable snow")
    @Output public double snow;

    @Override
    protected void run(Context context) {
        RainSnowPartitioning component = new RainSnowPartitioning();

        component.area = hru.area;
        component.snow_trs = snow_trs;
        component.snow_trans = snow_trans;
        component.tmin = tmin;
        component.tmean = tmean;
        component.precip = precip;

        component.execute();

        rain = component.rain;
        snow = component.snow;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
