/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package interception;

public class ProcessInterception {
    public double snow_trs;
    public double snow_trans;
    public double a_rain;
    public double a_snow;
    public double area;
    public double tmean;
    public double rain;
    public double snow;
    public double potET;
    public double LAI;
    public double intercStorage;
    public double actET;
    public double irrigation_amount;
    public double netRain;
    public double netSnow;
    public double throughfall;
    public double interception;

    public void execute() {
        throughfall = 0;
        interception = 0;

        if (snow > 0) {
            irrigation_amount = 0;
        }
        double sum_precip = rain + snow + irrigation_amount;
        double deltaETP = potET - actET;

        double relRain, relSnow;
        if (sum_precip > 0) {
            relRain = (rain + irrigation_amount) / sum_precip;
            relSnow = snow / sum_precip;
        } else {
            relRain = 1.0; // throughfall without precipitation is considered to be liquid
            relSnow = 0;
        }

        // determine if precipitation falls as rain or snow
        double alpha = tmean < (snow_trs - snow_trans) ? a_snow : a_rain;

        // determinine maximum interception capacity of actual day
        double maxIntcCap = LAI * alpha * area;

        /* if interception storage has changed from snow to rain then throughfall
         * occurs because interception storage for the antecedent day might be larger
         * than the maximum storage capacity of the actual time step
         */
        if (intercStorage > maxIntcCap) {
            throughfall = intercStorage - maxIntcCap;
            intercStorage = maxIntcCap;
        }

        // determinine the potential storage volume for daily interception
        double deltaIntc = maxIntcCap - intercStorage;

        // reduce rain and filling of interception storage
        if (deltaIntc > 0) {
            if (sum_precip > deltaIntc) {
                intercStorage = maxIntcCap;
                sum_precip -= deltaIntc;
                throughfall += sum_precip;
                interception = deltaIntc;
                deltaIntc = 0;
            } else {
                intercStorage = (intercStorage + sum_precip);
                interception = sum_precip;
                sum_precip = 0;
            }
        } else {
            throughfall += sum_precip;
        }

        /* depletion of interception storage (beside the throughfall from above,
         * interception storage can only be depleted by evapotranspiration)
         */
        if (deltaETP > 0) {
            if (intercStorage > deltaETP) {
                intercStorage -= deltaETP;
                actET += deltaETP;
                deltaETP = 0;

            } else {
                deltaETP -= intercStorage;
                actET += (potET - deltaETP);
                intercStorage = 0;
            }
        } else {
            actET = deltaETP;
        }
        netRain = throughfall * relRain;
        netSnow = throughfall * relSnow;
    }
}
