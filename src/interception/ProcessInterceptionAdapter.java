/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package interception;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add ProcessInterception module definition here")
@Author(name = "Olaf David, Peter Krause, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Interception, Hydrology")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/interception/ProcessInterception.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/interception/ProcessInterception.xml")
public class ProcessInterceptionAdapter extends AnnotatedAdapter {
    @Description("snow_trs")
    @Role(PARAMETER)
    @Input public double snow_trs;

    @Description("snow_trans")
    @Role(PARAMETER)
    @Input public double snow_trans;

    @Description("maximum storage capacity per LAI for rain")
    @Units("mm")
    @Role(PARAMETER)
    @Input public double a_rain;

    @Description("maximum storage capacity per LAI for snow [mm]")
    @Units("mm")
    @Role(PARAMETER)
    @Input public double a_snow;

    @Description("HRU")
    @Input public HRU hru;

    @Description("Mean Temperature")
    @Units("C")
    @Input public double tmean;

    @Description("State variable rain")
    @Input public double rain;

    @Description("state variable snow")
    @Input public double snow;

    @Description("HRU potential Evapotranspiration")
    @Units("mm")
    @Input public double potET;

    @Description("LAI")
    @Input public double LAI;

    @Description("HRU actual Evapotranspiration")
    @Units("mm")
    @Input @Output public double actET;

    @Description("Current irrigation amount in liter for the whole hru")
    @Input @Output public double irrigation_amount;

    @Description("state variable interception storage")
    @Optional
    @Input @Output public double intercStorage;

    @Description("state variable net rain")
    @Output public double netRain;

    @Description("state variable net snow")
    @Output public double netSnow;

    @Description("state variable throughfall")
    @Output public double throughfall;

    @Description("state variable dy-interception")
    @Output public double interception;

    @Override
    protected void run(Context context) {
        ProcessInterception component = new ProcessInterception();

        component.snow_trs = snow_trs;
        component.snow_trans = snow_trans;
        component.a_rain = a_rain;
        component.a_snow = a_snow;
        component.area = hru.area;
        component.tmean = tmean;
        component.rain = rain;
        component.snow = snow;
        component.potET = potET;
        component.LAI = LAI;
        component.actET = actET;
        component.irrigation_amount = irrigation_amount;
        component.intercStorage = intercStorage;

        component.execute();

        actET = component.actET;
        irrigation_amount = component.irrigation_amount;
        intercStorage = component.intercStorage;
        netRain = component.netRain;
        netSnow = component.netSnow;
        throughfall = component.throughfall;
        interception = component.interception;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
