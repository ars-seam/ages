/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package infiltration;

import annotations.VersionInfo;
import annotations.Bibliography;
import annotations.SourceInfo;
import annotations.License;
import annotations.Status;
import annotations.Author;
import annotations.Documentation;
import annotations.Keywords;
import annotations.Range;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Output;

@Description("Add CurveNumber module definition here")
@Author(name = "Nathan Lighthart, Jim Ascough II, Holm Kipka", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/infiltration/CurveNumber.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/infiltration/CurveNumber.xml")
public class CurveNumber {
    // Input
    @Description("SCS runoff curve number for moisture condition II")
    @Input public double cnn;

    @Description("amount of water held in soil profile at field capacity")
    @Units("mm")
    @Input public double sol_sumfc;

    @Description("amount of water held in soil profile at saturation")
    @Units("mm")
    @Input public double sol_sumul;

    @Description("amount of water stored in soil profile on any given day")
    @Units("mm")
    @Input public double sol_sw;

    @Description("daily average temperature of second soil layer")
    @Units("C")
    @Input public double sol_tmp;

    @Description("CN method flag (0,1,2)")
    @Input public int icn;

    @Description("CN retention parameter adjustment factor for flat slopes")
    @Range(min = 0.5, max = 3.0)
    @Input public double r2adj;

    @Description("parameter for frozen soil adjustment on infiltration/runoff")
    @Input public double cn_froz;

    @Description("fraction of HRU area that is classified as directly connected impervious")
    @Units("0-1")
    @Input public double fcimp;

    @Description("urban simulation code (0:no urban; 1:urban-USGS regression; 2:urban-build up/wash off algorithm)")
    @Input public int iurban; //

    @Description("HRU precipitation for the day")
    @Units("mm")
    @Input public double precipday;

    // Intermediary
    private double cn1; // SCS runoff curve number for moisture condition I
    private double cn2; // SCS runoff curve number for moisture condition II
    private double cn3; // SCS runoff curve number for moisture condition III
    private double sci; // retention coefficient for cn method based on plant ET
    private double smx; // retention coefficient for cn method based on soil moisture
    private double[] wrt = new double[2]; // 1st and 2nd shape parameter for calculation of water retention
    private double smxold; // retention coefficient for cn method based on soil moisture
    private double cnday; // curve number for current day, HRU, and at current soil moisture
    private double sol_cnsw; // amount of water stored in soil profile on any given day (mm)

    // Output
    @Description("HRU surface runoff for the day")
    @Units("mm")
    @Output public double surfq;

    private boolean initialized = false;  // flag to determine if curve number is initialized

    public void run() {
        if (!initialized) {
            curno();
            initialized = true;
        }

        dailycn();
        surq_daycn();
    }

    private void ascrv(double x1, double x2, double x3, double x4) {
        /*
	    * This subroutine computes shape parameters x5 and x6 for the S curve
	    * equation x = y/(y + exp(x5 + x6*y)) given 2 (x,y) points along the curve.
	    * x5 is determined by solving the equation with x and y values measured
	    * around the midpoint of the curve (approx. 50% of the maximum value for x)
	    * and x6 is determined by solving the equation with x and y values measured
	    * close to one of the endpoints of the curve (100% of the maximum value for
	    * x).
	    *
	    * @param x1 value for x in the above equation for first datapoint, x1
	    * should be close to 0.5 (the midpoint of the curve)
	    * @param x2 value for x in the above equation for second datapoint, x2
	    * should be close to 0.0 or 1.0
	    * @param x3 value for y in the above equation corresponding to x1
	    * @param x4 value for y in the above equation corresponding to x2
         */

        // temporary variable, used to hold calculated value needed in later equations
        double xx = Math.log(x3 / x1 - x3);
        // 2nd shape parameter for S curve equation characterizing the regions close to the endpoints of the curve
        double x6 = (xx - Math.log(x4 / x2 - x4)) / (x4 - x3);
        // 1st shape parameter for S curve equation characterizing the midpoint of the curve
        double x5 = xx + (x3 * x6);

        wrt[0] = x5;
        wrt[1] = x6;
    }

    private void curno() {
        /*
	     * This subroutine determines the curve numbers for moisture conditions
	     * I and III and calculates coefficents and shape parameters for the
	     * water retention curve. The coefficents and shape parameters are
	     * calculated as either a function of soil water (default) or a function of
         * accumulated PET, precipitation, and surface runoff.
         */

        double c2; // variable used to hold calculated value
        double s3; // retention parameter for CN3
        double rto3; // fraction difference between CN3 and CN1 retention parameters
        double rtos; // fraction difference between CN=99 and CN1 retention parameters

        cn2 = cnn;

        if (cn1 > 1.0e-6) {
            smxold = 254.0 * (100.0 / cn1 - 1.0);
        }

        // calculate moisture condition I and III curve numbers
        c2 = 100.0 - cnn;
        cn1 = cnn - 20.0 * c2 / (c2 + Math.exp(2.533 - 0.0636 * c2));
        cn1 = Math.max(cn1, 0.4 * cnn);
        cn3 = cnn * Math.exp(0.006729 * c2);

        // calculate maximum retention parameter value
        smx = 254.0 * (100.0 / cn1 - 1.0);

        // calculate retention paramter vallue for cn3
        s3 = 254.0 * (100.0 / cn3 - 1.0);

        // calculate fraction difference in retention parameters
        rto3 = 1.0 - s3 / smx;
        rtos = 1.0 - 2.54 / smx;

        // calculate shape parameters
        ascrv(rto3, rtos, sol_sumfc, sol_sumul);

        if (!initialized) {
            sci = 0.9 * smx;
        } else {
            // plant ET
            sci = 1.0 - ((smxold - sci) / smxold) * smx;
        }
    }

    private void dailycn() {
        // calculates curve number for the day in the HRU

        double xx; // variable used to store intermediate calculation result
        double r2 = 0.0; // retention parameter in CN equation

        xx = wrt[0] - wrt[1] * sol_sw;

        if (xx < -20.0) {
            xx = -20.0;
        }
        if (xx > 20.0) {
            xx = 20.0;
        }

        if (icn <= 0) {
            // traditional CN method (function of soil water)
            if (sol_sw + Math.exp(xx) > 0.001) {
                r2 = r2adj * smx * (1.0 - sol_sw / (sol_sw + Math.exp(xx)));
            }
        } else {
            // alternative CN method (function of plant ET)
            r2 = Math.max(3.0, sci);
        }

        if (sol_tmp <= 0.0) {
            r2 = smx * (1.0 - Math.exp(-cn_froz * r2));
        }
        r2 = Math.max(3.0, r2);
        cnday = 25400.0 / (r2 + 254.0);
        sol_cnsw = sol_sw;
    }

    private void surq_daycn() {
        // Predicts daily runoff using a modified SCS curve number approach

        double r2; // retention parameter in CN equation
        double bb; // variable used to store intermediate calculation result
        double pb; // variable used to store intermediate calculation result
        double cnimp; // curve number for impervious areas
        double surfqimp; // surface runoff from impervious areas (mm)

        r2 = 25400.0 / cnday - 254.0;
        bb = 0.2 * r2;
        pb = precipday - bb;

        if (pb > 0.0) {
            surfq = pb * pb / (precipday + 0.8 * r2);
        }

        if (iurban > 0) {
            surfqimp = 0.0;
            cnimp = 98.0;
            r2 = 25400.0 / cnimp - 254.0;
            bb = 0.2 * r2;
            pb = precipday - bb;

            if (pb > 0.0) {
                surfqimp = pb * pb / (precipday + 0.8 * r2);
            }

            surfq = surfq * (1.0 - fcimp) + surfqimp * fcimp;
        }
    }
}
