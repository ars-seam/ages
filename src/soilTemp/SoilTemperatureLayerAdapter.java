/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilTemp;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Range;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Description("Add SoilTemperatureLayer module definition here")
@Author(name = "Olaf David, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("Utilities")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilTemp/SoilTemperatureLayer.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilTemp/SoilTemperatureLayer.xml")
public class SoilTemperatureLayerAdapter extends AnnotatedAdapter {
    @Description("temperature lag factor for soil")
    @Role(PARAMETER)
    @Range(min = 0.0, max = 1.0)
    @Input public double temp_lag;

    @Description("switch for mulch drilling scenario")
    @Role(PARAMETER)
    @Input public double sceno;

    @Description("HRU")
    @Units("m^2")
    @Input public HRU hru;

    @Description("maximum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmax;

    @Description("Minimum temperature if available, else mean temp")
    @Units("C")
    @Input public double tmin;

    @Description("mean temperature of the simulation period")
    @Units("C")
    @Input public double tmeanavg;

    @Description("soil")
    @Input public SoilType soil;

    @Description("Soil bulk density")
    @Units("g/cm3")
    @Optional
    @Input public double[] bulkDensity_h;

    @Description("actual LPS in portion of sto_LPS soil water content")
    @Input public double[] satLPS_h;

    @Description("actual MPS in portion of sto_MPS soil water content")
    @Input public double[] satMPS_h;

    @Description("maximum MPS  in l soil water content")
    @Input public double[] maxMPS_h;

    @Description("maximum LPS  in l soil water content")
    @Input public double[] maxLPS_h;

    @Description("total snow water equivalent")
    @Units("mm")
    @Input public double snowTotSWE;

    @Description("Daily solar radiation")
    @Units("MJ/m2/day")
    @Input public double solRad;

    @Description("Biomass above ground on the day of harvest")
    @Units("kg/ha")
    @Input public double BioagAct;

    @Description("Residue in Layer")
    @Units("kgN/ha")
    @Input public double[] residue_pool;

    @Description("soil temperature in different layerdepths")
    @Units("C")
    @Input @Output public double[] soil_Temp_Layer;

    @Description("Output soil surface temperature")
    @Units("C")
    @Output public double surfacetemp;

    @Description("Output soil average temperature")
    @Units("C")
    @Output public double soil_Tempaverage;

    private Map<HRU, SoilTemperatureLayer> componentMap = new ConcurrentHashMap<>();

    @Override
    protected void run(Context context) {
        SoilTemperatureLayer component = componentMap.get(hru);
        if (component == null) {
            component = new SoilTemperatureLayer();
            componentMap.put(hru, component);
        }

        component.temp_lag = temp_lag;
        component.sceno = sceno;
        component.area = hru.area;
        component.tmax = tmax;
        component.tmin = tmin;
        component.tmeanavg = tmeanavg;
        component.depth_h = soil.depth_h;
        component.horizons = soil.horizons;
        component.bulkDensity_h = (bulkDensity_h == null ? soil.computedBulkDensity_h : bulkDensity_h);
        component.satLPS_h = satLPS_h;
        component.satMPS_h = satMPS_h;
        component.maxMPS_h = maxMPS_h;
        component.maxLPS_h = maxLPS_h;
        component.snowTotSWE = snowTotSWE;
        component.solRad = solRad;
        component.BioagAct = BioagAct;
        component.residue_pool = residue_pool;
        component.soil_Temp_Layer = soil_Temp_Layer;

        component.execute();

        soil_Temp_Layer = component.soil_Temp_Layer;
        surfacetemp = component.surfacetemp;
        soil_Tempaverage = component.soil_Tempaverage;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
