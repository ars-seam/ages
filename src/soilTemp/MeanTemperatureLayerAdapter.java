/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilTemp;

import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add MeanTemperatureLayer module definition here")
@Author(name = "Olaf David, Manfred Fink", contact = "jim.ascough@ars.usda.gov")
@Keywords("Utilities")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soilTemp/MeanTemperatureLayer.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soilTemp/MeanTemperatureLayer.xml")
public class MeanTemperatureLayerAdapter extends AnnotatedAdapter {
    @Description("Daily mean temperature")
    @Units("C")
    @Input public double tmean;

    @Description("soil")
    @Input public SoilType soil;

    @Description("mean temperature of the simulation period")
    @Units("C")
    @Optional
    @Input @Output public double tmeanavg;

    @Description("Average yearly temperature sum of the simulation period")
    @Units("C")
    @Optional
    @Input @Output public double tmeansum;

    @Description("number of current days")
    @Optional
    @Input @Output public int i;

    @Description("Output soil surface temperature")
    @Units("C")
    @Output public double surfacetemp;

    @Description("Soil temperature in layer depth")
    @Units("C")
    @Output public double[] soil_Temp_Layer;

    @Override
    protected void run(Context context) {
        MeanTemperatureLayer component = new MeanTemperatureLayer();

        component.tmean = tmean;
        component.horizons = soil.horizons;
        component.tmeanavg = tmeanavg;
        component.tmeansum = tmeansum;
        component.i = i;

        component.execute();

        tmeanavg = component.tmeanavg;
        tmeansum = component.tmeansum;
        i = component.i;
        surfacetemp = component.surfacetemp;
        soil_Temp_Layer = component.soil_Temp_Layer;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
