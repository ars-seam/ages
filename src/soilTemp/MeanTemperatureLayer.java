/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soilTemp;

public class MeanTemperatureLayer {
    public double tmean;
    public int horizons;
    public double tmeanavg;
    public double tmeansum;
    public int i;
    public double surfacetemp;
    public double[] soil_Temp_Layer;

    public void execute() {
        if (soil_Temp_Layer == null) {
            soil_Temp_Layer = new double[horizons];
        }
        i++;
        tmeanavg = ((tmeanavg * (i - 1)) + tmean) / i;
        tmeansum = ((tmeansum * ((i - 1) / 365.25)) + tmean) / (i / 365.25);

        for (int j = 0; j < soil_Temp_Layer.length; j++) {
            soil_Temp_Layer[j] = tmeanavg;
        }
        surfacetemp = tmeanavg;
    }
}
