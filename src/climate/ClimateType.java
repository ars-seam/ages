
package climate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nathan Lighthart
 */
public enum ClimateType {
    MINIMUM_TEMPERATURE("tmin", "Tmin", -273.0),
    MAXIMUM_TEMPERATURE("tmax", "Tmax", -273.0),
    MEAN_TEMPERATURE("tmean", "Tmean", -273.0),
    HUMIDITY("hum", "Hum", 0.0),
    PRECIPITATION("precip", "Precip", 0.0),
    SOLAR("sol", "Sol", 0.0),
    WIND("wind", "Wind", 0.0),
    CO2("co2", "CO2", 0.0);

    private static final Map<String, ClimateType> climateTypeMap;

    static {
        Map<String, ClimateType> map = new HashMap<>();

        ClimateType[] climateTypes = ClimateType.values();
        for (ClimateType climateType : climateTypes) {
            map.put(climateType.getName().toLowerCase(), climateType);
        }

        climateTypeMap = Collections.unmodifiableMap(map);
    }

    public static ClimateType of(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Invalid climate type: null");
        }
        name = name.trim().toLowerCase();
        ClimateType climateType = climateTypeMap.get(name);
        if (climateType == null) {
            throw new IllegalArgumentException("Invalid climate type: " + name);
        }
        return climateType;
    }

    private final String name;
    private final String suffix;
    private final double fixedMinimum;

    private ClimateType(String name, String suffix, double fixedMinimum) {
        this.name = name;
        this.suffix = suffix;
        this.fixedMinimum = fixedMinimum;
    }

    public String getName() {
        return name;
    }

    public String getSuffix() {
        return suffix;
    }

    public double getFixedMinimum() {
        return fixedMinimum;
    }
}
