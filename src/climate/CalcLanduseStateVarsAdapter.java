/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package climate;

import ages.types.HRU;
import ages.types.Landuse;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

@Description("Add CalcLanduseStateVars module definition here")
@Author(name = "Olaf David, Peter Krause, Sven Kralisch", contact = "jim.ascough@ars.usda.gov")
@Keywords("I/O")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/climate/CalcLanduseStateVars.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/climate/CalcLanduseStateVars.xml")
public class CalcLanduseStateVarsAdapter extends AnnotatedAdapter {
    @Description("HRU")
    @Input public HRU hru;

    @Description("Landuse")
    @Input public Landuse landuse;

    @Description("Leaf Area Index Array")
    @Output public double[] LAIArray;

    @Description("Effective Height Array")
    @Output public double[] effHArray;

    @Override
    protected void run(Context context) {
        CalcLanduseStateVars component = new CalcLanduseStateVars();

        component.elevation = hru.elevation;
        component.LAI = landuse.LAI;
        component.effHeight = landuse.effHeight;

        component.execute();

        LAIArray = component.LAIArray;
        effHArray = component.effHArray;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
