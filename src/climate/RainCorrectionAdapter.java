/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package climate;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.time.LocalDate;

@Description("Add RainCorrection module definition here")
@Author(name = "Olaf David, Holm Kipka, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("I/O")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/climate/RainCorrection.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/climate/RainCorrection.xml")
public class RainCorrectionAdapter extends AnnotatedAdapter {
    @Description("Current Time")
    @Input public LocalDate time;

    @Description("Precipitation")
    @Units("mm")
    @Input public double[] dataArrayPrecip;

    @Description("Wind")
    @Units("m/s")
    @Input public double[] dataArrayWind;

    @Description("temperature for the correction function.")
    @Input public double[] dataArrayTmean;

    @Description("number of closest temperature stations for precipitation correction")
    @Role(PARAMETER)
    @Input public int tempNIDW;

    @Description("power of IDW function for precipitation correction")
    @Role(PARAMETER)
    @Input public double pIDW;

    @Description("power of IDW function for wind correction")
    @Role(PARAMETER)
    @Input public int windNIDW;

    @Description("precipitation correction methods: 0 OFF; 1 Richter; 2 Sevruk; 3 Baisheng")
    @Role(PARAMETER)
    @Input public int precipCorrectMethod;

    @Description("regression threshold")
    @Role(PARAMETER)
    @Input public double regThres;

    @Description("snow_trs")
    @Role(PARAMETER)
    @Input public double snow_trs;

    @Description("snow_trans")
    @Role(PARAMETER)
    @Input public double snow_trans;

    @Description("Array of temperature station elevations")
    @Input public double[] elevationTmean;

    @Description("Array of temperature station's x coordinate")
    @Input public double[] xCoordTmean;

    @Description("Array of temperature station's y coordinate")
    @Input public double[] yCoordTmean;

    @Description("Regression coefficients for temperature")
    @Input public double[] regCoeffTmean;

    @Description("Array of precip station elevations")
    @Input public double[] elevationPrecip;

    @Description("Array of precip station's x coordinate")
    @Input public double[] xCoordPrecip;

    @Description("Array of precip station's y coordinate")
    @Input public double[] yCoordPrecip;

    @Description("Array of wind station elevations")
    @Input public double[] elevationWind;

    @Description("Array of wind station's x coordinate")
    @Input public double[] xCoordWind;

    @Description("Array of wind station's y coordinate")
    @Input public double[] yCoordWind;

    @Description("Regression coefficients for wind")
    @Input public double[] regCoeffWind;

    @Description("corrected precip values.")
    @Output public double[] dataArrayRcorr;

    @Override
    protected void run(Context context) {
        RainCorrection component = new RainCorrection();

        component.time = time;
        component.dataArrayPrecip = dataArrayPrecip;
        component.dataArrayWind = dataArrayWind;
        component.dataArrayTmean = dataArrayTmean;
        component.tempNIDW = tempNIDW;
        component.pIDW = pIDW;
        component.windNIDW = windNIDW;
        component.precipCorrectMethod = precipCorrectMethod;
        component.regThres = regThres;
        component.snow_trs = snow_trs;
        component.snow_trans = snow_trans;
        component.elevationTmean = elevationTmean;
        component.xCoordTmean = xCoordTmean;
        component.yCoordTmean = yCoordTmean;
        component.regCoeffTmean = regCoeffTmean;
        component.elevationPrecip = elevationPrecip;
        component.xCoordPrecip = xCoordPrecip;
        component.yCoordPrecip = yCoordPrecip;
        component.elevationWind = elevationWind;
        component.xCoordWind = xCoordWind;
        component.yCoordWind = yCoordWind;
        component.regCoeffWind = regCoeffWind;

        component.execute();

        dataArrayRcorr = component.dataArrayRcorr;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
