/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package climate;

import lib.Regression;

public class MeanTempCalc {
    private static final double NODATA = -9999;

    public double[] dataArrayTmin;
    public double[] dataArrayTmax;
    public double[] elevation;  //elevation array (maybe from tmin/tmax)
    public double[] dataArrayTmean;
    public double[] regCoeffTmean;

    public void execute() {
        if (dataArrayTmean == null) {
            dataArrayTmean = new double[dataArrayTmin.length];
        }
        for (int i = 0; i < dataArrayTmean.length; i++) {

            if (dataArrayTmax[i] == NODATA || dataArrayTmin[i] == NODATA) {
                dataArrayTmean[i] = NODATA;
            } else {
                dataArrayTmean[i] = (dataArrayTmax[i] + dataArrayTmin[i]) / 2;
            }
        }
        regCoeffTmean = Regression.calcLinReg(elevation, dataArrayTmean);
    }
}
