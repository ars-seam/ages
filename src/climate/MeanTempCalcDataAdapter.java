/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package climate;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedDataAdapter;

@Description("Add MeanTempCalc module definition here")
@Author(name = "Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/climate/MeanTempCalc.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/climate/MeanTempCalc.xml")
public class MeanTempCalcDataAdapter extends AnnotatedDataAdapter<MeanTempCalcDataAdapter.Data> {
    public static class Data {
        @Description("Minimum temperature if available, else mean temp")
        @Units("C")
        @Input public double[] dataArrayTmin;

        @Description("maximum temperature if available, else mean temp")
        @Units("C")
        @Input public double[] dataArrayTmax;

        @Description("Attribute Elevation")
        @Input public double[] elevationTmin;  //elevation array (maybe from tmin/tmax)

        @Description("Array of data values for current time step")
        @Output public double[] dataArrayTmean;

        @Description("Regression coefficients")
        @Output public double[] regCoeffTmean;
    }

    public MeanTempCalcDataAdapter() {
        super(Data.class);
    }

    @Override
    protected boolean run(Data data, Context context) {
        MeanTempCalc component = new MeanTempCalc();

        component.dataArrayTmin = data.dataArrayTmin;
        component.dataArrayTmax = data.dataArrayTmax;
        component.elevation = data.elevationTmin;

        component.execute();

        data.dataArrayTmean = component.dataArrayTmean;
        data.regCoeffTmean = component.regCoeffTmean;

        return true;
    }
}
