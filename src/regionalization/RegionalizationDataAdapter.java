/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package regionalization;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import climate.ClimateType;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedDataAdapter;
import java.util.Map;

@Description("Add Regionalization module definition here")
@Author(name = "Peter Krause, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Regionalization")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/regionalization/Regionalization.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/regionalization/Regionalization.xml")
public class RegionalizationDataAdapter extends AnnotatedDataAdapter<RegionalizationDataAdapter.Data> {
    public static class Data {
        @Description("Array of data values for current time step")
        @Input public double[] dataArray;

        @Description("Regression coefficients")
        @Input public double[] regCoeff;

        @Description("Array of station elevations")
        @Input public double[] stationElevation;

        @Description("Array position of weights")
        @Input public int[] weightedArray;

        @Description("Weights for IDW part of regionalisation.")
        @Input public double[] stationWeights;

        @Description("HRU")
        @Input public HRU hru;

        @Description("Apply elevation correction to measured data")
        @Input public int elevationCorrection;

        @Description("Minimum r2 value for elevation correction application")
        @Role(PARAMETER)
        @Input public double rsqThreshold;

        @Role(PARAMETER)
        @Input public int nidw;

        @Description("Attribute name y coordinate")
        @Units("hru")
        @Output public double regionalizedValue;
    }

    private ClimateType climateType;

    public RegionalizationDataAdapter() {
        super(Data.class);
    }

    public RegionalizationDataAdapter(ClimateType climateType) {
        super(Data.class);
        this.climateType = climateType;
        init();
    }

    @Override
    public void initialize(Map<String, String> parameters) {
        String type = parameters.get("type");
        if (type != null) {
            climateType = ClimateType.of(type);
            init();
        }
    }

    @Override
    protected boolean run(Data data, Context context) {
        Regionalization component = new Regionalization();

        component.dataArray = data.dataArray;
        component.regCoeff = data.regCoeff;
        component.stationElevation = data.stationElevation;
        component.weightedArray = data.weightedArray;
        component.stationWeights = data.stationWeights;
        component.hruElevation = data.hru.elevation;
        component.elevationCorrection = data.elevationCorrection;
        component.rsqThreshold = data.rsqThreshold;
        component.fixedMinimum = climateType.getFixedMinimum();
        component.nidw = data.nidw;

        component.execute();

        data.regionalizedValue = component.regionalizedValue;

        return true;
    }

    private void init() {
        String suffix = climateType.getSuffix();
        if (ClimateType.PRECIPITATION == climateType) {
            setUserVisibleName("dataArray", "dataArrayRcorr");
        } else {
            setUserVisibleName("dataArray", "dataArray" + suffix);
        }
        setUserVisibleName("regCoeff", "regCoeff" + suffix);
        setUserVisibleName("stationElevation", "elevation" + suffix);
        setUserVisibleName("stationWeights", "stationWeights" + suffix);
        setUserVisibleName("weightedArray", "weightedArray" + suffix);
        setUserVisibleName("elevationCorrection", "elevationCorrection" + suffix);
        setUserVisibleName("rsqThreshold", "rsqThreshold" + suffix);
        setUserVisibleName("nidw", "nidw" + suffix);
        setUserVisibleName("regionalizedValue", climateType.getName());
    }
}
