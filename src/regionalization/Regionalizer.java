
package regionalization;

import climate.ClimateType;
import climate.MeanTempCalcDataAdapter;
import climate.RainCorrectionDataAdapter;
import gov.usda.jcf.core.Context;

/**
 *
 * @author Nathan Lighthart
 */
public class Regionalizer {
    protected final String name;
    protected RegionalizationDataAdapter regionalization;

    private Regionalizer(ClimateType climateType) {
        name = climateType.getName();
        regionalization = new RegionalizationDataAdapter(climateType);
    }

    public static Regionalizer of(ClimateType climateType) {
        switch (climateType) {
            case MEAN_TEMPERATURE:
                return new MeanTemperatureRegionalizer(climateType);
            case PRECIPITATION:
                return new PrecipitationRegionalizer(climateType);
        }
        return new Regionalizer(climateType);
    }

    public static double regionalize(ClimateType climateType, Context hruContext) {
        Regionalizer regionalizer = of(climateType);
        regionalizer.regionalize(hruContext);
        return regionalizer.getRegionalizedValue(hruContext);
    }

    public void regionalize(Context hruContext) {
        regionalization.execute(hruContext);
    }

    public double getRegionalizedValue(Context hruContext) {
        return hruContext.getDouble(name);
    }

    private static class MeanTemperatureRegionalizer extends Regionalizer {
        protected MeanTempCalcDataAdapter meanTempCalc;

        private MeanTemperatureRegionalizer(ClimateType climateType) {
            super(climateType);
            meanTempCalc = new MeanTempCalcDataAdapter();
        }

        @Override
        public void regionalize(Context hruContext) {
            meanTempCalc.execute(hruContext);
            super.regionalize(hruContext);
        }
    }

    private static class PrecipitationRegionalizer extends Regionalizer {
        protected RainCorrectionDataAdapter rainCorrection;

        private PrecipitationRegionalizer(ClimateType climateType) {
            super(climateType);
            rainCorrection = new RainCorrectionDataAdapter();
        }

        @Override
        public void regionalize(Context hruContext) {
            rainCorrection.execute(hruContext);
            super.regionalize(hruContext);
        }
    }
}
