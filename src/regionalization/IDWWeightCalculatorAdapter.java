/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package regionalization;

import ages.types.HRU;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import climate.ClimateType;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Map;

@Description("Add IDWWeightCalculator module definition here")
@Author(name = "Peter Krause, Olaf David, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Regionalization")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/regionalization/IDWWeightCalculator.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/regionalization/IDWWeightCalculator.xml")
public class IDWWeightCalculatorAdapter extends AnnotatedAdapter {
    @Description("DB function")
    @Role(PARAMETER)
    @Input public boolean equalWeights;

    @Description("HRU entity")
    @Input public HRU hru;

    @Description("Array of station's x coordinates")
    @Input public double[] xCoord;

    @Description("Array of station's y coordinates")
    @Input public double[] yCoord;

    @Description("Power of IDW function")
    @Input public double pidw;

    @Description("Weights for IDW part of regionalisation.")
    @Output public double[] stationWeights;

    @Description("position array to determine best weights")
    @Output public int[] weightedArray;

    public IDWWeightCalculatorAdapter() {
    }

    public IDWWeightCalculatorAdapter(ClimateType climateType) {
        init(climateType);
    }

    @Override
    public void initialize(Map<String, String> parameters) {
        String type = parameters.get("type");
        if (type != null) {
            ClimateType climateType = ClimateType.of(type);
            init(climateType);
        }
    }

    @Override
    protected void run(Context context) {
        IDWWeightCalculator component = new IDWWeightCalculator();

        component.x = hru.x;
        component.y = hru.y;
        component.xCoord = xCoord;
        component.yCoord = yCoord;
        component.pidw = pidw;
        component.equalWeights = equalWeights;

        component.execute();

        stationWeights = component.stationWeights;
        weightedArray = component.weightedArray;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }

    private void init(ClimateType climateType) {
        String suffix = climateType.getSuffix();
        setUserVisibleName("xCoord", "xCoord" + suffix);
        setUserVisibleName("yCoord", "yCoord" + suffix);
        setUserVisibleName("pidw", "pidw" + suffix);
        setUserVisibleName("stationWeights", "stationWeights" + suffix);
        setUserVisibleName("weightedArray", "weightedArray" + suffix);
    }
}
