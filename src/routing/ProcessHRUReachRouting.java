
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessHRUReachRouting {
    public double weight;
    public double outRD1;
    public double[] outRD2_h;
    public double outRG1;
    public double outRG2;
    public double gwExcess;
    public int horizons;
    public double[] out_tile_water;
    public double in_tile_water;

    public double inRD1;
    public double inRD2;
    public double inRG1;
    public double inRG2;

    public void execute() {
        double TDin = 0;
        inRD2 = 0;

        for (int h = 0; h < horizons; h++) {
            inRD2 += outRD2_h[h] * weight;
            if (out_tile_water != null) {
                TDin += out_tile_water[h] * weight;
            }
        }

        inRD1 = outRD1 * weight;
        inRD2 += gwExcess + in_tile_water * weight + TDin;
        inRG1 = outRG1 * weight;
        inRG2 = outRG2 * weight;
    }
}
