
package routing;

import ages.types.SoilType;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessHRUReachRoutingNAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] hruRoutingContexts;

    @Output public double hruSurfaceN_in;
    @Output public double hruInterflowN_sum;
    @Output public double hruN_RG1_in;
    @Output public double hruN_RG2_in;

    @Override
    protected void run(Context context) {
        reset();
        for (RoutingContext routingContext : hruRoutingContexts) {
            route(routingContext);
        }
    }

    private void reset() {
        hruSurfaceN_in = 0;
        hruInterflowN_sum = 0;
        hruN_RG1_in = 0;
        hruN_RG2_in = 0;
    }

    private void route(RoutingContext routingContext) {
        Context hruContext = routingContext.spatialContext;

        ProcessHRUReachRoutingN component = new ProcessHRUReachRoutingN();

        component.weight = routingContext.weight;
        component.SurfaceNabs = hruContext.getDouble("SurfaceNabs");
        component.InterflowNabs = hruContext.getDoubleArray("InterflowNabs");
        component.N_RG1_out = hruContext.getDouble("N_RG1_out");
        component.N_RG2_out = hruContext.getDouble("N_RG2_out");
        component.NExcess = hruContext.getDouble("NExcess");
        component.horizons = hruContext.get("soil", SoilType.class).horizons;
        if (hruContext.contains("TDInterflowN")) {
            component.TDInterflowN = hruContext.getDoubleArray("TDInterflowN");
        }
        if (hruContext.contains("inTDN")) {
            component.inTDN = hruContext.getDouble("inTDN");
        }

        component.execute();

        hruSurfaceN_in += component.SurfaceN_in;
        hruInterflowN_sum += component.InterflowN_sum;
        hruN_RG1_in += component.N_RG1_in;
        hruN_RG2_in += component.N_RG2_in;
    }
}
