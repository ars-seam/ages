
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingMusle {
    private static final double OUT_FACTOR = 0.98;

    public double hruInsed;
    public double reachInsed;

    public double actsed;

    public double insed;
    public double outsed;

    public void execute() {
        insed = hruInsed + reachInsed;
        actsed += insed;
        outsed = OUT_FACTOR * actsed;
        actsed -= outsed;
    }
}
