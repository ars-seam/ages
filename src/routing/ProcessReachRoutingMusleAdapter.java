
package routing;

import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingMusleAdapter extends AnnotatedAdapter {
    @Input public double hruInsed;
    @Input public double reachInsed;

    @Optional
    @Input @Output public double actsed;

    @Output public double insed;
    @Output public double outsed;

    @Override
    protected void run(Context context) {
        ProcessReachRoutingMusle component = new ProcessReachRoutingMusle();

        component.hruInsed = hruInsed;
        component.reachInsed = reachInsed;
        component.actsed = actsed;

        component.execute();

        actsed = component.actsed;
        insed = component.insed;
        outsed = component.outsed;
    }
}
