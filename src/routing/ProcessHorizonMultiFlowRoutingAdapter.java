/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package routing;

import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import gov.usda.jcf.core.contexts.UnmodifiableContext;

@Description("Add ProcessHorizonMultiFlowRouting module definition here")
@Author(name = "Olaf David, Peter Krause, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Routing")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/routing/ProcessHorizonMultiFlowRouting.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/routing/ProcessHorizonMultiFlowRouting.xml")
public class ProcessHorizonMultiFlowRoutingAdapter extends AnnotatedAdapter {
    @Description("HRU Routing flag")
    @Input public boolean flagHRURouting;

    @Description("routing contexts")
    @Input public RoutingContext[] routingContexts;

    @Description("soil")
    @Input public SoilType soil;

    @Optional
    @Input @Output public double[] inRD2_h;

    @Output public double gwExcess;
    @Output public double inRD1;
    @Output public double inRG1;
    @Output public double inRG2;
    @Output public double in_tile_water;

    @Override
    protected void run(Context context) {
        if (!flagHRURouting) {
            return;
        }

        reset();
        for (RoutingContext routingContext : routingContexts) {
            route(routingContext.spatialContext, routingContext.weight);
        }
    }

    private void reset() {
        gwExcess = 0.0;
        inRD1 = 0.0;
        inRG1 = 0.0;
        inRG2 = 0.0;
        in_tile_water = 0.0;

        if (inRD2_h == null) {
            inRD2_h = new double[soil.horizons];
        }
    }

    private void route(UnmodifiableContext from, double weight) {
        ProcessHorizonMultiFlowRouting component = new ProcessHorizonMultiFlowRouting();

        component.weight = weight;
        component.RD1out = from.getDouble("outRD1");
        component.RD2out = from.getDoubleArray("outRD2_h");
        component.RG1out = from.getDouble("outRG1");
        component.RG2out = from.getDouble("outRG2");
        if (from.contains("out_tile_water")) {
            component.TDout = from.getDoubleArray("out_tile_water");
        } else {
            component.TDout = null;
        }
        component.TD_exist = from.getDouble("in_tile_water");
        component.srcDepth = from.get("soil", SoilType.class).depth_h;
        component.gwExcess = from.getDouble("gwExcess");
        component.RD1in = inRD1;
        component.recDepth = soil.depth_h;
        component.RG1in = inRG1;
        component.RG2in = inRG2;
        component.TDin = in_tile_water;
        component.inRD2_h = inRD2_h;

        component.execute();

        inRD1 = component.RD1in;
        inRD2_h = component.RD2in;
        inRG1 = component.RG1in;
        inRG2 = component.RG2in;
        in_tile_water = component.TDin;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
