/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package routing;

public class MultiRoutingLayerN {
    // weight of routing
    public double weight;

    // from variables (do not modify value of these variables)
    public double NRD1out;
    public double[] NRD2out_h;
    public double NRG1out;
    public double NRG2out;
    public double[] NTDout;
    public double[] srcDepth;
    public double NExcess;

    // to variables
    public double[] recDepth;
    public double[] rdArN;
    public double TDinN;
    public double NRD1in;
    public double NRG1in;
    public double NRG2in;
    public double[] NRD2in_h;

    public void execute() {
        int srcHors = srcDepth.length;
        int recHors = recDepth.length;

        if (NTDout != null) {
            for (int i = 0; i < srcHors; i++) {
                TDinN += NTDout[i] * weight;
            }
        }

        NRD2in_h = new double[recHors];

        for (int j = 0; j < recHors; j++) {
            NRD2in_h[j] = rdArN[j];
            for (int i = 0; i < srcHors; i++) {
                NRD2in_h[j] += (NRD2out_h[i] / recHors) * weight;
            }
        }

        NRD2in_h[recHors - 1] += NExcess;
        NRD1in += NRD1out * weight;
        NRG1in += NRG1out * weight;
        NRG2in += NRG2out * weight;
    }
}
