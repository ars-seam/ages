
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRouting {
    // calculate flow velocity in the reach
    private static final double TT = 2.0 / 3.0;

    public double flowRouteTA;
    public double Ksink;
    public double deepsink;
    public double width;
    public double slope;
    public double rough;
    public double length;
    public double hruInRD1;
    public double hruInRD2;
    public double hruInRG1;
    public double hruInRG2;
    public double reachInRD1;
    public double reachInRD2;
    public double reachInRG1;
    public double reachInRG2;

    public double actRD1;
    public double actRD2;
    public double actRG1;
    public double actRG2;

    public double inRD1;
    public double inRD2;
    public double inRG1;
    public double inRG2;
    public double outRD1;
    public double outRD2;
    public double outRG1;
    public double outRG2;
    public double simRunoff;
    public double channelStorage;
    public double q_act_tot;
    public double q_act_out;
    public double deepsinkW;

    private double rh;

    public void execute() {
        inRD1 = hruInRD1 + reachInRD1;
        inRD2 = hruInRD2 + reachInRD2;
        inRG1 = hruInRG1 + reachInRG1;
        inRG2 = hruInRG2 + reachInRG2;

        actRD1 += inRD1;
        actRD2 += inRD2;
        actRG1 += inRG1;
        actRG2 += inRG2;

        q_act_tot = actRD1 + actRD2 + actRG1 + actRG2;

        if (q_act_tot == 0) {
            outRD1 = 0;
            outRD2 = 0;
            outRG1 = 0;
            outRG2 = 0;

            return;
        }

        // calculate relative parts of runoff components for later redistribution
        double RD1_part = actRD1 / q_act_tot;
        double RD2_part = actRD2 / q_act_tot;
        double RG1_part = actRG1 / q_act_tot;
        double RG2_part = actRG2 / q_act_tot;

        // calculate flow velocity
        double flow_veloc = calcFlowVelocity(q_act_tot, width, slope, rough, 86400);

        // calculate recession coefficient
        double Rk = (flow_veloc / length) * flowRouteTA * 3600;

        // calculate outflow
        if (Rk > 0) {
            q_act_out = q_act_tot * Math.exp(-1 / Rk);
        } else {
            q_act_out = 0;
        }

        deepsinkW = 0;
        double Larea = 0;

        if (deepsink == 1.0) {
            // calculate leakage area
            Larea = rh * rh * length;

            // calculate deep sink amounts
            deepsinkW = Larea * Ksink * 10;
            deepsinkW = Math.min(deepsinkW, q_act_out);
            deepsinkW = Math.max(deepsinkW, 0);
        } else {
            deepsinkW = 0;
        }

        // calculate actual outflow from the reach
        double RD1outdeep = deepsinkW * RD1_part;
        double RD2outdeep = deepsinkW * RD2_part;
        double RG1outdeep = deepsinkW * RG1_part;
        double RG2outdeep = deepsinkW * RG2_part;

        outRD1 = q_act_out * RD1_part - RD1outdeep;
        outRD2 = q_act_out * RD2_part - RD2outdeep;
        outRG1 = q_act_out * RG1_part - RG1outdeep;
        outRG2 = q_act_out * RG2_part - RG2outdeep;

        // reduce the actual storages
        actRD1 = reduce(actRD1, outRD1, RD1outdeep);
        actRD2 = reduce(actRD2, outRD2, RD2outdeep);
        actRG1 = reduce(actRG1, outRG1, RG1outdeep);
        actRG2 = reduce(actRG2, outRG2, RG2outdeep);

        channelStorage = actRD1 + actRD2 + actRG1 + actRG2;
        simRunoff = outRD1 + outRD2 + outRG1 + outRG2;
    }

    public double calcFlowVelocity(double q, double width, double slope, double rough, int secondsOfTimeStep) {
        double afv = 1;

        // convert liter/d to m^3/s
        double q_m = q / (1000 * secondsOfTimeStep);
        rh = hydraulicRadius(afv, q_m, width);
        boolean cont = true;

        while (cont) {
            double veloc = rough * Math.pow(rh, TT) * Math.sqrt(slope);
            if (Math.abs(veloc - afv) > 0.001) {
                afv = veloc;
                rh = hydraulicRadius(afv, q_m, width);
            } else {
                cont = false;
                afv = veloc;
            }
        }
        return afv;
    }

    // calculate hydraulic radius of a rectangular stream bed using daily runoff and flow velocity
    public static double hydraulicRadius(double v, double q, double width) {
        double A = q / v;
        return A / (width + 2 * (A / width));
    }

    public static double reduce(double act, double out, double outDeep) {
        double reduced = act - out - outDeep;
        if (reduced < 0.0) {
            reduced = 0.0;
        }
        return reduced;
    }
}
