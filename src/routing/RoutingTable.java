
package routing;

import ages.types.HRU;
import ages.types.StreamReach;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Lighthart
 */
public class RoutingTable {
    private final Map<Integer, HRURoutingInformation> hruRoutingTable;
    private final Map<Integer, ReachRoutingInformation> reachRoutingTable;

    private int maxDepthHRU;
    private int minDepthReach;
    private int maxDepthReach;

    private boolean foundOutlet;
    private int outletId;

    public static RoutingTable createEmptyTable(List<HRU> hrus, List<StreamReach> reaches) {
        return new RoutingTable(hrus, reaches);
    }

    public RoutingTable(Map<Integer, List<RoutingEdge>> routingEdges) {
        hruRoutingTable = new HashMap<>();
        reachRoutingTable = new HashMap<>();

        buildTable(routingEdges);
    }

    /**
     * Creates an empty routing table with each hru and reach having edges to
     * nothing.
     *
     * @param hrus the hrus
     * @param reaches the reaches
     */
    private RoutingTable(List<HRU> hrus, List<StreamReach> reaches) {
        hruRoutingTable = new HashMap<>();
        reachRoutingTable = new HashMap<>();

        maxDepthHRU = 0;
        minDepthReach = 0;
        maxDepthReach = 0;

        for (HRU hru : hrus) {
            HRURoutingInformation info = new HRURoutingInformation(0,
                    new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
            hruRoutingTable.put(hru.ID, info);
        }

        for (StreamReach reach : reaches) {
            ReachRoutingInformation info = new ReachRoutingInformation(0,
                    new ArrayList<>(), new ArrayList<>(), null);
            reachRoutingTable.put(reach.ID, info);
            foundOutlet = true;
            outletId = reach.ID;
        }
    }

    public HRURoutingInformation getRoutingInformationHRU(int id) {
        return hruRoutingTable.get(id);
    }

    public ReachRoutingInformation getRoutingInformationReach(int id) {
        return reachRoutingTable.get(id);
    }

    public int getOutletId() {
        if (foundOutlet) {
            return outletId;
        }
        throw new RuntimeException("Outlet could not be found");
    }

    public int getMaxDepthHRU() {
        return maxDepthHRU;
    }

    public int getMaxDepthReach() {
        return maxDepthReach;
    }

    public int getMinDepthReach() {
        return minDepthReach;
    }

    public void sortEdgesById() {
        Comparator<RoutingEdge> comparator = new Comparator<RoutingEdge>() {
            @Override
            public int compare(RoutingEdge r1, RoutingEdge r2) {
                return Integer.compare(r1.getId(), r2.getId());
            }
        };

        sortBy(comparator);
    }

    public void sortEdgesByWeight() {
        Comparator<RoutingEdge> comparator = new Comparator<RoutingEdge>() {
            @Override
            public int compare(RoutingEdge r1, RoutingEdge r2) {
                return Double.compare(r1.getWeight(), r2.getWeight());
            }
        }.reversed(); // reverse to get highest weights first

        sortBy(comparator);
    }

    public void sortEdgesByIndices(List<HRU> hrus, List<StreamReach> reaches) {
        final Map<Integer, Integer> entityIndexMap = new HashMap<>();
        for (int i = 0; i < hrus.size(); ++i) {
            entityIndexMap.put(hrus.get(i).ID, i);
        }
        for (int i = 0; i < reaches.size(); ++i) {
            entityIndexMap.put(reaches.get(i).ID, i);
        }

        Comparator<RoutingEdge> indexComparator = new Comparator<RoutingEdge>() {
            @Override
            public int compare(RoutingEdge r1, RoutingEdge r2) {
                return Integer.compare(getValue(r1), getValue(r2));
            }

            private int getValue(RoutingEdge edge) {
                Integer value = entityIndexMap.get(edge.getId());
                return (value == null) ? Integer.MAX_VALUE : value;
            }
        };

        sortBy(indexComparator);
    }

    public void sortBy(Comparator<RoutingEdge> comparator) {
        for (HRURoutingInformation hruInfo : hruRoutingTable.values()) {
            hruInfo.sortBy(comparator);
        }
        for (ReachRoutingInformation reachInfo : reachRoutingTable.values()) {
            reachInfo.sortBy(comparator);
        }
    }

    public void printHRUTable() {
        for (Map.Entry<Integer, HRURoutingInformation> hruEntry : hruRoutingTable.entrySet()) {
            HRURoutingInformation hruInfo = hruEntry.getValue();
            System.out.println(hruEntry.getKey() + " -> " + hruInfo.getToHRUs());
            System.out.println(hruEntry.getKey() + " -> " + hruInfo.getToReaches());
            System.out.println(hruEntry.getKey() + " <- " + hruInfo.getFromHRUs());
        }
    }

    public void printReachTable() {
        for (Map.Entry<Integer, ReachRoutingInformation> reachEntry : reachRoutingTable.entrySet()) {
            ReachRoutingInformation hruInfo = reachEntry.getValue();
            System.out.println(reachEntry.getKey() + " -> " + hruInfo.getToReach());
            System.out.println(reachEntry.getKey() + " <- " + hruInfo.getFromHRUs());
            System.out.println(reachEntry.getKey() + " <- " + hruInfo.getFromReaches());
        }
    }

    public void printTable() {
        printHRUTable();
        printReachTable();
    }

    private void buildTable(Map<Integer, List<RoutingEdge>> routingEdges) {
        Map<Integer, HRURoutingInformation.Builder> hruTable = new HashMap<>();
        Map<Integer, ReachRoutingInformation.Builder> reachTable = new HashMap<>();

        // Build table information
        buildInformation(routingEdges, hruTable, reachTable);

        // Compute depths
        computeDepths(routingEdges, hruTable, reachTable);

        // Finalize tables
        for (Map.Entry<Integer, HRURoutingInformation.Builder> hruEntry : hruTable.entrySet()) {
            hruRoutingTable.put(hruEntry.getKey(), hruEntry.getValue().build());
        }
        for (Map.Entry<Integer, ReachRoutingInformation.Builder> reachEntry : reachTable.entrySet()) {
            reachRoutingTable.put(reachEntry.getKey(), reachEntry.getValue().build());
        }
    }

    private void buildInformation(Map<Integer, List<RoutingEdge>> routingEdges,
            Map<Integer, HRURoutingInformation.Builder> hruTable,
            Map<Integer, ReachRoutingInformation.Builder> reachTable) {
        for (Map.Entry<Integer, List<RoutingEdge>> edgeList : routingEdges.entrySet()) {
            int fromID = edgeList.getKey();
            List<RoutingEdge> toEdges = edgeList.getValue();

            if (fromID > 0) {
                // HRU
                HRURoutingInformation.Builder builder = hruTable.get(fromID);
                if (builder == null) {
                    builder = new HRURoutingInformation.Builder();
                    hruTable.put(fromID, builder);
                }

                for (RoutingEdge node : edgeList.getValue()) {
                    int toID = node.getId();
                    if (toID > 0) {
                        // HRU to HRU
                        if (fromID == toID) {
                            throw new IllegalArgumentException("HRU " + fromID + " cannot route to itself");
                        }

                        // Add edge
                        builder.addToHRU(node);

                        // Add reverse edge
                        HRURoutingInformation.Builder toBuilder = hruTable.get(toID);
                        if (toBuilder == null) {
                            toBuilder = new HRURoutingInformation.Builder();
                            hruTable.put(toID, toBuilder);
                        }
                        toBuilder.addFromHRU(new RoutingEdge(fromID, node.getWeight()));
                    } else {
                        // HRU to Reach

                        // Add edge
                        builder.addToReach(node);

                        // Add reverse edge
                        ReachRoutingInformation.Builder toBuilder = reachTable.get(toID);
                        if (toBuilder == null) {
                            toBuilder = new ReachRoutingInformation.Builder();
                            reachTable.put(toID, toBuilder);
                        }
                        toBuilder.addFromHRU(new RoutingEdge(fromID, node.getWeight()));
                    }
                }
            } else {
                // Reach
                ReachRoutingInformation.Builder builder = reachTable.get(fromID);
                if (builder == null) {
                    builder = new ReachRoutingInformation.Builder();
                    reachTable.put(fromID, builder);
                }

                if (toEdges.isEmpty()) {
                    // Reach to Nothing (outlet)
                    builder.setToReach(null);
                    foundOutlet = true;
                    outletId = fromID;
                } else if (toEdges.size() > 1) {
                    // Reach to many Somethings (invalid)
                    throw new IllegalArgumentException("Reach " + fromID + " has multiple outgoing edges " + toEdges);
                } else {
                    // Reach to one Something
                    RoutingEdge node = toEdges.get(0);
                    int toID = node.getId();
                    if (toID > 0) {
                        // Reach to HRU (invalid)
                        throw new IllegalArgumentException("Reach " + fromID + " cannot route to HRU " + toID);
                    } else if (fromID == toID) {
                        // Reach to self
                        throw new IllegalArgumentException("Reach " + fromID + " cannot route to itself");
                    } else {
                        // Reach to Reach

                        // Add edge
                        builder.setToReach(node);

                        // Add reverse edge
                        ReachRoutingInformation.Builder toBuilder = reachTable.get(toID);
                        if (toBuilder == null) {
                            toBuilder = new ReachRoutingInformation.Builder();
                            reachTable.put(toID, toBuilder);
                        }
                        toBuilder.addFromReach(new RoutingEdge(fromID, node.getWeight()));
                    }
                }
            }
        }
    }

    private void computeDepths(Map<Integer, List<RoutingEdge>> routingEdges,
            Map<Integer, HRURoutingInformation.Builder> hruTable,
            Map<Integer, ReachRoutingInformation.Builder> reachTable) {
        Map<Integer, Integer> depthMap = new HashMap<>();
        for (Integer id : routingEdges.keySet()) {
            depthMap.put(id, 0);
        }

        boolean mapChanged = true;
        while (mapChanged) {
            mapChanged = false;
            for (Map.Entry<Integer, List<RoutingEdge>> entry : routingEdges.entrySet()) {
                int depth = depthMap.get(entry.getKey());
                List<RoutingEdge> toEntities = entry.getValue();
                for (RoutingEdge node : toEntities) {
                    int toDepth = depthMap.get(node.getId());
                    if (toDepth <= depth) {
                        depthMap.put(node.getId(), depth + 1);
                        mapChanged = true;
                    }
                }
            }
        }

        maxDepthHRU = 0;
        for (Map.Entry<Integer, HRURoutingInformation.Builder> hruEntry : hruTable.entrySet()) {
            int depth = depthMap.get(hruEntry.getKey());
            hruEntry.getValue().setDepth(depth);
            if (depth > maxDepthHRU) {
                maxDepthHRU = depth;
            }
        }

        minDepthReach = Integer.MAX_VALUE;
        maxDepthReach = 0;
        for (Map.Entry<Integer, ReachRoutingInformation.Builder> reachEntry : reachTable.entrySet()) {
            int depth = depthMap.get(reachEntry.getKey());
            reachEntry.getValue().setDepth(depth);
            if (depth < minDepthReach) {
                minDepthReach = depth;
            }
            if (depth > maxDepthReach) {
                maxDepthReach = depth;
            }
        }
    }
}
