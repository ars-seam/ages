
package routing;

import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingInputMusleAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] reachRoutingContexts;

    @Output public double reachInsed;

    @Override
    protected void run(Context context) {
        reachInsed = 0.0;
        for (RoutingContext routingContext : reachRoutingContexts) {
            double outsed = routingContext.spatialContext.getDouble("outsed");
            reachInsed += outsed;
        }
    }
}
