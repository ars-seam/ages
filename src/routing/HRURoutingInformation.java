
package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class HRURoutingInformation {
    private final int depth;
    private final List<RoutingEdge> fromHRUs;
    private final List<RoutingEdge> toHRUs;
    private final List<RoutingEdge> toReaches;

    public HRURoutingInformation(int depth, List<RoutingEdge> fromHRUs, List<RoutingEdge> toHRUs, List<RoutingEdge> toReaches) {
        this.depth = depth;
        this.fromHRUs = fromHRUs;
        this.toHRUs = toHRUs;
        this.toReaches = toReaches;
    }

    public int getDepth() {
        return depth;
    }

    public List<RoutingEdge> getFromHRUs() {
        return Collections.unmodifiableList(fromHRUs);
    }

    public List<RoutingEdge> getToHRUs() {
        return Collections.unmodifiableList(toHRUs);
    }

    public List<RoutingEdge> getToReaches() {
        return Collections.unmodifiableList(toReaches);
    }

    public void sortBy(Comparator<RoutingEdge> comparator) {
        Collections.sort(fromHRUs, comparator);
        Collections.sort(toHRUs, comparator);
        Collections.sort(toReaches, comparator);
    }

    public static class Builder {
        private int depth;
        private List<RoutingEdge> fromHRUs;
        private List<RoutingEdge> toHRUs;
        private List<RoutingEdge> toReaches;

        public Builder() {
            depth = 0;
            fromHRUs = new ArrayList<>();
            toHRUs = new ArrayList<>();
            toReaches = new ArrayList<>();
        }

        public void addFromHRU(RoutingEdge routingNode) {
            fromHRUs.add(routingNode);
        }

        public void addFromHRUs(Collection<RoutingEdge> routingNodes) {
            fromHRUs.addAll(routingNodes);
        }

        public void addToHRU(RoutingEdge routingNode) {
            toHRUs.add(routingNode);
        }

        public void addToHRUs(Collection<RoutingEdge> routingNodes) {
            toHRUs.addAll(routingNodes);
        }

        public void addToReach(RoutingEdge routingNode) {
            toReaches.add(routingNode);
        }

        public void addToReaches(Collection<RoutingEdge> routingNodes) {
            toReaches.addAll(routingNodes);
        }

        public void setDepth(int depth) {
            this.depth = depth;
        }

        public HRURoutingInformation build() {
            return new HRURoutingInformation(depth, fromHRUs, toHRUs, toReaches);
        }
    }
}
