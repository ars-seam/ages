
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessHRUReachRoutingN {
    public double weight;
    public double SurfaceNabs;
    public double[] InterflowNabs;
    public double N_RG1_out;
    public double N_RG2_out;
    public double NExcess;
    public int horizons;
    public double[] TDInterflowN;
    public double inTDN;

    public double SurfaceN_in;
    public double InterflowN_sum;
    public double N_RG1_in;
    public double N_RG2_in;

    public void execute() {
        double TDinN = 0;
        InterflowN_sum = 0;

        for (int h = 0; h < horizons; h++) {
            InterflowN_sum += InterflowNabs[h] * weight;
            if (TDInterflowN != null) {
                TDinN += TDInterflowN[h] * weight;
            }
        }

        SurfaceN_in = SurfaceNabs * weight;
        InterflowN_sum += NExcess + inTDN * weight + TDinN;
        N_RG1_in = N_RG1_out * weight;
        N_RG2_in = N_RG2_out * weight;
    }
}
