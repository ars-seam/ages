
package routing;

import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingInputAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] reachRoutingContexts;

    @Output public double reachInRD1;
    @Output public double reachInRD2;
    @Output public double reachInRG1;
    @Output public double reachInRG2;

    @Override
    protected void run(Context context) {
        reset();
        for (RoutingContext routingContext : reachRoutingContexts) {
            route(routingContext);
        }
    }

    private void reset() {
        reachInRD1 = 0;
        reachInRD2 = 0;
        reachInRG1 = 0;
        reachInRG2 = 0;
    }

    private void route(RoutingContext routingContext) {
        Context hruContext = routingContext.spatialContext;

        double outRD1 = hruContext.getDouble("outRD1");
        double outRD2 = hruContext.getDouble("outRD2");
        double outRG1 = hruContext.getDouble("outRG1");
        double outRG2 = hruContext.getDouble("outRG2");

        reachInRD1 += outRD1;
        reachInRD2 += outRD2;
        reachInRG1 += outRG1;
        reachInRG2 += outRG2;
    }
}
