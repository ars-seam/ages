
package routing;

import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.contexts.UnmodifiableContext;

/**
 *
 * @author Nathan Lighthart
 */
public class RoutingContext {
    public final UnmodifiableContext spatialContext;
    public final double weight;

    public RoutingContext(UnmodifiableContext spatialContext, double weight) {
        this.spatialContext = spatialContext;
        this.weight = weight;
    }

    public RoutingContext(Context spatialContext, double weight) {
        this(new UnmodifiableContext(spatialContext), weight);
    }
}
