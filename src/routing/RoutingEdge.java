
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public final class RoutingEdge {
    private final int id;
    private final double weight;

    public RoutingEdge(int id, double weight) {
        if (weight < 0.0 || weight > 1.0) {
            throw new IllegalArgumentException("Invalid weight: " + weight);
        }
        this.id = id;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "(" + id + ", " + weight + ")";
    }
}
