
package routing;

import ages.types.StreamReach;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingNAdapter extends AnnotatedAdapter {
    @Description("reach")
    @Input public StreamReach reach;

    @Description("total outflow")
    @Input public double q_act_tot;

    @Description("actual outflow")
    @Input public double q_act_out;

    @Input public double deepsinkW;

    @Description("hru cumulative sum of nitrogen output going into RD1")
    @Input public double hruSurfaceN_in;

    @Description("hru cumulative sum of nitrogen output going into RD2")
    @Input public double hruInterflowN_sum;

    @Description("hru cumulative sum of nitrogen output going into RG1")
    @Input public double hruN_RG1_in;

    @Description("hru cumulative sum of nitrogen output going into RG2")
    @Input public double hruN_RG2_in;

    @Description("reach cumulative sum of nitrogen output going into RD1")
    @Input public double reachSurfaceN_in;

    @Description("reach cumulative sum of nitrogen output going into RD2")
    @Input public double reachInterflowN_sum;

    @Description("reach cumulative sum of nitrogen output going into RG1")
    @Input public double reachN_RG1_in;

    @Description("reach cumulative sum of nitrogen output going into RG2")
    @Input public double reachN_RG2_in;

    @Description("actual RD1 nitrogen storage")
    @Optional
    @Input @Output public double actRD1_N;

    @Description("actual RD2 nitrogen storage")
    @Optional
    @Input @Output public double actRD2_N;

    @Description("actual RG1 nitrogen storage")
    @Optional
    @Input @Output public double actRG1_N;

    @Description("actual RG2 nitrogen storage")
    @Optional
    @Input @Output public double actRG2_N;

    @Description("total amount of nitrogen coming into RD1")
    @Output public double SurfaceN_in;

    @Description("total amount of nitrogen coming into RD2")
    @Output public double InterflowN_sum;

    @Description("total amount of nitrogen coming into RG1")
    @Output public double N_RG1_in;

    @Description("total amount of nitrogen coming into RG2")
    @Output public double N_RG2_in;

    @Description("RD1 nitrogen outflow")
    @Output public double outRD1_N;

    @Description("RD2 nitrogen outflow")
    @Output public double outRD2_N;

    @Description("RG1 nitrogen outflow")
    @Output public double outRG1_N;

    @Description("RG2 nitrogen outflow")
    @Output public double outRG2_N;

    @Description("RD1 nitrogen outflow")
    @Output public double SurfaceNabs;

    @Description("RD2 nitrogen outflow")
    @Output public double InterflowNabs;

    @Description("RG1 nitrogen outflow")
    @Output public double N_RG1_out;

    @Description("RG2 nitrogen outflow")
    @Output public double N_RG2_out;

    @Description("simulated runoff")
    @Output public double simRunoff_N;

    @Output public double channelStorage_N;

    @Description("total nitrogen outflow")
    @Output public double q_act_tot_N;

    @Description("actual nitrogen outflow")
    @Output public double q_act_out_N;

    @Output public double deepsinkN;

    @Override
    protected void run(Context context) {
        ProcessReachRoutingN component = new ProcessReachRoutingN();

        component.deepsink = reach.deepsink;
        component.q_act_tot = q_act_tot;
        component.q_act_out = q_act_out;
        component.deepsinkW = deepsinkW;
        component.hruSurfaceN_in = hruSurfaceN_in;
        component.hruInterflowN_sum = hruInterflowN_sum;
        component.hruN_RG1_in = hruN_RG1_in;
        component.hruN_RG2_in = hruN_RG2_in;
        component.reachSurfaceN_in = reachSurfaceN_in;
        component.reachInterflowN_sum = reachInterflowN_sum;
        component.reachN_RG1_in = reachN_RG1_in;
        component.reachN_RG2_in = reachN_RG2_in;
        component.actRD1_N = actRD1_N;
        component.actRD2_N = actRD2_N;
        component.actRG1_N = actRG1_N;
        component.actRG2_N = actRG2_N;

        component.execute();

        actRD1_N = component.actRD1_N;
        actRD2_N = component.actRD2_N;
        actRG1_N = component.actRG1_N;
        actRG2_N = component.actRG2_N;
        SurfaceN_in = component.SurfaceN_in;
        InterflowN_sum = component.InterflowN_sum;
        N_RG1_in = component.N_RG1_in;
        N_RG2_in = component.N_RG2_in;
        outRD1_N = component.outRD1_N;
        outRD2_N = component.outRD2_N;
        outRG1_N = component.outRG1_N;
        outRG2_N = component.outRG2_N;
        SurfaceNabs = component.SurfaceNabs;
        InterflowNabs = component.InterflowNabs;
        N_RG1_out = component.N_RG1_out;
        N_RG2_out = component.N_RG2_out;
        simRunoff_N = component.simRunoff_N;
        channelStorage_N = component.channelStorage_N;
        q_act_tot_N = component.q_act_tot_N;
        q_act_out_N = component.q_act_out_N;
        deepsinkN = component.deepsinkN;
    }
}
