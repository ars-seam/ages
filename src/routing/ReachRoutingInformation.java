
package routing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Nathan Lighthart
 */
public class ReachRoutingInformation {
    private final int depth;
    private final List<RoutingEdge> fromHRUs;
    private final List<RoutingEdge> fromReaches;
    private final RoutingEdge toReach;

    public ReachRoutingInformation(int depth, List<RoutingEdge> fromHRUs, List<RoutingEdge> fromReaches, RoutingEdge toReach) {
        this.depth = depth;
        this.fromHRUs = fromHRUs;
        this.fromReaches = fromReaches;
        this.toReach = toReach;
    }

    public int getDepth() {
        return depth;
    }

    public List<RoutingEdge> getFromHRUs() {
        return Collections.unmodifiableList(fromHRUs);
    }

    public List<RoutingEdge> getFromReaches() {
        return Collections.unmodifiableList(fromReaches);
    }

    public RoutingEdge getToReach() {
        return toReach;
    }

    public void sortBy(Comparator<RoutingEdge> comparator) {
        Collections.sort(fromHRUs, comparator);
        Collections.sort(fromReaches, comparator);
    }

    public static class Builder {
        private int depth;
        private List<RoutingEdge> fromHRUs;
        private List<RoutingEdge> fromReaches;
        private RoutingEdge toReach;

        public Builder() {
            depth = 0;
            fromHRUs = new ArrayList<>();
            fromReaches = new ArrayList<>();
            toReach = null;
        }

        public void addFromHRU(RoutingEdge routingNode) {
            fromHRUs.add(routingNode);
        }

        public void addFromHRUs(Collection<RoutingEdge> routingNodes) {
            fromHRUs.addAll(routingNodes);
        }

        public void addFromReach(RoutingEdge routingNode) {
            fromReaches.add(routingNode);
        }

        public void addFromReaches(Collection<RoutingEdge> routingNodes) {
            fromReaches.addAll(routingNodes);
        }

        public void setToReach(RoutingEdge routingNode) {
            toReach = routingNode;
        }

        public void setDepth(int depth) {
            this.depth = depth;
        }

        public ReachRoutingInformation build() {
            return new ReachRoutingInformation(depth, fromHRUs, fromReaches, toReach);
        }
    }
}
