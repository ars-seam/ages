
package routing;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingN {
    public double deepsink;
    public double q_act_tot;
    public double q_act_out;
    public double deepsinkW;
    public double hruSurfaceN_in;
    public double hruInterflowN_sum;
    public double hruN_RG1_in;
    public double hruN_RG2_in;
    public double reachSurfaceN_in;
    public double reachInterflowN_sum;
    public double reachN_RG1_in;
    public double reachN_RG2_in;

    public double actRD1_N;
    public double actRD2_N;
    public double actRG1_N;
    public double actRG2_N;

    public double SurfaceN_in;
    public double InterflowN_sum;
    public double N_RG1_in;
    public double N_RG2_in;
    public double outRD1_N;
    public double outRD2_N;
    public double outRG1_N;
    public double outRG2_N;
    public double SurfaceNabs;
    public double InterflowNabs;
    public double N_RG1_out;
    public double N_RG2_out;
    public double simRunoff_N;
    public double channelStorage_N;
    public double q_act_tot_N;
    public double q_act_out_N;
    public double deepsinkN;

    public void execute() {
        SurfaceN_in = hruSurfaceN_in + reachSurfaceN_in;
        InterflowN_sum = hruInterflowN_sum + reachInterflowN_sum;
        N_RG1_in = hruN_RG1_in + reachN_RG1_in;
        N_RG2_in = hruN_RG2_in + reachN_RG2_in;

        actRD1_N += SurfaceN_in;
        actRD2_N += InterflowN_sum;
        actRG1_N += N_RG1_in;
        actRG2_N += N_RG2_in;

        q_act_tot_N = actRD1_N + actRD2_N + actRG1_N + actRG2_N;

        if (q_act_tot == 0) {
            outRD1_N = 0;
            outRD2_N = 0;
            outRG1_N = 0;
            outRG2_N = 0;

            SurfaceNabs = 0;
            InterflowNabs = 0;
            N_RG1_out = 0;
            N_RG2_out = 0;

            return;
        }

        // calculate relative parts of runoff components for later redistribution
        double RD1_part_N = 0;
        double RD2_part_N = 0;
        double RG1_part_N = 0;
        double RG2_part_N = 0;

        if (q_act_tot_N != 0) {
            RD1_part_N = actRD1_N / q_act_tot_N;
            RD2_part_N = actRD2_N / q_act_tot_N;
            RG1_part_N = actRG1_N / q_act_tot_N;
            RG2_part_N = actRG2_N / q_act_tot_N;
        }

        //calculate N concentration
        double N_conc_tot = q_act_tot_N / q_act_tot;

        // calculate N content in outflow
        q_act_out_N = q_act_out * N_conc_tot;

        if (deepsink == 1.0) {
            // calculate deep sink amounts
            deepsinkN = deepsinkW * N_conc_tot;
            deepsinkN = Math.min(deepsinkN, q_act_out_N);
            deepsinkN = Math.max(deepsinkN, 0);
        } else {
            deepsinkN = 0;
        }

        double RD1out_Ndeep = deepsinkN * RD1_part_N;
        double RD2out_Ndeep = deepsinkN * RD2_part_N;
        double RG1out_Ndeep = deepsinkN * RG1_part_N;
        double RG2out_Ndeep = deepsinkN * RG2_part_N;

        outRD1_N = q_act_out_N * RD1_part_N - RD1out_Ndeep;
        outRD2_N = q_act_out_N * RD2_part_N - RD2out_Ndeep;
        outRG1_N = q_act_out_N * RG1_part_N - RG1out_Ndeep;
        outRG2_N = q_act_out_N * RG2_part_N - RG2out_Ndeep;

        // reduce the actual storages
        actRD1_N = reduce(actRD1_N, outRD1_N, RD1out_Ndeep);
        actRD2_N = reduce(actRD2_N, outRD2_N, RD2out_Ndeep);
        actRG1_N = reduce(actRG1_N, outRG1_N, RG1out_Ndeep);
        actRG2_N = reduce(actRG2_N, outRG2_N, RG2out_Ndeep);

        channelStorage_N = actRD1_N + actRD2_N + actRG1_N + actRG2_N;
        simRunoff_N = outRD1_N + outRD2_N + outRG1_N + outRG2_N;

        SurfaceNabs = outRD1_N;
        InterflowNabs = outRD2_N;
        N_RG1_out = outRG1_N;
        N_RG2_out = outRG2_N;
    }

    public static double reduce(double act, double out, double outDeep) {
        double reduced = act - out - outDeep;
        if (reduced < 0.0) {
            reduced = 0.0;
        }
        return reduced;
    }
}
