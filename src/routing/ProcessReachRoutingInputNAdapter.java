
package routing;

import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingInputNAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] reachRoutingContexts;

    @Output public double reachSurfaceN_in;
    @Output public double reachInterflowN_sum;
    @Output public double reachN_RG1_in;
    @Output public double reachN_RG2_in;

    @Override
    protected void run(Context context) {
        reset();
        for (RoutingContext routingContext : reachRoutingContexts) {
            route(routingContext);
        }
    }

    private void reset() {
        reachSurfaceN_in = 0;
        reachInterflowN_sum = 0;
        reachN_RG1_in = 0;
        reachN_RG2_in = 0;
    }

    private void route(RoutingContext routingContext) {
        Context hruContext = routingContext.spatialContext;

        double outRD1_N = hruContext.getDouble("outRD1_N");
        double outRD2_N = hruContext.getDouble("outRD2_N");
        double outRG1_N = hruContext.getDouble("outRG1_N");
        double outRG2_N = hruContext.getDouble("outRG2_N");

        reachSurfaceN_in += outRD1_N;
        reachInterflowN_sum += outRD2_N;
        reachN_RG1_in += outRG1_N;
        reachN_RG2_in += outRG2_N;
    }
}
