/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package routing;

import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import gov.usda.jcf.core.contexts.UnmodifiableContext;
import java.util.Arrays;

@Description("Add MultiRoutingLayerN module definition here")
@Author(name = "Olaf David, Peter Krause, Manfred Fink, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Routing")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/routing/MultiRoutingLayerN.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/routing/MultiRoutingLayerN.xml")
public class MultiRoutingLayerNAdapter extends AnnotatedAdapter {
    @Description("HRU Routing flag")
    @Input public boolean flagHRURouting;

    @Description("routing contexts")
    @Input public RoutingContext[] routingContexts;

    @Description("soil")
    @Input public SoilType soil;

    @Optional
    @Input @Output public double[] InterflowN_in;

    @Output public double[] InterflowNabs;
    @Output public double N_RG1_out;
    @Output public double N_RG2_out;
    @Output public double NExcess;
    @Output public double inTDN;
    @Output public double SurfaceN_in;
    @Output public double N_RG1_in;
    @Output public double N_RG2_in;

    @Override
    protected void run(Context context) {
        if (!flagHRURouting) {
            return;
        }

        reset();
        for (RoutingContext routingContext : routingContexts) {
            route(routingContext.spatialContext, routingContext.weight);
        }
    }

    private void reset() {
        if (InterflowNabs != null) {
            Arrays.fill(InterflowNabs, 0.0);
        }
        if (InterflowN_in == null) {
            InterflowN_in = new double[soil.horizons];
        } else {
            Arrays.fill(InterflowN_in, 0.0);
        }

        N_RG1_out = 0.0;
        N_RG2_out = 0.0;
        NExcess = 0.0;
        inTDN = 0.0;
        SurfaceN_in = 0.0;
        N_RG1_in = 0.0;
        N_RG2_in = 0.0;
    }

    private void route(UnmodifiableContext from, double weight) {
        MultiRoutingLayerN component = new MultiRoutingLayerN();

        component.weight = weight;
        component.NRD1out = from.getDouble("SurfaceNabs");
        component.NRD2out_h = from.getDoubleArray("InterflowNabs");
        component.NRG1out = from.getDouble("N_RG1_out");
        component.NRG2out = from.getDouble("N_RG2_out");
        if (from.contains("TDInterflowN")) {
            component.NTDout = from.getDoubleArray("TDInterflowN");
        } else {
            component.NTDout = null;
        }
        component.srcDepth = from.get("soil", SoilType.class).depth_h;
        component.NExcess = from.getDouble("NExcess");
        component.recDepth = soil.depth_h;
        component.TDinN = inTDN;
        component.NRD1in = SurfaceN_in;
        component.rdArN = InterflowN_in;
        component.NRG1in = N_RG1_in;
        component.NRG2in = N_RG2_in;

        component.execute();

        InterflowN_in = component.NRD2in_h;
        SurfaceN_in = component.NRD1in;
        N_RG1_in = component.NRG1in;
        N_RG2_in = component.NRG2in;
        inTDN = component.TDinN;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
