
package routing;

import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessHRUReachRoutingMusleAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] hruRoutingContexts;

    @Output public double hruInsed;

    @Override
    protected void run(Context context) {
        hruInsed = 0.0;
        for (RoutingContext routingContext : hruRoutingContexts) {
            double outsed = routingContext.spatialContext.getDouble("outsed");
            hruInsed += outsed * routingContext.weight;
        }
    }
}
