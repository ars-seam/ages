
package routing;

import ages.types.SoilType;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessHRUReachRoutingAdapter extends AnnotatedAdapter {
    @Input public RoutingContext[] hruRoutingContexts;

    @Output public double hruInRD1;
    @Output public double hruInRD2;
    @Output public double hruInRG1;
    @Output public double hruInRG2;

    @Override
    protected void run(Context context) {
        reset();
        for (RoutingContext routingContext : hruRoutingContexts) {
            route(routingContext);
        }
    }

    private void reset() {
        hruInRD1 = 0;
        hruInRD2 = 0;
        hruInRG1 = 0;
        hruInRG2 = 0;
    }

    private void route(RoutingContext routingContext) {
        Context hruContext = routingContext.spatialContext;

        ProcessHRUReachRouting component = new ProcessHRUReachRouting();

        component.weight = routingContext.weight;
        component.outRD1 = hruContext.getDouble("outRD1");
        component.outRD2_h = hruContext.getDoubleArray("outRD2_h");
        component.outRG1 = hruContext.getDouble("outRG1");
        component.outRG2 = hruContext.getDouble("outRG2");
        component.gwExcess = hruContext.getDouble("gwExcess");
        component.horizons = hruContext.get("soil", SoilType.class).horizons;
        if (hruContext.contains("out_tile_water")) {
            component.out_tile_water = hruContext.getDoubleArray("out_tile_water");
        }
        if (hruContext.contains("in_tile_water")) {
            component.in_tile_water = hruContext.getDouble("in_tile_water");
        }

        component.execute();

        hruInRD1 += component.inRD1;
        hruInRD2 += component.inRD2;
        hruInRG1 += component.inRG1;
        hruInRG2 += component.inRG2;
    }
}
