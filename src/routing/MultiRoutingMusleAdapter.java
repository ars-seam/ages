/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package routing;

import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import gov.usda.jcf.core.contexts.UnmodifiableContext;

@Description("Add MultiRoutingMusle module definition here")
@Author(name = "Holm Kipka, James C. Ascough II", contact = "jim.ascough@ars.usda.gov")
@Keywords("Insert keywords")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/routing/MultiRoutingMusle.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/routing/MultiRoutingMusle.xml")
public class MultiRoutingMusleAdapter extends AnnotatedAdapter {
    @Description("HRU Routing flag")
    @Input public boolean flagHRURouting;

    @Description("routing contexts")
    @Input public RoutingContext[] routingContexts;

    @Output public double insed;

    @Override
    protected void run(Context context) {
        if (!flagHRURouting) {
            return;
        }

        reset();
        for (RoutingContext routingContext : routingContexts) {
            route(routingContext.spatialContext, routingContext.weight);
        }
    }

    private void reset() {
        insed = 0;
    }

    private void route(UnmodifiableContext from, double weight) {
        MultiRoutingMusle component = new MultiRoutingMusle();

        component.weight = weight;
        component.sedout = from.getDouble("outsed");
        component.sedin = insed;

        component.run();

        insed = component.sedin;
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
