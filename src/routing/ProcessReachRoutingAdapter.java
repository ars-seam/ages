
package routing;

import ages.types.StreamReach;
import annotations.Role;
import static annotations.Role.PARAMETER;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;

/**
 *
 * @author Nathan Lighthart
 */
public class ProcessReachRoutingAdapter extends AnnotatedAdapter {
    @Description("flow routing coefficient TA")
    @Role(PARAMETER)
    @Input public double flowRouteTA;

    @Description("K-Value for the streambed")
    @Units("cm/d")
    @Input public double Ksink;

    @Description("reach")
    @Input public StreamReach reach;

    @Description("hru cumulative sum of output going into RD1")
    @Input public double hruInRD1;

    @Description("hru cumulative sum of output going into RD2")
    @Input public double hruInRD2;

    @Description("hru cumulative sum of output going into RG1")
    @Input public double hruInRG1;

    @Description("hru cumulative sum of output going into RG2")
    @Input public double hruInRG2;

    @Description("reach cumulative sum of output going into RD1")
    @Input public double reachInRD1;

    @Description("reach cumulative sum of output going into RD2")
    @Input public double reachInRD2;

    @Description("reach cumulative sum of output going into RG1")
    @Input public double reachInRG1;

    @Description("reach cumulative sum of output going into RG2")
    @Input public double reachInRG2;

    @Description("actual RD1 storage")
    @Optional
    @Input @Output public double actRD1;

    @Description("actual RD2 storage")
    @Optional
    @Input @Output public double actRD2;

    @Description("actual RG1 storage")
    @Optional
    @Input @Output public double actRG1;

    @Description("actual RG2 storage")
    @Optional
    @Input @Output public double actRG2;

    @Description("total amount coming into RD1")
    @Output public double inRD1;

    @Description("total amount coming into RD2")
    @Output public double inRD2;

    @Description("total amount coming into RG1")
    @Output public double inRG1;

    @Description("total amount coming into RG2")
    @Output public double inRG2;

    @Description("RD1 outflow")
    @Output public double outRD1;

    @Description("RD2 outflow")
    @Output public double outRD2;

    @Description("RG1 outflow")
    @Output public double outRG1;

    @Description("RG2 outflow")
    @Output public double outRG2;

    @Description("simulated runoff")
    @Output public double simRunoff;

    @Output public double channelStorage;

    @Description("total outflow")
    @Output public double q_act_tot;

    @Description("actual outflow")
    @Output public double q_act_out;

    @Output public double deepsinkW;

    @Override
    protected void run(Context context) {
        ProcessReachRouting component = new ProcessReachRouting();

        component.flowRouteTA = flowRouteTA;
        component.Ksink = Ksink;
        component.deepsink = reach.deepsink;
        component.width = reach.width;
        component.slope = reach.slope;
        component.rough = reach.rough;
        component.length = reach.length;
        component.hruInRD1 = hruInRD1;
        component.hruInRD2 = hruInRD2;
        component.hruInRG1 = hruInRG1;
        component.hruInRG2 = hruInRG2;
        component.reachInRD1 = reachInRD1;
        component.reachInRD2 = reachInRD2;
        component.reachInRG1 = reachInRG1;
        component.reachInRG2 = reachInRG2;
        component.actRD1 = actRD1;
        component.actRD2 = actRD2;
        component.actRG1 = actRG1;
        component.actRG2 = actRG2;

        component.execute();

        actRD1 = component.actRD1;
        actRD2 = component.actRD2;
        actRG1 = component.actRG1;
        actRG2 = component.actRG2;
        inRD1 = component.inRD1;
        inRD2 = component.inRD2;
        inRG1 = component.inRG1;
        inRG2 = component.inRG2;
        outRD1 = component.outRD1;
        outRD2 = component.outRD2;
        outRG1 = component.outRG1;
        outRG2 = component.outRG2;
        simRunoff = component.simRunoff;
        channelStorage = component.channelStorage;
        q_act_tot = component.q_act_tot;
        q_act_out = component.q_act_out;
        deepsinkW = component.deepsinkW;
    }
}
