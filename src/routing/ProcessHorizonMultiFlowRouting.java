/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package routing;

public class ProcessHorizonMultiFlowRouting {
    // weight of routing
    public double weight;

    // from variables (do not modify value of these variables)
    public double RD1out;
    public double[] RD2out;
    public double RG1out;
    public double RG2out;
    public double[] TDout;
    public double TD_exist;
    public double[] srcDepth;
    public double gwExcess;

    // to variables
    public double RD1in;
    public double[] recDepth;
    public double[] RD2in;
    public double RG1in;
    public double RG2in;
    public double TDin;
    public double[] inRD2_h;

    private double[][] fracOut;
    private double[] percOut;

    public void execute() {
        int srcHors = srcDepth.length;
        int recHors = recDepth.length;
        RD2in = new double[recHors];

        calcParts(srcDepth, recDepth);

        if (TDout != null) {
            for (int i = 0; i < srcHors; i++) {
                TDin += TDout[i] * weight;
            }
        }

        for (int j = 0; j < recHors; j++) {
            RD2in[j] = inRD2_h[j];

            for (int i = 0; i < srcHors; i++) {
                RD2in[j] += RD2out[i] * weight * fracOut[i][j];
                RG1in += RD2out[i] * weight * percOut[i];
            }
        }

        RD2in[recHors - 1] += gwExcess;

        RD1in += RD1out * weight;
        RG1in += RG1out * weight;
        RG2in += RG2out * weight;
        TDin += TD_exist * weight;
    }

    private void calcParts(double[] depthSrc, double[] depthRec) {
        int srcHorizons = depthSrc.length;
        int recHorizons = depthRec.length;

        double[] upBoundSrc = new double[srcHorizons];
        double[] lowBoundSrc = new double[srcHorizons];
        double low = 0;
        double up = 0;

        for (int i = 0; i < srcHorizons; i++) {
            low += depthSrc[i];
            up = low - depthSrc[i];
            upBoundSrc[i] = up;
            lowBoundSrc[i] = low;
        }
        double[] upBoundRec = new double[recHorizons];
        double[] lowBoundRec = new double[recHorizons];
        low = 0;
        up = 0;

        for (int i = 0; i < recHorizons; i++) {
            low += depthRec[i];
            up = low - depthRec[i];
            upBoundRec[i] = up;
            lowBoundRec[i] = low;
        }

        fracOut = new double[depthSrc.length][depthRec.length];
        percOut = new double[depthSrc.length];

        for (int i = 0; i < depthSrc.length; i++) {
            double sumFrac = 0;

            for (int j = 0; j < depthRec.length; j++) {
                if ((lowBoundSrc[i] > upBoundRec[j]) && (upBoundSrc[i] < lowBoundRec[j])) {
                    double relDepth = Math.min(lowBoundSrc[i], lowBoundRec[j]) - Math.max(upBoundSrc[i], upBoundRec[j]);
                    double fracDepth = relDepth / depthSrc[i];
                    sumFrac += fracDepth;
                    fracOut[i][j] = fracDepth;
                }
            }
            percOut[i] = 1.0 - sumFrac;
        }
    }
}
