/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soil;

import ages.types.HRU;
import ages.types.SoilType;
import annotations.Author;
import annotations.Bibliography;
import annotations.Documentation;
import annotations.Execute;
import annotations.Keywords;
import annotations.License;
import annotations.Role;
import static annotations.Role.PARAMETER;
import annotations.SourceInfo;
import annotations.Status;
import annotations.VersionInfo;
import gov.usda.jcf.annotations.Description;
import gov.usda.jcf.annotations.Input;
import gov.usda.jcf.annotations.Optional;
import gov.usda.jcf.annotations.Output;
import gov.usda.jcf.annotations.Units;
import gov.usda.jcf.core.Context;
import gov.usda.jcf.core.adapters.AnnotatedAdapter;
import java.util.Arrays;
import management.ManagementOperation;
import management.Tillage;
import management.TillageOperation;

@Description("Add SParam module definition here")
@Author(name = "Candace Batts, Robert Streetman, Timothy Green", contact = "jim.ascough@ars.usda.gov")
@Keywords("Soil")
@Bibliography("Insert bibliography here")
@VersionInfo("$ID:$")
@SourceInfo("http://javaforge.com/scm/file/3979484/default/src/soil/SParam.java")
@License("http://www.gnu.org/licenses/gpl.html")
@Status(Status.TESTED)
@Documentation("src/soil/SParam.xml")
public class SParamAdapter extends AnnotatedAdapter {
    @Description("multiplier for air capacity")
    @Role(PARAMETER)
    @Input public double ACAdaptation;

    @Description("Flag for Tillage Calculations")
    @Input public boolean flagTillage;

    @Description("current management")
    @Input public ManagementOperation management;

    @Description("Precip")
    @Units("mm")
    @Input public double precip;

    @Description("soil")
    @Units("cm")
    @Input public SoilType soil;

    @Description("Current HRU")
    @Input public HRU hru;

    @Description("Rainfall Since Tillage")
    @Units("mm")
    @Optional
    @Input @Output public double rain_till;

    @Description("Horizons Last Tilled")
    @Optional
    @Input @Output public int tilled_hor;

    @Description("Maximum LPS  in l soil water content")
    @Input @Output public double[] maxLPS_h;

    @Description("HRU Bulk Density")
    @Units("mg/cm3")
    @Optional
    @Input @Output public double[] bulkDensity_h;

    @Description("HRU Air Capacity (Porosity)")
    @Units("Vol.%")
    @Optional
    @Input @Output public double[] volumeAirCapacity_h;

    @Description("HRU Saturated Hydraulic Conductivity (Kf)")
    @Units("cm/d")
    @Optional
    @Input @Output public double[] kf_h;

    @Description("Bulk Density Delta Due To Tillage")
    @Units("mg/cm3")
    @Optional
    @Input @Output public double[] deltaBlkDens;

    private TillageOperation lastTillageOperation;

    @Override
    protected void run(Context context) {
        if (!flagTillage) {
            skipOutput();
            return;
        }
        initializeOptional();

        SParam component = new SParam();

        boolean tillOccur;
        double till_intensity;
        double till_depth;

        if (lastTillageOperation != null) {
            Tillage tillage = lastTillageOperation.getTillage();
            tillOccur = true;
            till_intensity = tillage.effmix;
            till_depth = tillage.deptil;
            lastTillageOperation = (management instanceof TillageOperation)
                    ? (TillageOperation) management : null;
        } else {
            if (management instanceof TillageOperation) {
                lastTillageOperation = (TillageOperation) management;
            }
            tillOccur = false;
            till_intensity = 0.0;
            till_depth = 0.0;
        }

        component.ACAdaptation = ACAdaptation;
        component.tillOccur = tillOccur;
        component.till_intensity = till_intensity;
        component.till_depth = till_depth;
        component.totaldepth = soil.totaldepth;
        component.precip = precip;
        component.depth_h = soil.depth_h;
        component.volumeFieldCapacity_h = soil.volumeFieldCapacity_h;
        component.bulkDens_orig = soil.computedBulkDensity_h;
        component.area = hru.area;
        component.rain_till = rain_till;
        component.tilled_hor = tilled_hor;
        component.maxLPS_h = maxLPS_h;
        component.bulkDensity_h = bulkDensity_h;
        component.volumeAirCapacity_h = volumeAirCapacity_h;
        component.kf_h = kf_h;
        component.deltaBlkDens = deltaBlkDens;

        component.execute();

        rain_till = component.rain_till;
        tilled_hor = component.tilled_hor;
        maxLPS_h = component.maxLPS_h;
        bulkDensity_h = component.bulkDensity_h;
        volumeAirCapacity_h = component.volumeAirCapacity_h;
        kf_h = component.kf_h;
        deltaBlkDens = component.deltaBlkDens;
    }

    private void initializeOptional() {
        if (bulkDensity_h == null) {
            bulkDensity_h = Arrays.copyOf(soil.computedBulkDensity_h, soil.horizons);
        }
        if (volumeAirCapacity_h == null) {
            volumeAirCapacity_h = Arrays.copyOf(soil.volumeAirCapacity_h, soil.horizons);
        }
        if (kf_h == null) {
            kf_h = Arrays.copyOf(soil.kf_h, soil.horizons);
        }
        if (deltaBlkDens == null) {
            deltaBlkDens = new double[soil.horizons];
        }
    }

    /**
     * OMS execute statement for testing before JCF
     */
    @Execute
    public void execute() {
        run(null);
    }
}
