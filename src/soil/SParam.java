/*
 * This file is part of the AgroEcoSystem-Watershed (AgES-W) model component
 * collection. AgES-W components are derived from multiple agroecosystem models
 * including J2K and J2K-SN (FSU-Jena, DGHM, Germany), SWAT (USDA-ARS, USA),
 * WEPP (USDA-ARS, USA), RZWQM2 (USDA-ARS, USA), and others.
 *
 * The AgES-W model is free software; you can redistribute the model and/or
 * modify the components under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package soil;

public class SParam {
    public double ACAdaptation;
    public boolean tillOccur;
    public double till_intensity;
    public double till_depth;
    public double totaldepth;
    public double precip;
    public double[] depth_h;
    public double[] volumeFieldCapacity_h;
    public double[] bulkDens_orig;
    public double area;
    public double rain_till;
    public int tilled_hor;
    public double[] maxLPS_h;
    public double[] bulkDensity_h;
    public double[] volumeAirCapacity_h;
    public double[] kf_h;
    public double[] deltaBlkDens;

    private double till_fraction;

    public void execute() {
        if (tillOccur) {
            tilled_hor = calcHorizons();
            rain_till = 0.0;
            double delta_new, delta_old;

            for (int h = 0; h < tilled_hor; h++) {
                delta_old = deltaBlkDens[h];
                if (h == tilled_hor - 1) {
                    delta_new = (-0.333 * bulkDens_orig[h] * till_intensity) * till_fraction;
                } else {
                    delta_new = -0.333 * bulkDens_orig[h] * till_intensity;
                }
                deltaBlkDens[h] = (delta_new < delta_old) ? delta_new : delta_old;
            }
        } else {
            rain_till += precip;
        }

        int h = tilled_hor - 1;

        while (h >= 0) {
            if (rain_till < 100.0) {
                bulkDensity_h[h] = bulkDens_orig[h] + (deltaBlkDens[h] * (1 - (rain_till / 101)));
            } else {
                bulkDensity_h[h] = bulkDens_orig[h];
            }

            volumeAirCapacity_h[h] = (1 - (bulkDensity_h[h] / 2.65));
            kf_h[h] = 509.4 * Math.pow((volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]), 3.63) * 24;
            maxLPS_h[h] = (volumeAirCapacity_h[h] - volumeFieldCapacity_h[h]) * (depth_h[h] * 10) * area * ACAdaptation;
            h--;
        }
    }

    private int calcHorizons() {
        int hor = 0;
        if (till_depth > totaldepth * 10) {
            till_depth = (totaldepth * 10) * 0.8;
        }
        double tDepth = 0.;
        while (tDepth < till_depth) {
            tDepth += depth_h[hor] * 10;
            hor++;
        }
        till_fraction = 1 - ((tDepth - till_depth) / (depth_h[hor - 1] * 10));
        return hor;
    }
}
